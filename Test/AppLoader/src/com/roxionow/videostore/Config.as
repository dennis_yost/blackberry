package com.roxionow.videostore
{
	public class Config
	{
		public static var defaultApp:String = "main.swf";
		
		public static var updateApp:String = "latest.swf";
		
		public static var appVersion:String = "00001.6";
		
		public static var appBuild:String = "20110428";
		
		//version of loader application
		public static var playerVersion:String = "1";
		
		//last data when checkupdates
		public static var lastCheckDate:int;
	}
}