package com.roxionow.videostore.service 
{
	import flash.events.Event;
	
	public class AsyncRequestEvent extends Event
	{
		
		public function AsyncRequestEvent(type:String) 
		{
			super(type, false, false);
		}
		
		public var data:Object
	}
	
}