package com.roxionow.videostore.service
{
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	
	import com.roxionow.videostore.Version;
	
	public class WebServicesClient
	{		
			
		public static var url:String = "http://stgapi.cinemanow.com/api/orbit/util/default.asmx";
		
		// TODO: Load settings from config file
		
		public const APIKey:String = "tuKwA4yrdBfkg5fwCZuA8BJbYCuOoS4lso9dcQOZdwbc/tF16LNJmA==";
		public const DestinationTypeID:String = "51";
		public const DestinationUniqueID:String = "BlackberryPlaybook";
		public const Username:String = "SonicTestAPI";
		public const Password:String = "CN@s0N!cT35t&toK3N";
		public const CurrentEnvironment:String = "";
		public const AuthToken:String = "";		
		public const ParentChild:String = "Parent";
		public const FileFilter:String = "any";
		public const SessionID:String = "";
		
		
		public function checkForUpdates( handler:AsyncResponder ):void
		{
			var op:AsyncRequest = new AsyncRequest(url);	
			
			op.addEventListener(AsyncRequest.RESULT, function(e:AsyncRequestEvent):void {
				handler.result(e.data);
			});
			op.addEventListener(AsyncRequest.FAULT, function(e:AsyncRequestEvent):void {
				handler.fault(e.data);
			});
			
			var vars:Object = new Object();
	
			vars.AppVersion = Version.appVersion;	
			vars.SDKVersion = 0;	
			vars.IncludeTestUpdates = true;
			
			op.sendData("checkForUpdates",vars, wrapRequestHeader());
		}
		
		private function wrapRequestHeader():XML
		{		
			var xml:XML = 	<SettingsHeaderWSS xmlns="http://WebServices/OrbitServices">
											<APIKey>{APIKey}</APIKey>
											<DestinationUniqueID>{DestinationUniqueID}</DestinationUniqueID>
											<DestinationTypeID>{DestinationTypeID}</DestinationTypeID>
											<AuthToken>{AuthToken}</AuthToken>      
											<FileFilter>{FileFilter}</FileFilter>
											<SessionID>{SessionID}</SessionID>
											<AccountType>{ParentChild}</AccountType>
										</SettingsHeaderWSS>
			
			return xml;
		}
		
	}

}