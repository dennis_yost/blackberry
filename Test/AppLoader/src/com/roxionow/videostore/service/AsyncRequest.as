package com.roxionow.videostore.service
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	public class AsyncRequest extends EventDispatcher
	{
		private var url_request:URLRequest;
		private var url_loader:URLLoader;
		
		private var _url:String;
		
		public static var RESULT:String = "result";
		public static var FAULT:String = "fault";
		
		public function AsyncRequest(serviceURL:String) 
		{
			_url = serviceURL;
			
			init();
		}
		
		private function init():void
		{
			url_request = new URLRequest();
			url_request.contentType = "text/xml; charset=utf-8";
			url_request.method = URLRequestMethod.POST;

			url_loader = new URLLoader();
			url_loader.dataFormat = URLLoaderDataFormat.TEXT;
			url_loader.addEventListener(Event.COMPLETE, onServiceComplete);
			url_loader.addEventListener(IOErrorEvent.IO_ERROR, onServiceFailed);
		}
		
		public function sendData(method:String, vars:Object, header:XML = null):void
		{
			var xml:XML = createRequestXML(method, vars, header);
			
			url_request.requestHeaders.push(new URLRequestHeader("Content-Type", "text/xml; charset=utf-8"));
			url_request.requestHeaders.push(new URLRequestHeader("SOAPAction", "http://WebServices/OrbitServices/checkForUpdates"));

			url_request.data = xml;	
			url_request.url = _url;
			trace(xml);
			
			url_loader.load(url_request);
		}
		
		private function createRequestXML(method:String, vars:Object, header:XML = null):XML
		{
			
			var tagName:String =  method;

			var add_node:XML = <{tagName} xmlns = "http://WebServices/OrbitServices" /> ;
			
			for ( var prop:String in vars)
			{
				add_node.appendChild(
							<{prop}>
								{vars[prop]}
							</{prop}>
							);

			}

			var callXML:XML = <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
					<soap:Header>
						{header}
					</soap:Header>
					<soap:Body>
						{add_node}
					</soap:Body>
				</soap:Envelope>

			return callXML;
		}
		
		private function onServiceComplete(e:Event):void
		{
			var xml:XML = new XML(url_loader.data);
			var soap_nms:Namespace = xml.namespace();			
			var body_resp_xml:XMLList = xml.soap_nms::Body.children();
			
			var event:AsyncRequestEvent = new AsyncRequestEvent(AsyncRequest.RESULT);
			event.data = body_resp_xml[0];
			
			dispatchEvent(event);
		}
		
		private function onServiceFailed(e:IOErrorEvent):void
		{
			var event:AsyncRequestEvent = new AsyncRequestEvent(AsyncRequest.FAULT);
			event.data = e.text;
			dispatchEvent(event);
		}
	}

}