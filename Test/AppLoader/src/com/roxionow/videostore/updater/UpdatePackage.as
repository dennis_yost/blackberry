package com.roxionow.videostore.updater
{
	public class UpdatePackage
	{
		public var appVersion:String;
		
		public var type:String;
		
		public var url:String;
		
		public var filesize:int;
		
		public var checksum:String;
		
		public var comment:String;			
	}
}