package com.roxionow.videostore.updater
{
	public class UpdateType
	{
		public static const FORCED_UPDATE:String = "forced_update";
		
		public static const OPTIONAL_UPDATE:String = "optional_update";
		
		public static const TEST_UPDATE:String = "test_update";
	}
}