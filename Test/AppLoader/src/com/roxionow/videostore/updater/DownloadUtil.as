package com.roxionow.videostore.updater
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLStream;

	public class DownloadUtil
	{
		
		private var isDownloading:Boolean = false;
		
		private var urlStream:URLStream;
		
		public function DownloadUtil()
		{
			init();
		}
		
		private function init():void
		{
			urlStream = new URLStream();
			urlStream.addEventListener(Event.COMPLETE, onDownloadComplete);
			urlStream.addEventListener(ProgressEvent.PROGRESS, onDownloadProgress);
			urlStream.addEventListener(IOErrorEvent.IO_ERROR, onDownloadError);
			urlStream.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onDownloadError);
		}
		
		/**
		 * 
		 * @param url
		 * @param startPoint
		 * 
		 */		
		public function download( url:String, startPoint:int = 0 ):void
		{
			
		}
		
		private function onDownloadComplete(e:Event):void
		{
			
		}
		
		private function onDownloadProgress(e:ProgressEvent):void
		{
			
		}
		
		private function onDownloadError(e:Event):void
		{
			
		}
	}
}