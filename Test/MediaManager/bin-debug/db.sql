CREATE TABLE Contents (_id INTEGER PRIMARY KEY AUTOINCREMENT,pass_id INTEGER NOT NULL,sku_id INTEGER NOT NULL,title_id INTEGER NOT NULL,dq_id INTEGER,download_order INTEGER);

CREATE TABLE Passes (_id INTEGER PRIMARY KEY,amt_of_units INTEGER,unit TEXT(1),type INTEGER,autodelete INTEGER,first_play TEXT);

CREATE TABLE Skus (_id INTEGER PRIMARY KEY,file_name TEXT,video_bitrate INTEGER,audio_bitrate INTEGER,device INTEGER,download_url TEXT,license_url TEXT,downloaded TEXT,deleted TEXT,license_recieved INTEGER NOT NULL);

CREATE TABLE Titles (_id INTEGER PRIMARY KEY,title_name TEXT,desc_short TEXT,desc_long TEXT,box_art_prefix TEXT,mpaa_rating TEXT,clip_author TEXT,copyright TEXT,user_rating INTEGER,run_time TEXT,browse_expire_date TEXT,release_date TEXT,aspect_ratio TEXT,genre TEXT,directors TEXT,actors TEXT,producers TEXT,writers TEXT);

CREATE TABLE Downloads (_id INTEGER PRIMARY KEY,download_status INTEGER,target_file_size INTEGER,actual_file_size INTEGER,watch_now_countdown INTEGER,download_speed REAL);