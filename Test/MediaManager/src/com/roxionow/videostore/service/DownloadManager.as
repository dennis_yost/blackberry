package com.roxionow.videostore.service
{	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.events.EventDispatcher;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.Dictionary;
	
	import net.rim.aircommons.download.DownloadManagerFacade;
	import net.rim.aircommons.download.taskmanager.FileTransferTask;
	import net.rim.aircommons.download.taskmanager.Task;
	import net.rim.aircommons.download.taskmanager.TaskQueue;
	import net.rim.aircommons.download.taskmanager.events.TaskChangeEvent;
	import net.rim.aircommons.download.taskmanager.events.TaskProgressEvent;
	import net.rim.aircommons.download.taskmanager.listeners.TaskChangeListener;
	import net.rim.aircommons.download.taskmanager.listeners.TaskProgressListener;
	import net.rim.aircommons.download.taskmanager.utils.ProgressHandle;
	
 
	/**
	 * 
	 * Manage all download behavior: add new download task, listen task status and progress event...
	 * 
	 * You should create a singleton instance of this class
	 * 
	 * @author yanlin_qiu
	 * 
	 */	
	public class DownloadManager extends EventDispatcher
	{
		private var _mgr:DownloadManagerFacade;
		
		private var downloadItems:Vector.<DownloadItem> = new Vector.<DownloadItem>();
				
		public function DownloadManager()
		{				
			_mgr = new DownloadManagerFacade();	
			
			loadExistTasks();			
		}
		
		private function loadExistTasks():void
		{
			var tasks:Array = _mgr.getAllTasks();
			var task:FileTransferTask;
			
			var item:DownloadItem;
			
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as FileTransferTask;
				if( task != null)
				{
					item = new DownloadItem(task);
					downloadItems.push(item);
				}
			}	
		}
		
		public function resumeAllDownload():void
		{
			var item:DownloadItem;
			
			for( var i:int = 0,len:int = downloadItems.length;i<len;i++)
			{
				item = downloadItems[i];
				if( item.task != null)
				{
					item.resume();
				}
			}	
		}
		
		/**
		 * add a new DownloadItem, and start download it now
		 * 
		 * @param item DownloadItem
		 * 
		 */			
		public function addDownloadTask( contentID:String, remoteURL:String, saveLocation:String ):DownloadItem
		{			
			var task:FileTransferTask = new FileTransferTask(contentID, remoteURL, saveLocation );
			
			var item:DownloadItem = new DownloadItem(task);
			
			// Add listener to be notified of download task state changes 
			task.addEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);			
			
			// Add listener to be notified of download progress updates
			task.addEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);

			_mgr.addTask(task);
			
			task.setDownload(true);
			
			downloadItems.push(item);
			
			return item;
		}
		
		/**
		 *
		 * Find downloadItem by ID 
		 * 
		 * @param id
		 * @return DownloadItem
		 * 
		 */		
		public function getDownloadItemById( id:String ):DownloadItem
		{
			
			var item:DownloadItem;
			
			for( var i:uint = 0,len:uint = downloadItems.length;i<len;i++)
			{
				item = downloadItems[i];				
				if( item.getContentID() == id )
				{
					return item;
				}
			}
			return null;
		}
		
		/**
		 * remove an exsit DownloadItem and downloaded file
		 * 
		 * @param item			 DownloadItem
		 * @param removeTmpFile  whether remove download temporary file
		 */		
		public function removeDownloadItem( item:DownloadItem, removeTmpFile:Boolean = true ):Boolean
		{			
			var index:int = downloadItems.indexOf(item);
			
			if( index > -1 ) downloadItems.splice(index,1);
			
			_mgr.removeTask(item.task, removeTmpFile);
			
			item.task.removeEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);		
			item.task.removeEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
			
			item.task = null;
						
			return true;
		}
		
		/**
		 * remove all download task
		 * 
		 * @param removeTmpFile Boolean
		 * 
		 */		
		public function removeAllDownloadItem(removeTmpFile:Boolean = true):void
		{
			
			var item:DownloadItem;
			for( var i:uint = 0,len:uint = downloadItems.length;i<len;i++)
			{
				item = downloadItems[i];
				item.task.removeEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);				
				item.task.removeEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
				item.task = null;
			}
			
			_mgr.removeAllTasks(removeTmpFile);
			
			downloadItems.length = 0;
		}		
		
		/**
		 * 
		 */
		private function taskProgressChanged(event:TaskProgressEvent):void 
		{
			var item:DownloadItem = this.getDownloadItemById( event.task.getId() );
				
			var evt:DownloadEvent = new DownloadEvent(DownloadEvent.TASK_PROGRESS, item);
			
			dispatchEvent(evt);
		}
		
		private function taskStatusChanged(event:TaskChangeEvent):void 
		{
			var item:DownloadItem = this.getDownloadItemById( event.task.getId() );
			
			var evt:DownloadEvent = new DownloadEvent(DownloadEvent.TASK_PROGRESS, item);
			evt.status = event.status;
			
			dispatchEvent(evt);
		}
	}
}