package com.roxionow.videostore.service
{
	import flash.events.Event;
	
	import net.rim.aircommons.download.taskmanager.Task;

	public class DownloadEvent extends Event
	{
		
		public static const TASK_PROGRESS:String = "task_progress";
		
		public static const TASK_STATUS_CHANGED:String = "task_status_changed";
		
		public function DownloadEvent( type:String, t:DownloadItem )
		{
			super(type, false, false);
			
			item = t;
		}
		
		public var item:DownloadItem;
		
		public var status:int;
		
		public var bytesDownloaded:int;
	}
}