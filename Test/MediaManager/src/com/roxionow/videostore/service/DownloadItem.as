package com.roxionow.videostore.service
{
	import flash.filesystem.File;
	
	import net.rim.aircommons.download.taskmanager.FileTransferTask;
	import net.rim.aircommons.download.taskmanager.Task;

	public class DownloadItem
	{	
		
		private var _task:FileTransferTask;
		
		public function DownloadItem( task:FileTransferTask )
		{				
			_task = task;
		}
		
		public function set task( value:FileTransferTask ):void
		{
			_task = value;
		}
		
		public function get task():FileTransferTask
		{
			return _task;
		}
		
		public function getContentID():String
		{
			return task.getId();
		}
				
		public function resume():void
		{
			task.setResume();
		}
		
		public function pause():void
		{
			task.setPause();
		}
		
		public function getDownloadPercent():int
		{			
			return task.getProgress();
		}
		
		public function isDownloading():Boolean
		{
			return task.isRunnableState() == false;
		}
		

	}
}