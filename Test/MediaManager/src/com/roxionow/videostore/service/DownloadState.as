package com.roxionow.videostore.service
{
	public class DownloadState
	{
		
		public static const STATUS_AVAILABLE:int = 0x08;
		
		public static const STATUS_CANCELLED:int = 0x05;
		
		public static const STATUS_FAILED:int = 0x04;
		
		public static const STATUS_INITIALIZED:int = 0x01;
		
		public static const STATUS_IN_PROGRESS:int = 0x02;
		
		public static const STATUS_PAUSED:int = 0x06;
		
		public static const STATUS_PENDING:int = 0x07;
		
		public static const STATUS_SUCCEEDED:int = 0x03;
	}
}