package com.roxionow.videostore.database
{
	public interface ISQLCommand
	{
		function run(params:Dictionary = null):void
			
		function get name():String
		
		function set name( value:String ):void
	}
}