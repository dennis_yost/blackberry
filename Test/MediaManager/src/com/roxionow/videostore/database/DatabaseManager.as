package com.roxionow.videostore.database
{	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	
	/**
	 * Manage media content database 
	 */ 
	public class DatabaseManager
	{
		// Tables
		public static const TABLE_CONTENT:String = "Contents";
		public static const TABLE_DOWNLOAD:String = "Downloads";
		public static const TABLE_PASS:String = "Passes";
		public static const TABLE_SKU:String = "Skus";
		public static const TABLE_TITLE:String = "Titles";
		
		private var conn:SQLConnection;
		private var opened:Boolean = false;
		private var dbPath:String = "";
		
		private var successCB:Function;
		private var faultCB:Function;
		
		public function DatabaseManager(dbPath:String)
		{
			this.dbPath = dbPath;
		}
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get isDatabaseReady():Boolean
		{
			return opened;
		}
		
		/**
		 * 
		 * @param success
		 * @param fault
		 * 
		 */
		public function openDB(success:Function = null, fault:Function = null):void
		{
			if (dbPath == "" || opened == true) return;
			
			successCB = success;
			faultCB = fault;
			
			conn = new SQLConnection();
			conn.addEventListener(SQLEvent.OPEN, onDBOpenHandler, false, 0, true);
			conn.addEventListener(SQLErrorEvent.ERROR, onDBOpenError, false, 0, true);
			conn.openAsync(f, SQLMode.CREATE);				
		}
		
		private function onDBOpenHandler(e:SQLEvent):void
		{
			conn.removeEventListener(SQLEvent.OPEN, onDBOpenHandler);
			conn.removeEventListener(SQLErrorEvent.ERROR, onDBOpenError);
			
			opened = true;			
			if (successCB != null) successCB();
		}
		
		private function onDBOpenError(e:SQLErrorEvent):void
		{
			conn.removeEventListener(SQLEvent.OPEN, onDBOpenHandler);
			conn.removeEventListener(SQLErrorEvent.ERROR, onDBOpenError);
			
			if (faultCB != null) faultCB();
		}
		
		/**
		 *
		 * Execute one sql query
		 * 
		 * @param sqlQuery
		 * @param success
		 * @param fault
		 * @param fault
		 * 
		 */		 
		public function execSQL( sqlQuery:String,
								 success:Function = null,
								 fault:Function = null,
								 params:Dictionary = null ):void
		{
			var sqlCommand:SQLQuery = new SQLQuery(sqlQuery, conn);
			sqlCommand.run(new QueryResponder(success, fault), params);
		}

		/*
		public function addContent(Object item):void 
		{		
			//TODO
			var sqls:Vector.<String> = new Vector.<String>();
			
			//insert pass
			sqls[0] = "INSERT INTO " + TABLE_PASS + " (amt_units, unit, title_id, dq_id, download_order) " +
				" VALUES (:pass_id, :sku_id, :title_id, :dq_id, :download_order)";
			
			//insert title
			sqls[1] = "INSERT INTO " + TABLE_TITLE + " (title_name, desc_short, desc_long, box_art_prefix, mpaa_rating "
				+ ", clip_author, copyright, user_rating, run_time, browse_expire_date "
				+ ", release_date, aspect_ratio, genre, directors, actors, producers, writers )"
				+ " VALUES (:title_name, :desc_short, :desc_long, :box_art_prefix, :mpaa_rating"
				+ " :clip_author, :copyright, :user_rating, :run_time, :browse_expire_date"
				+ " :release_date, :aspect_ratio, :genre, :directors, :actors, :producers, :writers)";
			
			//insert sku
			sqls[2] = "INSERT INTO " + TABLE_SKU + " (file_name, video_bitrate, audio_bitrate, device, download_url " 
				+ ", license_url, downloaded, deleted, license_recieved)"
				+ " VALUES (:file_name, :video_bitrate, :audio_bitrate, :device, :download_url"
				+ ", :license_url, :downloaded, :deleted, :license_recieved)";
			
			//insert content
			sqls[3] = "INSERT INTO " + TABLE_CONTENT + " ( pass_id, sku_id, title_id, dq_id, download_order )"
				+ " VLAUES (:pass_id, :sku_id, :title_id, :dp_id, :download_order)";
			
		}
		
		private function getContent(passId:int, skuId:int, fields:Array = null):void
		{
			
		}
		
		public function getAllContent(responder:QueryResponder):void
		{
			var sql:String = "SELECT a._id, b.file_name, b.download_url, a.download_order, c.title_name, " 
				+ "a.dq_id, c.run_time, b.video_butrate, b.audio_bitrate"
				+ " FROM " + TABLE_CONTENT + " a, " + TABLE_SKU + " b, " + TABLE_TITLE + " c " 
				+ " WHERE a.sku_id = b._id AND a.title_id = c._id"
				// + " and b." + Skus.DELETED + " is null"  // allow to redownload the deleted movie;
				+ " AND a.download_order > 0" 
				+ " ORDER BY a.download_order ASC";
			
			var sqlCommand:SQLQuery = new SQLQuery(sql, responder);
			sqlCommand.st.sqlConnection = conn;
			sqlCommand.run();
		}
		
		
		public function getDownloadItems(responder:QueryResponder):void
		{
			
		}
		
		public function addDownloadItem(DownloadItem item):void
		{
			
		}
		
		private function getDownloadItem(int contentId)
		{
		
		}
		
		public function updateDownloadItem(item:Object):void
		{
			
		}
		*/
		
		
	}
}
