package com.roxionow.videostore.database
{
	import com.roxionow.videostore.database.QueryResponder;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.utils.Dictionary;
	
	/**
	 * Execute a group sql query by transaction 
	 *  
	 * @author yanlin_qiu
	 * 
	 */	
	public class SQLQueryQueue
	{
		public var conn:SQLConnection;
		
		private var responder:QueryResponder;
		
		private var autoCleanup:Boolean;	
		
		private var currentIndex:uint = -1;		
		
		private var queryString:Vector.<String>;
		
		private var queryParams:Vector.<Dictionary>;
		
		private var sqlStatements:Vector.<SQLStatement>;
		
		//temp variable
		private var st:SQLStatement;
		
		public function SQLQueryQueue( sqls:Vecotr.<String>, queryResponder:QueryResponder = null, autoDestroy:Boolean = true )
		{
			queryString = sqls;
			
			autoCleanup = autoDestroy;
			
			responder = queryResponder;
		}
		
		private var _name:String;
		public function set name( value:String ):void
		{
			_name = value;
		}
		
		public function get name():String
		{
			return _name;
		}
		
		public function run(params:Vector.<Dictionary> = null ):void
		{			
			queryParams = params;
			
			//begin transaction 
			conn.begin();
			
			currentIndex = 0;
			doQuery();
		}
		
		private function doQuery():void
		{
			var st:SQLStatement;
			
			if( autoCleanup == true )
			{
				st = sqlStatements[0];
				if( st == null )
				{
					st = new SQLStatement();					
					sqlStatements[0] = st;
				}
			}
			else
			{
				st = sqlStatements[currentIndex];
				if( st == null )
				{
					st = new SQLStatement();					
					sqlStatements[currentIndex] = st;
				}
			}
			st.sqlConnection = conn;
			
			if( queryParams != null && queryParams[currentIndex] != null )
			{
				var params:Dictionary = queryParams[currentIndex];
				for ( var prop:String in params )
				{
					st.parameters[prop] = params[prop];					
				}
			}
			
			st.addEventListener(SQLEvent.RESULT, onResult);
			st.addEventListener(SQLErrorEvent.ERROR, onError);
			
			st.execute();
		}
		
		private function nextQuery():void
		{
			currentIndex++;
			
			if( currentIndex >= queryString.length )
			{
				conn.commit();
				
				cleanUp();
				
				if( responder != null )
				{
					responder.result("success");
				}
				return;
			}
			
			doQuery();
		}
		
		private function onResult(e:SQLEvent):void
		{
			//next query
			
			removeEventListener();
		}
		
		private function onError(e:SQLErrorEvent):void
		{
			//if something wrong, we need t orollback
			if( conn.inTransaction )
			{
				conn.rollback();
			}
			
			removeEventListener();
			
			cleanUp();
		}
		
		private function removeEventListener():void
		{
			st.removeEventListener(SQLEvent.RESULT, onResult);
			st.removeEventListener(SQLErrorEvent.ERROR, onError);
		}
		
		private function cleanUp():void
		{			
			for( var i:uint = 0,len:uint = sqlStatements.length;i<len;i++)
			{
				var state:SQLStatement = sqlStatements[i];					
				state.sqlConnection = null;
			}
			
			if( queryParams != null ) queryParams = null;
			
			if( autoCleanup == true )
			{
				queryString = null;
				sqlStatements = null;
				conn = null;
			}
		}
		
	}
}