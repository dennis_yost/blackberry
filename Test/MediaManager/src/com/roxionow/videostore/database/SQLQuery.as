package com.roxionow.videostore.database
{
	import com.roxionow.videostore.database.QueryResponder;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.utils.Dictionary;
	
	public class SQLQuery implements ISQLCommand
	{
		private var st:SQLStatement;	
		private var responder:QueryResponder;
		
		//whether to cleanup object when sql executed
		private var autoCleanup:Boolean;	
		
		public function SQLQuery( sql:String, conn:SQLConnection, autoDestroy:Boolean = true )
		{
			st = new SQLStatement();
			st.sqlConnection = conn;
			st.text = sql;
			
			autoCleanup = autoDestroy;
		}
		
		private var _name:String;
		public function set name( value:String ):void
		{
			_name = value;
		}
		
		public function get name():String
		{
			return _name;
		}
				
		public function run( queryResponder:QueryResponder = null, params:Dictionary = null ):void
		{
			responder = queryResponder;		
			
			if( params != null )
			{
				for ( var prop:String in params )
				{
					st.parameters[prop] = params[prop];					
				}
			}
			st.addEventListener(SQLEvent.RESULT, onResult);
			st.addEventListener(SQLErrorEvent.ERROR, onError);
			
			st.execute();
		}
		
		private function onResult(e:SQLEvent):void
		{
			if( responder != null )
			{
				responder.result(e.getResult());
			}
			cleanUp();
		}
		
		private function onError(e:SQLErrorEvent):void
		{
			if( responder != null )
			{
				responder.fault(e.error);
			}
			
			cleanUp();
		}
		
		private function cleanUp():void
		{
			st.removeEventListener(SQLEvent.RESULT, onResult);
			st.removeEventListener(SQLErrorEvent.ERROR, onError);
			
			if( autoCleanup == false ) return;
			
			st.sqlConnection = null;
			st = null;
		}
		
	}
}