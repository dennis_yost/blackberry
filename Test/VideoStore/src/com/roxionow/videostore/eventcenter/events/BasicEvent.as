package com.roxionow.videostore.eventcenter.events
{
	import flash.events.Event;

	public class BasicEvent extends Event
	{
		public var success:Boolean = true;
		
		public function BasicEvent(type:String)
		{
			super(type, true, false);
		}
		
	}
}