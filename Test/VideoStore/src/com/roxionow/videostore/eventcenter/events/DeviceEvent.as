package com.roxionow.videostore.eventcenter.events
{
	import flash.net.NetworkInterface;

	public class DeviceEvent extends BasicEvent
	{
		public static const LOW_MEMORY:String = "NETWORK_CHANGE";
		public static const BATTERY_LEVEL_CHANGE:String = "BATTERY_LEVEL_CHANGE";
		
		public static const NETWORK_CHANGE:String = "NETWORK_CHANGE";
		
		public static const ORIENTATION_CHANGE:String = "ORIENTATION_CHANGE";
		
		public static const REFRESH_GEO_LOCATION:String = "REFRESH_GEO_LOCATION";
		public static const GEO_LOCATION_UPDATED:String = "GEO_LOCATION_UPDATED";
		
		public var networks:Vector.<NetworkInterface>;
		public var orientation:int = 0;
		public var latitude:Number;
		public var longitude:Number;
		public var batteryLevel:int;
		public var batteryState:int;
		
		public function DeviceEvent(type:String)
		{
			super(type);
		}

	}
}