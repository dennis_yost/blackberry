package com.roxionow.videostore.eventcenter.events
{
	import flash.display.Stage;
	
	import spark.components.Group;
	
	public class MainAppEvent extends BasicEvent
	{
		public static const REGISTER_APP:String = "REGISTER_APP";
		public static const APP_DID_LAUNCH:String = "APP_DID_LAUNCH";
		
		public static const LOAD_DEFAULT_STRING:String = "LOAD_DEFAULT_STRING";
		public static const LANGUAGE_STRING_REFRESH_DONE:String = "LANGUAGE_STRING_REFRESH_DONE";
		
		public static const USER_KEY_DOWN:String = "USER_KEY_DOWN";
		public static const USER_KEY_UP:String = "USER_KEY_UP";
		
		public static const APP_ACTIVE:String = "APP_ACTIVE";
		public static const APP_DEACTIVE:String = "APP_DEACTIVE";
		
		public static const CHECK_USER_CREDENTIALS:String = "CHECK_USER_CREDENTIALS";
		
		public static const DO_SEARCH:String = "DO_SEARCH";
		
		public var mainContentFrame:Group;
		public var mainFrame:Group;
		public var stage:Stage;
		public var keyCode:uint;
		public var language:String;
		public var searchContent:String;
		public var searchType:String;
		
		public function MainAppEvent(type:String)
		{
			super(type);
		}

	}
}