package com.roxionow.videostore.eventcenter.events
{
	public class UIEvent extends BasicEvent
	{
		public static const SHOW_LOADING_OVERLAY:String = "SHOW_LOADING_OVERLAY";
		public static const HIDE_LOADING_OVERLAY:String = "HIDE_LOADING_OVERLAY";
		
		public static const STAGE_SIZE_UPDATE:String = "STAGE_SIZE_UPDATE";
		
		public static const SHOW_SECONDARY_MENU:String = "SHOW_SECONDARY_MENU";
		public static const SECONDARY_MENU_ANIMATING:String = "SECONDARY_MENU_ANIMATING";
		
		public static const SWITCH_SCREEN:String = "SWITCH_SCREEN";

		public var width:Number;
		public var height:Number;
		public var targetScreen:String;
		public var showSecondaryMenu:Boolean;
		public var secondaryMenuAnimating:Boolean;
		
		public function UIEvent(type:String)
		{
			super(type);
		}

	}
}