package com.roxionow.videostore.model.vo
{
	public class ScreenName
	{
		public static const SCREEN_MOVIES:String = "SCREEN_MOVIES";
		public static const SCREEN_TV_SHOWS:String = "SCREEN_TV_SHOWS";
		public static const SCREEN_MY_LIBRARY:String = "SCREEN_MY_LIBRARY";
		public static const SCREEN_DOWNLOADS:String = "SCREEN_DOWNLOADS";
		public static const SCREEN_WISH_LIST:String = "SCREEN_WISH_LIST";
	}
}