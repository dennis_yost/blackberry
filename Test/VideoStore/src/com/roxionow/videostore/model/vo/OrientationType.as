package com.roxionow.videostore.model.vo
{
	public class OrientationType
	{
		public static const ORIENTATION_TYPE_LEFT_SIDE_UP:int = 100;
		public static const ORIENTATION_TYPE_RIGHT_SIDE_UP:int = 200;
		public static const ORIENTATION_TYPE_STANDING:int = 300;
		public static const ORIENTATION_TYPE_STANDING_UPSIDE_DOWN:int = 400;
		public static const ORIENTATION_TYPE_FACE_UP:int = 500;
		public static const ORIENTATION_TYPE_FACE_DOWN:int = 600;
	}
}