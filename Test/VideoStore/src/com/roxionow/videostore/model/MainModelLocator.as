package com.roxionow.videostore.model
{
	import flash.display.Stage;
	
	import spark.components.Group;

	[Bindable]
	public class MainModelLocator
	{
		private static var _inst:MainModelLocator;
		
		public static function get inst():MainModelLocator
		{
			if (_inst == null)
			{
				_inst = new MainModelLocator(new MainModelLocatorIniter);
			}
			
			return _inst;
		}
		
		public function MainModelLocator(val:MainModelLocatorIniter)
		{
		}
		
		//=========================================================
		
		// local debug
		public var localDebug:Boolean = false;
		
		// language
		public var language:String = "en";
		
		// main app frame we can use for adding overlay or something at the top
		public var mainFrame:Group;
		
		// main content frame that for showing the real content
		public var mainContentFrame:Group;
		
		// player stage
		public var stg:Stage;
		
		// buildNumber
		public var buildNumber:String = "2011-02-25";
	
		// default string url
		public var defaultStringUrl:String = "defaultString";
		
		// app finish its loading
		public var isAppLoaded:Boolean = false;
		
		// last down key
		public var lastDownKey:uint;
		
		// is full screen
		public var isFullscreen:Boolean = false;
		
		// current location
		public var curLocation:String;
		
		// current screen
		public var curScreen:String
		
		// current battery level
		// used by warning user the battery level is low
		public var curBatteryLevel:int = -1;
		
		// show / close second menu
		public var showSecondaryMenu:Boolean = false;
		
		// is showing / closing second menu
		public var isSecondaryMenuAnimating:Boolean = false;
		
		// secondary menu animation time, in second
		public var secondaryMenuAnimationDuration:Number = 0.5;
		
	}
}

class MainModelLocatorIniter{}
