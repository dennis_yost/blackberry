package com.roxionow.videostore.service
{
	import com.adobe.serialization.json.JSON;
	
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.sensors.Geolocation;

	public class GeoLocationService
	{
		private const GOOGLE_GEOCODING_URL:String = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";
		
		private var successCallback:Function;
		private var faultCallback:Function;
		private var checkCountryName:Boolean = false;
		private var geo:Geolocation;
		private var latitude:Number;
		private var longitude:Number;
		
		public function GeoLocationService()
		{
		}
		
		/**
		 * Get current location, includes latitude, longitude and country name.
		 * @param needCountryName should we check the country name based on the latitude & longitude
		 * @param successCB callback function when success
		 * @param faultCB callback function when fail
		 * @return 
		 * 
		 */
		public function getCurrentLocation(needCountryName:Boolean,
										   successCB:Function,
										   faultCB:Function):Boolean
		{
			successCallback = successCB;
			faultCallback = faultCB;
			checkCountryName = needCountryName;
			
			if(Geolocation.isSupported)
			{
				var geo:Geolocation = new Geolocation();
				geo.addEventListener(GeolocationEvent.UPDATE, onGeoUpdate, false, 0, true);
				geo.setRequestedUpdateInterval(10000); // update location every 10 seconds
				return true;
			}
			else
			{
				// for local test, remove the comment of next line
				//doReverseGeocoding(40.714224,-73.961452);
				
				return false;
			}
		}
		
		private function onGeoUpdate(e:GeolocationEvent):void
		{
			geo.removeEventListener(GeolocationEvent.UPDATE, onGeoUpdate);
			geo = null;
			latitude = e.latitude;
			longitude = e.longitude;
			
			if (checkCountryName == false)
			{
				if (successCallback != null) successCallback({latitude: e.latitude, longitude: e.longitude});
			}
			else
			{
				doReverseGeocoding(e.latitude, e.longitude);
			}
		}
		
		private function doReverseGeocoding(lat:Number, lng:Number):void
		{
			var url:String = GOOGLE_GEOCODING_URL + String(lat) + "," + String(lng) + "&sensor=true";
			var urlLoader:URLLoader = new URLLoader;
			urlLoader.addEventListener(Event.COMPLETE, onGeoCodingDone);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onGeoCodingError);
			urlLoader.load(new URLRequest(url));
		}
		
		private function onGeoCodingDone(e:Event):void
		{
			var d:String = (e.target as URLLoader).data;
			var o:Object = JSON.decode(d);
			
			if (o != null && o.status == "OK")
			{
				var streetAddress:String = "";
				var country:String = "";
				for each (var ad:Object in o.results)
				{
					if (ad.types[0] == "street_address")
					{
						streetAddress = ad.formatted_address;
						continue;
					}
					if (ad.types[0] == "country")
					{
						country = ad.formatted_address;
						continue;
					}
				}

				if (successCallback != null) successCallback({latitude: this.latitude, longitude: this.longitude, country: country});
			}
			else
			{
				getGeocodingAddressFail();
			}
		}
		
		private function onGeoCodingError(e:IOErrorEvent):void
		{
			getGeocodingAddressFail();
		}
		
		private function getGeocodingAddressFail():void
		{
			if (faultCallback != null) faultCallback();
		}
	}
}