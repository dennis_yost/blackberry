package com.roxionow.videostore.service
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	public class StringService
	{
		private var loader:URLLoader;
		private var successCallback:Function;
		private var faultCallback:Function;

		public function StringService()
		{	
		}
		
		/**
		 * Load external string file with the given url.
		 * @param url the url of external string file
		 * @param successCB callback function when success
		 * @param faultCB callback function when fail
		 * 
		 */
		public function loadDefaultString(url:String, successCB:Function, faultCB:Function):void
		{
			successCallback = successCB;
			faultCallback = faultCB;
			
			loader = new URLLoader;
			// can not use weak reference here, don't know why
			loader.addEventListener(Event.COMPLETE, onStringLoaded);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onStringLoadFault);
			loader.load(new URLRequest(url));
		}
		
		private function onStringLoaded(e:Event):void
		{
			loader.removeEventListener(Event.COMPLETE, onStringLoaded);
			loader.removeEventListener(IOErrorEvent.IO_ERROR, onStringLoadFault);

			if (successCallback != null) successCallback(String(loader.data));
		}
		
		private function onStringLoadFault(e:IOErrorEvent):void
		{
			loader.removeEventListener(Event.COMPLETE, onStringLoaded);
			loader.removeEventListener(IOErrorEvent.IO_ERROR, onStringLoadFault);
			
			if (faultCallback != null) faultCallback(e.text);
		}
	}
}