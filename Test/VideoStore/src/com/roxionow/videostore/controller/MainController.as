package com.roxionow.videostore.controller
{
	
	import com.roxionow.videostore.eventcenter.EventCenter;
	import com.roxionow.videostore.eventcenter.events.DeviceEvent;
	import com.roxionow.videostore.eventcenter.events.MainAppEvent;
	import com.roxionow.videostore.model.MainModelLocator;
	import com.roxionow.videostore.service.GeoLocationService;
	import com.roxionow.videostore.service.StringService;
	
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	import flash.xml.XMLDocument;
	import flash.xml.XMLNode;
	
	import mx.rpc.Fault;
	import mx.rpc.xml.SimpleXMLDecoder;
	
	import qnx.system.DeviceBatteryState;
	
	import spark.components.Group;
	
	public class MainController
	{
		private static var _inst:MainController;
		
		private var model:MainModelLocator = MainModelLocator.inst;
		private var eventCenter:EventCenter = EventCenter.inst;
		private var stringManager:StringManager = StringManager.inst;
		
		public static function get inst():MainController
		{
			if (_inst == null)
			{
				_inst = new MainController(new MainControllerIniter);
				_inst.registerListener();
			}
			return _inst;
		}
		
		public function MainController(val:MainControllerIniter){}
		
		/**
		 * Called when the class is initlized. It registes all listeners the class are interested in.
		 */
		public function registerListener():void
		{
			eventCenter.addEventListener(MainAppEvent.REGISTER_APP, onRegisterApp, false, 0, true);
			eventCenter.addEventListener(MainAppEvent.LOAD_DEFAULT_STRING, onLoadDefaultString, false, 0, true);
			eventCenter.addEventListener(DeviceEvent.REFRESH_GEO_LOCATION, onRefreshGeoLocation, false, 0, true);
			eventCenter.addEventListener(DeviceEvent.BATTERY_LEVEL_CHANGE, onBatteryLevelChange, false, 0, true);
		}
		
		private function onRegisterApp(e:MainAppEvent):void
		{
			registerApp(e.mainFrame, e.mainContentFrame, e.stage, e.language);
		}
		
		/**
		 * Save app componnets to Model for future use.
		 * @param mainFrame The main frame contains content.
		 * @param mainContentFrame The main content frame (within the Main Frame).
		 * @param stg The global Stage of this app.
		 * @param language Language of system.
		 */
		public function registerApp(mainFrame:Group,
									mainContentFrame:Group,
									stg:Stage,
									language:String):void
		{
			model.mainFrame = mainFrame;
			model.mainContentFrame = mainContentFrame;
			model.language = language;
			model.stg = stg;
			LogManager.inst.write("Language: " + model.language);
		}
		
		private function onLoadDefaultString(e:MainAppEvent):void
		{
			loadDefaultString();
		}
			
		/**
		 * Load language string based on system language. The string file is an external xml file.
		 */
		public function loadDefaultString():void
		{
			var s:StringService = new StringService;
			s.loadDefaultString(generateStringUrl(), defaultStringLoaded, loadDefaultStringError);
		}
				
		private function defaultStringLoaded(result:String):void
		{
			LogManager.inst.write("String for " + model.language + " is loaded.");
			
			var xmlDoc:XMLDocument = new XMLDocument(result);
			var decoder:SimpleXMLDecoder = new SimpleXMLDecoder();
			var defString:Object = decoder.decodeXML(xmlDoc);
			
			stringManager.loadDefString(defString);
			
			var e:MainAppEvent = new MainAppEvent(MainAppEvent.LANGUAGE_STRING_REFRESH_DONE);
			eventCenter.dispatchEvent(e);
		}
		
		private function loadDefaultStringError(str:String):void
		{
			LogManager.inst.write("Load deafult string error: " + str, true);
		}
		
		private function onRefreshGeoLocation(e:DeviceEvent):void
		{
			refreshGeoLocation();
		}
		
		/**
		 * Refresh current geo location.
		 */
		public function refreshGeoLocation():void
		{
			var geoService:GeoLocationService = new GeoLocationService;
			var isGeoServiceAvailable:Boolean = geoService.getCurrentLocation(false, refreshGeoLocationSuccess, refreshGeoLocationError);
			if(isGeoServiceAvailable == false)
			{
				refreshGeoLocationError();
			}
		}
		
		private function refreshGeoLocationSuccess(result:Object):void
		{
			LogManager.inst.write("GeoLocation Done: " + result.country);
			if (result.country) model.curLocation = result.country;
			
			var e:DeviceEvent = new DeviceEvent(DeviceEvent.GEO_LOCATION_UPDATED);
			e.latitude = result.latitude;
			e.longitude = result.longitude;
			eventCenter.dispatchEvent(e);
		}
		
		private function refreshGeoLocationError():void
		{
			LogManager.inst.write("GeoLocation Fail", true);
			var e:DeviceEvent = new DeviceEvent(DeviceEvent.GEO_LOCATION_UPDATED);
			e.success = false;
			eventCenter.dispatchEvent(e);
		}
		
		private function onBatteryLevelChange(e:DeviceEvent):void
		{
			if(e.batteryState == DeviceBatteryState.UNPLUGGED || e.batteryState == DeviceBatteryState.UNKNOWN)
			{
				if (e.batteryLevel < 2)
				{
					// TODO:
					// if video is playing it will stop the video, save the timecode for resuming and
					// display a critical battery indictor message (PRD 10.9)
				}
				else if (e.batteryLevel < 10)
				{
					// TODO:
					// When playing back videos if the battery reaches < 10% a low battery indicator
					// message is displayed (PRD 10.9)
				}
				model.curBatteryLevel = e.batteryLevel;
			}
		}

		private function userKeyDown(k:uint):void
		{
			model.lastDownKey = k;
		}
		
		private function userKeyUp(k:uint):void
		{
			model.lastDownKey = 0;
		}
				
		private function switchFullscreen(fullscreen:Boolean):void
		{
			model.isFullscreen = fullscreen;
			
			if (fullscreen == true)
			{
				model.stg.displayState = StageDisplayState.FULL_SCREEN;
			}
			else
			{
				model.stg.displayState = StageDisplayState.NORMAL;
			}
		}
		
		private function generateStringUrl():String
		{
			var bn:String = "?buildNumber=" + model.buildNumber;
			//bn = "";
			if (model.language == "en")
			{
				return model.defaultStringUrl + ".xml" + bn; 
			}
			else if (model.language.indexOf("zh") > -1)
			{
				return model.defaultStringUrl + "CHS.xml" + bn; 
			}
			
			// default
			return model.defaultStringUrl + ".xml" + bn;
		}
		
	}
}

class MainControllerIniter{}