package com.roxionow.videostore.controller
{
	import flash.utils.Dictionary;

	/**
	 * This the string manager class. It loads an exteranl string xml file and parse to a dictionary.
	 * String value can be get by using singleString() function.
	 * 
	 * @class StringManager
	 * @author Zunlin Zhang
	 */
	public class StringManager
	{
		private static var _inst:StringManager;
		
		private var useLocString:Boolean = false;
		private var locString:Dictionary = new Dictionary;
		private var defString:Dictionary = new Dictionary;
		private var isStringFilled:Boolean = false;
		
		public static function get inst():StringManager
		{
			if (_inst == null)
			{
				_inst = new StringManager(new StringManagerIniter);
			}
			return _inst;
		}
		
		public function StringManager(val:StringManagerIniter){}
		
		/**
		 * Check if the strings are loaded and can be used. 
		 * @return true if strings are ready.
		 */
		public function get isStringReady():Boolean
		{
			return isStringFilled;
		}
		
		/**
		 * Get the localized string by given a key 
		 * @return Localized string if the key can be found, otherwise, return the key as the string.
		 */
		public function singleString(key:String):String
		{
			return useLocString ? locStringGetter(key) : defStringGetter(key);
		}
		
		/**
		 * Load embedded string. 
		 * @return
		 */
		public function setLocString(strings:Object):void
		{
			fillString(strings, locString);
			useLocString = true;
		}
		
		/**
		 * Load external string.
		 * @return
		 */
		public function loadDefString(strings:Object):void
		{
			fillString(strings, defString);
		}
		
		private function locStringGetter(key:String):String
		{
			if (locString[key] == null)
			{
				return defStringGetter(key);
			}
			else
			{
				return locString[key];
			}
		}
		
		private function defStringGetter(key:String):String
		{
			if (defString[key] == null)
			{
				LogManager.inst.write("StringManager: String getter error on key -- " + key);
				return key;
			}
			else
			{
				return defString[key];
			}
		}
		
		private function fillString(strings:Object, dic:Dictionary):void
		{
			if (strings != null)
			{
				var localcString:Array = strings.MediaManagerString.UIString as Array;
				if (localcString != null && localcString.length > 0)
				{
					for (var i:int = 0; i < localcString.length; ++i)
					{
						dic[localcString[i].Key] = localcString[i].Value;
					}
					
					isStringFilled = true;
				}
			}
		}
	}
}

class StringManagerIniter{}