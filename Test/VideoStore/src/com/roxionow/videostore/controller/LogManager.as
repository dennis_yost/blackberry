package com.roxionow.videostore.controller
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	[Event(name="LogUpdate", type="flash.events.Event")]
	
	/**
	 * LogManager keeps logs in the system. For devices or system which can not do Flash trace,
	 * this class help keep logs.
	 * 
	 * NOTE: LogManager doesn't use EventCenter for log event dispatching, you need to add listener on the class.
	 * 
	 * @class LogManager
	 * @author ZunlinZhang
	 */
	public class LogManager extends EventDispatcher
	{
		public static const EVENT_LOG_UPDATE:String = "LogUpdate";
		
		private static const MAX_LOG_SIZE:int = 500;
		
		private static var _inst:LogManager;
		
		private var logHistory:Vector.<String>;
		private var logEnable:Boolean = true;
		private var lastLogString:String = "";
		
		public static function get inst():LogManager
		{
			if (_inst == null)
			{
				_inst = new LogManager(new LogManagerIniter);
				_inst.logHistory = new Vector.<String>;
			}
			return _inst;
		}
		
		public function LogManager(val:LogManagerIniter){}
		
		/**
		 * Turn on / off the Logger. When OFF, no log will be recorded anymore (doesn't affect existing logs).
		 * @param val on (true) or off (false)
		 */
		public function set logOn(val:Boolean):void
		{
			logEnable = val;
		}
		
		/**
		 * Get the latest log of the app.
		 * @return Log string of last log.
		 */
		public function get lastLog():String
		{
			return lastLogString;
		}
		
		/**
		 * Write log to LogManager.
		 * @param logInfo the log itself
		 * @param isError is the log a Error log or normal one
		 */
		public function write(logInfo:String, isError:Boolean = false):void
		{
			if (logEnable == false) return;
			
			var pre:String = isError ? "[FAULT] " : "";
			logHistory.push(pre + logInfo);
			lastLogString = pre + logInfo;
			if (logHistory.length >= MAX_LOG_SIZE) logHistory.splice(0, 1);
			trace(lastLogString);
			
			dispatchEvent(new Event(EVENT_LOG_UPDATE));
		}
		
		/**
		 * Get all logs recorded.
		 * @return logs in an Array
		 */
		public function get logArray():Vector.<String>
		{
			return logHistory;
		}

	}
}

class LogManagerIniter{}
