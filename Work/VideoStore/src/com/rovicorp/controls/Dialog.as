package com.rovicorp.controls
{
	import com.rovicorp.skins.DialogSkin;
	import com.rovicorp.skins.controls.GenericBtnSkin;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayList;
	import mx.core.FlexGlobals;
	import mx.core.IVisualElement;
	import mx.managers.PopUpManager;
	
	import spark.components.Group;
	import spark.components.Label;
	import spark.components.SkinnableContainer;
	import spark.components.VGroup;
	import spark.components.supportClasses.SkinnableComponent;
	
	public class Dialog extends SkinnableContainer
	{
		private var _title:String;
		private var _buttons:Vector.<DisplayObject>

		private var _selectedIndex:int = -1;
		
		public function Dialog()
		{
			super();
			setStyle("skinClass", DialogSkin); 
		}
		
		[SkinPart(required="true")]
		public var titleBar:Label;

		[SkinPart(required="true")]
		public var buttonBar:Group;  //TODO: Flex tips states button bar is not mobile optimized, consider replacing

		[Bindable]
		public function get title():String
		{
			return _title;
		}
		
		public function set title(value:String):void
		{
			_title = value;
		}

		public function get buttons():Vector.<DisplayObject>
		{
			return null;
		}
		
		public function set buttons( value:Vector.<DisplayObject> ):void
		{
			_buttons = value;
			
			if (buttonBar)
				addButtons();
		}

		private function addButtons():void
		{
			for each (var child:SkinnableComponent in _buttons)
			{
				child.addEventListener(MouseEvent.CLICK, onButtonClick);
				child.setStyle("skinClass", GenericBtnSkin);
				
				buttonBar.addElement(child as IVisualElement);
			}
		}

		override protected function partAdded(partName:String, instance:Object):void 
		{
			super.partAdded(partName, instance);
			
			if (partName == "titleBar")
				titleBar.text = _title;
			else if (partName == "buttonBar" && _buttons)
				addButtons();
		}
		
		override protected function partRemoved(partName:String, instance:Object):void 
		{
			super.partRemoved(partName, instance);
		}
		
		private function onButtonClick(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
			
			_selectedIndex = _buttons.indexOf(event.currentTarget);
			dispatchEvent(new Event(Event.SELECT));
		}
		
		public function show():void
		{
			PopUpManager.addPopUp(this, FlexGlobals.topLevelApplication as DisplayObject, true)
			PopUpManager.centerPopUp(this);
		}
	}
}