package com.rovicorp.utilities
{
	public class ImagePoolTask 
	{
		public var isCancelled:Boolean = false;
		public var loadFromWeb:Boolean;
		public var type:String;
		public var url:String;
		public var callback:Function;
		
		public function ImagePoolTask(type:String, url:String, callback:Function)
		{
			this.type = type;
			this.url = url;
			this.callback = callback
		}
		
		public function cancel():void
		{
			isCancelled = true;
		}
	}
}