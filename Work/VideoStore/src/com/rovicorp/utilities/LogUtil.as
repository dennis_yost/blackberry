package com.rovicorp.utilities
{
	import flash.display.DisplayObject;
	import flash.events.Event;

	public class LogUtil
	{
		import com.junkbyte.console.Console;		
		import com.junkbyte.console.Cc;
		import com.junkbyte.console.ConsoleChannel;
		import com.junkbyte.console.core.Remoting;		
		
		public static function init( ui:DisplayObject ):void
		{
			Cc.startOnStage(ui, "`"); // "`" - change for password. This will start hidden
			//Cc.visible = true; // Show console, because having password hides console.
			
			Cc.config.commandLineAllowed = true; // enable advanced (but security risk) features.
			Cc.config.tracing = true; // Also trace on flash's normal trace
			
			Cc.instance.remoter.addEventListener(Event.CONNECT, function(e:Event):void{
				Cc.log("Log server connected");
			});
			
			Cc.remotingSocket("10.178.15.45", 1990);
			
			Cc.remotingPassword = null; // Just so that remote don't ask for password
			Cc.remoting = true; // Start sending logs to remote (using LocalConnection)
			
			Cc.commandLine = true; // Show command line
			
			//Cc.height = 200;
			
			Cc.log("Log initalizing ...");
		}
		
		public static function info(...paras):void
		{
			Cc.info(paras);
		}
		
		public static function log(...paras):void
		{
			Cc.log(paras);
		}
		
		public static function debug(...paras):void
		{
			Cc.debug(paras);
		}
		
		public static function error(...paras):void
		{
			Cc.error(paras);
		}
		
	}
}