﻿package com.rovicorp.utilities
{
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.EventCenter;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	

	/**
	 * ImagePool caches image bitmapdata for future use. It will load the image if it can not be found in cache or local disk.
	 * just one place for holding all image data.
	 * 
	 * @author ZunlinZhang
	 */
	public class ImagePool
	{
		// ==================  [VARIABLES]   ==================
		private static var _inst:ImagePool;
		
		private var _imagePool:Dictionary;
		private var _imgPoolKeys:Vector.<String>;
		private var _cacheLength:int = 500;
		
		// Element: Object, {type: TASK_TYPE, url:String, cbFn:Function}
		private var _taskList:Vector.<ImagePoolTask>;
		private var _curTask:ImagePoolTask;
		
		private var _useCache:Boolean = true;
		private var _isProcessing:Boolean = false;
		private var _loader:Loader;
		private var _urlLoader:URLLoader;
		
		private var _eventCenter:EventCenter = EventCenter.inst;
		
		private const LOCAL_FILE_PREFIX:String = "assets/boxart/";
		private const LOCAL_CACHE_SIZE_LIMIT:Number = 10 * 1024 * 1024;	// 10M
		
		private const TASK_TYPE_GET_IMAGE_DATA:String = "TASK_TYPE_GET_IMAGE_DATA";
		private const TASK_TYPE_CHECK_CACHE_LIMIT:String = "TASK_TYPE_CHECK_CACHE_LIMIT";

		/**
		 * This is a singleton class. User should never call the constructor.
		 * @param val
		 */
		public function ImagePool(val:ImagePoolConstHelper)
		{
			_imagePool = new Dictionary;
			_imgPoolKeys = new Vector.<String>;
			_taskList = new Vector.<ImagePoolTask>;
			
			_loader = new Loader;
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onMediaLoaded, false, 0, true);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onMediaLoadError, false, 0, true);
			
			_urlLoader = new URLLoader();
			_urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			_urlLoader.addEventListener(Event.COMPLETE, onLoadImageDataDone, false, 0, true);
			_urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onLoadImageDataError, false, 0, true);
			
			_taskList.push(new ImagePoolTask(TASK_TYPE_CHECK_CACHE_LIMIT, null, null));
			processTask();
		}
		
		public static function get inst():ImagePool
		{
			if (_inst == null)
			{
				_inst = new ImagePool(new ImagePoolConstHelper());
			}
			
			return _inst;
		}
		
		/**
		 * Turn on / off the cache.
		 * 
		 * @param val
		 */
		public function set useCache(val:Boolean):void
		{
			_useCache = val;
		}
		
		/**
		 * Set how many image should be cached. Old image cache will be removed if newer one is coming.
		 * 
		 * @param val the number for image cache
		 */
		public function set cacheLength(val:int):void
		{
			_cacheLength = val;
			checkCacheLength();
		}
		
		/**
		 * Return the image cache number
		 * @return 
		 */
		public function get cacheLength():int
		{
			return _cacheLength;
		}
		
		/**
		 * Remove all image cache.
		 */
		public function emptyCache():void
		{
			var l:int = _imgPoolKeys.length;
			for (var i:int = 0; i < l; ++i)
			{
				(_imagePool[_imgPoolKeys[i]] as BitmapData).dispose();
				delete _imagePool[_imgPoolKeys[i]];
			}
			_imgPoolKeys.splice(0, l);
		}
		
		/**
		 * Get the image bitmapdata by given key (image load url).
		 * 
		 * @param key Image load url as the Key.
		 * @param cb call back function
		 */
		public function getImageBitmapData(key:String, cb:Function):ImagePoolTask
		{
			//trace("ADD NEW TASK: ", key);
			var result:ImagePoolTask = new ImagePoolTask(TASK_TYPE_GET_IMAGE_DATA, key, cb);
				
			_taskList.push(result);
			if (_isProcessing == false)
			{
				processTask();
			}
			
			return result;
		}
		
		/**
		 * Cancel the previous request for an image load.
		 * @param key
		 */
/*		public function cancelDataFetchRequest(key:String):void
		{
			if (_curTask != null && _curTask.url == key)
			{
				_curTask.cbFn = null;
			}
			else if (_taskList.length > 1)
			{
				var idx:int = -1;
				for (var i:int = 1; i < _taskList.length; ++i)
				{
					if (_taskList[i].url == key)
					{
						idx = i;
						break
					}
				}
				if (idx != -1) _taskList.splice(idx, 1);
			}
		}*/
		
		/**
		 * Get the cached image bitmapdata by given key (image load url).
		 * 
		 * @param key Image load url as the Key.
		 * @return Image data bitmapdata if there is.
		 */
		public function getCachedImageBitmapData(key:String):BitmapData
		{
			if (_useCache)
			{
				return _imagePool[key];
			}
			else
			{
				return null;
			}
		}
		
		/**
		 * Check if one image is cached.
		 * 
		 * @param key Image load url as the Key.
		 * @return true or false
		 */
		public function isImageCached(key:String):Boolean
		{
			return (_imagePool[key] != null);
		}
		
		/**
		 * Cache one image.
		 * 
		 * @param key Image load url as the Key.
		 * @param bp The bitmapdata for that image.
		 */
		public function addImageCache(key:String, bp:BitmapData):void
		{
			if (isImageCached(key) == false)
			{
				_imgPoolKeys.push(key);
			}
			_imagePool[key] = bp;
			
			checkCacheLength();
		}
		
		/**
		 * Remove specific image's cache by given key.
		 * 
		 * @param key Image load url as the Key.
		 */
		public function removeImageCache(key:String):void
		{
			if (isImageCached(key) == true)
			{
				(_imagePool[key] as BitmapData).dispose();
				delete _imagePool[key];
				var i:int = findKeyIndex(key);
				if (i != -1)
				{
					_imgPoolKeys.splice(i, 1);
				}
			}
		}
		
		private function processTask():void
		{
			if (_taskList.length == 0)
			{
				// all tasks are done
				_isProcessing = false;
				return;
			}
			
			//trace("START NEW TASK, REMAIN: ", _taskList.length - 1);
			_isProcessing = true;
			_curTask = _taskList[0];
			
			while (_curTask.isCancelled)
			{
				_taskList.splice(0, 1);
				
				if (_taskList.length == 0)
				{
					_curTask = null;
					return;
				}
				
				_curTask = _taskList[0];
			}
			
			if (_curTask.type == TASK_TYPE_GET_IMAGE_DATA)
			{
				// check if current request is cached
				var bd:BitmapData = getCachedImageBitmapData(_curTask.url);
				if (bd != null)
				{
					//trace("load from cache: ", _curTask.url);
					taskDone(true, bd);
					return;
				}
	
				// check if it's located on disk
				var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX + convertFileName(_curTask.url));
				if (f.exists)
				{
					// read bitmap from file
					//trace("load from local: ", f.size, _curTask.url);
					_curTask.loadFromWeb = false;
					_urlLoader.load(new URLRequest(f.url));
				}
				else
				{
					// load from web server
					//trace("load from server: ", _curTask.url);
					_curTask.loadFromWeb = true;
					_urlLoader.load(new URLRequest(_curTask.url));
				}
			}
			else if (_curTask.type == TASK_TYPE_CHECK_CACHE_LIMIT)
			{
				checkLocalCacheLimit();
			}
		}
		
		private function onLoadImageDataDone(e:Event):void
		{
			if (_urlLoader.data)
			{
				if (_curTask.loadFromWeb == true)
				{
					// write to file first
					var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX + convertFileName(_curTask.url));
					var fs:FileStream = new FileStream;
					
					try
					{
						fs.open(f, FileMode.WRITE);
						fs.writeBytes(_urlLoader.data);
						fs.close();
						
						// save to db
						var d:Date = new Date;
						var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_FILE_CACHE);
						e0.id = _curTask.url;
						e0.updateExistingItem = false;
						e0.fileCache = {Source: _curTask.url, FilePath: f.nativePath, Size: f.size, UpdateTime: d.toString()};
						_eventCenter.dispatchEvent(e0);
					}
					catch (err:Error)
					{
						trace("[--] Write to file error");
					}
					finally
					{
						loadImageFromByteArray(_urlLoader.data);
					}
				}
				else
				{
					loadImageFromByteArray(_urlLoader.data);
				}
			}
			else
			{
				if (_curTask.loadFromWeb == false)
				{
					// reload from webservice
					_urlLoader.load(new URLRequest(_curTask.url));
				}
				else
				{
					taskDone(false, "LoadBoxartFromWebFault");
				}
			}
		}
		
		private function onLoadImageDataError(e:IOErrorEvent):void
		{
			if (_curTask.loadFromWeb == false)
			{
				// reload from webservice
				_urlLoader.load(new URLRequest(_curTask.url));
			}
			else
			{
				taskDone(false, "LoadBoxartFromWebFault");
			}
		}
		
		private function loadImageFromByteArray(ba:ByteArray):void
		{
			_loader.loadBytes(ba);
		}
		
		private function onMediaLoadError(e:IOErrorEvent):void
		{
			taskDone(false, "LoadBoxartFromByteArrayFault");
		}
		
		private function onMediaLoaded(e:Event):void
		{
			var li:LoaderInfo = e.target as LoaderInfo;
			
			if (li.content && (li.content is Bitmap))
			{
				var bd:BitmapData = (li.content as Bitmap).bitmapData.clone();
				// save bd to cache
				//trace("[+++] add new cache from loaded file");
				addImageCache(_curTask.url, bd);
				taskDone(true, bd);
			}
			else
			{
				taskDone(false, "LoadBoxartFromWebFault");
			}
		}
		
		private function taskDone(success:Boolean, result:Object = null):void
		{
			//trace("TASK DONE: ", success, _curTask.url);
			if (_curTask.callback != null && !_curTask.isCancelled)
			{
				_curTask.callback({success: success, result: result});
			}
			
			_loader.unload();
			_taskList.splice(0, 1);
			processTask();
		}
		
		private function checkLocalCacheLimit():void
		{
			_eventCenter.addEventListener(DatabaseEvent.GET_FILE_CACHE_DONE, onGetFileCacheInfoForCheckSizeLimit, false, 0, true);
			_eventCenter.dispatchEvent(new DatabaseEvent(DatabaseEvent.GET_FILE_CACHE));
		}
		
		private function onGetFileCacheInfoForCheckSizeLimit(e:DatabaseEvent):void
		{
			_eventCenter.removeEventListener(DatabaseEvent.GET_FILE_CACHE_DONE, onGetFileCacheInfoForCheckSizeLimit);
			
			if (e.success == false || e.result == null)
			{
				taskDone(true);
				return;
			}
			
			var r:Array = [];
			if (e.result is Array)
			{
				r = e.result as Array;
			}
			else
			{
				r.push(e.result);
			}
			
			var curSize:Number = 0;
			var curCache:Object = {};
			var removeIdx:int = -1;
			for (var i:int = r.length - 1; i >= 0; --i)
			{
				curCache = r[i];
				if (curCache.Size)
				{
					curSize += curCache.Size;
				}
				
				if (curSize >= LOCAL_CACHE_SIZE_LIMIT)
				{
					removeIdx = i;
					break;
				}
			}
			
			if (removeIdx != -1)
			{
				for (i = 0; i <= removeIdx; ++i)
				{
					curCache = r[i];
					// remove from disk
					if (curCache.Source)
					{
						var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX + convertFileName(curCache.Source));
						if (f.exists)
						{
							trace("Remove Image Local Cache: ", f.nativePath);
							f.deleteFileAsync();
						}
					}
					// remove from db
					var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.REMOVE_FILE_CACHE);
					e0.id = curCache.Source;
					_eventCenter.dispatchEvent(e0);
				}
			}

			taskDone(true);
		}
		
		private function convertFileName(name:String):String
		{
			while (name.indexOf("/") > -1)
			{
				name = name.replace("/", "-");
			}
			while (name.indexOf(":") > -1)
			{
				name = name.replace(":", "-");
			}

			//trace("File name: ", name);
			return name;
		}
		
		private function checkCacheLength():void
		{
			var diff:int = _imgPoolKeys.length - _cacheLength;
			if (diff > 0)
			{
				for (var i:int = 0; i < diff; ++i)
				{
					(_imagePool[_imgPoolKeys[i]] as BitmapData).dispose();
					delete _imagePool[_imgPoolKeys[i]];
				}
				_imgPoolKeys.splice(0, diff);
			}
		}
		
		private function findKeyIndex(key:String):int
		{
			var l:int = _imgPoolKeys.length;
			for (var i:int = 0; i < l; ++i)
			{
				if (_imgPoolKeys[i] == key)
				{
					return i;
				}
			}
			return -1;
		}

	}
}

class ImagePoolConstHelper{}

