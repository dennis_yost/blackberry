package com.rovicorp.utilities
{
	import flash.text.TextField;

	/**
	 * 
	 * @author yanlin_qiu
	 * 
	 */	
	public class StringUtil
	{
		/**
		 * 
		 * @param bytes
		 * @return 
		 * 
		 */		
		public static function formatByteSize( bytes:int ):String
		{
			return (bytes/(1024*1024)).toFixed(1) + " MB";			
		}
		
		public static function ms2TimeString(val:Number):String
		{
			if (val < 0) return "";
			
			var s:int = val * 0.001;	// second
			var m:int = s / 60;			// minute
			s = s - m * 60;
			var h:int = m / 60;			// hour
			m = m - h * 60;
			
			var t:String = ((s < 10) ? ":0" : ":") + s;
			t = m + t;
			if (m < 10 && m > 0) t = "0" + t;
			if (h > 0) t = h + ":" + t;
			return t;
		}
		
		/**
		 * 
		 * @param seconds
		 * @return String
		 * 
		 */		
		public static function formatTime( seconds:int ):String
		{
			/*
			>= 2 days	 [x] days [y] hours
			> 1 day	1 day [y] hours
			>= 2 hours	 [x] hours [y] minutes
			> 1 hour	1 hour [y] minutes
			>= 2 minutes	 [x] minutes [y] seconds
			> 1 minute	1 minute [y] seconds
			> 1 second	 [y] seconds
			*/
			var secs:int = Math.floor(seconds % 60);
			var mins:int = Math.floor(seconds / 60);
			var left_mins:int = mins % 60;
			var hours:int = Math.floor(mins / 60);
			var days:int = Math.floor(hours/24);
			var left_hours:int = hours % 24;
			
			var ret_val:String = '';
			
			if( days >= 2 )
			{
				ret_val = days + " days " + left_hours + " hours";
			}
			else if( days == 1 && left_hours > 0 )
			{
				ret_val = days + " days " + left_hours + " hours";
			}
			else if ( hours >=2 ) 
			{
				ret_val = hours + " hours " + left_mins + " minutes";
			}
			else if( hours == 1 && left_mins > 0 ) 
			{
				ret_val = hours + " hours " + left_mins + " minutes";
			}
			else if ( mins >= 2 )
			{
				ret_val = mins + " minutes " + secs + " seconds";
			}
			else if( mins == 1 && secs > 0 )
			{
				ret_val = mins + " minutes " + secs + " seconds";
			}
			else 
			{
				ret_val = secs + " seconds";
			}
			
			return ret_val;
		}
		
		private static var tf:TextField;
		
		/**
		 * Convert html string to plain text
		 * @param value
		 * @return 
		 * 
		 */		
		public static function convertHtmlToPlainText(value:String, convertSpecailCharacter:Boolean = false ):String		
		{
			if( tf == null ) tf = new TextField();			
	
			tf.htmlText = value;
			
			if( convertSpecailCharacter )
			{
				value = convertSpecialCharacter(tf.text);
				
				return convertHtmlToPlainText(value);
			}
			return tf.text;
		}
		
		/**
		 * Convert special string in HTML format to characters, e.g. copy, trade
		 * @param value
		 * @return 
		 * 
		 */		
		public static function convertSpecialCharacter(value:String):String
		{
			//&trade; &#8482;
			//&copy;  &#169;
			value = value.replace(/&copy;/gi, "&#169;");
			value = value.replace(/&trade;/gi, "&#8482;");
			
			return value;
		}
	}
}