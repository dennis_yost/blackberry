package com.rovicorp.skins
{
	
	import com.rovicorp.skins.controls.ThumbnailSkin;
	import com.rovicorp.utilities.Boxart;
	
	import flash.sampler.NewObjectSample;
	
	import mx.styles.ISimpleStyleClient;
	import mx.styles.IStyleClient;
	import mx.styles.StyleManager;
	
	import spark.components.Image;
	import spark.components.Label;
	import spark.components.MobileItemRenderer;
	import spark.filters.DropShadowFilter;
	
	public class TitlesRenderer extends MobileItemRenderer
	{
		private var _image:Image;
		private var _titleLabel:Label;
		private var _data:Object;
		
		public static const IMAGE_WIDTH:int = 77;
		public static const IMAGE_HEIGHT:int = 107;
		public static const V_PADDING:int = 6;
		public static const H_PADDING:int = 6;
		public static const V_GAP:int = 4;
		public static const H_GAP:int = 15;
		
		public function TitlesRenderer()
		{
			super();
		}
		
		public function get image():Image
		{
			return _image;
		}
		
		public function set image(value:Image):void
		{
			_image = value;
		}
		
		override public function set data(value:Object):void {
			_data = value;
			label = _data.Name;
			image.source = Boxart.URL_107+_data.BoxartPrefix+Boxart.POST_FIX_107;
			image.width = IMAGE_WIDTH;
			image.height = IMAGE_HEIGHT;
		}
		
		override protected function drawBackground(unscaledWidth:Number, 
												   unscaledHeight:Number):void
		{
			
		}
		
		override protected function layoutContents(unscaledWidth:Number, unscaledHeight:Number):void
		{
			labelDisplay.commitStyles();
			labelDisplay.y = unscaledHeight - 20;
			labelDisplay.maxChars = 9;
			labelDisplay.truncateToFit();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			if (!image) {
				image = new Image();
				var dropShadow:DropShadowFilter = new DropShadowFilter();
				dropShadow.distance = 2;
				dropShadow.angle = 90;
				dropShadow.color = 0x000000;
				dropShadow.alpha = .49;
				dropShadow.blurX = 5;
				dropShadow.blurY = 5;
				dropShadow.inner = false;
				dropShadow.knockout = false;
				dropShadow.hideObject = false;
				image.filters = new Array(dropShadow);
				image.cacheAsBitmap = true;
			}
			addChild(image);
		}
	}
}