package com.rovicorp.skins
{
	import com.rovicorp.events.SelectionEvent;
	
	import flash.events.Event;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	import mx.core.FlexGlobals;
	import mx.styles.CSSStyleDeclaration;
	import mx.styles.IStyleManager2;
	
	import qnx.ui.data.DataProvider;
	import qnx.ui.events.ListEvent;
	import qnx.ui.listClasses.AlternatingCellRenderer;
	import qnx.ui.listClasses.List;
	import qnx.ui.listClasses.ListSelectionMode;
	import qnx.ui.listClasses.ScrollDirection;
	import qnx.ui.listClasses.TileList;
	import qnx.ui.text.Label;
	
	public class StoreCategoryRenderer extends AlternatingCellRenderer
	{
		static private var _subTextFormat:TextFormat;
		static private var _titleTextFormat:TextFormat;
		
		private var title:Label;
		private var text:Label;
		private var list:TileList;
		

		public function StoreCategoryRenderer()
		{
			super();
		}
		
		override public function set data(value:Object) : void
		{
			super.data = value;
	
			title.text = value.Name ? value.Name : "Loading...";
			list.dataProvider = new DataProvider(data.titles.toArray());
		}

		override protected function init():void 
		{
			super.init();

			setSkin(ListItemSkin);
			height = 145;

			title = new Label();
			title.x = 30;
			title.y = 10;
			title.autoSize = TextFieldAutoSize.LEFT;
			title.format = titleTextFormat;
			addChild(title);

			text = new Label();
			text.x = 30;
			text.y = 32;
			text.text = "(Select to view all)";
			text.format = subTextFormat;
			text.autoSize = TextFieldAutoSize.LEFT;
			addChild(text);

			list = new TileList();
			list.setSize(769, height);
			list.setPosition(255, 10);
			list.enableShadows = false;
			list.setSkin(TitleCellRenderer);
			list.scrollable = false;
			list.rowHeight = 130;
			list.columnWidth = 84;
			list.cellPadding = 25;
			list.columnCount = 7;
			addChild(list);
		}

		// TODO: Move to a utility function
		static private function textFormatFromStyle(selector:String):TextFormat
		{
			var styleManager:IStyleManager2 = FlexGlobals.topLevelApplication.styleManager;
			var style:CSSStyleDeclaration = styleManager.getStyleDeclaration(selector);
			
			var format:TextFormat = new TextFormat();
			format.font = style.getStyle("fontFamily");
			format.size = style.getStyle("fontSize");
			format.color = style.getStyle("color");
			format.bold = style.getStyle("fontWeight") == "bold";
			
			return format;
		}
		
		static private function get subTextFormat():TextFormat
		{
			if (_subTextFormat == null)
				_subTextFormat = textFormatFromStyle(".MessageText");
			
			return _subTextFormat;
		}

		static private function get titleTextFormat():TextFormat
		{
			if (_titleTextFormat == null)
				_titleTextFormat = textFormatFromStyle(".CategoryText");
			
			return _titleTextFormat;
		}
	}
}