package com.rovicorp.skins
{
	import qnx.ui.listClasses.CellRenderer;
	import qnx.ui.listClasses.List;
	import qnx.ui.listClasses.ScrollDirection;
	
	public class CategoriesRenderer extends CellRenderer
	{
		public var category:Object;
		protected var titles:List;
		
		
		public function CategoriesRenderer()
		{
			super();
		}
		
		override public function set data(value:Object):void
		{
			category = value;
			setLabel(category.Name);
			createUI();
		}
		
		/**
		 * 	Creates the titles list to show the titles for each category
		 */
		private function createUI():void {
			titles = new List();
			titles.dataProvider = category.titles;
			titles.x = 230;
			titles.scrollDirection = ScrollDirection.HORIZONTAL;
			titles.scrollable = false;
			titles.width=794;
			titles.height=165;
			titles.setSkin(TitlesCategoryRenderer);
			addChild(titles);
		}
	}
}