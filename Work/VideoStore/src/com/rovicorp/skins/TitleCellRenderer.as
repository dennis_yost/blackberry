package com.rovicorp.skins
{
	import com.rovicorp.utilities.Boxart;
	import com.rovicorp.utilities.ImagePool;
	import com.rovicorp.utilities.ImagePoolTask;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import mx.events.PropertyChangeEvent;
	import mx.events.PropertyChangeEventKind;
	
	import qnx.events.ImageCacheEvent;
	import qnx.ui.display.Image;
	import qnx.ui.listClasses.CellRenderer;
	import qnx.ui.text.Label;
	import qnx.utils.ImageCache;
	
	public class TitleCellRenderer extends CellRenderer
	{
		private var image:Image;
		private var title:Label;
		
		private static var cache:ImageCache = new ImageCache();
		private static var textFormat:TextFormat;
		
		private var token:ImagePoolTask;
		
		public function TitleCellRenderer()
		{
			super();
		}
		
		override protected function init():void 
		{
			super.init();
			this.skin.visible = false;

			if (image == null)
			{
				cache.cacheSize = 150;
				cache.addEventListener(ImageCacheEvent.IMAGE_LOADED, imageLoaded, false, 0, true);
				cache.addEventListener(ImageCacheEvent.IMAGE_LOAD_ERROR, imageLoadError, false, 0, true);

				image = new Image();
				image.width = 78;
				image.height = 107;
				image.cache = cache;
				image.filters = [new DropShadowFilter(2,90,0x000000,.49,5,5,1.0)];
				image.addEventListener(Event.COMPLETE, onImageLoaded, false, 0, true);
				image.addEventListener(IOErrorEvent.IO_ERROR,onIOError, false, 0, true);
				
				title = new Label();
				title.format = getTextFormat();
				title.setSize(80, 20);
				title.y = image.y+image.height;
				
				this.addChild(image);
				this.addChild(title);
			}
		}
		
		override public function set data(value:Object) : void
		{
			if (token)
			{
				trace("Cancel Previous Load");
				token.cancel();
				token = null;
			}
			
			if (super.data)
				(value as EventDispatcher).removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onPropertyChange);
			
			super.data = value;
			(value as EventDispatcher).addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onPropertyChange);
			
			
			if (value.BoxartPrefix != null)
				loadImage(Boxart.URL_77 + value.BoxartPrefix + Boxart.POST_FIX_77);
			else
				image.setImage("assets/images/title_boxart_placeholder_mini.png");				
			
			title.text = data.Name ? setMaxLabel(data.Name) : "Loading...";
		}
		
		private function loadImage(url:String):void
		{
			var data:BitmapData = cache.getImage(url, true);
			
			if (data)
			{
				image.setImage(url);
//				image.setSize(78, 107);
				cache.unlock(url);
			}
			else
			{
				image.setImage("assets/images/title_boxart_placeholder_mini.png");				
			}
		}

		private function imageLoaded(event:ImageCacheEvent):void
		{
			if (event.url == Boxart.URL_77 + data.BoxartPrefix + Boxart.POST_FIX_77)
			{
//				var data:BitmapData = cache.getImage(event.url, false);
				image.setImage(event.url);
//				image.setSize(78, 107);

//				cache.unlock(event.url);
			}
		}

		private function imageLoadError(event:ImageCacheEvent):void
		{
			if (event.url == Boxart.URL_77 + data.BoxartPrefix + Boxart.POST_FIX_77)
				image.setImage("assets/images/thumbnail_not_avail.png");
		}
		
		private function imageLoadComplete(result:Object):void
		{
			if (result && result.success == true)
			{
				image.setImage(new Bitmap(result.result));
				image.setSize(78, 107);
			}
			else
			{
				image.setImage("assets/images/thumbnail_not_avail.png");
			}
		}
		
		static private function getTextFormat():TextFormat
		{
			if (textFormat == null)
			{
				textFormat = new TextFormat();
				textFormat.font = "MyriadPro";
				textFormat.size = 11;
				textFormat.align = TextFormatAlign.CENTER;
				textFormat.bold = true;
				textFormat.color = 0x191919;
			}
			
			return textFormat;
		}
		private function onPropertyChange(event:PropertyChangeEvent) : void
		{
			if (event.property == "BoxartPrefix")
				loadImage(Boxart.URL_107 + data.BoxartPrefix + Boxart.POST_FIX_107);
			else if (event.property == "Name")
				title.text = data.Name;
		}
		
		private function onImageLoaded(e:Event):void
		{
			var image:Image = (e.target as Image);
			image.setSize(78, 107);
		}
		
		private function setMaxLabel(label:String):String
		{
			if(label.length>12) {
				return label.toString().substr(0,12)+"...";
			} else {
				return label;
			}
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			image.setImage("assets/images/thumbnail_not_avail.png");
		}
	}
}