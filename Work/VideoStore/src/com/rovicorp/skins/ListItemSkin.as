package com.rovicorp.skins
{
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	import qnx.ui.skins.SkinAssets;
	import qnx.ui.skins.SkinStates;
	import qnx.ui.skins.UISkin;
	
	public class ListItemSkin extends UISkin 
	{
		/** @private **/
		protected var upSkin:Sprite;
		/** @private **/
		protected var selectedSkin:Sprite;
		/** @private **/
		protected var disabledSkin:Sprite;
		/** @private **/
		protected var downSkin:Sprite;
		/** @private **/
		protected var gridRect:Rectangle;
		
		
		public function ListItemSkin() 
		{
			super();
		}
		
		
		override protected function initializeStates():void 
		{
//			gridRect = new Rectangle(8,8,30,30);
			
			upSkin = new Sprite();
			upSkin.graphics.beginFill(0x454545);
			upSkin.graphics.drawRect(0,0,50,50);
			upSkin.graphics.endFill();
			upSkin.alpha = 0;
//			upSkin.scale9Grid = gridRect;

			var upSkinOdd:Sprite = new Sprite();
			upSkinOdd.graphics.beginFill(0x454545);
			upSkinOdd.graphics.drawRect(0,0,50,50);
			upSkinOdd.graphics.endFill();
			upSkinOdd.alpha = .1;
			
/*			
			selectedSkin = new Sprite();
			selectedSkin.graphics.beginFill(0x000000);
			selectedSkin.graphics.drawRoundRect(0,0,50,50, 10,10);
			selectedSkin.graphics.endFill();
			selectedSkin.scale9Grid = gridRect;
*/			
			setSkinState(SkinStates.UP, upSkin);
			setSkinState(SkinStates.UP_ODD, upSkinOdd);
			setSkinState(SkinStates.SELECTED, null);
			setSkinState(SkinStates.DISABLED, null);
			setSkinState(SkinStates.DOWN, null);
			
			showSkin(upSkin);
			
		} 
	}
}