package com.rovicorp.events
{
	import flash.events.EventDispatcher;
	
	/**
	 * This class is kind a help class. It is desinged to be a singleton and can be access by all class.
	 * Each class can regist itself to this class and add listener to all event on this class. Any class
	 * who want to dispatch event which need to be cached by other class should use this class as the
	 * dispatcher. All cross-class events are defined in eventCenter.events.
	 * 
	 * @author ZunlinZhang
	 */
	public class EventCenter extends EventDispatcher
	{
		private static var _inst:EventCenter;
		
		public function EventCenter(val:EventCenterConstHelper){}
		
		public static function get inst():EventCenter
		{
			if (_inst == null)
			{
				_inst = new EventCenter(new EventCenterConstHelper);
			}
			return _inst;
		}
		
	}
	
}

class EventCenterConstHelper{}