package com.rovicorp.events
{	
	import flash.events.Event;
	
	public class DownloadEvent extends Event
	{
		
		public static const ADD_TASK:String = "add_task";
		
		public static const ADD_TASK_NONE:String = "add_task_done";
		
		public static const GET_DOWNLOAD_DETAIL:String = "get_download_detail";
		
		public static const DOWNLOAD_ERROR:String = "DOWNLOAD_ERROR";
		
		public static const WATCH:String = "watch";
		
		//public static const GET_DOWNLOAD_DETAIL_DONE:String = "get_download_detail_done";
		
		public static const REMOVE_TASK:String = "remove_task";
		
		public static const PAUSE_TASK:String = "pause_task";
		
		public static const RESUME_TASK:String = "resume_task";
		
		public static const DOWNLOAD_TASK:String = "download_task";
		
		public static const RETRY_TASK:String = "retry_task";
		
		public static const TASK_PROGRESS:String = "task_progress";
		
		public static const TASK_STATUS_CHANGED:String = "task_status_changed";	
		
		public static const TASK_INFO_UPDATED:String = "task_info_updated";
		
		
		public function DownloadEvent( type:String )
		{
			super(type, false, false);
		}
		
		//-----------------------------------------------------
		//-----------------------------------------------------
		//ONLY For add_task event;
		public var title_id:int;
		
		public var pass_id:int;
		
		public var dq_id:int;
		
		public var remoteURL:String;
		
		public var savePath:String;
		
		//Tifle information
		public var itemInfo:Object;
		
		//-----------------------------------------------------
		//-----------------------------------------------------
		
		//FOR TASK_PROGRESS and TASK_STATUS_CHANGED events
		
		public var status:int;
		
		// 
		public var id:String;
		
		
		//
		public var success:Boolean = false;
		
		public var act:String = "";

	}
}