package com.rovicorp.events
{
	import flash.events.Event;
	
	public class SelectionEvent extends Event
	{
		public static const TITLE_SELECTION_EVENT:String = "onTitleSelection";
		public static const CATEGORY_SELECTION_EVENT:String = "onCategorySelection";
		
		public var item:Object;
		public var index:int;

		public function SelectionEvent(type:String, item:Object, index:int, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			this.item = item;
			this.index = index;
		}

		public override function toString():String
		{
			return formatToString("SelectionEvent", "item", "index", "type", "bubbles", "cancelable");
		}
	}
}