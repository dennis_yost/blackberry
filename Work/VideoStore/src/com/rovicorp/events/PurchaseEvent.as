package com.rovicorp.events
{
	import flash.events.Event;
	
	public class PurchaseEvent extends Event
	{
		
		public var item:Object;
		public var product:Object;
		public static const PURCHASE_TITLE:String = "purchaseTitle"
		
		public function PurchaseEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function toString():String
		{
			return formatToString("PurchaseEvent", "view", "type", "bubbles", "cancelable");
		}
	}
}