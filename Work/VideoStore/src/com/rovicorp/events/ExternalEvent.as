package com.rovicorp.events
{
	
	import flash.events.Event;
	public class ExternalEvent extends Event {
		
		public static const DEFAULT_NAME:String = "com.rovicorp.events.ExternalEvent";
		public static const LOAD:String = "onLoad";
		public static const FAULT:String = "onFault";
		
		public var params:Object;
		
		public function ExternalEvent($type:String, $params:Object, $bubbles:Boolean = false, $cancelable:Boolean = false)
		{
			super($type, $bubbles, $cancelable);
			this.params = $params;
		}
		
		public override function clone():Event
		{
			return new ExternalEvent(type, this.params, bubbles, cancelable);
		}
		public override function toString():String
		{
			return formatToString("ExternalEvent", "params", "type", "bubbles", "cancelable");
		}
	}
}