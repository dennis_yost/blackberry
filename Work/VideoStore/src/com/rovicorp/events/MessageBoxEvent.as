package com.rovicorp.events
{
	import flash.events.Event;
	
	public class MessageBoxEvent extends Event
	{
		
		public static const MESSAGE_CLOSE:String = "messageClose";
		public static const MESSAGE_OPEN:String = "messageOpen";
		
		public var payload:Object = new Object();
		
		public function MessageBoxEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			// default values
			payload.width = 400;
			payload.height = 300;
			payload.title = 'Message';
		}
		
		public override function toString():String
		{
			return formatToString("MessageEvent", "view", "type", "bubbles", "cancelable");
		}
	}
}