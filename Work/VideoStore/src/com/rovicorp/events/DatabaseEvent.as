package com.rovicorp.events
{
	import com.sonic.response.Library.FullTitlePurchased;
	import com.sonic.response.TitleData.FullTitle;
	
	import flash.events.Event;

	public class DatabaseEvent extends Event
	{
		public static const DATABASE_READY:String = "DATABASE_READY";
		public static const DATABASE_OPEN_FAILED:String = "DATABASE_OPEN_FAILED";

		public static const UPDATE_TITLE:String = "UPDATE_TITLE";
		public static const UPDATE_TITLE_DONE:String = "UPDATE_TITLE_DONE";
		
		public static const UPDATE_PURCHASED_TITLE:String = "UPDATE_PURCHASED_TITLE";
		public static const UPDATE_PURCHASED_TITLE_DONE:String = "UPDATE_PURCHASED_TITLE_DONE";

		public static const UPDATE_LIBRARY_INFO:String = "UPDATE_LIBRARY_INFO";
		public static const UPDATE_LIBRARY_INFO_DONE:String = "UPDATE_LIBRARY_INFO_DONE";
		
		public static const UPDATE_FILE_CACHE:String = "UPDATE_FILE_CACHE";
		public static const UPDATE_FILE_CACHE_DONE:String = "UPDATE_FILE_CACHE_DONE";
		
		public static const REMOVE_FILE_CACHE:String = "REMOVE_FILE_CACHE";
		public static const REMOVE_FILE_CACHE_DONE:String = "REMOVE_FILE_CACHE_DONE";
		
		public static const GET_FILE_CACHE:String = "GET_FILE_CACHE";
		public static const GET_FILE_CACHE_DONE:String = "GET_FILE_CACHE_DONE";
		
		public static const GET_LIBRARY_INFO:String = "GET_LIBRARY_INFO";
		public static const GET_LIBRARY_INFO_DONE:String = "GET_LIBRARY_INFO_DONE";
		
		public static const GET_TITLE_INFO:String = "GET_TITLE_INFO";
		public static const GET_TITLE_INFO_DONE:String = "GET_TITLE_INFO_DONE";
		
		public static const GET_PURCHASED_TITLE_INFO:String = "GET_PURCHASED_TITLE_INFO";
		public static const GET_PURCHASED_TITLE_INFO_DONE:String = "GET_PURCHASED_TITLE_INFO_DONE";
		
		public var success:Boolean;
		public var id:String;	// empty means get all contents when getting content
		public var result:Object;
		public var contentItem:Object;
		public var title:FullTitle;
		public var purchasedTitle:FullTitlePurchased;
		public var titleObj:Object;
		public var library:Object;
		public var fileCache:Object;
		public var updateExistingItem:Boolean = true;
		
		public function DatabaseEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

	}
}