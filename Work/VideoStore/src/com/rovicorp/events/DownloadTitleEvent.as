package com.rovicorp.events
{		
	import flash.events.Event;
	
	public class DownloadTitleEvent extends Event
	{
		
		public static const ADD_TASK:String = "add_task";
		
		public static const ADD_TASK_NONE:String = "add_task_done";
		
		public static const GET_DOWNLOAD_DETAIL:String = "get_download_detail";
		
		public static const DOWNLOAD_ERROR:String = "DOWNLOAD_ERROR";
		
		public static const WATCH:String = "watch";
		
		public static const TASK_PROGRESS:String = "task_progress";
		
		public static const TASK_STATUS_CHANGED:String = "task_status_changed";	
		
		//		
		public static const REMOVE_TASK:String = "remove_task";
		
		public static const PAUSE_TASK:String = "pause_task";
		
		public static const RESUME_TASK:String = "resume_task";
		
		public static const DOWNLOAD_TASK:String = "download_task";
		
		public static const RETRY_TASK:String = "retry_task";
		
		
		public function DownloadTitleEvent( type:String )
		{
			super(type, false, false);
		}
		
		//-----------------------------------------------------
		//-----------------------------------------------------			
			
		public var remoteURL:String;
		
		public var savePath:String;
		
		//Title information
		public var itemInfo:Object;
		
		//-----------------------------------------------------
		//-----------------------------------------------------
		
		public var status:int;
		
		// 
		public var id:String;
		
		
		public var dq_id:int;
		
		//
		public var success:Boolean = false;
		
	}
}