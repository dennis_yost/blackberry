package com.rovicorp.events
{
	import flash.events.Event;
	
	public class RoxioNowMediaPlayerEvent extends Event
	{
		public static const MEDIAPLAYER_PLAY_START:String = "MEDIAPLAYER_PLAY_START";
		public static const MEDIAPLAYER_PLAY_PAUSE:String = "MEDIAPLAYER_PLAY_PAUSE";
		public static const MEDIAPLAYER_PLAY_BUFFER:String = "MEDIAPLAYER_PLAY_BUFFER";
		public static const MEDIAPLAYER_PLAY_STOP:String = "MEDIAPLAYER_PLAY_STOP";
		
		public static const MEDIAPLAYER_PLAY_ERROR:String = "MEDIAPLAYER_PLAY_ERROR";
		
		public static const MEDIAPLAYER_PLAY_POSITION_UPDATE:String = "MEDIAPLAYER_PLAY_POSITION_UPDATE";
		
		public var curPlayPosition:uint;
		public var totalLength:uint;
		public var errorCode:int;
		public var isBuffering:Boolean = false;
		public var isEnd:Boolean;
		public var isEof:Boolean;
		
		public function RoxioNowMediaPlayerEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

	}
}