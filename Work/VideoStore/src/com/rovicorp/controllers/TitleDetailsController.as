package com.rovicorp.controllers
{
	import com.rovicorp.model.Store;
	import com.rovicorp.model.TitleListing;
	import com.rovicorp.skins.TitleDetailsRenderer;
	import com.rovicorp.skins.TitlesCategoryRenderer;
	import com.rovicorp.views.MovieDetailsView;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TransformGestureEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	import mx.events.FlexEvent;
	
	import qnx.events.QNXApplicationEvent;
	import qnx.system.QNXApplication;
	
	import spark.components.Button;
	import spark.components.List;
	import spark.components.RichEditableText;
	
		
	public class TitleDetailsController
	{
		
		private var movieDetails:MovieDetailsView;
		private var titles:Store;
		
		[Bindable]
		public var totalPages:Number;
		[Bindable]
		public var currentPage:Number=0;
		
		private var startX:Number;
		private var startY:Number;
		private var endX:Number;
		private var endY:Number;
		
		/**
		 * Constructor for the class, sets a list that will be populated
		 * with the titles to be shown with full details
		 * 
		 */
		public function TitleDetailsController()
		{
		}
		
		
		/**
		 * Creates the elements required for the view to display 
		 * elements when the view is created 
		 * @param event information of the event
		 * 
		 */	
		public function onCreationComplete(event:Event):void {
			movieDetails = event.target as MovieDetailsView;
			currentPage = movieDetails.data.index;
			movieDetails.titlesList.ensureIndexIsVisible(currentPage);
			movieDetails.titlesList.scroller.setStyle("horizontalScrollPolicy", "off");
			movieDetails.titlesList.scroller.setStyle("verticalScrollPolicy", "off");
			movieDetails.titlesList.scroller.horizontalScrollBar.setStyle("smoothScrolling", true);
			movieDetails.titlesList.addEventListener(TransformGestureEvent.GESTURE_SWIPE,handleSwipe);
			movieDetails.leftButton.addEventListener(MouseEvent.CLICK,goToPreviousPage);
			movieDetails.rightButton.addEventListener(MouseEvent.CLICK,goToNextPage);
			totalPages=movieDetails.titlesList.dataProvider.length;
		}

		
		/**
		 * Called when the user taps on the back button
		 * @param event information of the mouse event
		 * 
		 */	
		public function onBackButtonClick(e:MouseEvent):void {
			movieDetails.navigator.popView();
		}
		
		/**
		 * Called when the user taps on the back button
		 * @param event information of the mouse event
		 * 
		 */	
		
		private function handleSwipe(event:TransformGestureEvent):void
		{
			if (event.offsetX == 1 && event.phase == "end") {
				goToPreviousPage();
			} else if (event.offsetX == -1  && event.phase == "end") {
				goToNextPage();
			} else {
				return;
			}
			
		}
		
		private function goToNextPage(event:Event = null):void {
			if (currentPage == totalPages-1) {
				currentPage = 0;
				movieDetails.titlesList.ensureIndexIsVisible(currentPage);
			} else {
				currentPage++;
				movieDetails.titlesList.scroller.horizontalScrollBar.changeValueByPage(true);
			}
		}
		
		private function goToPreviousPage(event:Event = null):void {
			if (currentPage == 0) {
				currentPage = totalPages-1;
				movieDetails.titlesList.ensureIndexIsVisible(currentPage);
			} else {
				movieDetails.titlesList.scroller.horizontalScrollBar.changeValueByPage(false);
				currentPage--;
			}
		}
		
	}
}