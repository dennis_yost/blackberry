package com.rovicorp.controllers {
	import com.rovicorp.controls.CVVToolTip;
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.ChangeBillingView;
	import com.rovicorp.views.View;
	import com.sonic.StreamAPIWse;
	import com.sonic.User;
	import com.sonic.response.Auth.AuthTokenResponse;
	import com.sonic.response.StandardResponse;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.core.IVisualElement;
	import mx.events.ValidationResultEvent;
	import mx.managers.IFocusManagerComponent;
	import mx.managers.PopUpManager;
	import mx.rpc.AsyncResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	import qnx.events.PPSEvent;
	import qnx.input.IMFConnection;
	
	import spark.components.TextInput;
	import spark.layouts.VerticalLayout;
		
	
	
	public class ChangeBillingController extends ViewController {
		public var server:RoxioNowDAO = new RoxioNowDAO();
		private var _playbookVideoStore:PlaybookVideoStore;
		private var _view:ChangeBillingView;
		private var validationResult:Array;
		private var billingValidators:Array;
		private var _cvvToolTip:CVVToolTip;
		
		public function ChangeBillingController() {
			super();
		}
		
		protected override function onCreationComplete():void {
			_view = view as ChangeBillingView;
			_view.addEventListener('cvv', _onCVV);
			_view.addEventListener('submit', _onSubmit);
			_view.monthsList.addEventListener(MouseEvent.CLICK,dropdownClicked);
			_view.yearsList.addEventListener(MouseEvent.CLICK,dropdownClicked);
			_view.statesList.addEventListener(MouseEvent.CLICK,dropdownClicked);
			_view.ccNumberInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			_view.cvvInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			_view.ccNameInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			_view.billingAddress1Input.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			_view.billingAddress2Input.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			_view.cityInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			_view.zipInput.addEventListener(DeviceEvent.RETURN_PRESSED,_onSubmit);
			
			_view.billingAddress1Input.addEventListener(FocusEvent.FOCUS_IN,focusIn);
			_view.billingAddress2Input.addEventListener(FocusEvent.FOCUS_IN,focusIn);
			
			_view.billingAddress1Input.addEventListener(FocusEvent.FOCUS_OUT,resetScroller);
			_view.billingAddress2Input.addEventListener(FocusEvent.FOCUS_OUT,resetScroller);
			
			billingValidators = [ChangeBillingView(view).ccNumberValidator,ChangeBillingView(view).cvvValidator,ChangeBillingView(view).ccNameValidator,
				ChangeBillingView(view).yearsValidator,ChangeBillingView(view).monthsValidator,ChangeBillingView(view).billingAddress1Validator,
				ChangeBillingView(view).cityValidator,ChangeBillingView(view).stateValidator,ChangeBillingView(view).zipValidator];
		}
				
		private function _onSubmit($e:Event):void {
			validateAndSubmit();
			IMFConnection.imfConnection.hideInput();
			resetScroller();
		}
		
		private function dropdownClicked(event:Event):void {
			IMFConnection.imfConnection.hideInput();
			resetScroller();
		}
		
		private function _onCVV($e:Event):void {
			IMFConnection.imfConnection.hideInput();
			_cvvToolTip = PopUpManager.createPopUp(_view, CVVToolTip, true) as CVVToolTip;
			_cvvToolTip.move(750,150);
			_cvvToolTip.addEventListener(Event.CLOSE,onCVVClose);
		}
		
		private function onCVVClose(event:Event):void {
			event.target.removeEventListener(Event.CLOSE,onCVVClose);
			PopUpManager.removePopUp(event.target as IFlexDisplayObject);
		}
		
		private function setBillingInfo():void {
			if(server.isLoggedIn) {
				RoxioNowDAO.api.User.setUserBillingInfo(
					_view.ccNumberInput.text,
					_view.monthsList.selectedItem.label.toString()+(_view.yearsList.selectedItem.label.toString()).substr(2,4),
					_view.ccNameInput.text,
					_view.billingAddress1Input.text,
					_view.billingAddress2Input.text,
					_view.cityInput.text,
					_view.statesList.selectedItem.label.toString(),
					"USA",
					_view.zipInput.text,
					"",
					_onSetBillingInfoResult
				);
			}
		}
		
		private function validateAndSubmit():void {
			validationResult = Validator.validateAll(billingValidators);
			if (validationResult.length > 0) {
				var dialog:MessageDialog = new MessageDialog();
				var error:ValidationResultEvent;
				var errorMessageArray:Array = [];
				
				for each (error in validationResult) {
					errorMessageArray.push(error.message);
					dialog.height = dialog.height + 20;
				}
				
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Change Billing Validation";
				dialog.message = errorMessageArray.join("\n");
				dialog.show();
			} else {
				setBillingInfo();
			}
		}
		
		private function _onSetBillingInfoResult(response:StandardResponse):void {
			trace(' billing information,response: '+response.ResponseMessage);
			var dialog:MessageDialog = new MessageDialog();
			dialog.addEventListener(Event.SELECT, onMessageSelect, false, 0, true);
			dialog.title = "Billing Information";
			dialog.message = (response.ResponseCode == 0) ? "Your billing information has been updated" : "Billing Information Failed To Be Added. "+response.ResponseMessage;
			dialog.show();
		}
		
		private function onMessageSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onMessageSelect);
			_view.navigator.popView();
		}
		
		private function onErrorSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onErrorSelect);
		}
		
		private function setFocus(event:DeviceEvent):void {
			var nextFocusObj:IFocusManagerComponent = null;
			var focusMap:Dictionary = new Dictionary(true);
			focusMap[_view.ccNumberInput] = _view.cvvInput;
			focusMap[_view.cvvInput] = _view.ccNameInput;
			focusMap[_view.ccNameInput] = null;
			focusMap[_view.billingAddress1Input] = _view.billingAddress2Input;
			focusMap[_view.billingAddress2Input] = _view.cityInput;
			focusMap[_view.cityInput] = null;
			focusMap[_view.zipInput] = null;
			nextFocusObj = focusMap[event.target];
			if (null !== nextFocusObj) {
				_view.focusManager.setFocus(nextFocusObj);
				var idx:int = _view.vg.getElementIndex(focusMap[event.target].parent as IVisualElement);
				var lay:VerticalLayout = _view.vg.layout as VerticalLayout;
				if (lay.fractionOfElementInView(idx) < 1) {
					lay.verticalScrollPosition += lay.getScrollPositionDeltaToElement(idx).y + 30;
				}
			} else {
				FlexGlobals.topLevelApplication.setFocus();
				resetScroller();
			}
		}
		
		
		private function focusIn(event:FocusEvent):void {
			_view.scroller.height = 250; 
		}
		
		private function resetScroller(event:FocusEvent = null):void {
			if(_view.focusManager.getFocus() != _view.billingAddress1Input 
				|| _view.focusManager.getFocus() != _view.billingAddress2Input 
				|| _view.focusManager.getFocus() != _view.cityInput) {
				_view.scroller.percentHeight = 100;
			}
		}
	}
}