package com.rovicorp.controllers
{
	import com.rovicorp.controls.LegalInfoDialog;
	import com.rovicorp.events.VideoStoreEvent;

	public class SettingsController extends ViewController
	{
		public function SettingsController(){
			super();
		}

		protected override function onCreationComplete():void
		{
			// TODO: Determine policy for when to use a local controller vs. application controller
			view.addEventListener(VideoStoreEvent.AUTHORIZE_DEVICE, onAuthorizeDevice);
			view.addEventListener(VideoStoreEvent.CHANGE_BILLING_INFO, onChangeBillingInfo);
			view.addEventListener(VideoStoreEvent.CHANGE_PASSWORD, onChangePassword);
			view.addEventListener(VideoStoreEvent.VIEW_LEGAL, onViewLegal);
			view.addEventListener(VideoStoreEvent.SUPPORT, onSupport);
		}
		
		private function onAuthorizeDevice(event:VideoStoreEvent): void 
		{
			trace("authorize device requested");
		}
		
		private function onChangeBillingInfo(event:VideoStoreEvent): void 
		{
			trace("change billing requested");
		}
		
		private function onChangePassword(event:VideoStoreEvent): void 
		{
			trace("change password requested");
		}
		
		private function onViewLegal(event:VideoStoreEvent): void 
		{
			var dialog:LegalInfoDialog = new LegalInfoDialog();
			dialog.show();
		}

		public function onSupport($e:VideoStoreEvent):void {
			trace('support requested');	
		}
				
	}
}