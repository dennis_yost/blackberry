package com.rovicorp.controllers
{
	import com.rovicorp.events.MenuBarEvent;
	import com.rovicorp.views.modules.MenuBar;
	
	import flash.events.Event;

	public class MenuBarController
	{
		
		private var _menuBar:MenuBar;
		
		public function MenuBarController(menuBar:MenuBar) 
		{
			_menuBar = menuBar;
		}
		
		/**
		 * Dispatches an event when the featured menu option is clicked 
		 * @param event information of the event
		 * 
		 */		
		public function onFeaturedClicked(event:Event):void {
			_menuBar.dispatchEvent(new MenuBarEvent(MenuBarEvent.PUSH_VIEW,"Featured"));
		}
		
		/**
		 * Dispatches an event when the movies menu option is clicked 
		 * @param event information of the event
		 * 
		 */	
		public function onMoviesClicked(event:Event):void {
			_menuBar.dispatchEvent(new MenuBarEvent(MenuBarEvent.PUSH_VIEW,"Movies"));
		}
		
		/**
		 * Dispatches an event when the TVShows menu option is clicked 
		 * @param event information of the event
		 * 
		 */	
		public function onTVShowsClicked(event:Event):void {
			_menuBar.dispatchEvent(new MenuBarEvent(MenuBarEvent.PUSH_VIEW,"TVShows"));
		}
		
		/**
		 * Dispatches an event when the Library menu option is clicked 
		 * @param event information of the event
		 * 
		 */	
		public function onLibraryClicked(event:Event):void {
			_menuBar.dispatchEvent(new MenuBarEvent(MenuBarEvent.PUSH_VIEW,"Library"));
		}
		
		/**
		 * Dispatches an event when the Downloads menu option is clicked 
		 * @param event information of the event
		 * 
		 */	
		public function onDownloadsClicked(event:Event):void {
			_menuBar.dispatchEvent(new MenuBarEvent(MenuBarEvent.PUSH_VIEW,"Downloads"));
		}
		
		/**
		 * Dispatches an event when the WishList menu option is clicked 
		 * @param event information of the event
		 * 
		 */	
		public function onWishListClicked(event:Event):void {
			_menuBar.dispatchEvent(new MenuBarEvent(MenuBarEvent.PUSH_VIEW,"WishList"));
		}
	}
}