package com.rovicorp.controllers
{
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.MoviesCategoriesView;
	import com.sonic.StreamEvent;
	
	import flash.events.Event;
	
	import qnx.ui.events.DataProviderEvent;

	public class TabbedApplicationController
	{
		
		private var _playbookVideoStore:PlaybookVideoStoreTabbed;
		
		public function TabbedApplicationController()
		{
			RoxioNowDAO.api.addEventListener(StreamEvent.READY,onApplicationReady);
			RoxioNowDAO.api.addEventListener(StreamEvent.FAULT,onApplicationFault);
		}
		
		/**
		 * Traces an error thrown by the services when trying to set the device
		 * @param event information of the event
		 * 
		 */	
		public function onApplicationFault(event:StreamEvent): void {
			trace("Error: " + event.params.fault.faultString);
		}
		
		/**
		 * It sets the display objects when the application is loaded
		 * @param event information of the event
		 * 
		 */	
		public function onApplicationReady(event:StreamEvent): void {
			trace(event.toString());
			PlaybookVideoStoreTabbed.service.movies.categories.addEventListener(DataProviderEvent.UPDATE_ALL,moviesLoaded);
			_playbookVideoStore.moviesNavigatorView.firstView= MoviesCategoriesView;
			_playbookVideoStore.navigator.tabBar.enabled = true;
		}
		
		/**
		 * Creates a local variable to access application elements and
		 * initializes the menu.
		 * @param event information of the event
		 * 
		 */	
		public function onCreationComplete(event:Event): void {
			_playbookVideoStore = event.target as PlaybookVideoStoreTabbed;
			_playbookVideoStore.navigator.tabBar.enabled = false;
		}
		
		
		/**
		 * Sets the 
		 * @param event information of the event
		 * 
		 */	
		public function moviesLoaded(event:Event): void {
			_playbookVideoStore.navigator.tabBar.selectedIndex = 1;
		}
		
	}
}