package com.rovicorp.controllers
{
	
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.DownloadTitleEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.MessageBoxEvent;
	import com.rovicorp.events.TitleEvent;
	import com.rovicorp.model.DownloadList;
	import com.rovicorp.model.DownloadListing;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.model.TitleListingPurchased;
	import com.rovicorp.service.download.DownloadManager;
	import com.rovicorp.service.download.DownloadStatus;
	import com.rovicorp.service.download.DownloadTask;
	import com.rovicorp.views.DownloadsView;
	import com.rovicorp.utilities.LogUtil;
	import com.sonic.response.Library.FullTitlePurchased;
	
	import flash.events.Event;
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	import net.rim.aircommons.download.taskmanager.GlobalConstants;
	import net.rim.aircommons.download.taskmanager.Task;
	import net.rim.aircommons.download.taskmanager.utils.ProgressHandle;

	public class DownloadListController
	{
		private var view:DownloadsView;
		
		private var _server:RoxioNowDAO = new RoxioNowDAO();
		
		private var downloadList:DownloadList;
		
		private var dMgr:DownloadManager = DownloadManager.getInstance();
			
		public function DownloadListController( theView:DownloadsView )
		{		
			view = theView;
			
			downloadList = _server.downloadList;
			
			//bind downloadlist to List component
			view.data = downloadList.dataProvider;
			
			addEventListeners();
			
			EventCenter.inst.addEventListener(DatabaseEvent.GET_PURCHASED_TITLE_INFO_DONE, onLibraryListDone);
			EventCenter.inst.dispatchEvent(new DatabaseEvent(DatabaseEvent.GET_PURCHASED_TITLE_INFO));
		}
		
		//get library list from db
		private function onLibraryListDone(e:DatabaseEvent):void
		{
			EventCenter.inst.removeEventListener(DatabaseEvent.GET_PURCHASED_TITLE_INFO_DONE, onLibraryListDone);
			
			if(e.result == null) return;
			
			var datas:Array = new Array();
			
			var item:FullTitlePurchased;			
			var newItem:DownloadListing;
			
			var task:DownloadTask;
			
			var items:Array;
			if( e.result is Array)
			{
				items = e.result as Array;
			}
			else
			{
				items = [e.result];
			}
			
			for( var i:uint = 0,len:int = items.length;i<len;i++)
			{
				item = items[i] as FullTitlePurchased;
				if( item != null )
				{
					newItem = new DownloadListing();
					newItem.formatDataFromFullTitlePurchased(item);
					
					LogUtil.log("newItem:" + newItem.DownloadStatus);
						
					if( newItem.DownloadStatus != DownloadStatus.TASK_COMPLETE )
					{
						task = dMgr.getDownloadTaskById(newItem.TitleID.toString());
						
						LogUtil.log("newItem -> task :" + task);
						if( task != null )
							updateDownloadTitleInfo(task);
						
						datas.push(newItem);
					}					
				}					
			}			
			downloadList.setDataSource(datas);
		}
			
		private function setStates( state:String ):void
		{
			if (state == "loaded")
			{				
				if (_server.downloadList.length == 0) 
					view.currentState = "empty";
				else
					view.currentState = "loaded";
			}
			else
				view.currentState = "loading";
		}
		
		private function onSourceChanged(e:Event = null):void
		{				
			setStates("loaded");			
		}
		
		private function addEventListeners():void
		{
			downloadList.addEventListener(CollectionEvent.COLLECTION_CHANGE, onSourceChanged);			
			
			dMgr.addEventListener(DownloadTitleEvent.TASK_STATUS_CHANGED, onTaskHandler);
			dMgr.addEventListener(DownloadTitleEvent.TASK_PROGRESS, onTaskHandler);
			dMgr.addEventListener(DownloadTitleEvent.DOWNLOAD_ERROR, onDownloadError);			
			
			var eventCenter:EventCenter = EventCenter.inst;			
			
			eventCenter.addEventListener(DownloadTitleEvent.ADD_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.DOWNLOAD_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.REMOVE_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.PAUSE_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.RESUME_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.WATCH, onTaskHandler);
			
			//get download detail
			//eventCenter.addEventListener(DownloadTitleEvent.GET_DOWNLOAD_DETAIL, onGetDownloadDetailHandler);
			
		}
		
		/**
		 * get download task details 
		 */
		/*
		private function onGetDownloadDetailHandler(e:DownloadTitleEvent):void
		{
			if( e.title_id == 0 || e.pass_id == 0 ) 
			{
				//var evt:DownloadEvent = new DownloadEvent(DownloadEvent.GET_DOWNLOAD_DETAIL_DONE);
				//evt.itemInfo = null;
				
				//EventCenter.inst.dispatchEvent(evt);
				return;
			}			
			//TODO - get title detail from RoxioNowDAO
			//_server.getPurchasedTitle(e.pass_id, e.title_id, getDetailResult);
		}
		
		private function getDetailResult(data:FullTitlePurchased):void
		{			
			var dMgr:DownloadManager = DownloadManager.getInstance();
			var item:DownloadItem = dMgr.getDownloadItemById(data.TitleID+"_"+data.PassID);
			
			if( item != null ) item.itemInfo = data;
		}*/
		
		private function onDownloadError(e:DownloadTitleEvent):void
		{
			if( e.status == GlobalConstants.INSUFFICIENT_DISKSPACE )
			{
				var resourceMgr:IResourceManager = ResourceManager.getInstance();
				
				var messageEvent:MessageBoxEvent = new MessageBoxEvent(MessageBoxEvent.MESSAGE_OPEN);
				messageEvent.payload.title = resourceMgr.getString('LocalizedStrings', 'DOWNLOAD_ERROR_TITLE');	
				messageEvent.payload.message = resourceMgr.getString('LocalizedStrings', 'DOWNLOAD_ERROR_NO_DISKSPACE');
				EventCenter.inst.dispatchEvent(messageEvent);
			}
		}
		
		/**
		 * task event handler, start / pause / resume / remove 
		 */ 
		private function onTaskHandler(e:DownloadTitleEvent):void
		{			
			var item:DownloadTask;
			
			switch( e.type )
			{
				case DownloadTitleEvent.ADD_TASK:						
					//title_id _ pass_id					
					item = dMgr.addDownloadTask(e.id, e.remoteURL, e.savePath, e.dq_id.toString());
					trace("> add download task -- title id:"+e.id+", dqid:"+e.dq_id);	
					if( item != null )
					{
						var downloadListing:DownloadListing = new DownloadListing();
						downloadListing.formatDataFromContentitem(e.itemInfo);					
						downloadList.addItem(downloadListing);
					}
					
					break;
				case DownloadTitleEvent.DOWNLOAD_TASK:
					dMgr.startDownloadItemByID(e.id);
					break;
				case DownloadTitleEvent.REMOVE_TASK:
					dMgr.removeDownloadItemByID(e.id);
					break;
				case DownloadTitleEvent.PAUSE_TASK:
					dMgr.pauseDownloadItemByID(e.id);
					break;
				case DownloadTitleEvent.RESUME_TASK:
					dMgr.resumeDownloadItemByID(e.id);
					break;
				case DownloadTitleEvent.WATCH:
					item = dMgr.getDownloadTaskById(e.id);	
					if( item == null ) return;
					var evt:TitleEvent = new TitleEvent(TitleEvent.WATCH_TITLE, item, null); 
					EventCenter.inst.dispatchEvent(evt);
					break;
				case DownloadTitleEvent.TASK_PROGRESS:
					//RoxioNowDAO.api.Download.sendDownloadStatusUpdate();
					item = dMgr.getDownloadTaskById(e.id);		
					trace(DownloadTitleEvent.TASK_PROGRESS);
					if( item == null) return;	
					updateDownloadTitleInfo(item);
					
					break;
				case DownloadTitleEvent.TASK_STATUS_CHANGED:
					trace(DownloadTitleEvent.TASK_STATUS_CHANGED);
					item = dMgr.getDownloadTaskById(e.id);						
					if( item == null) return;
					
					var dl:DownloadListing = downloadList.getItemByID(e.id);
					var newStatus:int = convertDownloadStatus(e.status, dl.DownloadPercent);
					dl.DownloadStatus = newStatus;
					
					if( item.getCustomData() == ""  ) return;
					var dqid:int = parseInt(item.getCustomData());	
					//TODO -- get download percent from downloadLiting
					RoxioNowDAO.api.Download.sendDownloadStatusUpdate(dqid, convertDownloadStatus( e.status, dl.DownloadPercent));
					break;
			}			
		}
		
		private function updateDownloadTitleInfo( task:DownloadTask ):void
		{
			var handler:ProgressHandle = task.getProgressHandle();	
			
			trace("updateDownloadTitleInfo:"+ task);
			
			var item:DownloadListing = downloadList.getItemByID(task.getId());
			
			if( item != null )
			{								
				item.BytesDownloaded = handler.getPosition();
				item.BytesTotal = handler.getLength();
				item.TimeRemaining = handler.getEstimatedTimeRemaining();
				
				if( item.BytesTotal > 0 )
				{		
					item.DownloadPercent = item.BytesDownloaded/item.BytesTotal;
				}
				
				if( item.DownloadStatus != DownloadStatus.TASK_IN_PROGRESS )
				{
					item.DownloadStatus = convertDownloadStatus(task.getStatus(), item.DownloadPercent);
				}
			}
			
			if( item.fileURL == "") item.fileURL = task.getLocalFileURL();
		}
		
		public function convertDownloadStatus( status:int, downloadPercent:Number):int
		{
			var val:int = 0;
			switch( status )
			{
				case Task.STATUS_AVAILABLE:
					//
				case Task.STATUS_CANCELLED:
					val = DownloadStatus.TASK_PAUSED;
					break;
				case Task.STATUS_FAILED:
					val = DownloadStatus.TASK_ERROR;
					break;
				case Task.STATUS_IN_PROGRESS:
					val = DownloadStatus.TASK_IN_PROGRESS;
					if( downloadPercent > 0.1 )
					{
						val = DownloadStatus.TASK_READY_WATCH;
					}
					break;
				case Task.STATUS_INITIALIZED:
					val = DownloadStatus.TASK_INITIALIZED;
					break;
				case Task.STATUS_PAUSED:
					val = DownloadStatus.TASK_PAUSED;
					break;
				case Task.STATUS_PENDING:
					val = DownloadStatus.TASK_PENDING;
					break;
				case Task.STATUS_SUCCEEDED:
					val = DownloadStatus.TASK_COMPLETE;
					break;				
			}
			
			return val;
		}

	}
}