package com.rovicorp.controllers
{
	import com.rovicorp.model.Store;
	import com.rovicorp.model.TitleListing;
	import com.rovicorp.skins.TVEpisodeDetailsRenderer;
	import com.rovicorp.views.TVEpisodeDetailsView;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.events.FlexEvent;
	
	public class TVEpisodeDetailsController
	{
		
		private var details:TVEpisodeDetailsView;
		private var titles:Store;
		
		
		/**
		 * Constructor for the class, sets a list that will be populated
		 * with the titles to be shown with full details
		 * 
		 */
		public function TVEpisodeDetailsController()
		{
		}
		
		
		/**
		 * Creates the elements required for the view to display 
		 * elements when the view is created 
		 * @param event information of the event
		 * 
		 */	
		public function onCreationComplete(event:Event):void {
			details = event.target as TVEpisodeDetailsView;
			details.titlesList.layout.horizontalScrollPosition = details.data.index * details.titlesList.width;
		}
		
		
		/**
		 * Called when the user taps on the back button
		 * @param event information of the mouse event
		 * 
		 */	
		public function onBackButtonClick(e:MouseEvent):void {
			details.navigator.popView();
		}
	}
}