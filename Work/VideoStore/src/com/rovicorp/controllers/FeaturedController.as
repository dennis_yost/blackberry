package com.rovicorp.controllers
{
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.FeaturedView;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.View;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;
	
	import spark.components.List;
	import spark.events.IndexChangeEvent;
	
	public class FeaturedController extends ViewController
	{
		private var _server:RoxioNowDAO = new RoxioNowDAO();
		private var _featuredView:FeaturedView;
		
		public function FeaturedController()
		{
		}
		
		private function setState(state:String):void
		{
			
			if (state == "loaded")
			{
				if (_server.featured.recommended.length == 0) 
					_featuredView.currentState = "empty";
				else
				{
					_featuredView.currentState = "loaded";
					_featuredView.list1.addEventListener(IndexChangeEvent.CHANGE, onSelectionChange, false, 0, true);
					_featuredView.list2.addEventListener(IndexChangeEvent.CHANGE, onSelectionChange, false, 0, true);
					//_featuredView.extendCollapseButton.addEventListener(MouseEvent.CLICK, onExtendCollapseClick);
				}
			}
			else
				_featuredView.currentState = "loading";
		}
		
		protected override function onCreationComplete():void
		{
			_featuredView = view as FeaturedView;
			view.data = _server.featured;
			setState(_server.featured.recommended.state);
			_server.featured.recommended.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onFeaturedPropertyChange, false, 0, true);
		}
		
		private function onSelectionChange(event:IndexChangeEvent):void
		{
			view.navigator.pushView(MovieDetailsView, {titles:(event.target as List).dataProvider, index:event.newIndex});
		}
		
		private function onFeaturedPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setState(event.newValue as String);
		}
		
		private function onExtendCollapseClick(event:MouseEvent):void {
			if (_featuredView.currentState == "loaded") {
				
				
				//_featuredView.currentState = "extended";
			} else {
				
				
				//_featuredView.currentState = "loaded";
			}
		}
		
		
	}
}