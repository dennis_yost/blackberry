package com.rovicorp.controllers
{
	import com.rovicorp.controls.EULADialog;
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.controls.PurchaseDialog;
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.events.DownloadEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.MainAppEvent;
	import com.rovicorp.events.MenuBarEvent;
	import com.rovicorp.events.MessageBoxEvent;
	import com.rovicorp.events.PurchaseEvent;
	import com.rovicorp.events.RoxioNowMediaPlayerEvent;
	import com.rovicorp.events.SelectionEvent;
	import com.rovicorp.events.TitleEvent;
	import com.rovicorp.events.UIEvent;
	import com.rovicorp.events.VideoStoreEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.model.TitleListingPurchased;
	import com.rovicorp.service.download.DownloadItem;
	import com.rovicorp.utilities.StringUtil;
	import com.rovicorp.utilities.LogUtil;
	import com.rovicorp.views.AccountCreationView;
	import com.rovicorp.views.ChangeBillingView;
	import com.rovicorp.views.ChangePasswordView;
	import com.rovicorp.views.FeaturedView;
	import com.rovicorp.views.ForgotPasswordView;
	import com.rovicorp.views.LocationErrorView;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.RoxioNowMediaPlayer;
	import com.rovicorp.views.SearchView;
	import com.rovicorp.views.SettingsView;
	import com.rovicorp.views.SignInView;
	import com.rovicorp.views.View;
	import com.sonic.SearchOption;
	import com.sonic.StreamEvent;
	import com.sonic.response.Library.FullTitlePurchased;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;
	
	import mx.core.IFlexDisplayObject;
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;
	import mx.managers.PopUpManager;
	
	import qnx.events.QNXApplicationEvent;
	import qnx.system.QNXApplication;
	
	import spark.components.View;
	import spark.components.ViewNavigator;

	public class ApplicationController
	{
		private var _playbookVideoStore:PlaybookVideoStore;
		
		private var _purchaseDialog:PurchaseDialog;
		private var _mediaPlayer:RoxioNowMediaPlayer;
		
		private var _eventCenter:EventCenter = EventCenter.inst;
		private var _service:RoxioNowDAO = new RoxioNowDAO();
		private var _preState:String = "normal";
		
		private var _curPlayTitleStopPos:uint;
		private var _currentWatchItem:DownloadItem;
		private var _resumePlayback:Boolean = false;
		
		private var copyfile:File;
		private var dbPath:String;
		private var dbController:DatabaseController;
		
		private const DATABASE_FILE_NAME:String = "dbv5.db";
		private const LOCAL_FILE_PREFIX:String = "assets/";
		
		public function ApplicationController()
		{
			// check if db is existing and if not, copy the one we have
			var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX + DATABASE_FILE_NAME);
			trace(f.nativePath);
			dbPath = f.nativePath;
			if (f.exists == false)
			{
				// copy db
				copyfile = File.applicationDirectory.resolvePath("assets/db/" + DATABASE_FILE_NAME);
				copyfile.addEventListener(Event.COMPLETE, onDatabaseCopySuccess, false, 0, true);
				copyfile.addEventListener(IOErrorEvent.IO_ERROR, onDatabaseCopyFail, false, 0, true);
				copyfile.copyToAsync(f);
			}
			else
			{
				dbController = new DatabaseController(dbPath);
			}
		}
		
		public function get featuredView():Class
		{
			return RoxioNowDAO.api.DeviceSettings.CountryID == 1 ? FeaturedView : LocationErrorView;
		}
		
		private function onDatabaseCopyFail(e:IOErrorEvent):void
		{
			copyfile.removeEventListener(Event.COMPLETE, onDatabaseCopySuccess);
			copyfile.removeEventListener(IOErrorEvent.IO_ERROR, onDatabaseCopyFail);
			copyfile = null;
		}
		
		private function onDatabaseCopySuccess(e:Event):void
		{
			copyfile.removeEventListener(Event.COMPLETE, onDatabaseCopySuccess);
			copyfile.removeEventListener(IOErrorEvent.IO_ERROR, onDatabaseCopyFail);
			copyfile = null;
			
			dbController = new DatabaseController(dbPath);
		}
		
		/**
		 * Traces an error thrown by the services when trying to set the device
		 * @param event information of the event
		 * 
		 */	
		public function onApplicationFault(event:StreamEvent): void {
			trace("RoxioNowDAO.api fail" + event.params.fault.faultString);
			
			// TODO: need to figure how to re-init RoxioNowDAO
		}
		
		/**
		 * It sets the display objects when the application is loaded
		 * @param event information of the event
		 * 
		 */	
		public function onApplicationReady(event:StreamEvent): void {
			trace("RoxioNowDAO.api ready", event.toString());

			var params:Object = event.params;
			
			// validate authtoken
			if (params.AuthTokenActive == "False")
				_service.logoutUser();

			_purchaseDialog = new PurchaseDialog();

			if (params.CountryID == -1)
			{
				_playbookVideoStore.currentState = "loaded";
				_playbookVideoStore.moviesNavigatorView.firstView = LocationErrorView;
				_playbookVideoStore.tvShowsNavigatorView.firstView = LocationErrorView;
				_playbookVideoStore.wishListNavigatorView.firstView = LocationErrorView;
			}
			else
			{
				_service.featured.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onFeaturedPropertyChange, false, 0, true);
			}
			
			_service.settings.acceptedEULA = params.DisplayEula == "False";
			
			if (!_service.settings.acceptedEULA)
			{
				var dialog:EULADialog = new EULADialog();
				dialog.addEventListener(Event.SELECT, onEULASelect, false, 0, true);
				dialog.show();
			}
		}
		
		private function onEULASelect(event:Event):void
		{
			if (!_service.settings.acceptedEULA)
			{
				_service.settings.acceptedEULA = true;	
				RoxioNowDAO.api.Utilities.acceptEula(null);
			}
		}
		
		private function onFeaturedPropertyChange(e:PropertyChangeEvent):void
		{
			_service.featured.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onFeaturedPropertyChange);
			_playbookVideoStore.currentState = "loaded"; //_service.featured.state;
			
/*			_playbookVideoStore.moviesNavigatorView.firstView = MoviesCategoriesView;
			_playbookVideoStore.tvShowsNavigatorView.firstView = TVCategoriesView;
			_playbookVideoStore.libraryNavigatorView.firstView = LibraryCategoryView;
			_playbookVideoStore.downloadsNavigatorView.firstView = DownloadsView;
			_playbookVideoStore.wishListNavigatorView.firstView = WishListView; */
		}
		
		/**
		 * Creates a local variable to access application elements and
		 * initializes the menu.
		 * @param event information of the event
		 * 
		 */	
		public function onApplicationComplete(event:FlexEvent): void {
			_playbookVideoStore = event.target as PlaybookVideoStore;

			RoxioNowDAO.api.addEventListener(StreamEvent.READY,onApplicationReady);
			RoxioNowDAO.api.addEventListener(StreamEvent.FAULT,onApplicationFault);
			
			_playbookVideoStore.addEventListener(MenuBarEvent.SEARCH,showSearch);
			_playbookVideoStore.addEventListener(MenuBarEvent.SETTINGS,showSettings);
			
			_playbookVideoStore.addEventListener(TitleEvent.WISHLIST_ADD, onWishlistAdd);
			_playbookVideoStore.addEventListener(TitleEvent.WISHLIST_REMOVE, onWishlistRemove);
			_playbookVideoStore.addEventListener(TitleEvent.WATCH_TRAILER, onWatchTrailer);
			_playbookVideoStore.addEventListener(TitleEvent.DOWNLOAD_TITLE, onDownloadTitle);
			_playbookVideoStore.addEventListener(PurchaseEvent.PURCHASE_TITLE, showCheckoutModal);
			
			_playbookVideoStore.addEventListener(SelectionEvent.TITLE_SELECTION_EVENT, onTitleSelected);
			
			_playbookVideoStore.addEventListener(VideoStoreEvent.SIGN_IN, onSignIn);
			_eventCenter.addEventListener(VideoStoreEvent.SIGN_IN, onSignIn);
			
			_playbookVideoStore.addEventListener(VideoStoreEvent.SIGN_OUT, onSignOut);
			_playbookVideoStore.addEventListener(VideoStoreEvent.CREATE_ACCOUNT, onCreateAccount);
			_playbookVideoStore.addEventListener(VideoStoreEvent.VIEW_EULA, onViewEULA);
			_playbookVideoStore.addEventListener(VideoStoreEvent.CHANGE_PASSWORD, onChangePassword);
			_playbookVideoStore.addEventListener(VideoStoreEvent.FORGOT_PASSWORD, onForgotPassword);
			_playbookVideoStore.addEventListener(VideoStoreEvent.CHANGE_BILLING_INFO, onChangeBilling);
			
			_eventCenter.addEventListener(MessageBoxEvent.MESSAGE_OPEN, _onMessageOpen);
			_playbookVideoStore.addEventListener(MessageBoxEvent.MESSAGE_CLOSE, hideModal);
			
			_eventCenter.addEventListener(MainAppEvent.DO_SEARCH, onAdvancedSearch, false, 0, true);
			
			_eventCenter.addEventListener(UIEvent.SHOW_SECONDARY_MENU, onShowSecondaryMenu, false, 0, true);
			_eventCenter.addEventListener(UIEvent.HIDE_SECONDARY_MENU, onHideSecondaryMenu, false, 0, true);
			_eventCenter.addEventListener(UIEvent.SWITCH_SCREEN, onSwitchScreen, false, 0, true);
			
			_eventCenter.addEventListener(DeviceEvent.BATTERY_LEVEL_CHANGE, onBatteryLevelChange, false, 0, true);
			_eventCenter.addEventListener(DeviceEvent.NETWORK_AVAILABLE, onNetworkBacktoAvailable, false, 0, true);
			
			_eventCenter.addEventListener(TitleEvent.WATCH_TITLE, onWatchTitle, false, 0, true);
			
			if (NetworkInfo.isSupported == true)
			{
				NetworkInfo.networkInfo.addEventListener(Event.NETWORK_CHANGE, onNetworkChanged);
			}
			
			// low memory
			//QNXApplication.qnxApplication.addEventListener(QNXApplicationEvent.LOW_MEMORY, onDeviceLowMemory, false, 0, true);
			
			// battery
			//Device.device.batteryMonitoringEnabled = true;
			//this.addEventListener(DeviceBatteryEvent.LEVEL_CHANGE, onDeviceBatteryLevelChange, false, 0, true);
			
			// swipe down, Comment this line for desktop debugging
			CONFIG::device { QNXApplication.qnxApplication.addEventListener(QNXApplicationEvent.SWIPE_DOWN, onSwipeDown, false, 0, true); }
			
			LogUtil.init(_playbookVideoStore);
		}
		
		public function navigatorActiveHandler(event:Event):void {
			var view:ViewNavigator = event.target as ViewNavigator;
			if(view.length > 1){
				view.popAll(null);
			}
		}
		
		public function onWatchTrailer(e:TitleEvent): void 
		{
			var url:String = "http://stgapi.cinemanow.com/ces/3G%20harrypotter%206_H264_514k_3G.mp4";
			var name:String = "";
			if (e.item && e.item.Details)
			{
				// TODO: Need to know how to get the trail url
				//url = e.item.Details.;
				name = e.item.Name;
			}
			launchMediaPlayer(url, name);
		}
		
		private function onWatchTitle(e:TitleEvent):void
		{
			var item:DownloadItem = e.item as DownloadItem;
			if( item == null ) return;
			
			_currentWatchItem = item;
			
			_resumePlayback = true;
			if (e.params && e.params.resumePlayback && e.params.resumePlayback == false) _resumePlayback = false;
			
			// get title info from library table in db
			var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.GET_LIBRARY_INFO);
			e0.id = _currentWatchItem.itemInfo.PassID.toString();
			
			_eventCenter.addEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, onGetLibraryInfoForPlayingTitle, false, 0, true);
			_eventCenter.dispatchEvent(e0);
		}
		
		private function onGetLibraryInfoForPlayingTitle(e:DatabaseEvent):void
		{
			_eventCenter.removeEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, onGetLibraryInfoForPlayingTitle);
			
			var so:uint = 0;
			if (e.success == true)
			{
				var diid:Object = e.result[0];
				
				// check if we need resume playback from last position
				if (_resumePlayback == true)
				{
					so = parseInt(diid.last_play_position, 10);
				}
				
				// check if we need update first play time
				if (diid.first_play == "")
				{
					var d:Date = new Date;
					diid.first_play = d.toString();
					trace("Update title play time to :", diid.first_play);
					var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO);
					e0.id = diid._id.toString();
					e0.library = diid;
					_eventCenter.dispatchEvent(e0);
				}
			}

			launchMediaPlayer(_currentWatchItem.getLocalFileURL(),
								_currentWatchItem.itemInfo.Name,
								_currentWatchItem.downloadPercent,
								so);
		}
		
		private function launchMediaPlayer(url:String, tName:String, dPercentage:Number = 1, startOffset:uint = 0):void
		{
			if (_mediaPlayer == null)
			{
				trace("PLAYBACK: ", url, tName, dPercentage, startOffset);
				_playbookVideoStore.currentState = "videoPlayer";
				_playbookVideoStore.skin.currentState = "videoPlayer";
				
				// keep an eye on the download percent if needed
				if (_currentWatchItem && _currentWatchItem.downloadPercent < 1)
				{
					_currentWatchItem.addEventListener(DownloadEvent.TASK_INFO_UPDATED, onWatchingTitleDownloadPercentChange, false, 0, true);
				}
				
				_mediaPlayer = new RoxioNowMediaPlayer;
				_playbookVideoStore.addElement(_mediaPlayer);
				_mediaPlayer.percentWidth = 100;
				_mediaPlayer.percentHeight = 100;
				
				_mediaPlayer.videoUrl = url;
				_mediaPlayer.titleName = StringUtil.convertHtmlToPlainText(tName);
				_mediaPlayer.downloadPercentage = dPercentage;
				_mediaPlayer.startOffset = startOffset;
				
				_eventCenter.addEventListener(RoxioNowMediaPlayerEvent.MEDIAPLAYER_PLAY_STOP, onMediaPlayerStop, false, 0, true);
			}
		}
		
		private function onWatchingTitleDownloadPercentChange(e:DownloadEvent):void
		{
			//trace("DOWNLOAD UPDATE: ", _currentWatchItem.downloadPercent);
			if (_mediaPlayer != null) _mediaPlayer.downloadPercentage = _currentWatchItem.downloadPercent;
		}
		
		private function onMediaPlayerStop(e:RoxioNowMediaPlayerEvent):void
		{
			// save current position
			if (_currentWatchItem != null)
			{
				_curPlayTitleStopPos = e.curPlayPosition;
				
				if (_curPlayTitleStopPos && _curPlayTitleStopPos > 0)
				{
					var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.GET_LIBRARY_INFO);
					e0.id = _currentWatchItem.itemInfo.PassID.toString();
	
					_eventCenter.addEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, onGetLibraryInfoForSavingPlabackPosition, false, 0, true);
					_eventCenter.dispatchEvent(e0);
				}
				
				_currentWatchItem.removeEventListener(DownloadEvent.TASK_INFO_UPDATED, onWatchingTitleDownloadPercentChange);
				_currentWatchItem = null;
			}
			
			disposeMediaPlayer();
		}
		
		private function onGetLibraryInfoForSavingPlabackPosition(e:DatabaseEvent):void
		{
			_eventCenter.removeEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, onGetLibraryInfoForSavingPlabackPosition);
			
			if (e.success == true)
			{
				var diid:Object = e.result[0];
				diid.last_play_position = _curPlayTitleStopPos.toString();
				
				var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO);
				e0.id = diid._id.toString();
				e0.library = diid;
				_eventCenter.dispatchEvent(e0);
			}
		}
		
		private function disposeMediaPlayer():void
		{
			if (_mediaPlayer != null)
			{
				_mediaPlayer.dispose();
				_playbookVideoStore.removeElement(_mediaPlayer);
				_mediaPlayer = null;
				
				_eventCenter.removeEventListener(RoxioNowMediaPlayerEvent.MEDIAPLAYER_PLAY_STOP, onMediaPlayerStop);
				
				_playbookVideoStore.currentState = "loaded";
				_playbookVideoStore.skin.currentState = "normal";
			}
		}
		
		private function onShowSecondaryMenu(e:UIEvent):void
		{
			_preState = _playbookVideoStore.mainTabs.skin.currentState;
			_playbookVideoStore.mainTabs.skin.currentState = "secondaryMenu";
		}
		
		private function onHideSecondaryMenu(e:UIEvent):void
		{
			_playbookVideoStore.mainTabs.skin.currentState = _preState;
		}
		
		private function onSwitchScreen(e:UIEvent):void {
			_playbookVideoStore.mainTabs.selectedIndex = 4; // e.targetScreen;
		}
		private function onBatteryLevelChange(e:DeviceEvent):void
		{
			/*
			if(e.batteryState == DeviceBatteryState.UNPLUGGED || e.batteryState == DeviceBatteryState.UNKNOWN)
			{
				if (e.batteryLevel < 2)
				{
					// TODO:
					// if video is playing it will stop the video, save the timecode for resuming and
					// display a critical battery indictor message (PRD 10.9)
				}
				else if (e.batteryLevel < 10)
				{
					// TODO:
					// When playing back videos if the battery reaches < 10% a low battery indicator
					// message is displayed (PRD 10.9)
				}
				_model.curBatteryLevel = e.batteryLevel;
			}
			*/
		}
		
		/*
		private function onDeviceLowMemory(e:QNXApplicationEvent):void
		{
			var e1:DeviceEvent = new DeviceEvent(DeviceEvent.LOW_MEMORY);
			eventCenter.dispatchEvent(e1);
		}
		*/
		private function onSwipeDown(e:QNXApplicationEvent):void
		{
			var e1:UIEvent = new UIEvent(UIEvent.SHOW_SECONDARY_MENU);
			_eventCenter.dispatchEvent(e1);
		}
		
		
		private function onNetworkChanged(e:Event):void
		{
			var nis:Vector.<NetworkInterface> = NetworkInfo.networkInfo.findInterfaces(); 
			var e1:DeviceEvent = new DeviceEvent(DeviceEvent.NETWORK_CHANGE);
			e1.networks = nis;
			_eventCenter.dispatchEvent(e1);
			
			var e2:DeviceEvent;
			var networkavailable:Boolean = false;
			for each (var n:NetworkInterface in nis)
			{
				if (n.active == true)
				{
					networkavailable = true;
					e2 = new DeviceEvent(DeviceEvent.NETWORK_AVAILABLE);
					break;
				}
			}
			
			if (e2 == null) e2 = new DeviceEvent(DeviceEvent.NETWORK_UNAVAILABLE);
			_eventCenter.dispatchEvent(e2);
		}
		
		private function onNetworkBacktoAvailable(e:DeviceEvent):void
		{
/*			if (_model.isStartUpProcessDone == false)
			{
				startStartUpProcess();
			}
*/
		}
		
		public function onWishlistAdd(event:TitleEvent): void 
		{
			if (_service.isLoggedIn)
				_service.addItemToWishList(event.item.TitleID);
			else
				ViewNavigator(_playbookVideoStore.mainTabs.selectedNavigator).pushView(SignInView, event.clone());
		}
		
		public function onWishlistRemove(event:TitleEvent): void 
		{
			_service.removeItemFromWishList(event.item.TitleID);
		}

		public function onDownloadTitle(event:TitleEvent): void 
		{
			_service.queueDownload(int(event.item));
		}
		
		private function onSignIn(event:VideoStoreEvent): void 
		{
			if (event.params)
				_service.loginUser(event.params.username, event.params.password, event.callback);
			else
				ViewNavigator(_playbookVideoStore.mainTabs.selectedNavigator).pushView(SignInView);
		}

		private function onSignOut(event:VideoStoreEvent): void 
		{
			_service.logoutUser();
		}

		private function onCreateAccount(event:VideoStoreEvent): void 
		{
			ViewNavigator(_playbookVideoStore.mainTabs.selectedNavigator).pushView(AccountCreationView);
		}

		private function onViewEULA(event:VideoStoreEvent): void 
		{
			var dialog:EULADialog = new EULADialog();
			dialog.currentState = "readonly";
			dialog.show();
		}
		
		private function onChangePassword(event:VideoStoreEvent): void 
		{
			_playbookVideoStore.miscellaneousNavigatorView.pushView(ChangePasswordView);
		}
		
		private function onForgotPassword(event:VideoStoreEvent): void 
		{
			_playbookVideoStore.miscellaneousNavigatorView.pushView(ForgotPasswordView);
		}
		
		private function onChangeBilling(event:VideoStoreEvent): void 
		{
			_playbookVideoStore.miscellaneousNavigatorView.pushView(ChangeBillingView);
		}
		
		public function onTitleSelected(event:SelectionEvent): void 
		{
			ViewNavigator(_playbookVideoStore.mainTabs.selectedNavigator).pushView(MovieDetailsView, {titles:event.item, index:event.index});
		}
		
		private function _onMessageOpen($event:MessageBoxEvent): void {
			
			var dialog:MessageDialog = new MessageDialog();
			dialog.title = $event.payload.title;
			dialog.message = $event.payload.message;
			dialog.show();
		}
		
		public function showCheckoutModal($evt:PurchaseEvent): void {
			
			_purchaseDialog = PopUpManager.createPopUp(_playbookVideoStore, PurchaseDialog, true) as PurchaseDialog;
			var dataItem:Object = new Object();
			dataItem.title = $evt.item;
			dataItem.product = $evt.product;
			dataItem.state = 'loading';
			_purchaseDialog.data = dataItem;
			_purchaseDialog.addEventListener(MessageBoxEvent.MESSAGE_CLOSE, hideModal);
			
			PopUpManager.centerPopUp(_purchaseDialog);
			
		}
		
		public function hideModal($evt:MessageBoxEvent): void {
			$evt.target.removeEventListener(MessageBoxEvent.MESSAGE_CLOSE, hideModal);
			PopUpManager.removePopUp($evt.target as IFlexDisplayObject);
		}
		
		
		/**
		 * Sets the 
		 * @param event information of the event
		 * 
		 */	
		public function showSearch(event:MenuBarEvent): void {
			_playbookVideoStore.mainTabs.selectedIndex = 6;
			if(!(_playbookVideoStore.miscellaneousNavigatorView.activeView is SearchView)){
				_playbookVideoStore.miscellaneousNavigatorView.pushView(SearchView);
			}
			_service.search.query = event.info.searchString;
			
		}
		
		private function onAdvancedSearch(e:MainAppEvent):void
		{
			_service.search.option = e.searchType;
		}
		
		public function showSettings(event:MenuBarEvent): void {

			trace("show settings dispatched");
			_playbookVideoStore.mainTabs.selectedIndex = 6;
			_playbookVideoStore.miscellaneousNavigatorView.pushView(SettingsView);
		}
	}
}
