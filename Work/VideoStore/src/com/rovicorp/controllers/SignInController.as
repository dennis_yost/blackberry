package com.rovicorp.controllers
{
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.events.VideoStoreEvent;
	import com.rovicorp.views.SignInView;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.TimerEvent;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import mx.core.FlexGlobals;
	import mx.core.IVisualElement;
	import mx.managers.IFocusManagerComponent;
	
	import qnx.input.IMFConnection;
	
	import spark.components.TextInput;
	import spark.layouts.VerticalLayout;
	
	public class SignInController extends ViewController
	{
		
		private var _view:SignInView;
		
		public function SignInController()
		{
			super();
		}
		
		protected override function onCreationComplete():void
		{
			_view = view as SignInView;
			//view.addEventListener(VideoStoreEvent.CREATE_ACCOUNT, onCreateAccount, false, 0, true);
			view.addEventListener(VideoStoreEvent.FORGOT_PASSWORD, onForgotPassword, false, 0, true);
			
			_view.username.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			_view.password.addEventListener(DeviceEvent.RETURN_PRESSED,onSubmit);
			_view.username.addEventListener(FocusEvent.FOCUS_IN,focusIn);
			_view.password.addEventListener(FocusEvent.FOCUS_IN,focusIn);
			_view.username.addEventListener(FocusEvent.FOCUS_OUT,resetState);
			_view.password.addEventListener(FocusEvent.FOCUS_OUT,resetState);
		}

		private function onForgotPassword(event:VideoStoreEvent):void
		{
			
		}
		
		private function onSignInCompleted(result:Object):void
		{
			if (_view.data is Event)
				_view.dispatchEvent(_view.data as Event);
			_view.navigator.popView();
		}

		private function onSubmit(event:DeviceEvent):void
		{
			_view.dispatchEvent(new VideoStoreEvent(VideoStoreEvent.SIGN_IN, {username: _view.username.text, password: _view.password.text}, onSignInCompleted));
			FlexGlobals.topLevelApplication.setFocus();
		}
		
		private function setFocus(event:DeviceEvent):void {
			var nextFocusObj:IFocusManagerComponent = null;
			var focusMap:Dictionary = new Dictionary(true);
			focusMap[_view.username] = _view.password;
			focusMap[_view.password] = null;
			nextFocusObj = focusMap[event.target];
			if (null !== nextFocusObj) {
				_view.focusManager.setFocus(nextFocusObj);
				var idx:int = _view.vg.getElementIndex(focusMap[event.target].parent as IVisualElement);
				var lay:VerticalLayout = _view.vg.layout as VerticalLayout;
				if (lay.fractionOfElementInView(idx) < 1) {
					lay.verticalScrollPosition += lay.getScrollPositionDeltaToElement(idx).y + 30;
				}
			} else {
				FlexGlobals.topLevelApplication.setFocus();
				resetState();
			}
		}
		
		
		private function focusIn(event:FocusEvent):void {
			_view.currentState = "members";
		}
		
		private function resetState(event:FocusEvent=null):void {
			if(_view.focusManager.getFocus() != _view.username ||_view.focusManager.getFocus() != _view.password) {
				_view.currentState = "signIn";	
			}
		}
		
	}
}