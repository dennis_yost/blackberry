package com.rovicorp.controllers
{
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.WishListView;
	import com.rovicorp.views.MovieDetailsView;
	
	import mx.binding.utils.ChangeWatcher;
	import mx.events.PropertyChangeEvent;

	import spark.events.IndexChangeEvent;

	public class WishListController extends ViewController
	{
		
		private var _wishListView:WishListView;
		private var _server:RoxioNowDAO = new RoxioNowDAO();
		
		public function WishListController()
		{
			super();
		}
		
		private function setState(state:String):void
		{
			_wishListView = view as WishListView;
			
			
			if (state == "loaded")
			{
				if (!_server.isLoggedIn) {
					_wishListView.currentState = "signedout";
				} else if (_server.wishlist.length == 0) 
					_wishListView.currentState = "empty";
				else
				{
					_wishListView.currentState = "loaded";
				}
			}
			else
				_wishListView.currentState = "loading";
		}
		
		protected override function onCreationComplete():void {
			view.data = _server.wishlist;
			setState(_server.wishlist.state);

			_server.wishlist.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onWishlistPropertyChange, false, 0, true);
		}

		private function onWishlistPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setState(event.newValue as String);
		}
	}
}