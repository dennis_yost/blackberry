package com.rovicorp.controllers
{
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.model.Store;
	import com.rovicorp.skins.CategoriesRenderer;
	import com.rovicorp.skins.TitlesCategoryRenderer;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.MoviesView;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.binding.utils.ChangeWatcher;
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	
	import qnx.ui.buttons.BackButton;
	import qnx.ui.data.DataProvider;
	import qnx.ui.events.ListEvent;
	import qnx.ui.listClasses.List;
	import qnx.ui.listClasses.ListSelectionMode;
	import qnx.ui.listClasses.ScrollDirection;
	import qnx.ui.skins.buttons.BackButtonSkinBlack;
	
	import spark.components.Application;
	import spark.components.MobileApplication;

	public class MoviesController
	{
		
		private var changeWatcher:ChangeWatcher;
		public var moviesView:MoviesView;
		public var arrMonth:Array = new Array();
		private var list:List = new List();
		private var _movies:Store;
		private var _currentInfo:Object;
		
		/**
		 * Constructor for the class, sets an object that will be passed to the
		 * navigation views  
		 * 
		 */
		public function MoviesController()
		{
			_currentInfo = new Object();
		}
		
		/**
		 * Creates the elements required for the view to display 
		 * elements when the view is created 
		 * @param event information of the event
		 * 
		 */	
		public function onCreationComplete(event:Event):void {
			moviesView = event.target as MoviesView;
			list.height=515;
			list.width=1024;
			list.rowHeight=165;
			list.setSkin(CategoriesRenderer);
			list.selectionMode = ListSelectionMode.SINGLE;
			list.addEventListener(ListEvent.ITEM_CLICKED, onCategorySelected);
			_movies = PlaybookVideoStore.service.movies;
			changeWatcher = BindingUtils.bindProperty(list,"dataProvider",_movies, "categories");
			moviesView.content.addChild(list);
		}
		
		/**
		 * Manages the event when a category or a title is selected 
		 * sets an object to be passed
		 * @param event information of the list event
		 * 
		 */	
		public function onCategorySelected(event:ListEvent):void {
			_currentInfo.index = event.index;
			if(event.cell is CategoriesRenderer) {
				_currentInfo.category = (event.cell as CategoriesRenderer).category;
				listAllTitlesForCategory((event.cell as CategoriesRenderer).category);
			} else if (event.cell is TitlesCategoryRenderer) {
				_currentInfo.movies = _movies;
				//_currentInfo.catIndex = event.index;
				getTitleDetails(_currentInfo);
			}
		}
		
		/**
		 * Called when the user selects a category and
		 * passes an object to the next view to be pushed
		 * @param event information of the list event
		 * 
		 */	
		private function listAllTitlesForCategory(category:Object):void {
			list.dataProvider = category.titles;
			list.scrollDirection = ScrollDirection.HORIZONTAL;
			list.height=165;
			list.setSkin(TitlesCategoryRenderer);
		}
		
		/**
		 * Called when the user selects a title in the list and
		 * passes an object to the next view to be pushed
		 * @param event information of the list event
		 * 
		 */
		private function getTitleDetails(currentInfo:Object):void {
			moviesView.navigator.pushView(MovieDetailsView,currentInfo);
		}
		
	}
}