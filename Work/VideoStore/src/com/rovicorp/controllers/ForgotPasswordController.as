package com.rovicorp.controllers
{
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.ForgotPasswordView;
	import com.sonic.response.StandardResponse;
	
	import flash.events.*;
	
	import mx.events.ValidationResultEvent;
	
	import qnx.input.IMFConnection;
	
	public class ForgotPasswordController extends ViewController
	{
		public var server:RoxioNowDAO = new RoxioNowDAO();
		private var vResult:ValidationResultEvent;
		
		public function ForgotPasswordController()
		{
			super();
		}
		
		protected override function onCreationComplete():void
		{
			view.addEventListener('send', _onSend);
			ForgotPasswordView(view).email.addEventListener(DeviceEvent.RETURN_PRESSED,_onSend);
		}
		
		private function _onSend($e:Event):void {
			validateAndSubmit();
			IMFConnection.imfConnection.hideInput();
		}
		
		private function validateAndSubmit():void {
			// Validate the required fields. 
			vResult = ForgotPasswordView(view).emailValidator.validate();

			if (vResult.type==ValidationResultEvent.INVALID) {
				var dialog:MessageDialog = new MessageDialog();
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Forgot Password Validation";
				dialog.message = vResult.message;
				dialog.show();
			} else {
				RoxioNowDAO.api.User.emailPassword(ForgotPasswordView(view).email.text,"","",onEmailPasswordComplete);
			}
		}
		
		private function onEmailPasswordComplete(response:StandardResponse):void {
			trace('Email password sent,response: '+response.ResponseMessage);
			var dialog:MessageDialog = new MessageDialog();

			dialog.title = "Forgot Password";
			dialog.addEventListener(Event.SELECT, onMessageSelect, false, 0, true);
			
			if (response.ResponseCode == 0) {
				dialog.message = "Your password will be sent by the system";
			} else {
				dialog.message = "There was a problem sending your password. Message: "+response.ResponseMessage;
			}
			dialog.show();
		}
		
		private function onMessageSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onMessageSelect);
			view.navigator.popView();
		}
		
		private function onErrorSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onMessageSelect);
		}
	}
}