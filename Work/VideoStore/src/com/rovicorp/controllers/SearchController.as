package com.rovicorp.controllers
{
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.SearchView;
	
	import flash.events.Event;
	
	import mx.binding.utils.BindingUtils;
	import mx.binding.utils.ChangeWatcher;
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;
	
	import spark.events.IndexChangeEvent;

	public class SearchController extends ViewController
	{
		
		private var _searchView:SearchView;
		private var _changeWatcher:ChangeWatcher;
		private var _server:RoxioNowDAO = new RoxioNowDAO();
		
		public function SearchController()
		{
		}
		
		
		private function setState(state:String):void
		{
			_searchView = view as SearchView;
			
			if (state == "loaded")
			{
				if (_server.search.length == 0) 
					_searchView.currentState = "empty";
				else
				{
					_searchView.currentState = "loaded";
					_searchView.list.addEventListener(IndexChangeEvent.CHANGE, onSelectionChange, false, 0, true);
				}
			}
			else
				_searchView.currentState = "loading";
		}
		
		protected override function onCreationComplete():void
		{
			setState(_server.search.state);
			view.data = _server.search;
			_server.search.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onSearchPropertyChange, false, 0, true);
		}
		
		private function onSearchPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state") {
				setState(event.newValue as String);
			}
		}
		
		private function onSelectionChange(event:IndexChangeEvent):void
		{
			view.navigator.pushView(MovieDetailsView, {titles:view.data, index:event.newIndex});
		}
	}
}