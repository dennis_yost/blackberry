package com.rovicorp.controllers
{
	import com.rovicorp.events.SelectionEvent;
	import com.rovicorp.model.Library;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.LibraryCategoryView;
	import com.rovicorp.views.LibraryView;
	import com.rovicorp.views.View;
	
	import mx.events.PropertyChangeEvent;

	public class LibraryCategoryController extends ViewController
	{
		private var library:Library;

		public function LibraryCategoryController()
		{
		}
		
		private function setState(state:String):void
		{
			if (state == "loaded")
			{
				if (LibraryCategoryView(view).service.isLoggedIn == false)
					view.currentState = "signedout";
				else if (library.filters.length == 0) 
					view.currentState = "empty";
				else
				{
//					categoryView.data = _server.library.filters;
					view.currentState = "loaded";
//					categoryView.list.dataProvider = _server.library.filters;
//					categoryView.list.addEventListener(IndexChangeEvent.CHANGE, onSelectionChange, false, 0, true);
				}
			}
			else
				view.currentState = "loading";
		}

		protected override function onCreationComplete():void
		{
			library = LibraryCategoryView(view).service.library;
			library.filters.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onFiltersPropertyChange, false, 0, true);

			view.addEventListener(SelectionEvent.CATEGORY_SELECTION_EVENT, onSelectionChange);
			setState(library.filters.state);
		}
		
		private function onSelectionChange(event:SelectionEvent):void
		{
			view.navigator.pushView(LibraryView, { index:event.index, title:library.filters.getItemAt(event.index).id });
		}
		
		private function onFiltersPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setState(event.newValue as String);
		}
		
	}
}
