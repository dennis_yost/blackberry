package com.rovicorp.controllers
{
	import com.rovicorp.model.Library;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.MessageBoxEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.LibraryView;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.View;
	
	import mx.events.PropertyChangeEvent;
	
	import spark.events.IndexChangeEvent;
	
	public class LibraryController extends ViewController
	{
		private var library:Library;
		
		public function LibraryController()
		{
		}
		
		private function setState(state:String):void
		{
			if (state == "loaded")
			{
				if (LibraryView(view).service.isLoggedIn == false)
					view.currentState = "signedout";
				else if (library.length == 0) 
					view.currentState = "empty";
				else
					view.currentState = "loaded";
			}
			else
				view.currentState = "loading";
		}

		protected override function onCreationComplete():void
		{
			library = LibraryView(view).service.library;
			library.filter = view.data.index as int
			setState(library.state);

			library.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onLibraryPropertyChange, false, 0, true);
		}
		
		private function onLibraryPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setState(event.newValue as String);
		}
	}
}
