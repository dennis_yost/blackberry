package com.rovicorp.controllers
{
	import com.rovicorp.events.SelectionEvent;
	import com.rovicorp.model.Store;
	import com.rovicorp.model.StoreCategory;
	import com.rovicorp.skins.StoreCategoryRenderer;
	import com.rovicorp.views.BrowseView;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.StoreCategoriesView;
	
	import mx.events.ItemClickEvent;
	import mx.events.PropertyChangeEvent;
	
	import qnx.ui.events.ListEvent;
	
	import spark.events.IndexChangeEvent;

	public class StoreCategoriesController extends ViewController
	{
		
		private var _store:Store;
		private var _storeCategoriesView:StoreCategoriesView;

		public function StoreCategoriesController(store:Store)
		{
			_store = store;
		}

		public function get store():Store
		{
			return _store;
		}
		
		protected override function onCreationComplete():void
		{
			_storeCategoriesView = view as StoreCategoriesView; 
			view.data = store.categories;
			view.addEventListener(SelectionEvent.TITLE_SELECTION_EVENT, onSelectTitle, false, 0, true);
			view.addEventListener(SelectionEvent.CATEGORY_SELECTION_EVENT, onSelectCategory, false, 0, true);
			setState(store.categories.state);
			store.categories.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onFeaturedPropertyChange, false, 0, true);
		}
		
		private function setState(state:String):void
		{
			if (state == "loaded")
			{
				_storeCategoriesView.currentState = "loaded";
				_storeCategoriesView.list.setSkin(StoreCategoryRenderer);
			}
			else
				_storeCategoriesView.currentState = "loading";
		}
		
		private function onSelectCategory(event:SelectionEvent):void
		{
			var category:StoreCategory = event.item.getItemAt(event.index);

			store.category = category.ID;
			view.navigator.pushView(BrowseView,{ store:store, title:category.Name });
			
			event.stopImmediatePropagation();
		}

		private function onSelectTitle(event:SelectionEvent):void
		{
			var info:Object = { titles:event.item, index:event.index };
			view.navigator.pushView(MovieDetailsView, info);

			event.stopImmediatePropagation();
		}
		
		private function onFeaturedPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setState(event.newValue as String);
		}
	}
}