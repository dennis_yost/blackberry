package com.rovicorp.controllers
{
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.ChangePasswordView;
	import com.sonic.response.StandardResponse;
	
	import flash.display.DisplayObject;
	import flash.events.*;
	import flash.sampler.NewObjectSample;
	import flash.utils.Dictionary;
	
	import mx.core.FlexGlobals;
	import mx.events.ValidationResultEvent;
	import mx.managers.FocusManager;
	import mx.managers.IFocusManagerComponent;
	import mx.validators.Validator;
	
	import qnx.input.IMFConnection;
		
	public class ChangePasswordController extends ViewController
	{
		
		private var validationResult:Array;
		private var passwordValidators:Array;
		private var changePasswordView:ChangePasswordView;
				
		public function ChangePasswordController()
		{
			super();
		}
		
		protected override function onCreationComplete():void {
			view.addEventListener('submit', _onSubmit);
			changePasswordView = ChangePasswordView(view);
			passwordValidators = [changePasswordView.currentPasswordValidator,changePasswordView.passwordValidator];
			changePasswordView.currentPassword.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			changePasswordView.newPassword.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			changePasswordView.newPasswordConfirm.addEventListener(DeviceEvent.RETURN_PRESSED,_onSubmit);
		}
		
		private function _onSubmit($e:Event):void {
			validateAndSubmit();
			IMFConnection.imfConnection.hideInput();
		}
		
		private function validateAndSubmit():void {
			validationResult = Validator.validateAll(passwordValidators);

			if (validationResult.length > 0) {
				var dialog:MessageDialog = new MessageDialog();
				var error:ValidationResultEvent;
				var errorMessageArray:Array = [];

				for each (error in validationResult) {
					errorMessageArray.push(error.message);
					dialog.height = dialog.height + 20;
				}

				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Change Password Validation";
				dialog.message = errorMessageArray.join("\n");
				dialog.show();
			} else {
				RoxioNowDAO.api.User.setUserPassword(ChangePasswordView(view).currentPassword.text,ChangePasswordView(view).newPassword.text,onPasswordChangeComplete);
			}
		}
		
		private function onPasswordChangeComplete(response:StandardResponse):void {
			var dialog:MessageDialog = new MessageDialog();
			dialog.addEventListener(Event.SELECT, onMessageSelect, false, 0, true);
			dialog.title = "Password Change";
			dialog.message = (response.ResponseCode == 0) ? "Your password has been changed" : "There was a problem changing your password. "+response.ResponseMessage;
			dialog.show();
		}
		
		private function onMessageSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onMessageSelect);
			view.navigator.popView();
		}
		
		private function onErrorSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onErrorSelect);
		}
		
		private function setFocus(event:DeviceEvent):void {
			var nextFocusObj:IFocusManagerComponent = null;
			var focusMap:Dictionary = new Dictionary(true);
			focusMap[changePasswordView.currentPassword] = changePasswordView.newPassword;
			focusMap[changePasswordView.newPassword] = changePasswordView.newPasswordConfirm;
			focusMap[changePasswordView.newPasswordConfirm] = null;
			nextFocusObj = focusMap[event.target];
			if (null !== nextFocusObj) {
				changePasswordView.focusManager.setFocus(nextFocusObj);
			} else {
				FlexGlobals.topLevelApplication.setFocus();
			}
		}
	}
}