package com.rovicorp.controllers {
	import com.rovicorp.controls.CVVToolTip;
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.AccountCreationView;
	import com.rovicorp.views.View;
	import com.sonic.StreamAPIWse;
	import com.sonic.User;
	import com.sonic.response.Auth.AuthTokenResponse;
	import com.sonic.response.StandardResponse;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.Dictionary;
	
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.core.IVisualElement;
	import mx.events.ValidationResultEvent;
	import mx.managers.IFocusManagerComponent;
	import mx.managers.PopUpManager;
	import mx.rpc.AsyncResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.validators.Validator;
	
	import qnx.input.IMFConnection;
	
	import spark.layouts.VerticalLayout;
		
	
	
	public class AccountCreationController extends ViewController {
		public var server:RoxioNowDAO = new RoxioNowDAO();
		private var _playbookVideoStore:PlaybookVideoStore;
		private var _view:AccountCreationView;
		private var validationResult:Array;
		private var accountValidators:Array;
		private var billingValidators:Array;
		private var _cvvToolTip:CVVToolTip;
		
		public function AccountCreationController() {
			super();
		}
		
		protected override function onCreationComplete():void {
			_view = view as AccountCreationView;
			_view.addEventListener('next', _onNext);
			_view.addEventListener('back', _onBack);
			_view.addEventListener('accept', _onTermsChecked);
			_view.addEventListener('terms', _onTermsView);
			_view.addEventListener('submit', _onSubmit);
			_view.addEventListener('cvv', _onCVV);
			_view.monthsList.addEventListener(MouseEvent.CLICK,dropdownClicked);
			_view.yearsList.addEventListener(MouseEvent.CLICK,dropdownClicked);
			_view.statesList.addEventListener(MouseEvent.CLICK,dropdownClicked);
			_view.emailInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			_view.userNameInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			_view.passwordInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			_view.passwordVerifyInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
			
			_view.passwordInput.addEventListener(FocusEvent.FOCUS_IN,focusIn);
			
			accountValidators = [_view.emailValidator,_view.userNameValidator,_view.passwordValidator,_view.passwordLenghtValidator]
			billingValidators = [_view.ccNumberValidator,_view.cvvValidator,_view.ccNameValidator,
				_view.yearsValidator,_view.monthsValidator,_view.billingAddress1Validator,
				_view.cityValidator,_view.stateValidator,_view.zipValidator];
		}
		
		private function _onNext($e:Event):void {
			validateAndContinue();
		}
		
		private function _onBack($e:Event):void {
			trace('back requested');
			_view.currentState = 'step1';
		}
		
		private function _onTermsChecked($e:Event):void {
			trace('terms accepted');	
			//_settingsView.keepSignedIn.selected = !_settingsView.keepSignedIn.selected;
			//server.settings.accept = _accountCreationView.accept.selected;
		}
		
		private function _onTermsView($e:Event):void {
			var req:URLRequest = new URLRequest("http://www.cinemanow.com/TermsBBY.aspx");
			navigateToURL(req, "_blank");
		}
		
		private function _onSubmit($e:Event):void {
			validateAndSubmit()
			_view.scroller.percentHeight = 100;
		}
		
		private function _onCVV($e:Event):void {
			IMFConnection.imfConnection.hideInput();
			_cvvToolTip = PopUpManager.createPopUp(_view, CVVToolTip, true) as CVVToolTip;
			_cvvToolTip.move(750,150);
			_cvvToolTip.addEventListener(Event.CLOSE,onCVVClose);
		}
		
		private function onCVVClose(event:Event):void {
			event.target.removeEventListener(Event.CLOSE,onCVVClose);
			PopUpManager.removePopUp(event.target as IFlexDisplayObject);
		}
		
		private function _registerAccount():void {
			if(!server.isLoggedIn) {
				server.registerUser(_view.userNameInput.text,_view.passwordInput.text," "," ","","",_view.emailInput.text,_registrationComplete);
			}
		}
		
		private function _registrationComplete($authTokenResponse:AuthTokenResponse):void {
			trace("message: "+$authTokenResponse.ResponseMessage);
			if($authTokenResponse.ResponseCode == 0) {
				_createAccount();
			} else {
				var dialog:MessageDialog = new MessageDialog();
				dialog.addEventListener(Event.SELECT, _onErrorSelect, false, 0, true);
				dialog.title = "Billing Information";
				dialog.message = "There was a problem with your registration. Message: "+$authTokenResponse.ResponseMessage;
				dialog.show();
			}
		}
		
		private function _createAccount():void {
			RoxioNowDAO.api.User.setUserBillingInfo(
				_view.ccNumberInput.text,
				_view.monthsList.selectedItem.label.toString()+(_view.yearsList.selectedItem.label.toString()).substr(2,4),
				_view.ccNameInput.text,
				_view.billingAddress1Input.text,
				_view.billingAddress2Input.text,
				_view.cityInput.text,
				_view.statesList.selectedItem.label.toString(),
				"USA",
				_view.zipInput.text,
				_view.emailInput.text,
				_onAccountCreationResult);
		}
		
		private function validateAndSubmit():void {
			validationResult = Validator.validateAll(billingValidators);
			if (validationResult.length > 0) {
				var dialog:MessageDialog = new MessageDialog();
				var error:ValidationResultEvent;
				var errorMessageArray:Array = [];
				
				for each (error in validationResult) {
					errorMessageArray.push(error.message);
					dialog.height = dialog.height + 20;
				}
				
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Create Account Validation";
				dialog.message = errorMessageArray.join("\n");
				dialog.show();
			} else {
				_registerAccount();
			}
		}
		
		private function validateAndContinue():void {
			validationResult = Validator.validateAll(accountValidators);
			if (validationResult.length > 0) {
				var dialog:MessageDialog = new MessageDialog();
				var error:ValidationResultEvent;
				var errorMessageArray:Array = [];
				
				for each (error in validationResult) {
					errorMessageArray.push(error.message);
					dialog.height = dialog.height + 20;
				}
				
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Create Account Validation";
				dialog.message = errorMessageArray.join("\n");
				dialog.show();
			} else {
				_view.currentState = 'step2';
				_view.ccNumberInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
				_view.cvvInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
				_view.ccNameInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
				_view.billingAddress1Input.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
				_view.billingAddress2Input.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
				_view.cityInput.addEventListener(DeviceEvent.RETURN_PRESSED,setFocus);
				_view.zipInput.addEventListener(DeviceEvent.RETURN_PRESSED,_onSubmit);
				
				_view.billingAddress1Input.addEventListener(FocusEvent.FOCUS_IN,focusIn);
				_view.billingAddress2Input.addEventListener(FocusEvent.FOCUS_IN,focusIn);
				
				_view.billingAddress1Input.addEventListener(FocusEvent.FOCUS_OUT,resetScroller);
				_view.billingAddress2Input.addEventListener(FocusEvent.FOCUS_OUT,resetScroller);
			}
		}
		
		private function _onAccountCreationResult($response:StandardResponse):void {
			var dialog:MessageDialog = new MessageDialog();
			dialog.addEventListener(Event.SELECT, onMessageSelect, false, 0, true);
			dialog.title = "Billing Information";
			dialog.message = ($response.ResponseCode == 0) ? "Your account has been created" : "There was a problem creating your account. Message: "+$response.ResponseMessage;
			dialog.show();
		}
		
		private function _onErrorSelect(event:Event):void {
			trace("Error on registration");
		}

		private function onMessageSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onMessageSelect);
			_view.navigator.popView();
		}
		
		private function onErrorSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onErrorSelect);
		}
		
		private function setFocus(event:DeviceEvent):void {
			var nextFocusObj:IFocusManagerComponent = null;
			var focusMap:Dictionary = new Dictionary(true);
			focusMap[_view.emailInput] = _view.userNameInput;
			focusMap[_view.userNameInput] = _view.passwordInput;
			focusMap[_view.passwordInput] = _view.passwordVerifyInput;
			focusMap[_view.passwordVerifyInput] = null;
			focusMap[_view.ccNumberInput] = _view.cvvInput;
			focusMap[_view.cvvInput] = _view.ccNameInput;
			focusMap[_view.ccNameInput] = null;
			focusMap[_view.billingAddress1Input] = _view.billingAddress2Input;
			focusMap[_view.billingAddress2Input] = _view.cityInput;
			focusMap[_view.cityInput] = null;
			focusMap[_view.zipInput] = null;
			nextFocusObj = focusMap[event.target];
			if (null !== nextFocusObj) {
				_view.focusManager.setFocus(nextFocusObj);
				var idx:int = _view.vg.getElementIndex(focusMap[event.target].parent as IVisualElement);
				var lay:VerticalLayout = _view.vg.layout as VerticalLayout;
				if (lay.fractionOfElementInView(idx) < 1) {
					lay.verticalScrollPosition += lay.getScrollPositionDeltaToElement(idx).y + 30;
				}
			} else {
				FlexGlobals.topLevelApplication.setFocus();
				resetScroller();
			}
		}
		
		private function focusIn(event:FocusEvent):void {
			_view.scroller.height = 250; 
		}
		
		private function resetScroller(event:FocusEvent = null):void {
			if(_view.focusManager.getFocus() != _view.passwordInput
				|| _view.focusManager.getFocus() != _view.passwordVerifyInput
				|| _view.focusManager.getFocus() != _view.billingAddress1Input 
				|| _view.focusManager.getFocus() != _view.billingAddress2Input 
				|| _view.focusManager.getFocus() != _view.cityInput) {
				_view.scroller.percentHeight = 100;
			}
		}
		
		private function dropdownClicked(event:Event):void {
			IMFConnection.imfConnection.hideInput();
			_view.scroller.percentHeight = 100;
		}
	}
}