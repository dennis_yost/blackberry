package com.rovicorp.service.database
{	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	import flash.utils.Dictionary;
	
	/**
	 * Manage media content database 
	 */ 
	public class DatabaseManager
	{
		// Tables
		public static const TABLE_LIBRARY:String = "Library";
		public static const TABLE_TITLE:String = "Titles";
		public static const TABLE_FILE_CACHE:String = "FileCache";
		
		private var conn:SQLConnection;
		private var opened:Boolean = false;
		private var dbPath:String = "";
		
		private var successCB:Function;
		private var faultCB:Function;
		private var updateItem:Object;
		private var targetId:String;
		
		public function DatabaseManager(dbPath:String)
		{
			this.dbPath = dbPath;
		}
		
		/**
		 * Check if the database if opened.
		 * 
		 * @return Yes or No
		 */
		public function get isDatabaseReady():Boolean
		{
			return opened;
		}
		
		/**
		 * Open the database.
		 * 
		 * @param success Callback function when open db successfully.
		 * @param fault Callback function when open db failed.
		 */
		public function openDB(success:Function = null, fault:Function = null):void
		{
			if (dbPath == "" || opened == true) return;
			
			successCB = success;
			faultCB = fault;
			
			conn = new SQLConnection();
			conn.addEventListener(SQLEvent.OPEN, onDBOpenHandler, false, 0, true);
			conn.addEventListener(SQLErrorEvent.ERROR, onDBOpenError, false, 0, true);
			conn.openAsync(new File(dbPath), SQLMode.CREATE);				
		}
		
		private function onDBOpenHandler(e:SQLEvent):void
		{
			conn.removeEventListener(SQLEvent.OPEN, onDBOpenHandler);
			conn.removeEventListener(SQLErrorEvent.ERROR, onDBOpenError);
			
			opened = true;			
			if (successCB != null) successCB();
		}
		
		private function onDBOpenError(e:SQLErrorEvent):void
		{
			conn.removeEventListener(SQLEvent.OPEN, onDBOpenHandler);
			conn.removeEventListener(SQLErrorEvent.ERROR, onDBOpenError);
			
			if (faultCB != null) faultCB();
		}
		
		/**
		 * Execute one sql query
		 * 
		 * @param sqlQuery the sql language string
		 * @param success success callback function
		 * @param fault fault callback function
		 * @param params parameters send with the sql string
		 */		 
		public function execSQL( sqlQuery:String,
								 success:Function = null,
								 fault:Function = null,
								 params:Object = null ):void
		{
			var sqlCommand:SQLQuery = new SQLQuery(sqlQuery, conn);
			sqlCommand.run(new QueryResponder(success, fault), params);
		}
		
		/**
		 * Add a new title with the given information
		 * 
		 * @param item an object with all information
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function addTitle(item:Object, success:Function = null, fault:Function = null):void 
		{
			var sql:String = "INSERT INTO " + TABLE_TITLE
				+ " (t_id, title_name, box_art_prefix, mpaa_rating, actors, buy_price, copyright, directors, producers, rating_reason, release_year, rent_price, season_title_id, show_title_id, synopsys, buy_avail, rent_avail, writers, title_type, is_thx_media_director_enabled, hd, similar_avail, in_user_wishlist, run_time, air_date, your_rating, critics_review, flixster, buy_skuid, buy_expire_date_utc, buy_promotext, buy_purchasetype, rent_skuid, rent_expire_date_utc, rent_promotext, rent_purchasetype, rent_rental_period, bonus_asset_id, g_id, master_option, slave_option )"
				+ "VALUES (:t_id, :title_name, :box_art_prefix, :mpaa_rating, :actors, :buy_price, :copyright, :directors, :producers, :rating_reason, :release_year, :rent_price, :season_title_id, :show_title_id, :synopsys, :buy_avail, :rent_avail, :writers, :title_type, :is_thx_media_director_enabled, :hd, :similar_avail, :in_user_wishlist, :run_time, :air_date, :your_rating, :critics_review, :flixster, :buy_skuid, :buy_expire_date_utc, :buy_promotext, :buy_purchasetype, :rent_skuid, :rent_expire_date_utc, :rent_promotext, :rent_purchasetype, :rent_rental_period, :bonus_asset_id, :g_id, :master_option, :slave_option)";
			this.execSQL(sql, success, fault, paramConvertor(item));
		}
		
		private function paramConvertor(o:Object):Object
		{
			var p:Object = {};
			for (var k:String in o)
			{
				p[(":" + k)] = o[k];
			}
			return p;
		}

		/**
		 * Update the title item with the given information
		 * 
		 * @param id the id of the item needs to be updated
		 * @param updateItem an object with all information
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function updateTitle(id:String,
									  updateItem:Object,
									  success:Function = null,
									  fault:Function = null):void 
		{
			var sql:String = "UPDATE " + TABLE_TITLE + " SET "
				//+ "t_id = " + updateItem.t_id + ", "
				+ "title_name = \"" + updateItem.title_name + "\", "
				+ "box_art_prefix = \"" + updateItem.box_art_prefix + "\", "
				+ "mpaa_rating = \"" + updateItem.mpaa_rating + "\", "
				+ "actors = \"" + updateItem.actors + "\", "
				+ "buy_price = " + updateItem.buy_price + ", "
				+ "copyright = \"" + updateItem.copyright + "\", "
				+ "directors = \"" + updateItem.directors + "\", "
				+ "producers = \"" + updateItem.producers + "\", "
				+ "rating_reason = \"" + updateItem.rating_reason + "\", "
				+ "release_year = " + updateItem.release_year + ", "
				+ "rent_price = " + updateItem.rent_price + ", "
				+ "season_title_id = " + updateItem.season_title_id + ", "
				+ "show_title_id = " + updateItem.show_title_id + ", "
				+ "synopsys = \"" + updateItem.synopsys + "\", "
				+ "buy_avail = \"" + updateItem.buy_avail + "\", "
				+ "rent_avail = \"" + updateItem.rent_avail + "\", "
				+ "writers = \"" + updateItem.writers + "\", "
				+ "title_type = \"" + updateItem.title_type + "\", "
				+ "is_thx_media_director_enabled = \"" + updateItem.is_thx_media_director_enabled + "\", "
				+ "hd = \"" + updateItem.hd + "\", "
				+ "similar_avail = \"" + updateItem.similar_avail + "\", "
				+ "in_user_wishlist = \"" + updateItem.in_user_wishlist + "\", "
				+ "run_time = \"" + updateItem.run_time + "\", "
				+ "air_date = \"" + updateItem.air_date + "\", "
				+ "your_rating = " + updateItem.your_rating + ", "
				+ "critics_review = \"" + updateItem.critics_review + "\", "
				+ "flixster = \"" + updateItem.flixster + "\", "
				+ "buy_skuid = " + updateItem.buy_skuid + ", "
				+ "buy_expire_date_utc = \"" + updateItem.buy_expire_date_utc + "\", "
				+ "buy_promotext = \"" + updateItem.buy_promotext + "\", "
				+ "buy_purchasetype = \"" + updateItem.buy_purchasetype + "\", "
				+ "rent_skuid = " + updateItem.rent_skuid + ", "
				+ "rent_expire_date_utc = \"" + updateItem.rent_expire_date_utc + "\", "
				+ "rent_promotext = \"" + updateItem.rent_promotext + "\", "
				+ "rent_purchasetype = \"" + updateItem.rent_purchasetype + "\", "
				+ "rent_rental_period = " + updateItem.rent_rental_period + ", "
				+ "bonus_asset_id = " + updateItem.bonus_asset_id + ", "
				+ "g_id = " + updateItem.g_id + ", "
				+ "master_option = \"" + updateItem.master_option + "\", "
				+ "slave_option = \"" + updateItem.slave_option + "\""
				+ " WHERE t_id = " + id;
			this.execSQL(sql, success, fault);
		}
		
		/**
		 * Get the title with the given information
		 * 
		 * @param id the id of the target title
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getTitle(id:String, success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_TITLE + " WHERE t_id = " + id;
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Get all titles with the given information
		 * 
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getAllTitles(success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_TITLE;
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Add a new library with the given information
		 * 
		 * @param item an object with all information
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function addLibraryItem(item:Object, success:Function = null, fault:Function = null):void
		{
			var sql:String = "INSERT INTO " + TABLE_LIBRARY
				+ " (_id, title_id, purchase_type, first_play, view_hours, expire_time, expired, has_portable, is_local, is_premium_local, is_playable, sku_id, download_order, download_status, download_url, dq_id, device, target_file_size, actual_file_size, watch_now_countdown, autodelete, download_speed, file_name, video_bitrate, audio_bitrate, license_url, license_receieved, user_name, deleted, last_play_position, date_expired, date_purchased, expiration_message, store_logo_url, store_name, stream_play_status, stream_start_time_seconds, watch_status )"
				+ " VALUES (:_id, :title_id, :purchase_type, :first_play, :view_hours, :expire_time, :expired, :has_portable, :is_local, :is_premium_local, :is_playable, :sku_id, :download_order, :download_status, :download_url, :dq_id, :device, :target_file_size, :actual_file_size, :watch_now_countdown, :autodelete, :download_speed, :file_name, :video_bitrate, :audio_bitrate, :license_url, :license_receieved, :user_name, :deleted, :last_play_position, :date_expired, :date_purchased, :expiration_message, :store_logo_url, :store_name, :stream_play_status, :stream_start_time_seconds, :watch_status)";
			this.execSQL(sql, success, fault, paramConvertor(item));
		}

		/**
		 * Get library info with the given information
		 * 
		 * @param id the target id of download info
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getLibraryItem(id:String, success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_LIBRARY + " WHERE _id = " + id;
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Get library info with the given title id
		 * 
		 * @param id the target id of download info
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getLibraryItemByTitleID(id:String, success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_LIBRARY + " WHERE title_id = " + id;
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Get all library with the given information
		 * 
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getAllLibraryItems(success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_LIBRARY;
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Update the library item with the given information
		 * 
		 * @param id the id of the item needs to be updated
		 * @param updateItem an object with all information
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function updateLibraryItem(id:String,
										   updateItem:Object,
										   success:Function = null,
										   fault:Function = null):void
		{
			var sql:String = "UPDATE " + TABLE_LIBRARY + " SET "
				//+ "_id = " + updateItem._id + ", "
				+ "title_id = " + updateItem.title_id + ", "
				+ "purchase_type = " + updateItem.purchase_type + ", "
				+ "first_play = \"" + updateItem.first_play + "\", "
				+ "view_hours = " + updateItem.view_hours + ", "
				+ "expire_time = \"" + updateItem.expire_time + "\", "
				+ "expired = " + updateItem.expired + ", "
				+ "has_portable = " + updateItem.has_portable + ", "
				+ "is_local = " + updateItem.is_local + ", "
				+ "is_premium_local = " + updateItem.is_premium_local + ", "
				+ "is_playable = " + updateItem.is_playable + ", "
				+ "sku_id = " + updateItem.sku_id + ", "
				+ "download_order = " + updateItem.download_order + ", "
				+ "download_status = " + updateItem.download_status + ", "
				+ "download_url = \"" + updateItem.download_url + "\", "
				+ "dq_id = " + updateItem.dq_id + ", "
				+ "device = " + updateItem.device + ", "
				+ "target_file_size = " + updateItem.target_file_size + ", "
				+ "actual_file_size = " + updateItem.actual_file_size + ", "
				+ "watch_now_countdown = " + updateItem.watch_now_countdown + ", "
				+ "autodelete = " + updateItem.autodelete + ", "
				+ "download_speed = " + updateItem.download_speed + ", "
				+ "file_name = \"" + updateItem.file_name + "\", "
				+ "video_bitrate = " + updateItem.video_bitrate + ", "
				+ "audio_bitrate = " + updateItem.audio_bitrate + ", "
				+ "license_url = \"" + updateItem.license_url + "\", "
				+ "license_receieved = " + updateItem.license_receieved + ", "
				+ "user_name = \"" + updateItem.user_name + "\", "
				+ "deleted = \"" + updateItem.deleted + "\", "
				+ "last_play_position = \"" + updateItem.last_play_position + "\", "
				+ "date_expired = \"" + updateItem.date_expired + "\", "
				+ "date_purchased = \"" + updateItem.date_purchased + "\", "
				+ "expiration_message = \"" + updateItem.expiration_message + "\", "
				+ "store_logo_url = \"" + updateItem.store_logo_url + "\", "
				+ "store_name = \"" + updateItem.store_name + "\", "
				+ "stream_play_status = \"" + updateItem.stream_play_status + "\", "
				+ "stream_start_time_seconds = " + updateItem.stream_start_time_seconds + ", "
				+ "watch_status = \"" + updateItem.watch_status + "\""
				+ " WHERE _id = " + id;
			this.execSQL(sql, success, fault);
		}
		
		/**
		 * Get all item with has the same value of title_id in Library and t_id in Titles
		 * 
		 * @param success success success callback
		 * @param fault fault callback
		 */
		public function getAllCrossItems(success:Function = null, fault:Function = null):void
		{
			var sql:String = "SELECT t.*, l.* FROM " + TABLE_LIBRARY + " l INNER JOIN " + TABLE_TITLE + " t on t.t_id = l.title_id";
			this.execSQL(sql, success, fault);
		}
		
		/**
		 * Get item with has the same value of title_id in Library and t_id in Titles by the given _id in Library
		 * 
		 * @param id the id of the item to be found
		 * @param success success success callback
		 * @param fault fault callback
		 */
		public function getCrossItemByID(id:String,
										 success:Function = null,
										 fault:Function = null):void
		{
			var sql:String = "SELECT t.*, l.* FROM " + TABLE_LIBRARY + " l INNER JOIN " + TABLE_TITLE + " t on t.t_id = l.title_id WHERE _id = " + id;
			this.execSQL(sql, success, fault);
		}
		
		/**
		 * Add a new file cache with the given information
		 * 
		 * @param item an object with all information
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function addFileCacheItem(item:Object, success:Function = null, fault:Function = null):void
		{
			var sql:String = "INSERT INTO " + TABLE_FILE_CACHE
				+ " (Source, FilePath, Size, UpdateTime )"
				+ " VALUES (:Source, :FilePath, :Size, :UpdateTime)";
			this.execSQL(sql, success, fault, paramConvertor(item));
		}
		
		/**
		 * Get file cache info with the given information
		 * 
		 * @param id the target id of download info
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getFileCacheItem(id:String, success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_FILE_CACHE + " WHERE Source = \"" + id + "\"";
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Remove file cache info with the given information
		 * 
		 * @param id the target id of download info
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function removeFileCacheItem(id:String, success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "DELETE FROM " + TABLE_FILE_CACHE + " WHERE Source = \"" + id + "\"";
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Get all file cache with the given information
		 * 
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getAllFileCacheItems(success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_FILE_CACHE;
			this.execSQL(searchSql, success, fault);
		}
	}
}
