package com.rovicorp.service.download
{		
	import com.rovicorp.events.DownloadTitleEvent;
	import com.rovicorp.model.DownloadList;
	import com.rovicorp.model.RoxioNowDAO;
	import com.sonic.response.Library.FullTitlePurchased;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	
	import net.rim.aircommons.download.DownloadManagerFacade;
	import net.rim.aircommons.download.taskmanager.FileTransferTask;
	import net.rim.aircommons.download.taskmanager.GlobalConstants;
	import net.rim.aircommons.download.taskmanager.Task;
	import net.rim.aircommons.download.taskmanager.TaskQueue;
	import net.rim.aircommons.download.taskmanager.ds.TaskSerializer;
	import net.rim.aircommons.download.taskmanager.events.TaskChangeEvent;
	import net.rim.aircommons.download.taskmanager.events.TaskProgressEvent;
	import net.rim.aircommons.download.taskmanager.utils.ProgressHandle;
	
 
	/**
	 * 
	 * Manage all download behavior: add new download task, listen task status and progress event...
	 * 
	 * We should create a singleton instance of this class
	 * 
	 * @author yanlin_qiu
	 * 
	 */	
	public class DownloadManager extends EventDispatcher
	{
		
		private static var instance:DownloadManager;
		
		public static function getInstance():DownloadManager
		{
			if( instance == null )
			{
				instance = new DownloadManager();
			}
			return instance;
		}
		
		//----------------------------------------------------------------------
		
		private var _mgr:DownloadManagerFacade;	
				
		public function DownloadManager()
		{				
			if( instance != null )
			{
				throw new Error("You have to access the instance via getInstance.");
			}
			
			//Add download task to availables_types
			TaskSerializer.AVAILABLE_TYPES[DownloadTask.TYPE_DOWNLOAD_TASK] = DownloadTask;			
			
			_mgr = new DownloadManagerFacade();				
			
			loadExistTasks();	
			
			startTaskAtIdle();
		}
		
		private function loadExistTasks():void
		{
			var tasks:Array = _mgr.getAllTasks();
			var task:FileTransferTask;			
			
			var items:Array = new Array();
			
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as FileTransferTask;
					
				if( task == null) continue;
	
				if( task.validateTaskComplete() == false )
				{
					// Add listener to be notified of download task state changes 
					task.addEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);			
					
					// Add listener to be notified of download progress updates
					task.addEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);					
				}
			}
		}
		
		/**
		 * start downloading if no task is started automatically
		 * 
		 * find the first "paused" one in the queue and resume it.
		 */		
		public function startTaskAtIdle():void
		{
			
			var item:Task = getRunningTask();	
			trace("startTaskAtIdle--" + item);
			if( item != null ) return;
			
			item = findNextHighestTask();
			trace("startTaskAtIdle--" + item);
			
			if( item != null ) 
			{				
				try
				{
					_mgr.resumeTask(item);
				}
				catch( e:Error)
				{
					
				}
			}
		}
		
		/**
		 * Get the running task
		 */ 
		public function getRunningTask():Task
		{
			var tasks:Array = _mgr.getAllTasks();
			var task:FileTransferTask;			
			
			var items:Array = new Array();
			
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as FileTransferTask;
				
				if( task == null) continue;
				
				if( task.getStatus() == Task.STATUS_IN_PROGRESS )
				{
					return task;
				}
			}	
			return null;
		}
		
		/**
		 * resume all download tasks 
		 */
		/*
		public function resumeAllDownload():void
		{
			var item:DownloadItem;
			
			for( var i:int = 0,len:int = downloadItems.length;i<len;i++)
			{
				item = downloadItems[i] as DownloadItem;
				if( item.task != null)
				{
					item.resume();
				}
			}	
		}*/
		
		/**
		 * pause any running task if exist 
		 */			
		private function pauseAllDownload():void
		{
			var tasks:Array = _mgr.getAllTasks();
			var task:FileTransferTask;			
						
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as FileTransferTask;
				
				if( task == null) continue;
				
				if( task.getStatus() == Task.STATUS_IN_PROGRESS )
				{
					_mgr.pauseTask(task);
				}
			}
		}		
		
		private function findNextHighestTask():Task
		{
			
			var tasks:Array = _mgr.getAllTasks();
			var task:FileTransferTask;			
			
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as FileTransferTask;
				
				if( task == null) continue;
				
				var status:int = task.getStatus();
				
				if( status != Task.STATUS_CANCELLED || status != Task.STATUS_FAILED || status != Task.STATUS_SUCCEEDED)
				{
					return task;
				}
			}
			return null;
		}
		
		/**
		 * add a new DownloadItem, and start download it now
		 * 
		 * @param contentID
		 * @param remoteURL
		 * @param saveLocation
		 * @param itemInfo		Content information
		 * @param customData	customData information
		 * @return 
		 * 
		 */		
		public function addDownloadTask( contentID:String, remoteURL:String, saveLocation:String, customData:String = "" ):DownloadTask
		{			
			var task:DownloadTask = new DownloadTask(contentID, remoteURL, saveLocation, true );
			if( customData != "" ) task.setCustomData(customData);
			
			var item:Task = this.getDownloadTaskById(contentID);			
			
			if( item != null )
			{
				trace("id already exist");
				return null;
			}			
			// Add listener to be notified of download task state changes 
			// Add listener to be notified of download progress updates
			task.addEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);			
			task.addEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
			_mgr.addTask(task);
			
			//Only downloading one file at once
			//startDownloadItem(item);			
			
			return task;
		}
		
		/**
		 *
		 * Find downloadItem by ID 
		 * 
		 * @param id
		 * @return DownloadItem
		 * 
		 */		
		public function getDownloadTaskById( id:String ):DownloadTask
		{
			
			var tasks:Array = _mgr.getAllTasks();
			var task:DownloadTask;			
			
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as DownloadTask;
				
				if( task == null) continue;
				
				if( task.getId() == id )
				{
					return task;
				}
			}		
			return null;
		}
				
		public function startDownloadItemByID(id:String):void
		{
			var item:Task = getDownloadTaskById(id);
			if( item != null )
			{
				_mgr.restartTask(item);				
			}	
		}
		
		/**
		 * 
		 * @param id
		 * 
		 */		
		public function pauseDownloadItemByID( id:String ):void
		{
			var item:Task = getDownloadTaskById(id);
			if( item != null )
			{
				_mgr.pauseTask(item);
			}
		}
		
		/**
		 * 
		 * @param id
		 * 
		 */		
		public function resumeDownloadItemByID( id:String ):void
		{
			var item:Task = getDownloadTaskById(id);
			var handler:ProgressHandle 
			
			if( item != null )
			{
				//pause running task
				pauseAllDownload();
				
				var bytesTotal:int = 0;
				var handler:ProgressHandle = item.getProgressHandle();				
				if( handler != null )
				{			
					bytesTotal = handler.getLength();
				}
				
				if( checkDiskSpace(bytesTotal + MAX_SPACE ) == false )
				{
					var e:DownloadTitleEvent = new DownloadTitleEvent(DownloadTitleEvent.DOWNLOAD_ERROR);
					e.status = GlobalConstants.INSUFFICIENT_DISKSPACE;
					dispatchEvent(e);		
					return;
				}
				
				_mgr.resumeTask(item);
			}			
		}
		
		/**
		 * remove an exsit DownloadItem and downloaded file
		 * 
		 * @param item			 DownloadItem
		 * @param removeTmpFile  whether remove download temporary file
		 */		
		public function removeDownloadItemByID( id:String, clearUp:Boolean = true ):Boolean
		{			
			var task:Task = getDownloadTaskById(id);
			
			if( task == null ) return false;
			
			_mgr.removeTask(task, clearUp);
			
			task.removeEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);		
			task.removeEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
			
			task = null;
						
			return true;
		}
		
		/**
		 * remove all download task
		 * 
		 * @param removeTmpFile Boolean
		 * 
		 */		
		public function removeAllDownloadItem(clearUp:Boolean = true):void
		{
			var tasks:Array = _mgr.getAllTasks();
			var task:Task;
			for( var i:uint = 0,len:uint = tasks.length;i<len;i++)
			{
				task = tasks[i] as Task;
				task.removeEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);				
				task.removeEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
				task = null;
			}
			
			_mgr.removeAllTasks(clearUp);
		}		

		private function taskProgressChanged(event:TaskProgressEvent):void 
		{
			var task:Task = event.task;
				
			var evt:DownloadTitleEvent = new DownloadTitleEvent(DownloadTitleEvent.TASK_PROGRESS);
			evt.id = task.getId();
			
			dispatchEvent(evt);
		}
		
		private function checkDiskSpace(requiredSize:int) : Boolean
		{
			var leftSize:Number = File.applicationDirectory.spaceAvailable;
			if (requiredSize >= leftSize)
			{
				return false;
			}
			return true;
		}
		
		private const MAX_SPACE:int = 1024 * 1024 * 20;
		
		private function taskStatusChanged(event:TaskChangeEvent):void 
		{
			
			var task:Task = event.task;
			
			var status:int = event.status;
			
			if( status == Task.STATUS_IN_PROGRESS )
			{
				var handler:ProgressHandle = task.getProgressHandle();	
				var bytesTotal:int = handler.getLength();
				if( bytesTotal > 0 )
				{
					if( checkDiskSpace( bytesTotal + MAX_SPACE ) == false )
					{
						pauseAllDownload();
						
						var e:DownloadTitleEvent = new DownloadTitleEvent(DownloadTitleEvent.DOWNLOAD_ERROR);
						e.status = GlobalConstants.INSUFFICIENT_DISKSPACE;
						dispatchEvent(e);						
						return;
					}
				}
			}			
			
			var evt:DownloadTitleEvent = new DownloadTitleEvent(DownloadTitleEvent.TASK_STATUS_CHANGED);
			evt.id = task.getId();
			evt.status = status; //event.status
			
			dispatchEvent(evt);
		
		}
		
	}
}