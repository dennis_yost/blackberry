package com.rovicorp.service.download
{
	import net.rim.aircommons.download.taskmanager.FileTransferTask;
	
	public class DownloadTask extends FileTransferTask
	{
		public static const TYPE_DOWNLOAD_TASK:int = 2;
		
		public function DownloadTask(id:String=null, remoteFileURL:String=null, localFileURL:String=null, download:Boolean=false)
		{
			super(id, remoteFileURL, localFileURL, download);			
		}
		
		override public function getTaskType() : int
		{
			return DownloadTask.TYPE_DOWNLOAD_TASK;
		}
		
		override public function get useTempFile():Boolean
		{			
			return false;
		}
		
		override public function getTmpFileURL() : String
		{
			return this.getLocalFileURL();			
		}
	}
}