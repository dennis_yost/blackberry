package com.rovicorp.service.download
{
	public class DownloadStatus
	{
		
		public static const TASK_PENDING:uint = 0;
		
		public static const TASK_INITIALIZED:uint = 1;
		
		public static const TASK_IN_PROGRESS:uint = 2;
		
		public static const TASK_READY_WATCH:uint = 3;
		
		public static const TASK_PAUSED:uint = 4;
		
		public static const TASK_RESUMED:uint = 5;
		
		public static const TASK_COMPLETE:uint = 7;
		
		public static const TASK_ERROR:uint = 8;
		
		
		/*
		0: Request Acknowledged *
		1: Download Initialized *
		2: Download Started
		3: Ready to watch
		4: Download paused
		5: Download resumed
		6: Download reset
		7: Download completed*
		*/
		
	}
}