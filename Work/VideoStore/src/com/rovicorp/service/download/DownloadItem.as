package com.rovicorp.service.download
{	
	import com.rovicorp.events.DownloadEvent;
	
	import flash.events.EventDispatcher;
	
	import net.rim.aircommons.download.taskmanager.FileTransferTask;
	import net.rim.aircommons.download.taskmanager.Task;
	import net.rim.aircommons.download.taskmanager.utils.ProgressHandle;
	
	public class DownloadItem extends EventDispatcher
	{	
	
		//downloading information		
		public var bytesTotal:int;
		
		public var bytesDownloaded:int;
		
		public var elapsedTime:int;
		
		public var estimatedTimeRemaining:int;
		
		public var downloadPercent:Number;
		
		public var status:int;		
		
		public function DownloadItem( task:FileTransferTask )
		{				
			_task = task;
			
			updateDownloadInfo();
		}
		
		private var _task:FileTransferTask;		
		public function set task( value:FileTransferTask ):void
		{
			_task = value;
		}
		
		public function get task():FileTransferTask
		{
			return _task;
		}
		
		private var _itemInfo:Object;
		
		public function set itemInfo( value:Object):void
		{
			if( _itemInfo != value )
			{
				_itemInfo = value;
				
				var evt:DownloadEvent = new DownloadEvent(DownloadEvent.TASK_INFO_UPDATED);
				evt.act = "info";
				dispatchEvent(evt);
			}
		}
		
		public function get itemInfo():Object
		{
			return _itemInfo;
		}
		
		public function get id():String
		{
			return task.getId();
		}
		
		public function getLocalFileURL():String
		{
			if( downloadPercent < 100 )
			{
				return task.getTmpFileURL();
			}
			return task.getLocalFileURL();
		}		
				
		public function updateDownloadInfo( updateStatus:Boolean = true ):void
		{
			var handler:ProgressHandle = task.getProgressHandle();
			
			if( handler != null )
			{			
				bytesTotal = handler.getLength();
				
				bytesDownloaded = handler.getPosition();
				
				estimatedTimeRemaining = handler.getEstimatedTimeRemaining();
				
				elapsedTime = handler.getElapsedTime();
				
				downloadPercent = bytesDownloaded/bytesTotal;
			}
			
			if( updateStatus ) status = _task.getStatus();
			
			var evt:DownloadEvent = new DownloadEvent(DownloadEvent.TASK_INFO_UPDATED);
			evt.act = "status";
			
			dispatchEvent(evt);
		}
		
		public function get isDownloading():Boolean
		{
			return status == Task.STATUS_IN_PROGRESS;
		}
		
		/**
		 * whether task could be resumed? 
		 */		
		public function get isAvailable():Boolean
		{
			if( status == Task.STATUS_CANCELLED || status == Task.STATUS_FAILED || status == Task.STATUS_SUCCEEDED)
			{
				return false;
			}
			
			return true;
		}
	}
}