package com.rovicorp.model
{
	import com.sonic.response.Browse.BrowseList;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;
	import mx.events.PropertyChangeEventKind;
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.events.ResultEvent;
	
	internal class StoreCollection extends AsyncCollection implements IResponder
	{
		public static const PAGE_SIZE:int = 100;
		public static const PAGE_MARK:int = 50;
		
		private var _token:AsyncToken;
		private var _currentPage:int = 1;
		private var _nextPage:int;

		public function StoreCollection()
		{
		}
		
		private function set token(value:AsyncToken):void
		{
			_token = value;
			
			if (_token)
				_token.addResponder(this);
		}
		
		private function queuePage(itemIndex:int):void 
		{
			if (itemIndex < 0)
				itemIndex += length;
			if (itemIndex >= length)
				itemIndex -= length;
			
			var item:TitleListing = source[itemIndex]
				
			if (item == null || item.TitleID == 0)
			{
				var page:int = (itemIndex / PAGE_SIZE) + 1;
				
				if (_token)
				{
					if (page != _currentPage)
						_nextPage = page;
				}
				else
				{
					_currentPage = page;
					token = loadPage(page);
				}
			}
		}
		
		protected function loadPage(pageNo:int):AsyncToken 
		{
			return null;
		}
		
		public override function invalidate():void
		{
			_token = null;
			_currentPage = 1;
			_nextPage = 0;
			
			super.invalidate();
		}
		
		protected override function setState(value:String):void
		{
			super.setState(value);

			if (value == LOADING)
				token = loadPage(_currentPage);
		}
		
		public override function getItemAt(index:int, prefetch:int=0):Object
		{
			var result:Object = super.getItemAt(index, prefetch);
			
			if (result == null)
				result = source[index] = new TitleListing();
			
			queuePage(index);
			queuePage(index - Math.min(length, PAGE_MARK));
			queuePage(index + Math.min(length, PAGE_MARK));
			
			return result;		
		}
		
		public override function setItemAt(item:Object, index:int):Object
		{
			if (index < 0 || index >= length)
				throw new RangeError("Index out of range");
			
			var result:TitleListing = super.getItemAt(index) as TitleListing;
			
			if (result != null)
			{
				var pe1:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(result, "TitleID", result.TitleID, item.TitleID);
				result.TitleID = item.TitleID;
				var pe2:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(result, "Name", result.Name, item.Name);
				result.Name = item.Name;
				var pe3:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(result, "BoxartPrefix", result.BoxartPrefix, item.BoxartPrefix);
				result.BoxartPrefix = item.BoxartPrefix;

				dispatchEvent(new CollectionEvent(CollectionEvent.COLLECTION_CHANGE, false, false, CollectionEventKind.UPDATE, index, index, [pe1, pe2, pe3]));
				
				return result;
			}
			
			return super.setItemAt(item, index);
		}
		
		/**
		 * IResponder override to create an empty list when web service calls fail
		 * 
		 * @param info The FaultEvent containing the fault information 
		 */
		public function fault(info:Object):void
		{
		}
		
		/**
		 * IResponder override for obtaining the result from asynchronous web service calls
		 * 
		 * @param data The ResultEvent containing the data returned from the web service call
		 */
		public function result(data:Object):void
		{
			if ((data as ResultEvent).token == _token)
			{
				var response:BrowseList = data.result;
				var items:Array = response.Items.source;
				
				if (state == AsyncCollection.LOADED)
				{
					var base:int = (response.PageNum - 1) * PAGE_SIZE
					for (var i:int = 0; i < items.length; ++i)
						setItemAt(items[i], base + i);
				}
				else
				{
					setSource(items, response.TotalItems);
				}
				
				_token = null;
				
				if (_nextPage > 0)
				{
					_currentPage = _nextPage;
					token = loadPage(_nextPage);
					_nextPage = 0;
				}
			}
		}		
	}
}