package com.rovicorp.model
{
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.service.download.DownloadManager;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;
	
	[Bindable]
	public class DownloadList extends AsyncCollection
	{
		
		private var _list:ArrayCollection = new ArrayCollection();
		
		private var downloadMgr:DownloadManager = DownloadManager.getInstance();	
		
		public function DownloadList():void
		{
			
		}
		
		private function sendSourceUpdateEvent(type:String = CollectionEventKind.REFRESH ):void
		{
			dispatchEvent(new CollectionEvent(CollectionEvent.COLLECTION_CHANGE, false, false, type));
		}
		
		public function setDataSource(items:Array):void
		{
			_list.source = items;
			
			setState("loaded");
			sendSourceUpdateEvent();
		}
		
		public function get dataProvider():ArrayCollection
		{
			return _list;
		}
		
		override public function get length():int
		{
			trace("get length:" + _list.length);
			return _list.length;
		}
		
		override public function addItem( item:Object ):void
		{			
			dataProvider.addItem(item);
			
			sendSourceUpdateEvent(CollectionEventKind.ADD);
		}		
		
		override public function removeItemAt( index:int ):Object
		{
			if( index > -1 && index < length )
			{
				var obj:Object = dataProvider.removeItemAt(index);
				sendSourceUpdateEvent(CollectionEventKind.REMOVE);
				return obj;
			}
			return null;
		}
		
		override public function removeAll():void
		{
			dataProvider.removeAll();
			
			sendSourceUpdateEvent(CollectionEventKind.RESET);
		}
		
		override public function getItemAt( index:int, prefetch:int=0 ):Object
		{
			if( index > -1 && index < length )
			{
				return dataProvider.getItemAt(index);				
			}
			return null;
		}
		
		public function removeItemByID(titleId:String):Object
		{
			var item:DownloadListing = getItemByID(titleId);
			if( item != null )
			{
				var index:int = getItemIndex(item);
				return removeItemAt(index);
			}
			return null;
		}		
		
		//get download listing by titleId
		public function getItemByID( titleId:String ):DownloadListing
		{
			var item:DownloadListing;
			
			for( var i:uint = 0,len:uint = length;i<len;i++)
			{
				item = dataProvider.getItemAt(i) as DownloadListing;							
				if( item && item.TitleID.toString() == titleId )
				{
					return item;
				}
			}
			return null;
		}
		
	}
}