package com.rovicorp.model
{
	import com.sonic.response.Browse.BundleList;
	import com.sonic.response.TitleData.FullTitle;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	
	public class TitleDetails extends FullTitle implements IEventDispatcher
	{
		private var _dispatcher:EventDispatcher = new EventDispatcher(this as IEventDispatcher);
		private var _bundleItems:AsyncCollection;
		
		public function TitleDetails()
		{
			super();
		}
		
		public function get BundleItems():AsyncCollection
		{
			if (_bundleItems == null && (TitleType == "TV_Season" || TitleType == "TV_Show"))
			{
				_bundleItems = new AsyncCollection();
				RoxioNowDAO.api.Browse.getBundleListing(TitleID, onGetBundleListingCompleted);
			}
			
			return _bundleItems;
		}
		
		public function set BundleItems(value:AsyncCollection):void
		{
//			_bundleItems = value;
		}

		[Bindable]
		public function get InUserWishlist():Boolean
		{
			return MetaValues.InUserWishlist == "True";
		}
		
		public function set InUserWishlist(value:Boolean):void
		{
			var oldValue:Boolean = MetaValues.InUserWishlist == "True";

			if (oldValue != value)
			{
				MetaValues.InUserWishlist = value ? "True" : "False";
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "InUserWishlist", oldValue, value));
			}
		}
		
		private function onGetBundleListingCompleted(data:BundleList):void
		{
			_bundleItems.setSource(data.BundleItems.toArray());
		}

		public function addEventListener(type:String, listener:Function, useCapture:Boolean=false, priority:int=0, useWeakReference:Boolean=false):void
		{
			_dispatcher.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean=false):void
		{
			_dispatcher.removeEventListener(type, listener, useCapture);
		}
		
		public function dispatchEvent(event:Event):Boolean
		{
			return _dispatcher.dispatchEvent(event);
		}
		
		public function hasEventListener(type:String):Boolean
		{
			return _dispatcher.hasEventListener(type);
		}
		
		public function willTrigger(type:String):Boolean
		{
			return _dispatcher.willTrigger(type);
		}
	}
}