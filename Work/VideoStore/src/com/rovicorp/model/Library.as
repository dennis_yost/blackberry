package com.rovicorp.model
{
	
	import com.sonic.response.Library.UserLibraryCounts;
	import com.sonic.response.Library.UserLibraryGenresAndSlaveWheelOptions;
	import com.sonic.response.Library.UserLibraryList;
	import com.sonic.response.NameAndValue;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.events.PropertyChangeEvent;
	
	public class Library extends AsyncCollection
	{
		public static const SORT_STANDARD:String = "standard";
		public static const SORT_ALPHABETICAL:String = "alpha";
		
		// TODO:  Should this really be client structure since readyToWatch is function of download list
		private static const _filterArray:Array = [
			new LibraryCategory("movies", "Movies", "All"),
			new LibraryCategory("tvShows", "TV_Shows", "All"),
			new LibraryCategory("readyToWatch", "Movies,TV_Shows", "All"),
			new LibraryCategory("rentals", "Movie_Rentals", "All"),
			new LibraryCategory("rentalsExpiringSoon", "Movie_Rentals", "Expiring_Soon"),
			new LibraryCategory("purchases", "Movies,TV_Shows", "All"),
			new LibraryCategory("expiredRentals", "Movie_Rentals", "Expired"),
			new LibraryCategory("unavailable", "Movies,TV_Shows", "Unavailable")
		];
		
		private var _server:RoxioNowDAO = new RoxioNowDAO();
		
		private var _genre:int;
		private var _genres:AsyncCollection;
		private var _type:String = "Movies";
		private var _filter:int = 0;
		private var _sort:String = SORT_STANDARD;
		private var _items:ArrayCollection;
//		private var _filter:String = "All";
		private var _filters:AsyncCollection;

		public function Library()
		{
			_server.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onServerPropertyChange);
			_server.addEventListener(RoxioNowDAO.TITLE_PURCHASED, onTitlePurchased);
		}
		
		public function get genre():int
		{
			return _genre;
		}
		
		public function set genre(value:int):void
		{
			if (_genre !== value)
			{
				var event:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "genre", _genre, value);
				_genre = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
			
		}

		public function get type():String
		{
			return _type;
		}
		
		public function set type(value:String):void
		{
			if (_type !== value)
			{
				var event:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "type", _type, value);
				_type = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
			
		}
		
		public function get genres():AsyncCollection
		{
			if (_genres == null)
			{
				_genres = new AsyncCollection();
				
				if (_filterArray == null)
					RoxioNowDAO.api.Library.getAvailLibSlaveWheelOptionByMasterWheel(_type, onGetAvailLibSlaveWheelOptionsCompleted);
			}
			
			return _genres;
		}

/*      *** Disable V2 functionalty for now		
		public function get filter():String
		{
			return _filter;
		}
		
		public function set filter(value:String):void
		{
			if (_filter !== value)
			{
				var event:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "filter", _filter, value);
				_filter = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
		}

		public function get filters():AsyncCollection
		{
			if (_filters == null)
			{
				_filters = new AsyncCollection();
				
				if (_genres == null)
					RoxioNowDAO.api.Library.getAvailLibSlaveWheelOptionByMasterWheel(_type, onGetAvailLibSlaveWheelOptionsCompleted);
			}
			
			return _filters;
		}
		
		**** And use hard coded values instead
*/
		public function get filter():int
		{
			return _filter;
		}
		
		public function set filter(value:int):void
		{
			if (_filter !== value)
			{
				var event:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "filter", _filter, value);
				_filter = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
		}
		
		public function get filters():AsyncCollection
		{
			if (_filters == null)
			{
				_filters = new AsyncCollection();
				loadFilters();
				
				/*			Disabled v2 functionlity
				if (_genres == null)
					RoxioNowDAO.api.Library.getAvailLibSlaveWheelOptionByMasterWheel(_type, onGetAvailLibSlaveWheelOptionsCompleted);
				*/
			}
			
			return _filters;
		}
		
		private function loadFilters():void
		{
			if (_server.isLoggedIn)
				RoxioNowDAO.api.Library.getUserLibraryCounts(onGetUserLibraryCountsCompleted);
			else
				_filters.setSource([]);		// TODO: Check downloads here or at client
		}
		
		public override function invalidate():void
		{
			super.invalidate();

			/* Disabled v2 functionality
			filters.invalidate();
			genres.invalidate();
			RoxioNowDAO.api.Library.getAvailLibSlaveWheelOptionByMasterWheel(_type, onGetAvailLibSlaveWheelOptionsCompleted);
			*/
			if (_filters)
			{
				_filters.invalidate();
				loadFilters();
			}
		}
		
		protected override function setState(value:String):void
		{
			super.setState(value);
			
			if (value == LOADING)
			{
				if (_server.isLoggedIn)
				{
					var category:LibraryCategory = _filterArray[_filter];
					var types:Array = category.type.split(",");
					
					_items = null;

					// TODO: get download list for "readyToWatch" filter

					RoxioNowDAO.api.Library.getUserLibrary(_genre.toString(), types[0], category.filter).addResponder(
						new AsyncResponder(onLibraryLoaded, 0) );
				}
				else
					setSource([]);
			}
		}
		
		private function onGetUserLibraryCountsCompleted(response:UserLibraryCounts):void
		{
			var source:Array = [];
			
			if (response.ResponseCode == 0)
			{
				for each (var item:NameAndValue in response.LibraryCounts)
				{
					if (int(item.KeyValue) > 0)
					{
						source = _filterArray;
						break;
					}
				}
			}

			filters.setSource(source);
		}

		private function onGetAvailLibSlaveWheelOptionsCompleted(response:UserLibraryGenresAndSlaveWheelOptions):void
		{
			if (response.ResponseCode == 0)
			{
				genres.setSource(response.GenreList.toArray());
				filters.setSource(response.SlaveWheelOptionList.toArray());
			}
		}
		
		private function onLibraryLoaded(response:UserLibraryList, index:int):void
		{
			var category:LibraryCategory = _filterArray[_filter];
			var types:Array = category.type.split(",");

			if (_items == null)
				_items = response.Items;
			else
				_items.addAll(response.Items);
			
			if (++index < types.length)
			{
				RoxioNowDAO.api.Library.getUserLibrary(_genre.toString(), types[index], category.filter).addResponder(
					new AsyncResponder(onLibraryLoaded, index) );
			}
			else
			{
				var sort:Sort;
				
				if (types.length > 1 && _sort == SORT_STANDARD)
				{
					var passSortField:SortField = new SortField();
					passSortField.name = "PassID";
					passSortField.numeric = true;
					passSortField.descending = true;
					
					/* Create the Sort object and add the SortField object created earlier to the array of fields to sort on. */
					sort = new Sort();
					sort.fields = [passSortField];
					
					/* Set the ArrayCollection object's sort property to our custom sort, and refresh the ArrayCollection. */
					_items.sort = sort;
					_items.refresh();
				}
				else if (_sort == SORT_ALPHABETICAL)
				{
					var nameSortField:SortField = new SortField();
					nameSortField.name = "Name";
					
					/* Create the Sort object and add the SortField object created earlier to the array of fields to sort on. */
					sort = new Sort();
					sort.fields = [nameSortField];
					
					/* Set the ArrayCollection object's sort property to our custom sort, and refresh the ArrayCollection. */
					_items.sort = sort;
					_items.refresh();
				}
				
				setSource(_items.toArray());
			}
		}
		
		private function onTitlePurchased(event:Event):void
		{
			// Trigger a reload
			invalidate();
		}
		
		
		private function onServerPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "isLoggedIn")
				invalidate();
		}
	}
}