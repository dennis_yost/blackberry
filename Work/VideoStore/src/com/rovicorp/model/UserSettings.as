﻿package com.rovicorp.model
{
//	import flash.data.EncryptedLocalStore;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.net.SharedObject;
	import flash.utils.ByteArray;
	
	import mx.events.PropertyChangeEvent;
	
	public class UserSettings extends EventDispatcher
	{
		private var _storage:SharedObject = SharedObject.getLocal("user-settings");
		
		public function UserSettings(target:IEventDispatcher=null)
		{
			super(target);
		}

		public function get acceptedEULA(): Boolean
		{
			return _storage.data.acceptedEULA;
		}
		
		public function set acceptedEULA(value:Boolean):void
		{
			var oldValue:Boolean = _storage.data.acceptedEULA;
			
			if (value != oldValue)
			{
				_storage.data.acceptedEULA = value;
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "acceptedEULA", oldValue, value));			
			}
		}

		public function get deleteExpiredRentals(): Boolean
		{
			return _storage.data.deleteExpiredRentals;
		}

		public function set deleteExpiredRentals(value:Boolean):void
		{
			var oldValue:Boolean = _storage.data.deleteExpiredRentals;
			
			if (value != oldValue)
			{
				_storage.data.deleteExpiredRentals = value;
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "deleteExpiredRentals", oldValue, value));			
			}
		}

		public function get keepSignedIn(): Boolean
		{
			if (_storage.data.keepSignedIn == null)
				_storage.data.keepSignedIn = true;
			
			return _storage.data.keepSignedIn;
		}
		
		public function set keepSignedIn(value:Boolean):void
		{
			var oldValue:Boolean = _storage.data.keepSignedIn;
			
			if (value != oldValue)
			{
				_storage.data.keepSignedIn = value;
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "keepSignedIn", oldValue, value));			
			}
		}
		
		public function get rentalsOnly(): Boolean
		{
			return _storage.data.rentalsOnly;
		}
		
		public function set rentalsOnly(value:Boolean):void
		{
			var oldValue:Boolean = _storage.data.rentalsOnly;
			
			if (value != oldValue)
			{
				_storage.data.rentalsOnly = value;
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "rentalsOnly", oldValue, value));			
			}
		}
		
		public function get userName(): String
		{
			return _storage.data.userName;
		}
		
		public function set userName(value:String):void
		{
			var oldValue:String = _storage.data.userName;
			
			if (value != oldValue)
			{
				_storage.data.userName = value;
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "userName", oldValue, value));			
			}
		}
		
		internal function get authToken():String
		{
			var result:String = "";
			/* TODO: EncryptedLocalStore is not supported in current builds.  Will need to
			 * switchover authtoken storage as soon as it is available.
			var storedValue:ByteArray = EncryptedLocalStore.getItem("authtoken");
			
			if (storedValue)
			{
				result = storedValue.readUTFBytes(storedValue.length); 
				setLoggedIn(result.length > 0);
			}
			*/
			if (_storage.data.authToken)
				result = _storage.data.authToken;
			
			return result;
		}

		internal function set authToken(value:String):void
		{
			/* TODO: EncryptedLocalStore is not supported in current builds.  Will need to
			 * switchover authtoken storage as soon as it is available.
			var bytes:ByteArray = new ByteArray();
			bytes.writeUTFBytes(value);

			EncryptedLocalStore.setItem("authtoken", bytes);
			*/
			_storage.data.authToken = value;
		}
	}
}