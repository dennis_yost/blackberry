package com.rovicorp.model
{
	import com.sonic.StreamAPIResponder;
	import com.sonic.response.Browse.BrowseItem;
	import com.sonic.response.Browse.BrowseList;
	import com.sonic.response.Genre;
	import com.sonic.response.GenreList;
	
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.AsyncToken;
	
	[Bindable]
	public class Store extends StoreCollection
	{
		public static const MOVIES:int = 10954;
		public static const TV_SHOWS:int = 11062;
		
		private static const PRELOAD_COUNT:int = 7;
		
		private var _server:RoxioNowDAO = new RoxioNowDAO();
		
		private var _id:int;
		private var _categories:AsyncCollection;
		//		private var _titles:Dictionary;
		private var _category:int;
		private var _sort:String = "standard";
		private var _purchaseType:String = "any";
		private var _profile:String = "none";
		
		public function Store(id:int)
		{
			_id = id;
			
			if (categories.length)
				category = categories[0].ID;
			
			if (id == MOVIES)
			{
				_purchaseType = _server.settings.rentalsOnly ? "rent" : "any";
				_server.settings.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onSettingsChange);
			}
		}
		
		public function get category():int
		{
			return _category;
		}
		
		public function set category(value:int):void
		{
			if (_category !== value)
			{
				var event:Event = PropertyChangeEvent.createUpdateEvent(this, "category", _category, value);
				_category = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
			
		}
		
		public function get categories():AsyncCollection
		{
			if (_categories == null)
			{
				_categories = new AsyncCollection();
				
				var navigation:Dictionary = _server.navigation;
				
				if (_server.navigation[_id] == null)
					_server.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onServerPropertyChange);
				else
					loadCategories(navigation[_id]);
			}
			
			return _categories;
		}
		
		public function get sort():String
		{
			return _sort;
		}
		
		public function set sort(value:String):void
		{
			if (_sort !== value)
			{
				var event:Event = PropertyChangeEvent.createUpdateEvent(this, "sort", _sort, value);
				_sort = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
			
		}
		
		public function get purchaseType():String
		{
			return _purchaseType;
		}
		
		public function set purchaseType(value:String):void
		{
			if (_purchaseType !== value)
			{
				var event:Event = PropertyChangeEvent.createUpdateEvent(this, "purchaseType", _purchaseType, value);
				_purchaseType = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
			
		}
		
		public function get profile():String
		{
			return _profile;
		}
		
		public function set profile(value:String):void
		{
			if (_profile != value)
			{
				var event:Event = PropertyChangeEvent.createUpdateEvent(this, "profile", _profile, value);
				_profile = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
		}		
		
		public override function invalidate():void
		{
			super.invalidate();
		}
		
		private function loadCategories(genres:Array):void
		{
			this.category = genres[0].ID;
			
			var list:String = genres[0].ID;
			for (var i:int = 1; i < genres.length; ++i)
				list += "," + genres[i].ID;
			
			var responder:AsyncResponder = new AsyncResponder(onGetBrowseListPluralCompleted, genres);
			RoxioNowDAO.api.Browse.getBrowseListPlural(list, _sort, 1, PRELOAD_COUNT, _purchaseType, _profile).addResponder(responder);
		}
		
		private function onGetBrowseListPluralCompleted(browseLists:ArrayCollection, genres:Array):void
		{
			var categories:Array = [];
			
			for (var i:int = 0; i < browseLists.length; ++i) 
			{
				var category:StoreCategory = StoreCategory.fromGenre(genres[i]);
				category.totalItems = browseLists[i].TotalItems;
				category.titles = browseLists[i].Items;
				
				categories.push(category);
			}
			
			_categories.setSource(categories);
		}
		
		private function onSettingsChange(event:PropertyChangeEvent):void
		{
			if (event.property == "rentalsOnly")
			{
				purchaseType = Boolean(event.newValue) ? "rent" : "any";

				if (_categories)
				{
					_categories.invalidate();
					loadCategories(_server.navigation[_id]);
				}
				
			}
		}
		
		private function onServerPropertyChange(event:PropertyChangeEvent):void
		{
			_server.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onServerPropertyChange);
			loadCategories(_server.navigation[_id]);
		}
		
		protected override function loadPage(pageNo:int):AsyncToken 
		{
			var result:AsyncToken = null;
			
			if (_category)
				result = RoxioNowDAO.api.Browse.getBrowseList(_category, _sort, pageNo, PAGE_SIZE, _purchaseType, _profile);
			
			return result;
		}
	}
}