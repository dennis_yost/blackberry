package com.rovicorp.model
{
	import com.sonic.response.Browse.BrowseItem;
	import com.sonic.response.Browse.BundleList;
	import com.sonic.response.TitleData.FullTitle;
	import com.sonic.response.TitleData.ShortTitle;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	
	[Bindable]
	public class TitleListingBase extends EventDispatcher 
	{
		private static const LOADING:int = 1;
		
		private var _state:int = 0;
		private var _details:FullTitle;
		
		public var Name:String;
		public var TitleID:int;
		
		public function TitleListingBase()
		{
			super();
		}
		
		protected function getDetails():void
		{
			_state = LOADING;
			new RoxioNowDAO().getFullSummary(TitleID, true, onGetFullSummaryCompleted);	
		}
		
		public function get Details():FullTitle
		{
			if (_details == null && _state != LOADING)
			{
				if (TitleID != 0)
					getDetails();	
			}			
			
			return _details;
		}
		
		public function set Details(value:FullTitle):void
		{
			_details = value;
			_state = 0;
			
			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "Details", null, value));
		}
		
		protected function onGetFullSummaryCompleted(data:FullTitle):void
		{
			Details = data;
		}
	}
}