package com.rovicorp.model
{
	public class TitleListingPurchased extends TitleListing
	{
		public var PassID:int;
		
		public var DownloadStatus:int;

		public function TitleListingPurchased()
		{
			super();
		}

		protected override function getDetails():void
		{
			new RoxioNowDAO().getPurchasedTitle(PassID, TitleID, onGetFullSummaryCompleted);
		}
	}
}