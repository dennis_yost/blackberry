package com.rovicorp.model
{
	import mx.events.PropertyChangeEvent;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class Wishlist extends AsyncCollection
	{
		private var _server:RoxioNowDAO = new RoxioNowDAO();
		private var _type:String = "All";
		
		public function Wishlist()
		{
			_server.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onSettingsChange);
			
			RoxioNowDAO.api.Wishlist.getOperation("addItemToWishList").addEventListener(ResultEvent.RESULT, onWishlistChanged, false, 0, true);
			RoxioNowDAO.api.Wishlist.getOperation("removeAllItemsFromWishList").addEventListener(ResultEvent.RESULT, onWishlistChanged, false, 0, true);
			RoxioNowDAO.api.Wishlist.getOperation("removeItemFromWishList").addEventListener(ResultEvent.RESULT, onWishlistChanged, false, 0, true);
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function set type(value:String):void
		{
			if (_type !== value)
			{
				var event:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "type", _type, value);
				_type = value;
				
				dispatchEvent(event);
				invalidate();
			}
		}
		
		protected override function setState(value:String):void
		{
			super.setState(value);
			
			if (value == LOADING)
			{
				if (_server.isLoggedIn)
					RoxioNowDAO.api.Wishlist.getWishlist(_type, onGetWishlistCompleted);	
				else
					setSource([]);
			}
		}
		
		private function onSettingsChange(event:PropertyChangeEvent):void
		{
			if (event.property == "isLoggedIn")
				invalidate();
		}
		
		private function onGetWishlistCompleted(response:Object):void
		{
			setSource(response.Items.toArray());
		}
		
		private function onWishlistChanged(data:ResultEvent):void
		{
			if (data.result.ResponseCode == 0)
				invalidate();
		}
	}
}