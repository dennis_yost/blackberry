package com.rovicorp.model
{
	import com.sonic.response.Library.FullTitlePurchased;
	
	import flash.filesystem.File;
	
	public class DownloadListing extends TitleListingPurchased
	{
		public var BytesTotal:int;
		
		public var BytesDownloaded:int;
		
		public var TimeRemaining:int;			
		
		public var DownloadPercent:Number = 0;
		
		public var fileURL:String = "";
		
		//Format title detail from pollForDownloads
		public function formatDataFromContentitem( contentItem:Object):void
		{			
			var pass:Object = contentItem.pass;
			var title:Object = contentItem.title;
			
			var item:FullTitlePurchased = new FullTitlePurchased();			
			item.TitleID = title.titleid;			
			item.PassID = pass.pid;
			item.BoxartPrefix = title.metadata.boxartPrefix;
			item.MPAARating = title.metadata.rating;
			item.Name = title.metadata.name;
			
			this.Details = item;
		}
		
		public function formatDataFromFullTitlePurchased(item:FullTitlePurchased):void
		{
			this.TitleID = item.TitleID;
			this.PassID = item.PassID;
			this.BoxartPrefix = item.BoxartPrefix;
			this.Name = item.Name;
			
			this.Details = item;
		}
		
		public function getLocalVideoURL():String
		{			
			return fileURL;	
		}
	}
}