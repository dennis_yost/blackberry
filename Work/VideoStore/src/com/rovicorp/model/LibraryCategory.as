package com.rovicorp.model
{
	public class LibraryCategory
	{
		public var id:String;
		public var filter:String;
		public var type:String;
		
		public function LibraryCategory(id:String, type:String, filter:String)
		{
			this.id = id;
			this.filter = filter;
			this.type = type;
		}
	}
}