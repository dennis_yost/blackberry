package com.rovicorp.model
{
	import mx.rpc.IResponder;
	
	public class AsyncResponder implements IResponder
	{
		private var _callback:Function;
		private var _token:Object;
		
		public function AsyncResponder(callback:Function, token:Object)
		{
			_callback = callback;
			_token = token;
		}
		
		public function result(data:Object):void
		{
			_callback(data.result, _token);
		}
		
		public function fault(info:Object):void
		{
		}
	}
}