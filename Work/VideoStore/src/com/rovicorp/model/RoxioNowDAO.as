package com.rovicorp.model
{
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.DownloadTitleEvent;
	import com.rovicorp.events.EventCenter;
	import com.sonic.StreamAPIResponder;
	import com.sonic.StreamAPIWse;
	import com.sonic.StreamDeviceRequestSettings;
	import com.sonic.StreamEvent;
	import com.sonic.response.Auth.AuthTokenResponse;
	import com.sonic.response.Commerce.Transaction;
	import com.sonic.response.Genre;
	import com.sonic.response.GenreList;
	import com.sonic.response.Library.FullTitlePurchased;
	import com.sonic.response.TitleData.FullTitle;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.net.SharedObject;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	import flash.xml.XMLDocument;
	
	import mx.events.PropertyChangeEvent;
	import mx.rpc.AsyncResponder;
	import mx.rpc.AsyncToken;
	import mx.rpc.CallResponder;
	import mx.rpc.Responder;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.xml.SchemaTypeRegistry;
	import mx.rpc.xml.SimpleXMLDecoder;
	
	import qnx.system.Device;
	
	public final class RoxioNowDAO extends CallResponder  
	{
		// Events
		public static const NAVIGATION_LOADED:String = "onNavigationLoaded";
		public static const TITLE_PURCHASED:String = "onPurchase";
		
		public static const CLIENT_NAME:String = "Blackberry Playbook"; // TODO: determine client name
		public static const VERSION:String = "1.0.0";
		
		private static var _api:StreamAPIWse;
		private static var _dispatcher:EventDispatcher = new EventDispatcher();
		private static var _cache:Dictionary;

		private static var _isLoggedIn:Boolean = false;

		private static var _featured:Featured;
		private static var _library:Library;
		private static var _movies:Store;
		private static var _tvShows:Store;
		private static var _navigation:Dictionary;
		private static var _search:Search;
		private static var _settings:UserSettings = new UserSettings();
		private static var _wishlist:Wishlist;
		
		private static var _downloadList:DownloadList;
		
		public function RoxioNowDAO()
		{
			_dispatcher.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onPropertyChange, false, 0, true);
		}

		public static function get api():StreamAPIWse
		{
			if (_api == null)
			{
				// TODO: Replace tag literals once defined in objects
				StreamAPIWse.registerClass("BrowseItem", com.rovicorp.model.TitleListing);
				StreamAPIWse.registerClass("BrowseItemPurchased", com.rovicorp.model.TitleListingPurchased);
				StreamAPIWse.registerClass("BundleItem", com.rovicorp.model.BundleItem);
				StreamAPIWse.registerClass("FullTitle", com.rovicorp.model.TitleDetails);
				
				_api = new StreamAPIWse(getDeviceSettings());
			}
			
			return _api;
		}

		private static function getDeviceSettings():StreamDeviceRequestSettings
		{
			var deviceSettings:StreamDeviceRequestSettings;

			if (_settings.keepSignedIn == false)
				_settings.authToken = "";
			
			// TODO: Load settings from config file
			deviceSettings = new StreamDeviceRequestSettings()
			deviceSettings.APIKey = "tuKwA4yrdBfkg5fwCZuA8BJbYCuOoS4lso9dcQOZdwbc/tF16LNJmA==";
			deviceSettings.DestinationTypeID = "51";
			deviceSettings.DestinationUniqueID = "BlackberryPlaybook";
			deviceSettings.Username = "SonicTestAPI";
			deviceSettings.Password = "CN@s0N!cT35t&toK3N";
			deviceSettings.CurrentEnvironment = null;
			deviceSettings.AuthToken = _settings.authToken;

			try
			{
				deviceSettings.DestinationUniqueID = Device.device.serialNumber
			}
			catch (e:Error)	{}
			
			// DEBUG code
			if (deviceSettings.DestinationUniqueID == null)
			{
				var storage:SharedObject = SharedObject.getLocal("debug-settings");
				
				if (storage.data.DestinationUniqueID == null)
					storage.data.DestinationUniqueID = "BBPB" + Math.floor(Math.random() * 100000); //

				deviceSettings.DestinationUniqueID = storage.data.DestinationUniqueID;
			}
			
			_isLoggedIn = deviceSettings.AuthToken.length > 0;
			
			return deviceSettings;
		}

		////////////////////////////////////////////////////////////////////////////////
		// Properties
		
		[Bindable]
		public function get isLoggedIn(): Boolean
		{
			return _isLoggedIn;
		}

		public function set isLoggedIn(value:Boolean):void
		{
			// set function is only for binding purposes	
		}
		
		public function get featured():Featured
		{
			if (_featured == null)
				_featured = new Featured();
			
			return _featured;
		}
		
		public function get library():Library
		{
			if (_library == null)
				_library = new Library();
			
			return _library;
		}
		
		public function get movies():Store
		{
			if (_movies == null)
				_movies = new Store(Store.MOVIES);
			
			return _movies;
		}

		public function get tvShows():Store
		{
			if (_tvShows == null)
				_tvShows = new Store(Store.TV_SHOWS);
			
			return _tvShows;
		}
		
		public function get search():Search
		{
			if (_search == null)
				_search = new Search();
			
			return _search;
		}

		public function get settings():UserSettings
		{
			return _settings;
		}

		/**
		 * Gets the navigation list for the designated category
		 *  
		 * @param categoryId 
		 * @param callback 
		 */
		internal function get navigation():Dictionary
		{
			if (_navigation == null)
			{
				_navigation = new Dictionary();
				api.Browse.getNavigation(onGetNavigationCompleted).addResponder(this);
			}
			
			return _navigation;
		}
		
		public function get downloadList():DownloadList
		{
			if (_downloadList == null)
				_downloadList = new DownloadList();
			
			return _downloadList;
		}

		public function get wishlist():Wishlist
		{
			if (_wishlist == null)
				_wishlist = new Wishlist();
			
			return _wishlist;
		}

		////////////////////////////////////////////////////////////////////////////////
		// Implementation

		/**
		 * Gets/Creates cache for the specified object type
		 * @params type Object or Class that defines type of cache
		 *
		 * @return Dictionary to populate with objects to be cached
		 */ 
		private function getCache(type:Object):Dictionary
		{
			if (_cache == null)
				_cache = new Dictionary();
			
			var key:String = flash.utils.getQualifiedClassName(result);
			var cache:Dictionary = _cache[key];
			
			if (cache == null)
				_cache[key] = cache = new Dictionary();
			
			return cache;
		}
		
		public function fault2(info:Object, token:Object):void
		{
			fault(info);
		}
		
		////////////////////////////////////////////////////////////////////////////////
		// API

		public function addItemToWishList(TitleID:int, callback:Function = null) : void
		{
			var token:AsyncToken = api.Wishlist.addItemToWishList(TitleID, callback);
			token.addResponder(new mx.rpc.AsyncResponder(onAddItemToWishList, fault2, TitleID));
		}
		
		public function removeItemFromWishList(TitleID:int, callback:Function = null) : void
		{
			var token:AsyncToken = api.Wishlist.removeItemFromWishList(TitleID, callback);
			token.addResponder(new mx.rpc.AsyncResponder(onRemoveItemFromWishList, fault2, TitleID));
		}	
		
		public function doPurchase(SKUID:int, CouponCode:String, callback:Function = null):void 
		{
			api.Commerce.doPurchase(SKUID, CouponCode, callback).addResponder(new Responder(onPurchaseCompleted, fault));
		}
		
		public function getBillingInfo(callback:Function):void 
		{
			api.Commerce.getBillingInfo(callback).addResponder(this);
		}
		
		public function getBundleListing(titleId:int, callback:Function = null):void
		{
			api.Browse.getBundleListing(titleId, callback).addResponder(this);
		}
		
		public function getEulaText(callback:Function = null):void
		{
			api.Utilities.getEulaText(callback).addResponder(this);			
		}
		
		public function getLegalInfo(callback:Function = null):void
		{
			api.Utilities.getLegalInfo(callback).addResponder(this);			
		}
		
		public function getFullSummary(titleID:int, includePurchaseData:Boolean, callback:Function = null):AsyncToken 
		{
			var cache:Dictionary = getCache(FullTitle);
			var value:Object = cache[titleID];
			var token:AsyncToken;
			
			if (value == null)
			{
				token = api.TitleData.getFullSummary(titleID, includePurchaseData, callback);
				token.addResponder(new Responder(onGetFullSummaryCompleted, fault));	
				
				cache[titleID] = token;
			}
			else if (value is FullTitle)
			{
				if (callback != null)
					callback(value);
				
				lastResult = value;
			}
			else if (value is AsyncToken)
			{	
				token = value as AsyncToken;
				token.addResponder(new StreamAPIResponder(callback));
			}
			
			return token;
		}
		
		public function getPurchasedTitle(passId:int, titleId:int, callback:Function):void
		{
			var cache:Dictionary = getCache(FullTitlePurchased);
			var value:Object = cache[passId];
			
			if (value == null)
			{
				var token:AsyncToken = api.Library.getPurchasedTitle(passId, titleId, callback);
				token.addResponder(new Responder(onGetPurchasedTitleCompleted, fault));	
				
				cache[passId] = token;
			}
			else if (value is FullTitlePurchased)
				callback(value);
			else if (value is AsyncToken)
				AsyncToken(value).addResponder(new StreamAPIResponder(callback));
		}
		
		public function loginUser(username:String, password:String, callback:Function = null):void
		{
			settings.userName = username;
			api.User.loginUser(username, password, CLIENT_NAME, VERSION).addResponder(new mx.rpc.AsyncResponder(onLoginUserCompleted, fault2, callback));
		}
		
		public function logoutUser():void
		{
			api.setAuthToken("");
			settings.authToken = "";
			
			_isLoggedIn = false;
			_dispatcher.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "isLoggedIn", true, false));
		}
		
		public function queueDownload(passid:int, callback:Function = null):void
		{
			api.Download.queueDownload(passid, callback).addResponder(new Responder(onQueueDownloadCompleted, fault));
		}
		
		public function registerUser(username:String, password:String, firstName:String, lastName:String, gender:String, age:String, email:String, callback:Function = null):void
		{
			settings.userName = username;
			api.User.registerUser(username, password, firstName, lastName, gender, age, email, CLIENT_NAME, VERSION).addResponder(new mx.rpc.AsyncResponder(onRegisterUserCompleted, fault2, callback));
		}
		
		////////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		
		private function onGetNavigationCompleted(genreList:GenreList):void
		{
			if (genreList)
			{
				for each (var item:com.sonic.response.Genre in genreList.Genres)
				{
					if (item.ParentId != 0)
					{
						var parent:Array = _navigation[item.ParentId];
						
						if (parent == null)
							parent = _navigation[item.ParentId] = [];
						
						parent.push(item);
					}
				}
			}

			_dispatcher.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "navigation", null, _navigation));
		}

		private function onAddItemToWishList(data:ResultEvent, titleID:int):void
		{
			result(data);
			
			var cache:Dictionary = getCache(FullTitle);
			var title:TitleDetails = cache[titleID];
			
			if (title)
				title.InUserWishlist = true;
		}

		private function onRemoveItemFromWishList(data:ResultEvent, titleID:int):void
		{
			result(data);

			var cache:Dictionary = getCache(FullTitle);
			var title:TitleDetails = cache[titleID];
			
			if (title)
				title.InUserWishlist = false;
		}
		
		private function onLoginUserCompleted(data:ResultEvent, callback:Function):void
		{
			var response:AuthTokenResponse = data.result as AuthTokenResponse;
			
			if (response.ResponseCode == 0)
			{
				// TODO: Determine what data we need to keep
				api.setAuthToken(response.AuthToken);
				settings.authToken = response.AuthToken;
				
				_isLoggedIn = true;
				_dispatcher.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "isLoggedIn", false, true));
				
				if (callback != null)
					callback(data.result);
			}
			
			result(data);
		}

		private function onRegisterUserCompleted(data:ResultEvent, callback:Function):void
		{
			var response:AuthTokenResponse = data.result as AuthTokenResponse;
			
			if (response.ResponseCode == 0)
			{
				// TODO: Determine what data we need to keep
				api.setAuthToken(response.AuthToken);
				settings.authToken = response.AuthToken;
				
				_isLoggedIn = true;
				_dispatcher.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "isLoggedIn", false, true));
				
				if (callback != null)
					callback(data.result);
			}
			
			result(data);
		}
		
		private function onPurchaseCompleted(data:ResultEvent):void
		{
			if (data.result.ResponseCode == 0 && _library)
			{
				_library.invalidate();
			}
			
			result(data);
		}
		
		private function onGetFullSummaryCompleted(data:ResultEvent):void
		{
			getCache(FullTitle)[data.result.TitleID] = data.result;
			result(data);
			
			/* No need to save all titles browsed by user for now
			// update full title info to db
			var e:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_TITLE);
			e.title = data.result as FullTitle;
			e.id = data.result.TitleID.toString();
			e.updateExistingItem = false;
			EventCenter.inst.dispatchEvent(e);
			*/
		}
		
		private function onGetPurchasedTitleCompleted(data:ResultEvent):void
		{
			getCache(data.result)[data.result.PassID] = data.result;
			result(data);
			
			var ft:FullTitlePurchased = data.result as FullTitlePurchased;
			// update purchased title info to db
			var dbe:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_PURCHASED_TITLE);
			dbe.id = ft.PassID.toString();
			dbe.purchasedTitle = ft;
			EventCenter.inst.dispatchEvent(dbe);
		}
		
		private function onQueueDownloadCompleted(result:Object):void
		{
			// result is ResultEvent
			// result.result is com.sonic.response.Download.v1.AssignPassToDownloadDestinationResponse
			// DownloadQueueID can be get from here.
			
			// AssignPassToDownloadDestinationResponse:
			// ResponseCode: 0
			// ResponseMessage: "Success"
			// DownloadDestinationID: 63510
			// DownloadQueueID: 652456
			// PurchaseQueueID: 0
			// SKUID: 406783
			// TotalDownloads: 3
			// TotalLicenses: 1
			
//			if (result.result.ResponseCode == 0)
				api.Download.pollForDownloads(onPollForDownloadsCompleted);
	
		}
		
		private function onPollForDownloadsCompleted(result:Object):void
		{
			// result is XMLList contains pollForDownloadsResponse
			// result[0] is XML
			
			//trace((result as XMLList).toXMLString());
			var xmlDoc:XMLDocument = new XMLDocument(result.toString());
			var sd:SimpleXMLDecoder = new SimpleXMLDecoder;
			var item:Object = sd.decodeXML(xmlDoc);
			
			if (item.pollForDownloadsResponse.pollForDownloadsResult.ResponseCode == "0")
			{
				if (!item.pollForDownloadsResponse.pollForDownloadsResult.ContentItems
					|| !item.pollForDownloadsResponse.pollForDownloadsResult.ContentItems.contentitem)
				{
					trace("no download item");
					return;
				}
				
				var contentItems:Array;
				if (item.pollForDownloadsResponse.pollForDownloadsResult.ContentItems.contentitem is Array)
				{
					contentItems = item.pollForDownloadsResponse.pollForDownloadsResult.ContentItems.contentitem;
				}
				else
				{
					contentItems = [item.pollForDownloadsResponse.pollForDownloadsResult.ContentItems.contentitem];
				}
				
				var f:File;
				
				for (var i:int = 0; i < contentItems.length; ++i)
				{
					var contentItem:Object = contentItems[i];
					var content:Object = contentItem.Content;
					var pass:Object = contentItem.pass;
					var title:Object = contentItem.title;

					// save title info to titles table
					//trace("save title info to titles table", title.titleid);
					var dbe_title:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_TITLE);
					dbe_title.id = title.titleid.toString();
					dbe_title.titleObj = title;
					dbe_title.updateExistingItem = false;
					EventCenter.inst.dispatchEvent(dbe_title);
					
					// save current content item to library
					//trace("save current content item to library", pass.pid);
					var dbe_lib:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO);
					dbe_lib.id = pass.pid.toString();
					dbe_lib.contentItem = contentItem;
					EventCenter.inst.dispatchEvent(dbe_lib);
					
					f = File.applicationStorageDirectory.resolvePath("videos/" + title.titleid +"_"+pass.pid);
					
					// send out event to invoke download process
					//trace("send out event to invoke download process", pass.sku.dqid);
					var de:DownloadTitleEvent = new DownloadTitleEvent(DownloadTitleEvent.ADD_TASK);
					de.dq_id = pass.sku.dqid;
					//de.remoteURL = content.file.dl.url;
					de.remoteURL = "http://stgapi.cinemanow.com/ces/3G%20harrypotter%206_H264_514k_3G.mp4";
					de.savePath = f.nativePath;
					de.itemInfo = contentItem;
					de.id = title.titleid;					

					EventCenter.inst.dispatchEvent(de);
				}
			}
			else
			{
				trace("pollForDownload Error");
			}
		}
		
		private function onPropertyChange(event:Event):void
		{
			dispatchEvent(event);
		}

	}
}
