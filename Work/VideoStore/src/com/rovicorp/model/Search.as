package com.rovicorp.model
{
	import com.sonic.response.Browse.BrowseItem;
	import com.sonic.Search;
	import com.sonic.SearchOption;
	
	import flash.events.Event;
	
	import mx.events.PropertyChangeEvent;
	import mx.rpc.AsyncToken;
	
	public class Search extends StoreCollection
	{
		private var _query:String = "";
		private var _searchOption:String = SearchOption.TITLE;
		private var _sort:String = "standard";
		private var _purchaseType:String = "any";
		private var _profile:String = "none";
		
		public function Search()
		{
			super();
		}

		public function get query():String
		{
			return _query;
		}
		
		public function set query(value:String):void
		{
			if (_query !== value)
			{
				var event:Event = mx.events.PropertyChangeEvent.createUpdateEvent(this, "query", _query, value);
				_query = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
		}

		public function get option():String
		{
			return _searchOption;
		}
		
		public function set option(value:String):void
		{
			if (_searchOption !== value)
			{
				var event:Event = mx.events.PropertyChangeEvent.createUpdateEvent(this, "option", _searchOption, value);
				_searchOption = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
		}
		
		public function get sort():String
		{
			return _sort;
		}
		
		public function set sort(value:String):void
		{
			if (_sort !== value)
			{
				var event:Event = mx.events.PropertyChangeEvent.createUpdateEvent(this, "sort", _sort, value);
				_sort = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
			
		}
		
		public function get purchaseType():String
		{
			return _purchaseType;
		}
		
		public function set purchaseType(value:String):void
		{
			if (_purchaseType !== value)
			{
				var event:Event = mx.events.PropertyChangeEvent.createUpdateEvent(this, "purchaseType", _purchaseType, value);
				_purchaseType = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
			
		}
		
		public function get profile():String
		{
			return _profile;
		}
		
		public function set profile(value:String):void
		{
			if (_profile !== value)
			{
				var event:Event = mx.events.PropertyChangeEvent.createUpdateEvent(this, "profile", _profile, value);
				_profile = value;
				
				this.dispatchEvent(event);
				setSource(null);
			}
		}		
		
		protected override function loadPage(pageNo:int):AsyncToken 
		{
			if (_query.length)
				return RoxioNowDAO.api.Search.searchTitleSetOptions(_query, _searchOption, _sort, pageNo, PAGE_SIZE, _purchaseType, _profile);
			
			setSource([]);
			return null;
		}
	}
}