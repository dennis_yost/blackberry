package com.rovicorp.model
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.IList;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	
	import qnx.ui.data.IDataProvider;
	import qnx.ui.events.DataProviderEvent;
	
	public class ListProvider extends EventDispatcher implements IDataProvider
	{
		public var list:IList;
		
		public function ListProvider(source:IList)
		{
			var e:CollectionEvent
			list = source;
			list.addEventListener(CollectionEvent.COLLECTION_CHANGE, onCollectionChange, false, 0, true);
		}
		
		private function onCollectionChange(event:CollectionEvent):void
		{
			var dataEvent:DataProviderEvent;
			
			switch (event.kind)
			{
				case CollectionEventKind.ADD:
					dispatchEvent(new DataProviderEvent(DataProviderEvent.ADD_ITEM, event.items, event.oldLocation, event.location));
					break;
				case CollectionEventKind.MOVE:
					break;
				case CollectionEventKind.REFRESH:
					dispatchEvent(new DataProviderEvent(DataProviderEvent.UPDATE_ALL, event.items, 0, event.items.length));
					break;
				case CollectionEventKind.REMOVE:
					dispatchEvent(new DataProviderEvent(DataProviderEvent.REMOVE_ITEM, event.items, event.oldLocation, event.location));
					break;
				case CollectionEventKind.REPLACE:
					dispatchEvent(new DataProviderEvent(DataProviderEvent.REPLACE_ITEM, event.items, event.oldLocation, event.location));
					break;
				case CollectionEventKind.RESET:
					dispatchEvent(new DataProviderEvent(DataProviderEvent.UPDATE_ALL, event.items, 0, event.items.length));
					break;
				case CollectionEventKind.UPDATE:
					dispatchEvent(new DataProviderEvent(DataProviderEvent.UPDATE_ITEM, event.items, event.oldLocation, event.location));
					break;
			}
		}
		
		public function addItem(item:Object):void
		{
			list.addItem(item);
		}
		
		public function addItemAt(item:Object, index:int):void
		{
			list.addItemAt(item, index);
		}
		
		public function addItemsAt(items:Array, index:int):void
		{
		}
		
		public function removeItem(item:Object):void
		{
			removeItemAt(indexOf(item));
		}
		
		public function removeItemAt(index:int):void
		{
			list.removeItemAt(index);
		}
		
		public function removeAll():void
		{
			list.removeAll();
		}
		
		public function replaceItemAt(item:Object, index:int):void
		{
			list.setItemAt(item, index);
		}
		
		public function replaceItem(item:Object, oldObject:Object):void
		{
			list.setItemAt(item, indexOf(oldObject));
		}
		
		public function updateItemAt(item:Object, index:int):void
		{
		}
		
		public function updateItem(item:Object, oldObject:Object):void
		{
		}
		
		public function updateItemsAt(items:Array, index:int):void
		{
		}
		
		public function getItemAt(index:int):Object
		{
			return list.getItemAt(index);
		}
		
		public function get length():int
		{
			return list.length;
		}
		
		public function clone():IDataProvider
		{
			return null;
		}
		
		public function indexOf(obj:Object):int
		{
			return list.getItemIndex(obj);
		}
		
		public function setItems(arr:Array, throwEvent:Boolean=false):void
		{
		}
		
		public function get data():Array
		{
			return list.toArray();
		}
	}
}