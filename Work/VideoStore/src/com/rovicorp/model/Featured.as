package com.rovicorp.model
{
	import com.sonic.response.Browse.BrowseList;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.events.PropertyChangeEvent;
	import mx.rpc.IResponder;
	
	public class Featured extends EventDispatcher implements IResponder
	{
		private static const NEW_RELEASES:int = 10954;
		
		private var _errorCode:int;
		private var _state:String = "loading";
		
		private var _features:AsyncCollection;
		private var _recommended:AsyncCollection;
		
		public function Featured(target:IEventDispatcher=null)
		{
			super(target);
			
			RoxioNowDAO.api.Browse.getBrowseList(NEW_RELEASES, "standard", 1, 24, "any", "none").addResponder(this);
		}
		
		[Bindable]
		public function get errorCode():int
		{
			return _errorCode;
		}
		
		public function set errorCode(value:int):void 
		{
			if (value != _errorCode)
			{
				var oldValue:int = _errorCode;
				
				_errorCode = value;
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "errorCode", oldValue, value));
			}
		}
		
		public function get features():AsyncCollection
		{
			if (_features == null)
				_features = new AsyncCollection();
			
			return _features;
		}

		public function get recommended():AsyncCollection
		{
			if (_recommended == null)
				_recommended = new AsyncCollection();
			
			return _recommended;
		}
		
		[Bindable]
		public function get state():String
		{
			return _state;
		}
		
		public function set state(value:String):void 
		{
			if (value != _state)
			{
				var oldState:String = _state;

				_state = value;
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "state", oldState, value));
			}
		}

		/**
		 * IResponder override to create an empty list when web service calls fail
		 * 
		 * @param info The FaultEvent containing the fault information 
		 */
		public function fault(info:Object):void
		{
			// TODO: Figure out error codes, if network error listen for network update event
			state = "error";
		}
		
		/**
		 * IResponder override for obtaining the result from asynchronous web service calls
		 * 
		 * @param data The ResultEvent containing the data returned from the web service call
		 */
		public function result(data:Object):void
		{
			var result:BrowseList = data.result;
			var array:Array = result.Items.toArray();

			features.setSource(array.splice(0, 3));
			recommended.setSource(array);
			
			state = "loaded";
		}		
	}
}