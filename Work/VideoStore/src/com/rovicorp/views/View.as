package com.rovicorp.views
{
	import com.rovicorp.controllers.ViewController;
	
	import spark.components.View;
	
	public class View extends spark.components.View
	{
		private var _controller:ViewController;
		
		public function View()
		{
			super();
		}
		
		public function get controller():ViewController
		{
			return _controller;
		}

		public function set controller(value:ViewController):void
		{
			_controller = value;
			_controller.view = this;
		}
	}
}