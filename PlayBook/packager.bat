@echo off
set PAUSE_ERRORS=1

echo make build for RIM Playbook VideoStore
echo Choose build target:
echo.
echo [1] USA
echo [2] Canada
echo.
set /P C=[input choice number] 
echo.

set BUILD_TARGET=usa

if "%C%"=="1" set BUILD_TARGET=usa
if "%C%"=="2" set BUILD_TARGET=canada

::echo application version (in application description file, e.g. 1.1.0):
::set /P V=[input version number] 
::echo.

::set APP_VERSION=%V%

if "%C%"=="1" set APP_VERSION=1.1.0
if "%C%"=="2" set APP_VERSION=1.2.0

if "%C%"=="1" set APP_ID=com.rovi.videostore
if "%C%"=="2" set APP_ID=com.rovi.videostoreca

echo build id (in blackberry-tablet.xml):
set /P B=[input build id] 
echo.

set BUILD_ID=%B%

echo Make build for %BUILD_TARGET% %APP_VERSION%_%BUILD_ID%...

call ant -DBUILD_TARGET=%BUILD_TARGET% -DAPP_ID=%APP_ID% -DVERSION=%APP_VERSION% -DBUILD_ID=%BUILD_ID% -buildfile packageApp.xml

pause