package com.rovicorp.validators
{
	import mx.validators.StringValidator;
	import mx.validators.ValidationResult;
	
	
	/**
	 * zip rule for CA
	 */ 
	public class ZIPRuleValidator extends StringValidator
	{
		override protected function doValidation(value:Object):Array
		{
			var results:Array = super.doValidation(value);
			
			var zip:String = value.toString();
			if (zip.length != 7 && zip.length != 6) return results;
			
			//LNL NLN
			var reg:RegExp = /^[A-Z][0-9][A-Z] ?[0-9][A-Z][0-9]$/i;
			
			var result:Array = reg.exec(zip);
			
			if (result == null)
			{
				results.push(new ValidationResult(true, null, "ZIPRuleFail", "Invalid postal code."));
			}
			
			return results;
		}
	}
}