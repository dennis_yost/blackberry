package com.rovicorp.validators
{
	import mx.validators.EmailValidator;
	import mx.validators.ValidationResult;
	
	public class CustomEmailValidator extends EmailValidator
	{
		protected var _maxLength:uint = 100;
		
		public function CustomEmailValidator()
		{
			super();
		}
		
		public function set maxLength(val:uint):void
		{
			_maxLength = val;
		}
		
		override protected function doValidation(value:Object):Array
		{
			var results:Array = super.doValidation(value);
			
			var emalString:String = value.toString();
			if (emalString.length > _maxLength)
			{
				results.push(new ValidationResult(true, null, "EmailTooLong", "Invalid email address"));
			}
			
			return results;
		} 
	}
}