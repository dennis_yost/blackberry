package com.rovicorp.validators
{
	import mx.validators.StringValidator;
	import mx.validators.ValidationResult;
	
	public class PasswordRuleValidator extends StringValidator
	{
		public function PasswordRuleValidator()
		{
			super();
		}
		
		override protected function doValidation(value:Object):Array
		{
			var results:Array = super.doValidation(value);
			
			var pass:String = value.toString();
			if (pass.length < 7 || pass.length > 20) return [];
			
			if (isContainSpace(pass) == true || isContainNumber(pass) == false || isContainLetter(pass) == false)
			{
				results.push(new ValidationResult(true, null, "PasswordRuleFail", "Your password must be between 7 and 20 characters and contain at least 1 letter and 1 number."));
			}
			
			return results;
		}
		
		protected function isContainSpace(val:String):Boolean
		{
			return (val.indexOf(" ") >= 0);
		}
		
		protected function isContainNumber(val:String):Boolean
		{
			var pattern2:RegExp = /[0-9]/;
			return (pattern2.test(val));
		}
		
		protected function isContainLetter(val:String):Boolean
		{
			var pattern:RegExp = /[a-zA-Z]/;
			return (pattern.test(val));
		}
	}
}