package com.rovicorp.validators
{
	import mx.validators.StringValidator;
	import mx.validators.ValidationResult;
	
	
	/**
	 * zip rule for CA
	 */ 
	public class ExpiredDateValidator extends StringValidator
	{
		override protected function doValidation(value:Object):Array
		{
			var results:Array = super.doValidation(value);
			
			if( value != null && value is Date )
			{
				var today:Date = new Date();
				var currentDate:Date = new Date( today.getFullYear(), today.getMonth());
								
				if (value < currentDate)
				{
					results.push(new ValidationResult(true, null, "ExpiredDateFail", "Invalid Expiration Date."));
				}
			}
			return results;
		}
	}
}