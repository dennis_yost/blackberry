package com.rovicorp.validators
{
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	public class PasswordValidator extends Validator
	{
		private var results:Array;
		
		public function PasswordValidator()
		{
			super();
		}
		
		public var confirmationSource: Object;
		public var confirmationProperty: String;
		
		override protected function doValidation(value:Object):Array {
			
			var results:Array = super.doValidation(value.password);
			
			if (value.password != value.confirmation) {
				results.push(new ValidationResult(true, null, "Mismatch",
					"Password confirmation doesn't match your new password"));
				
			}
			
			return results;
		}       
		
		/**
		 *  @private
		 *  Grabs the data for the confirmation password from its different sources
		 *  if its there and bundles it to be processed by the doValidation routine.
		 */
		override protected function getValueFromSource():Object
		{
			var value:Object = {};
			
			value.password = super.getValueFromSource();
			
			if (confirmationSource && confirmationProperty)
			{
				value.confirmation = confirmationSource[confirmationProperty];
			}
			
			return  value;
		}               
		
	}
}