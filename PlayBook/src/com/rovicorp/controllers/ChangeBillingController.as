package com.rovicorp.controllers {
	import com.rovicorp.controls.CVVToolTip;
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.controls.QNXTextInputWrapper;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.ChangeBillingView;
	import com.rovicorp.views.View;
	import com.sonic.StreamAPIWse;
	import com.sonic.User;
	import com.sonic.response.Auth.AuthTokenResponse;
	import com.sonic.response.StandardResponse;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.core.IVisualElement;
	import mx.events.FlexMouseEvent;
	import mx.events.ValidationResultEvent;
	import mx.managers.IFocusManagerComponent;
	import mx.managers.PopUpManager;
	import mx.rpc.AsyncResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	import qnx.events.PPSEvent;
	import qnx.input.IMFConnection;
	import qnx.ui.text.KeyboardType;
	
	import spark.components.TextInput;
	import spark.layouts.VerticalLayout;
		
	
	
	public class ChangeBillingController extends MiscViewController {
		private var _playbookVideoStore:PlaybookVideoStore;
		private var _view:ChangeBillingView;
		private var validationResult:Array;
		private var billingValidators:Array;
		private var _cvvToolTip:CVVToolTip;
		private var focusMap:Dictionary;
		
		public function ChangeBillingController() {
			super();
		}
		
		protected override function onCreationComplete():void {
			_view = view as ChangeBillingView;
			_view.currentState = "normal";
			_view.addEventListener('cvv', _onCVV);
			_view.addEventListener('submit', _onSubmit);
			_view.statesList.addEventListener(MouseEvent.CLICK,dropdownClicked);
			_view.ccNumberInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
//			_view.cvvInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			_view.ccNameInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			_view.billingAddress1Input.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			_view.billingAddress2Input.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			_view.cityInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			_view.zipInput.addEventListener(DeviceEvent.RETURN_PRESSED,_onSubmit);
			_view.back.addEventListener(MouseEvent.CLICK, onBack);
			
			billingValidators = [_view.ccNumberValidator,_view.ccNameValidator, _view.expiredDateValidator,
				_view.billingAddress1Validator,
				_view.cityValidator,_view.stateValidator];
			
			focusMap = new Dictionary(true);
			focusMap[_view.ccNumberInput] = _view.ccNameInput;
			//focusMap[_view.cvvInput] = _view.ccNameInput;
			focusMap[_view.ccNameInput] = null;
			focusMap[_view.billingAddress1Input] = _view.billingAddress2Input;
			focusMap[_view.billingAddress2Input] = _view.cityInput;
			focusMap[_view.cityInput] = null;
			focusMap[_view.zipInput] = null;
			
			var country:String = CONFIG::location;	
			
			if( country == "usa" )
			{
				billingValidators.push(_view.zipValidator);
			}else
			{
				billingValidators.push(_view.CAZipValidator);
				
				_view.zipInput.restrictString = "0-9 A-Z";
				_view.zipInput.maxChars = 7;
				_view.zipInput.keyboardType=qnx.ui.text.KeyboardType.PASSWORD;
				
				_view.stateValidator.lowerThanMinError = "Enter a province";
			}
		}
				
		private function _onSubmit($e:Event):void {
			CONFIG::device{ IMFConnection.imfConnection.hideInput(); }
			validateAndSubmit();
		}
		
		private function dropdownClicked(event:Event):void {
			CONFIG::device{ IMFConnection.imfConnection.hideInput(); }
		}
		
		private function _onCVV($e:Event):void {
			CONFIG::device{ IMFConnection.imfConnection.hideInput(); }
			_cvvToolTip = PopUpManager.createPopUp(_view, CVVToolTip, true) as CVVToolTip;
			_cvvToolTip.move(750,150);
			_cvvToolTip.addEventListener(Event.CLOSE,onCVVClose);
			_cvvToolTip.addEventListener(FlexMouseEvent.MOUSE_DOWN_OUTSIDE, onCVVClose);
		}
		
		private function onCVVClose(event:Event):void {
			event.target.removeEventListener(Event.CLOSE,onCVVClose);
			event.target.removeEventListener(FlexMouseEvent.MOUSE_DOWN_OUTSIDE, onCVVClose);
			PopUpManager.removePopUp(event.target as IFlexDisplayObject);
		}
		
		private function setBillingInfo():void {
			var request:RoxioNowDAO = new RoxioNowDAO();
			
			var country:String = CONFIG::location;
			
			var state:String = _view.statesList.selectedItem.label.toString();
			
			if(request.isLoggedIn) {
				_view.currentState = "loading";
				request.addEventListener(FaultEvent.FAULT, _onSetBillingInfoFault);
				request.setUserBillingInfo(
					_view.ccNumberInput.text,
					_view.exp_date_picker.formatedValue,
					_view.ccNameInput.text,
					_view.billingAddress1Input.text,
					_view.billingAddress2Input.text,
					_view.cityInput.text,
					state,
					country.toUpperCase(),
					_view.zipInput.text,
					"",
					_onSetBillingInfoResult
				);
			}
		}
		
		private function validateAndSubmit():void {
			if( _view.currentState == "loading" ) return;
			
			var errorMessageArray:Array = [];
			validationResult = Validator.validateAll(billingValidators);

			if (validationResult.length > 0) {
				var error:ValidationResultEvent;
				
				for each (error in validationResult) {
					errorMessageArray.push(error.message);
				}
			}
					
			if (errorMessageArray.length > 0) {
				var dialog:MessageDialog = new MessageDialog();
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Change Billing Validation";
				dialog.warning = true;
				dialog.message = errorMessageArray.join("\n");
				dialog.height += 20 * (errorMessageArray.length - 1);	// TODO: Remove when dialog can resize
				dialog.show();
			} else {
				
				var zip:String = _view.zipInput.text;
				if( CONFIG::location == "canada" && zip.length == 6 )
				{
					_view.zipInput.text = zip.substr(0,3) + " " + zip.substr(3);
				}
				setBillingInfo();
			}
		}
		
		private function _onSetBillingInfoResult(response:StandardResponse):void {
			trace(' billing information,response: '+response.ResponseMessage);
			_view.currentState = "normal";
			var dialog:MessageDialog = new MessageDialog();
			
			if (response.ResponseCode == 0)
				dialog.addEventListener(Event.SELECT, onMessageSelect, false, 0, true);

			dialog.title = "Billing Information";
			dialog.message = (response.ResponseCode == 0) ? 
				"Your billing information has been updated" : 
				"There was an error updating your billing information. Please check your information and try again. We are currently experiencing issues with Amex card. Please use Visa or Master Card.";
			dialog.show();
		}

		private function _onSetBillingInfoFault(event:FaultEvent):void {
			_view.currentState = "normal";
			view.dispatchEvent(new FaultEvent(FaultEvent.FAULT, true, true, event.fault, event.token, event.message));
		}
		
		private function onMessageSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onMessageSelect);
			goBack();
		}
		
		private function onErrorSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onErrorSelect);
		}
		
		private function setNextFocus(event:DeviceEvent):void
		{
			if (focusMap[event.target] == null)
			{
				resetState();
			}
			else if (focusMap[event.target] is QNXTextInputWrapper)
			{
				(focusMap[event.target] as QNXTextInputWrapper).setFocus();
			}
			else
			{
				var nextFocusObj:IFocusManagerComponent = focusMap[event.target];
				_view.focusManager.setFocus(nextFocusObj);
			}
		}
		
		private function resetState(event:MouseEvent=null):void
		{
			_view.stage.focus = null;
			
			CONFIG::device
			{
				IMFConnection.imfConnection.hideInput();
			}
		}
	}
}