package com.rovicorp.controllers
{
	import com.rovicorp.controls.Dialog;
	import com.rovicorp.controls.EULADialog;
	import com.rovicorp.controls.FullScreenCanvas;
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.controls.PurchaseDialog;
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.MainAppEvent;
	import com.rovicorp.events.MenuBarEvent;
	import com.rovicorp.events.MessageBoxEvent;
	import com.rovicorp.events.PurchaseEvent;
	import com.rovicorp.events.RoxioNowMediaPlayerEvent;
	import com.rovicorp.events.SelectionEvent;
	import com.rovicorp.events.TitleEvent;
	import com.rovicorp.events.UIEvent;
	import com.rovicorp.events.VideoStoreEvent;
	import com.rovicorp.model.DownloadListing;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.model.TitleListingPurchased;
	import com.rovicorp.service.BackgroundService;
	import com.rovicorp.service.ExpiredRentalCleanService;
	import com.rovicorp.service.download.DownloadManager;
	import com.rovicorp.service.download.LicenseEvent;
	import com.rovicorp.service.download.LicenseRequest;
	import com.rovicorp.service.download.LicenseStatus;
	import com.rovicorp.utilities.LogUtil;
	import com.rovicorp.utilities.StringUtil;
	import com.rovicorp.views.AccountCreationView;
	import com.rovicorp.views.ChangeBillingView;
	import com.rovicorp.views.ChangePasswordView;
	import com.rovicorp.views.FeaturedView;
	import com.rovicorp.views.ForgotPasswordView;
	import com.rovicorp.views.LibraryView;
	import com.rovicorp.views.LocationErrorView;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.RoxioNowMediaPlayer;
	import com.rovicorp.views.SearchView;
	import com.rovicorp.views.SettingsView;
	import com.rovicorp.views.SignInView;
	import com.roxionow.flash.util.crypto.XOREncryption;
	import com.sonic.StreamEvent;
	import com.sonic.response.Library.FullTitlePurchased;
	import com.sonic.response.StandardResponse;
	import com.sonic.response.Stream.GetStreamingAssetLocationResponse;
	import com.sonic.response.TitleData.BonusAsset;
	import com.sonic.response.TitleData.Product;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;
	import mx.managers.PopUpManager;
	import mx.resources.IResourceBundle;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.FaultEvent;
	
	import net.rim.aircommons.download.taskmanager.FileTransferTask;
	import net.rim.aircommons.download.taskmanager.Task;
	
	import qnx.events.DeviceBatteryEvent;
	import qnx.events.QNXApplicationEvent;
	import qnx.events.QNXSystemEvent;
	import qnx.net.NetworkManager;
	import qnx.net.NetworkType;
	import qnx.system.QNXApplication;
	import qnx.system.QNXSystem;
	
	import spark.components.View;
	import spark.components.ViewNavigator;
	import spark.events.IndexChangeEvent;
	import spark.transitions.ViewTransitionBase;

	public class ApplicationController
	{
		private var appXML:XML = NativeApplication.nativeApplication.applicationDescriptor;
		private var ns:Namespace = appXML.namespace();
	
		private var _playbookVideoStore:PlaybookVideoStore;
		private var _roxionowDaoInited:Boolean = false;
		
		private var _purchaseDialog:PurchaseDialog;
		private var _mediaPlayer:RoxioNowMediaPlayer;
		
		private var _eventCenter:EventCenter = EventCenter.inst;
		private var _service:RoxioNowDAO;
		private var _preState:String = "normal";
		private var _isPopUpDlgShowing:Boolean = false;
		private var _needShowBatteryLowWarning10:Boolean = true;
		private var _needShowBatteryLowWarning2:Boolean = true;
		
		private var _curTitleName:String = "";
		private var _curPlayTitleStopPos:uint;
		private var _currentWatchItem:TitleListingPurchased;
		private var _currentWatchItemParam:Object;
		private var _currentWatchDB:Object;
		private var _resumePlayback:Boolean = false;
		private var _reInitAPIAfterVideoPlayEnd:Boolean = false;
		
		private var _lastIndex:int;
		private var _searchData:Object = new Object();
		
		private var _appReadyEvent:StreamEvent;
		private var _currentShowingDialog:Dialog;
		
		//private var copyfile:File;
		//private var dbPath:String;
		private var dbController:DatabaseController;
		private var _fsCav:FullScreenCanvas;
		
		public var updateStartupMode:Boolean = false;
		public var startupModeOnline:Boolean = true;
		
		static private var nullTransition:ViewTransitionBase;
		
		private const DATABASE_FILE_NAME:String = "videostore.db";
		private const LOCAL_FILE_PREFIX:String = "assets/";
		
		public function ApplicationController()
		{
			RoxioNowDAO.VERSION = appXML.ns::versionNumber;
			RoxioNowDAO.BUILD = appXML..ns::buildId;
			setupListeners();
			
			nullTransition = new ViewTransitionBase();
			nullTransition.duration = 0;
		}
		
		public function get featuredView():Class
		{
			return RoxioNowDAO.api.DeviceSettings.CountryID == 1 ? FeaturedView : LocationErrorView;
		}
		
/*		private function onDatabaseCopyFail(e:IOErrorEvent):void
		{
			copyfile.removeEventListener(Event.COMPLETE, onDatabaseCopySuccess);
			copyfile.removeEventListener(IOErrorEvent.IO_ERROR, onDatabaseCopyFail);
			copyfile = null;
		}
		
		private function onDatabaseCopySuccess(e:Event):void
		{
			copyfile.removeEventListener(Event.COMPLETE, onDatabaseCopySuccess);
			copyfile.removeEventListener(IOErrorEvent.IO_ERROR, onDatabaseCopyFail);
			copyfile = null;
			
			dbController = new DatabaseController(dbPath);
		}
*/		
		/**
		 * Traces an error thrown by the services when trying to set the device
		 * @param event information of the event
		 * 
		 */	
		public function onApplicationFault(event:StreamEvent): void {
			trace("RoxioNowDAO.api fail", RoxioNowDAO.api_init_try_count);
			NetworkStateController.inst.startUpWithNoInternet = false;
			NetworkStateController.inst.internetAccessAvailable = false;
			_roxionowDaoInited = false;
			
			if (RoxioNowDAO.api_init_try_count == 1)
			{
				var resourceMgr:IResourceManager = ResourceManager.getInstance();
				var dialog:MessageDialog = new MessageDialog();
				dialog.title = appXML.ns::name;
				dialog.heading = resourceMgr.getString('LocalizedStrings', 'ERROR_512_MESSAGE');
				
				if (!_service.settings.acceptedEULA)
				{
					dialog.addEventListener(Event.SELECT, onNoInternetNoEULADone, false, 0, true);
					dialog.message = resourceMgr.getString('LocalizedStrings', 'NO_INTERNET_NO_EULA_DLG', [appXML..ns::name]);
					dialog.btnClose.label = resourceMgr.getString('LocalizedStrings', 'MESSAGE_DIALOG_BUTTON_EXIT');
				}
				else
				{
					dialog.addEventListener(Event.SELECT, onNoInternetGoToMyLibrary, false, 0, true);
					dialog.message = resourceMgr.getString('LocalizedStrings', 'NO_INTERNET_GO_TO_LIBRARY_DLG', [appXML..ns::name]);
					dialog.btnClose.label = resourceMgr.getString('LocalizedStrings', 'MESSAGE_DIALOG_BUTTON_OK');
				}
				
				dialog.height += 40;
				showDialog(dialog);
				
				setupMainTab(false);
			}
			
			DownloadListController.getIntance();
			
			updateLocaliztionString();
		}
		
		private function showDialog(dialog:Dialog):void
		{
			if (_currentShowingDialog != null)
			{
				_currentShowingDialog.hide();
			}
			
			_isPopUpDlgShowing = true;
			dialog.setFocus();
			dialog.show();
			_currentShowingDialog = dialog;
		}
		
		private function onNoInternetNoEULADone(e:Event):void
		{
			_isPopUpDlgShowing = false;
			_currentShowingDialog = null;
			NativeApplication.nativeApplication.exit();
		}
		
		private function onSystemOfflineMsg(e:MainAppEvent):void
		{
			var resourceMgr:IResourceManager = ResourceManager.getInstance();
			var dialog:MessageDialog = new MessageDialog();
			dialog.title = resourceMgr.getString('LocalizedStrings', 'SYSTEM_OFFLINE_MESSAGE_DLG_TITLE');
			dialog.addEventListener(Event.SELECT, onSystemOfflineMessageDone, false, 0, true);
			dialog.message = e.offlineMessage;
			dialog.btnClose.label = resourceMgr.getString('LocalizedStrings', 'MESSAGE_DIALOG_BUTTON_EXIT');
			dialog.height = 300;
			showDialog(dialog);
		}
		
		private function onSystemOfflineMessageDone(e:Event):void
		{
			_isPopUpDlgShowing = false;
			_currentShowingDialog = null;
			NativeApplication.nativeApplication.exit();
		}
		
		private function setupMainTab(onlineMode:Boolean):void
		{
			if (_playbookVideoStore == null)
			{
				updateStartupMode = true;
				startupModeOnline = onlineMode;
			}
			else
			{
				_playbookVideoStore.cleanUpViews();
				_playbookVideoStore.setupMainTab(onlineMode);
			}
		}
		
		/**
		 * It sets the display objects when the application is loaded
		 * @param event information of the event
		 * 
		 */	
		public function onApplicationReady(event:StreamEvent): void {
			
			trace("RoxioNowDAO.api ready", event.toString());
			
			_roxionowDaoInited = true;
			NetworkStateController.inst.startUpWithNoInternet = true;
			NetworkStateController.inst.internetAccessAvailable = true;
			
			_service.removeEventListener(StreamEvent.READY, onApplicationReady);
			_service.removeEventListener(StreamEvent.FAULT, onApplicationFault);
			_service.addEventListener(FaultEvent.FAULT, onFault, false, 0, true);

			if (RoxioNowDAO.api_init_try_count > 1)
			{
				_appReadyEvent = event;
				
				// show dialog
				var resourceMgr:IResourceManager = ResourceManager.getInstance();
				var dialog:MessageDialog = new MessageDialog();
				dialog.title = appXML.ns::name;
				dialog.heading = resourceMgr.getString('LocalizedStrings', 'CONNECTION_RESUME_DLG_TITLE');
				dialog.addEventListener(Event.SELECT, onInternetResumeToResetApp, false, 0, true);
				dialog.message = resourceMgr.getString('LocalizedStrings', 'CONNECTION_RESUME_DLG_CONTENT');
				dialog.btnClose.label = resourceMgr.getString('LocalizedStrings', 'MESSAGE_DIALOG_BUTTON_OK');
				showDialog(dialog);
			}
			else
			{
				prepareContent(event);
			}
			
			CONFIG::debug
			{
				var conf:Object = _service.settings.conf;				
				if( conf && conf.Debug_host )
				{
					XOREncryption.key = XOREncryption.defaultKey;
					LogUtil.init(_playbookVideoStore, XOREncryption.decrypt(conf.Debug_host), Number(XOREncryption.decrypt(conf.Debug_port)));
				}
				else
				{
					LogUtil.init(_playbookVideoStore);
				}
			}
			
			updateLocaliztionString();
		}
		
		private function updateLocaliztionString():void
		{
			var resourceMgr:IResourceManager = ResourceManager.getInstance();
			var rb:IResourceBundle = resourceMgr.getResourceBundle("en_US", "LocalizedStrings");
			
			var country_name:String = _service.settings.country_name;
			
			//usa, canada
			if( country_name == "canada" )
			{
				rb.content["CURRENCY"] = "$";
				rb.content["LOCATION"] = "Canada";
				rb.content["ZIP"] = "Postal code:";
				rb.content["STATE"] = "Province";
			}
			else
			{
				rb.content["CURRENCY"] = "$";
				rb.content["LOCATION"] = "US territories";
			}
		}
		
		private function onInternetResumeToResetApp(e:Event):void
		{
			_isPopUpDlgShowing = false;
			_currentShowingDialog = null;
			prepareContent(_appReadyEvent);
		}
		
		protected function prepareContent(event:StreamEvent):void
		{
			setupMainTab(true);
			
			var params:Object = event.params;
			
			// validate authtoken
			if (params.AuthTokenActive == "False")
				_service.logoutUser();
			
			var country_name:String = _service.settings.country_name;
			
			var isValidLocation:Boolean = false;
			//check location
			switch( country_name )
			{
				case "usa":
					RoxioNowDAO.CURRENT_COUNTRY_ID = RoxioNowDAO.COUNTRY_US;
					isValidLocation = (params.CountryID == RoxioNowDAO.COUNTRY_US);
					break;
				case "canada":
					RoxioNowDAO.CURRENT_COUNTRY_ID = RoxioNowDAO.COUNTRY_CANADA;
					isValidLocation = (params.CountryID == RoxioNowDAO.COUNTRY_CANADA);
					break;
			}	
			trace("params.CountryID:" + params.CountryID +":" + country_name);
			
			if (isValidLocation == false)
			{
				_playbookVideoStore.currentState = "loaded";
//dpy(20140625)-->				_playbookVideoStore.featuredNavigatorView.firstView = LocationErrorView;
//dpy(20140625)-->				_playbookVideoStore.moviesNavigatorView.firstView = LocationErrorView;
//dpy(20140625)-->				_playbookVideoStore.tvShowsNavigatorView.firstView = LocationErrorView;
				_playbookVideoStore.libraryNavigatorView.firstView = LibraryView;
//dpy(20140625)-->				_playbookVideoStore.wishListNavigatorView.firstView = LocationErrorView;

//dpy(20140625)--> Use the Downloads page to show out of region message!
//
				_playbookVideoStore.downloadsNavigatorView.firstView = LocationErrorView;
				_playbookVideoStore.mainTabs.selectedIndex = 1;
//
//<--dpy(20140625)

			}
			else
			{
				_service.featured.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onFeaturedPropertyChange, false, 0, true);
			}
			
			if (_service.settings.acceptedEULA)
				_service.settings.acceptedEULA = ((params.DisplayEula == "False") || (params.DisplayEula == "false"));
			
			if (!_service.settings.acceptedEULA)
			{
				var dialog:EULADialog = new EULADialog();
				dialog.addEventListener(Event.SELECT, onEULASelect);
				dialog.show();
			}
			else
			{
				showDataChargeWarning();
			}
			
			_appReadyEvent = null;
		}
		
		protected function onNoInternetGoToMyLibrary(e:Event):void
		{
			_isPopUpDlgShowing = false;
			_currentShowingDialog = null;
			_playbookVideoStore.mainTabs.selectedIndex = 0;
		}
		
		private function onEULASelect(event:Event):void
		{
			var dialog:EULADialog = event.target as EULADialog;
			dialog.removeEventListener(Event.SELECT, onEULASelect);			
			//decline
			if( dialog.selectedIndex == 0 )
			{
				NativeApplication.nativeApplication.exit(0);
				return;
			}
			
			if (!_service.settings.acceptedEULA)
			{
				_service.settings.acceptedEULA = true;	
				_service.acceptEula(null);
			}
			
			showDataChargeWarning();
		}
		
		private function showDataChargeWarning():void
		{
			//check whether to show dialog
			if( _service.settings.dataChargeWarning )
			{
				var resourceMgr:IResourceManager = ResourceManager.getInstance();
				var dialog:MessageDialog = new MessageDialog();
				dialog.title = resourceMgr.getString('LocalizedStrings', 'WARNING_TITLE');
				dialog.currentState = "warningWithcheckbox";
				dialog.checkboxMessage = resourceMgr.getString('LocalizedStrings', 'NOT_DISPLAY_AGAIN');
				dialog.addEventListener(Event.SELECT, onDataChargeWarning, false, 0, true);
				dialog.message = resourceMgr.getString('LocalizedStrings', 'DATA_CHARGE_MESSAGE');
				dialog.btnClose.label = resourceMgr.getString('LocalizedStrings', 'MESSAGE_DIALOG_BUTTON_OK');
				dialog.height = 260;
				dialog.setFocus();
				dialog.show();
			}
		}
		
		
		private function onDataChargeWarning(e:Event):void
		{
			e.target.removeEventListener(Event.SELECT, onDataChargeWarning);
			
			var dialog:MessageDialog = e.target as MessageDialog;
			if( dialog != null )
			{
				if( dialog.checkboxSelected )
				{
					_service.settings.dataChargeWarning = false;
				}
			}
		}
		
		private function onFeaturedPropertyChange(e:PropertyChangeEvent):void
		{			
			_service.featured.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onFeaturedPropertyChange);
			
			if( _playbookVideoStore.currentState != "videoPlayer" )
			{
				_playbookVideoStore.currentState = "loaded"; //_service.featured.state;
			}
			
/*			_playbookVideoStore.moviesNavigatorView.firstView = MoviesCategoriesView;
			_playbookVideoStore.tvShowsNavigatorView.firstView = TVCategoriesView;
			_playbookVideoStore.libraryNavigatorView.firstView = LibraryCategoryView;
			_playbookVideoStore.downloadsNavigatorView.firstView = DownloadsView;
			_playbookVideoStore.wishListNavigatorView.firstView = WishListView; */
			
			//initialize DownloadListController, try to read library list before goto library/download page
			DownloadListController.getIntance();
		}
		
		/**
		 * Creates a local variable to access application elements and
		 * initializes the menu.
		 * @param event information of the event
		 * 
		 */	
		public function onApplicationComplete(event:FlexEvent): void
		{
			var f:File = File.applicationStorageDirectory.resolvePath(DATABASE_FILE_NAME);
			dbController = new DatabaseController(f.nativePath);
			
			_playbookVideoStore = event.target as PlaybookVideoStore;

			_playbookVideoStore.addEventListener(MenuBarEvent.SEARCH,showSearch);
			_playbookVideoStore.addEventListener(MenuBarEvent.SETTINGS,showSettings);
			
			_playbookVideoStore.addEventListener(TitleEvent.WISHLIST_ADD, onWishlistAdd);
			_playbookVideoStore.addEventListener(TitleEvent.WISHLIST_REMOVE, onWishlistRemove);
			_playbookVideoStore.addEventListener(TitleEvent.WATCH_TRAILER, onWatchTrailer);
			_playbookVideoStore.addEventListener(TitleEvent.DOWNLOAD_TITLE, onDownloadTitle);
			_playbookVideoStore.addEventListener(PurchaseEvent.PURCHASE_TITLE, showCheckoutModal);
			
			_playbookVideoStore.addEventListener(SelectionEvent.TITLE_SELECTION_EVENT, onTitleSelected);
			
			_playbookVideoStore.addEventListener(VideoStoreEvent.SIGN_IN, onSignIn);
			_playbookVideoStore.addEventListener(VideoStoreEvent.GO_BACK, onBack);
			_eventCenter.addEventListener(VideoStoreEvent.SIGN_IN, onSignIn);
			_eventCenter.addEventListener(MenuBarEvent.BACK,showPreviousTab);
			
			_playbookVideoStore.addEventListener(VideoStoreEvent.SIGN_OUT, onSignOut);
//dpy(20140625)--> 			_playbookVideoStore.addEventListener(VideoStoreEvent.CREATE_ACCOUNT, onCreateAccount);
			_playbookVideoStore.addEventListener(VideoStoreEvent.VIEW_EULA, onViewEULA);
			_playbookVideoStore.addEventListener(VideoStoreEvent.VIEW_PRIVACY, onViewPrivacy);
			_playbookVideoStore.addEventListener(VideoStoreEvent.VIEW_TERMS, onViewTerms);
			_playbookVideoStore.addEventListener(VideoStoreEvent.CHANGE_PASSWORD, onChangePassword);
			_playbookVideoStore.addEventListener(VideoStoreEvent.FORGOT_PASSWORD, onForgotPassword);
//dpy(20140625)-->			_playbookVideoStore.addEventListener(VideoStoreEvent.CHANGE_BILLING_INFO, onChangeBilling);
			
			_eventCenter.addEventListener(MessageBoxEvent.MESSAGE_OPEN, _onMessageOpen);
			_playbookVideoStore.addEventListener(MessageBoxEvent.MESSAGE_CLOSE, hideModal);
			_playbookVideoStore.addEventListener(FaultEvent.FAULT, onFault);
			
			BackgroundService.getInstance().addTask(new ExpiredRentalCleanService());
		}
		
		private function setupListeners():void
		{
			_service = new RoxioNowDAO();
			_service.addEventListener(StreamEvent.READY,onApplicationReady, false, 0, true);
			_service.addEventListener(StreamEvent.FAULT,onApplicationFault, false, 0, true);

			_eventCenter.addEventListener(UIEvent.SHOW_SECONDARY_MENU, onShowSecondaryMenu, false, 0, true);
			_eventCenter.addEventListener(UIEvent.HIDE_SECONDARY_MENU, onHideSecondaryMenu, false, 0, true);
			_eventCenter.addEventListener(UIEvent.SWITCH_SCREEN, onSwitchScreen, false, 0, true);
			_eventCenter.addEventListener(UIEvent.POPUP_NO_INTERNET_DIALOG, onPopUpNoInternetDialog, false, 0, true);
			_eventCenter.addEventListener(MainAppEvent.SYSTEM_TIME_FORMAT_ERROR, onSystemTimeFormatError, false, 0, true);
			
			_eventCenter.addEventListener(DeviceEvent.NETWORK_AVAILABILITY_CHANGE, onNetworkAvailabilityChange, false, 0, true);
			_eventCenter.addEventListener(MainAppEvent.SYSTEM_OFFLINE_MESSAGE, onSystemOfflineMsg, false, 0, true);
			
			_eventCenter.addEventListener(TitleEvent.WATCH_TITLE, onWatchTitle, false, 0, true);
			
			// app actrivate or deactivate
			NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, onAppActivate, false, 0, true);
			NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, onAppDeactivate, false, 0, true);
			NativeApplication.nativeApplication.addEventListener(Event.EXITING, onAppExisting, false, 0, true);
			
			CONFIG::device
			{
				NetworkManager.networkManager.addEventListener(Event.CHANGE,onNetworkChanged); 				
				
				// low memory
				QNXApplication.qnxApplication.addEventListener(QNXApplicationEvent.LOW_MEMORY, onDeviceLowMemory, false, 0, true);
				
				// battery
//				Device.device.batteryMonitoringEnabled = true;
//				Device.device.addEventListener(DeviceBatteryEvent.LEVEL_CHANGE, onDeviceBatteryLevelChange, false, 0, true);
				
				// swipe down
				QNXApplication.qnxApplication.addEventListener(QNXApplicationEvent.SWIPE_DOWN, onSwipeDown, false, 0, true);
				
				// system event
				QNXSystem.system.addEventListener(QNXSystemEvent.ACTIVE, onSystemActive, false, 0, true);
				QNXSystem.system.addEventListener(QNXSystemEvent.STANDBY, onSystemStandby, false, 0, true);
				
				QNXSystem.system.transitionTime = 500;
			}
		}
		
		protected function onSystemTimeFormatError(event:MainAppEvent):void
		{
			var resourceMgr:IResourceManager = ResourceManager.getInstance();
			var dialog:MessageDialog = new MessageDialog();
			dialog.title = appXML.ns::name;
			dialog.heading = resourceMgr.getString('LocalizedStrings', 'TIME_FORMAT_ERROR_DLG_TITLE', [appXML..ns::brand]);
			dialog.addEventListener(Event.SELECT, onSystemTimeFormatErrorDlg, false, 0, true);
			dialog.message = resourceMgr.getString('LocalizedStrings', 'TIME_FORMAT_ERROR_DLG_CONTENT', [appXML..ns::brand]);
			dialog.btnClose.label = resourceMgr.getString('LocalizedStrings', 'MESSAGE_DIALOG_BUTTON_OK');
			dialog.setFocus();
			dialog.height = 265;
			dialog.show();
		}
		
		protected function onSystemTimeFormatErrorDlg(event:Event):void
		{
			NativeApplication.nativeApplication.exit(0);
		}
		
		protected function onAppActivate(event:Event):void
		{
			LogUtil.debug("App is now resume to be activated.");
			
			// media player related process
			if (_mediaPlayer != null)
			{
				_mediaPlayer.resumePlayer();
			}
		}
		
		protected function onAppDeactivate(event:Event):void
		{
			LogUtil.debug("App is deactiavted.");
			
			// media player related process
			if (_mediaPlayer != null)
			{
				savePlayPosition();
				_mediaPlayer.holdPlayer();
			}
		}
		
	CONFIG::device
	{
		protected function onSystemActive(event:QNXSystemEvent):void
		{
			trace("SYSTEM is ACTIVE.");
		}
		
		protected function onSystemStandby(event:QNXSystemEvent):void
		{
			trace("SYSTEM is STANDBY.");
		}
	}
	
		protected function onAppExisting(event:Event):void
		{
			trace("App is existing.");
			
			// media player related process
			if (_mediaPlayer != null)
			{
				event.preventDefault();
				savePlayPosition();
				_mediaPlayer.holdPlayer();
			}
		}
		
		private function savePlayPosition():void			
		{
			//save play position when app deactivated.
			if(_currentWatchDB != null && _mediaPlayer != null)
			{
				LogUtil.debug("save current playback position", _curPlayTitleStopPos, _currentWatchDB._id.toString());
				_currentWatchDB.last_play_position = _curPlayTitleStopPos.toString();
				
				var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO);
				e0.id = _currentWatchDB._id.toString();
				e0.library = _currentWatchDB;
				_eventCenter.dispatchEvent(e0);
				
				_service.library.updatePurchaseTitleInfo(_currentWatchItem.PassID, "lastPlayPosition", _currentWatchDB.last_play_position);
			}
		}
		
		protected function onPopUpNoInternetDialog(event:UIEvent):void
		{
			if (_roxionowDaoInited == false || _isPopUpDlgShowing == true) return;
			var resourceMgr:IResourceManager = ResourceManager.getInstance();
			
			var dialog:MessageDialog = new MessageDialog();
			dialog.addEventListener(Event.SELECT, onPopUpNoInternetDialogDone, false, 0, true);
			dialog.title = resourceMgr.getString('LocalizedStrings', 'NO_CONNECTION_ERROR_TITLE');
			dialog.message = (event.errorMsg == "" ? resourceMgr.getString('LocalizedStrings', 'NO_INTERNET_CONNECTION') : event.errorMsg);
			showDialog(dialog);
		}
		
		protected function onPopUpNoInternetDialogDone(e:Event):void
		{
			_isPopUpDlgShowing = false;
			_currentShowingDialog = null;
			//if (_playbookVideoStore.mainTabs.activeView) _playbookVideoStore.mainTabs.activeView.navigator.popToFirstView(nullTransition);
		}
		
		public function navigatorActiveHandler(event:IndexChangeEvent):void {
			_lastIndex = event.oldIndex;
//			if(event.oldIndex != 6 && event.newIndex != 6){
//			 	if (_playbookVideoStore.mainTabs.activeView) _playbookVideoStore.mainTabs.activeView.navigator.popToFirstView(nullTransition);
//			}
		}
		
		public function onWatchTrailer(e:TitleEvent): void 
		{
			if (e.item && e.item.Details)
			{
				_curTitleName = e.item.Name;
				
				for each (var asset:BonusAsset in e.item.Details.BonusAssets)
				{
					if (asset.BonusType == ',Trailer')
					{
						_service.getBonusAssetLocation(asset.BonusAssetID, onTrailerAssetFetched);
						break;
					}
				}
			}
		}
		
		private function onTrailerAssetFetched(result:GetStreamingAssetLocationResponse):void
		{
			if (result.ResponseCode == 0)
			{
				var url:String = result.StreamingAssetLocation;
				if (url.length == 0)
					url = "http://stgapi.cinemanow.com/ces/3G%20harrypotter%206_H264_514k_3G.mp4";
				
				launchMediaPlayer(url, _curTitleName);
			}
			else
			{
				// TODO: Display an error message	
			}
		}
		
		private function onWatchTitle(e:TitleEvent):void
		{
			var item:TitleListingPurchased = e.item as TitleListingPurchased;
			if( item == null ) return;
			
			var file:File = new File(_service.getAssetLocation(item.PassID));
			
			if (file.exists)
			{
				_currentWatchItem = item;
				_currentWatchItemParam = e.params;
				
				_resumePlayback = true;
				if (e.params && e.params.resumePlayback && e.params.resumePlayback == false) _resumePlayback = false;
				
				// get title info from library table in db
				var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.GET_LIBRARY_INFO);
				e0.id = _currentWatchItem.PassID.toString();
				
				_eventCenter.addEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, onGetLibraryInfoForPlayingTitle, false, 0, true);
				_eventCenter.dispatchEvent(e0);
			}
			else
			{
				DownloadManager.getInstance().removeDownloadItemByID(item.PassID.toString());
				
				var dialog:MessageDialog = MessageDialog.create(null, "ERROR_FILE_NOT_FOUND", "ERROR_FILE_NOT_FOUND_MESSAGE");
				dialog.show();
			}
		}
		
		private function onGetLibraryInfoForPlayingTitle(e:DatabaseEvent):void
		{
			_eventCenter.removeEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, onGetLibraryInfoForPlayingTitle);			
			
			if (e.success == true)
			{
				var diid:Object = e.result[0];
				_currentWatchDB = diid;
				LogUtil.debug("To watch", diid.purchase_type, diid.first_play);
				
				/* we now do this in the mediaplayer
				// check if we have license for this title
				if (diid.license_receieved != 1)
				{
					tryGetLicenseForTitle(diid);
					return;
				}
				*/
				
				// check if we need update first play time
				if ( (!diid.first_play || diid.first_play == "null" ) && diid.purchase_type == 0)
				{
					var resourceMgr:IResourceManager = ResourceManager.getInstance();						
					var dialog:MessageDialog = new MessageDialog();
					
					var hours:int = _currentWatchItem.getRentalPeriod();									
						
					dialog.title = resourceMgr.getString('LocalizedStrings', 'WATCH_Rental_Title')
					dialog.heading = resourceMgr.getString('LocalizedStrings', 'WATCH_Rental_Heading',[StringUtil.convertHtmlToPlainText(_currentWatchItem.Name)])
					dialog.message = resourceMgr.getString('LocalizedStrings', 'WATCH_Rental_First',[hours])
					dialog.currentState = "yesno";
					dialog.addEventListener(Event.SELECT, onPlayRentalConfirm);
					dialog.height = dialog.height + 100;
					dialog.show();
					return;
				}

				$startPlay();
				
			}else
			{
				trace("error: missing downloaded file; add error handler");
			}
		}
		
		public function onPlayRentalConfirm(event:Event): void {
			
			var dialog:MessageDialog = event.target as MessageDialog;
			dialog.removeEventListener(Event.SELECT, onPlayRentalConfirm);			
			//no
			if( dialog.selectedIndex == 1 )
			{
				return;	
			}				
			
			$startPlay();
		}
		
		private function $startPlay():void			
		{
			
			if ( (!_currentWatchDB.first_play || _currentWatchDB.first_play == "null" ) && _currentWatchDB.purchase_type == 0 )
			{
				
				var d:Date = new Date;
				_currentWatchDB.first_play = d.toString();
				_currentWatchItem.First_play = _currentWatchDB.first_play;
				LogUtil.debug("Update title play time to :", _currentWatchDB.first_play);
				var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO);
				e0.id = _currentWatchDB._id.toString();
				
				//update rental's expire date
				if( _currentWatchDB.purchase_type == 0 )
				{						
					var product:Product = (_currentWatchItem.Details as FullTitlePurchased).AvailableProducts.getItemAt(0) as Product;	
					
					var expire_date:Date = new Date();					
					expire_date.setHours(expire_date.getHours() + Number(product.RentalPeriod));
					_currentWatchDB.date_expired = expire_date.toString();
				}				
				_service.library.updatePurchaseTitleInfo(_currentWatchItem.PassID, "First_play", _currentWatchDB.first_play);
				
				e0.library = _currentWatchDB;
				_eventCenter.dispatchEvent(e0);
			}
			
			var so:uint = 0;
						
			// check if we need resume playback from last position
			if (_resumePlayback == true)
			{
				so = parseInt(_currentWatchDB.last_play_position, 10);
			}
		
			var url:String = _service.getAssetLocation(_currentWatchDB._id);
			
			if (url.length)
			{
				var dp:Number = 1;
				if (_currentWatchItem is DownloadListing) dp = (_currentWatchItem as DownloadListing).DownloadPercent;
				launchMediaPlayer(url, _currentWatchItem.Name, dp, so);
			}
			else
			{
				trace("error: missing downloaded file; add error handler");
			}
		}
		
		private function launchMediaPlayer(url:String, tName:String, dPercentage:Number = 1, startOffset:uint = 0):void
		{
			if (_mediaPlayer == null)
			{
				LogUtil.debug("PLAYBACK: ", url, tName, dPercentage, startOffset);
				_playbookVideoStore.currentState = "videoPlayer";
				_playbookVideoStore.skin.currentState = "videoPlayer";
				
				// change the file prefix if the file is still downloading
				if (dPercentage < 1)
				{
					if (url.indexOf("file") == 0)
					{
						url = url.replace("file", "file2b");
					}
					else
					{
						trace("FILE NAME ERROR");
					}
				}
				
				LogUtil.debug("launchMediaPlayer: " + url);
				_mediaPlayer = new RoxioNowMediaPlayer;
				_playbookVideoStore.addElement(_mediaPlayer);
				_mediaPlayer.percentWidth = 100;
				_mediaPlayer.percentHeight = 100;
				
				_mediaPlayer.videoUrl = url;
				_mediaPlayer.titleName = StringUtil.convertHtmlToPlainText(tName);
//				_mediaPlayer.imageUrl = Boxart.URL_203 + _currentWatchItem.BoxartPrefix + Boxart.POST_FIX_203; 
				_mediaPlayer.downloadPercentage = dPercentage;
				_mediaPlayer.cbFn = LogUtil.debug;
				_mediaPlayer.startOffset = startOffset;
				
				// keep an eye on the download percent if needed
				if (_currentWatchItem && _currentWatchItem is TitleListingPurchased)
				{
					var item:TitleListingPurchased = (_currentWatchItem as TitleListingPurchased);
					if (item.DownloadPercent < 1)
					{
						item.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onWatchingTitleDownloadPercentChange, false, 0, true);
					}
				}
				else
				{
					//Don't show download progress when watch trailer
					_mediaPlayer.downloadPercentage = 0;
				}
				
				_eventCenter.addEventListener(RoxioNowMediaPlayerEvent.MEDIAPLAYER_PLAY_STOP, onMediaPlayerStop, false, 0, true);
				_eventCenter.addEventListener(RoxioNowMediaPlayerEvent.MEDIAPLAYER_PLAY_NO_DRM, onMediaPlayerNoDRMFound, false, 0, true);
			}
		}
		
		private function onMediaPlayerNoDRMFound(e:RoxioNowMediaPlayerEvent):void
		{
			tryGetLicenseForTitle(_currentWatchDB);
		}
		
		private function tryGetLicenseForTitle(titleInfo:Object):void
		{
			DownloadManager.getInstance().addEventListener(LicenseEvent.LICENSE_EVENT, onLicenseReceived, false, 0, true);
			var t:Task = DownloadManager.getInstance().getDownloadTaskById(_currentWatchItem.PassID.toString());
			new LicenseRequest(titleInfo, t as FileTransferTask);
		}
		
		private function onLicenseReceived(e:LicenseEvent):void
		{
			LogUtil.debug("onLicenseReceived",e.id, e.status,  _currentWatchItem.PassID);
			
			if( e.id != _currentWatchItem.PassID.toString() || e.status == LicenseStatus.LICENSE_REQUESTED ) return;
			
			DownloadManager.getInstance().removeEventListener(LicenseEvent.LICENSE_EVENT, onLicenseReceived);			
			if (e.status == LicenseStatus.LICENSE_RECEIVED)
			{
				if (_mediaPlayer != null) _mediaPlayer.licenseAcquireDone(true);
			}
			else if (e.status == LicenseStatus.LICENSE_ERROR)
			{
				LogUtil.debug("error: download license error");
				if (_mediaPlayer != null) _mediaPlayer.licenseAcquireDone(false);
			}
		}
		
		private function onWatchingTitleDownloadPercentChange(e:PropertyChangeEvent):void
		{
			var item:TitleListingPurchased = (_currentWatchItem as TitleListingPurchased);
			//trace("DOWNLOAD UPDATE: ", item.DownloadPercent);
			if (_mediaPlayer != null) _mediaPlayer.downloadPercentage = item.DownloadPercent;
		}
		
		private function onMediaPlayerStop(e:RoxioNowMediaPlayerEvent):void
		{
			trace("onMediaPlayerStop");
			// save current position
			if (_currentWatchItem != null)
			{
				_curPlayTitleStopPos = e.curPlayPosition;
				
				savePlayPosition();
				
				_currentWatchItem.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onWatchingTitleDownloadPercentChange);
				_currentWatchItem = null;
				_currentWatchItemParam = null;
			}
			
			disposeMediaPlayer();
		}
		
		private function onMediaPlayerUpdate(e:RoxioNowMediaPlayerEvent):void
		{
			LogUtil.debug("onMediaPlayerUpdate");
			_curPlayTitleStopPos = e.curPlayPosition;
			savePlayPosition();
		}
		/*
		private function onGetLibraryInfoForSavingPlabackPosition(e:DatabaseEvent):void
		{
			_eventCenter.removeEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, onGetLibraryInfoForSavingPlabackPosition);
			
			if (e.success == true)
			{
				trace("save current playback position");
				var diid:Object = e.result[0];
				diid.last_play_position = _curPlayTitleStopPos.toString();
				
				var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO);
				e0.id = diid._id.toString();
				e0.library = diid;
				_eventCenter.dispatchEvent(e0);
			}
		}
		*/
		private function disposeMediaPlayer():void
		{
			if (_mediaPlayer != null)
			{
				_mediaPlayer.dispose();
				_playbookVideoStore.removeElement(_mediaPlayer);
				_mediaPlayer = null;
				
				_eventCenter.removeEventListener(RoxioNowMediaPlayerEvent.MEDIAPLAYER_PLAY_STOP, onMediaPlayerStop);
				_eventCenter.removeEventListener(RoxioNowMediaPlayerEvent.MEDIAPLAYER_PLAY_NO_DRM, onMediaPlayerNoDRMFound);
				
				_playbookVideoStore.currentState = "loaded";
				_playbookVideoStore.skin.currentState = "normal";
			}
			
			if (_reInitAPIAfterVideoPlayEnd == true)
			{
				_reInitAPIAfterVideoPlayEnd = false;
				_eventCenter.dispatchEvent(new MainAppEvent(MainAppEvent.RE_INIT_API));
			}
		}
		
		private function onShowSecondaryMenu(e:UIEvent):void
		{
			if (_playbookVideoStore.mainTabs.skin.currentState != "secondaryMenu")
			{
				_preState = _playbookVideoStore.mainTabs.skin.currentState;
				_playbookVideoStore.mainTabs.skin.currentState = "secondaryMenu";
			} else {
				EventCenter.inst.dispatchEvent(new UIEvent(UIEvent.HIDE_SECONDARY_MENU));
			}
		}
		
		private function onHideSecondaryMenu(e:UIEvent):void
		{
			_playbookVideoStore.mainTabs.skin.currentState = _preState;
		}
		
		private function onSwitchScreen(e:UIEvent):void
		{
			switch (e.targetScreen)
			{
/*dpy(20140625)-->
				case "downloadsNavigatorView":
					_playbookVideoStore.mainTabs.selectedIndex = 4;
					break;
				
				case "mylibraryNavigatorView":
					_playbookVideoStore.mainTabs.selectedIndex = 3;
<--dpy(20140625)*/

				case "downloadsNavigatorView":
					_playbookVideoStore.mainTabs.selectedIndex = 1;
					break;
				
				case "mylibraryNavigatorView":
					_playbookVideoStore.mainTabs.selectedIndex = 0;
			}
		}
		
		private function onDeviceBatteryLevelChange(e:DeviceBatteryEvent):void
		{
			LogUtil.debug("onDeviceBatteryLevelChange", e.batteryLevel, e.batteryState);
			if (e.batteryLevel > 10) _needShowBatteryLowWarning10 = true;
			if (e.batteryLevel > 2) _needShowBatteryLowWarning2 = true;
			
			if (e.batteryLevel <= 10 && e.batteryLevel > 2 && _needShowBatteryLowWarning10 == true)
			{
				showLowBetteryWarning10();
			}
			else if (e.batteryLevel <= 2 && _needShowBatteryLowWarning2 == true)
			{
				showLowBetteryWarning2();
			}
		}
		
		private function showLowBetteryWarning10():void
		{
			trace("showLowBetteryWarning10");
			_needShowBatteryLowWarning10 = false;
			
			var resourceMgr:IResourceManager = ResourceManager.getInstance();
			var dialog:MessageDialog = new MessageDialog();
			dialog.title = resourceMgr.getString('LocalizedStrings', 'WARNING_TITLE');
			dialog.heading = resourceMgr.getString('LocalizedStrings', 'LOW_BATTERY_10_DLG_TITLE');
			dialog.message = resourceMgr.getString('LocalizedStrings', 'LOW_BATTERY_10_DLG_CONTENT');
			dialog.btnClose.label = resourceMgr.getString('LocalizedStrings', 'MESSAGE_DIALOG_BUTTON_OK');
			dialog.addEventListener(Event.SELECT, onLowBatteryDismiss, false, 0, true);
			dialog.setFocus();
			dialog.show();
			
			// media player related process
			if (_mediaPlayer != null)
			{
				_mediaPlayer.holdPlayer();
			}
		}
		
		private function onLowBatteryDismiss(e:Event):void
		{
			// media player related process
			if (_mediaPlayer != null)
			{
				_mediaPlayer.resumePlayer();
			}
		}
		
		private function showLowBetteryWarning2():void
		{
			trace("showLowBetteryWarning2");
			_needShowBatteryLowWarning2 = false;
			
			var resourceMgr:IResourceManager = ResourceManager.getInstance();
			var dialog:MessageDialog = new MessageDialog();
			dialog.title = resourceMgr.getString('LocalizedStrings', 'WARNING_TITLE');
			dialog.title = resourceMgr.getString('LocalizedStrings', 'LOW_BATTERY_2_DLG_TITLE');
			dialog.message = resourceMgr.getString('LocalizedStrings', 'LOW_BATTERY_2_DLG_CONTENT');
			dialog.btnClose.label = resourceMgr.getString('LocalizedStrings', 'MESSAGE_DIALOG_BUTTON_OK');
			dialog.setFocus();
			dialog.show();
			
			// media player related process
			if (_mediaPlayer != null)
			{
				_mediaPlayer.stopVideo();
			}
		}
		
		private function onDeviceLowMemory(e:QNXApplicationEvent):void
		{
			//var e1:DeviceEvent = new DeviceEvent(DeviceEvent.LOW_MEMORY);
			//_eventCenter.dispatchEvent(e1);
		}
		
		private function onSwipeDown(e:QNXApplicationEvent):void
		{
			if( _playbookVideoStore.currentState == "videoPlayer" ) return;
			
			var e1:UIEvent = new UIEvent(UIEvent.SHOW_SECONDARY_MENU);
			_eventCenter.dispatchEvent(e1);
		}
		
		private function onNetworkChanged(e:Event = null):void
		{			
			var types:Array = new Array();
			types.push(NetworkType.WIFI);			
			types.push(NetworkType.CELLULAR);
			types.push(NetworkType.BRIDGE_BES);
			types.push(NetworkType.BRIDGE_BIS);
			types.push(NetworkType.BLACKBERRY_MDS);
			types.push(NetworkType.VPN);
			
			var networkavailable:Boolean = false;
			for( var i:int = 0,len:int = types.length;i<len;i++)
			{
				if( NetworkManager.networkManager.isConnected(types[i]) )
				{
					networkavailable = true;
					break;
				}
			}
			
			if( NetworkStateController.inst.internetAccessAvailable == networkavailable ) return;
			
			NetworkStateController.inst.internetAccessAvailable = networkavailable;
			var e1:DeviceEvent = new DeviceEvent(DeviceEvent.NETWORK_AVAILABILITY_CHANGE);
			e1.networkAvailable = networkavailable;
			_eventCenter.dispatchEvent(e1);
		}
		
		private function onNetworkAvailabilityChange(e:DeviceEvent):void
		{
			if (e.networkAvailable == true && RoxioNowDAO.apiInitState != RoxioNowDAO.API_INIT_STATE_LOADED)
			{
				if (_mediaPlayer != null && _playbookVideoStore != null && _playbookVideoStore.currentState == "videoPlayer")
				{
					// user is playing video now, try to reinit after video plays end
					_reInitAPIAfterVideoPlayEnd = true;
				}
				else
				{
					_eventCenter.dispatchEvent(new MainAppEvent(MainAppEvent.RE_INIT_API));
				}
			}
			
			if (e.networkAvailable == false &&  _service.downloadList.length > 0 )
			{
				var dialog:MessageDialog = MessageDialog.create(null, 'ERROR_512_MESSAGE', 'ERROR_512_INSTRUCTION');
				dialog.addEventListener(Event.SELECT, onMessageClose, false, 0, true);
				showDialog(dialog);
			}
			
			if (e.networkAvailable == true && RoxioNowDAO.apiInitState == RoxioNowDAO.API_INIT_STATE_LOADED &&  _service.downloadList.length > 0 )
			{
				if( _currentShowingDialog != null )
					_currentShowingDialog.removeEventListener(Event.SELECT, onMessageClose, false);
				
				var dialog2:MessageDialog = MessageDialog.create(null, 'CONNECTION_RESUME_DLG_TITLE', 'CONNECTION_RESUME_DLG_CONTENT');
				showDialog(dialog2);
			}
		}
		
		private function onMessageClose(e:Event):void
		{
			e.target.removeEventListener(Event.SELECT, onMessageClose, false);
			_isPopUpDlgShowing = false;
			_currentShowingDialog = null;
		}

		public function onWishlistAdd(event:TitleEvent): void 
		{
			if (_service.isLoggedIn)
				_service.addItemToWishList(event.item.TitleID);
			else
				ViewNavigator(_playbookVideoStore.mainTabs.selectedNavigator).pushView(SignInView, event.clone());
		}
		
		public function onWishlistRemove(event:TitleEvent): void 
		{
			_service.removeItemFromWishList(event.item.TitleID);
			EventCenter.inst.dispatchEvent(new TitleEvent(TitleEvent.WISHLIST_REMOVE_DONE, null, null));
		}

		public function onDownloadTitle(event:TitleEvent): void 
		{
			_service.queueDownload(int(event.item), onQueueDownloadCompleted);
		}
		
		private function onQueueDownloadCompleted(response:StandardResponse):void
		{
			if (response.ResponseCode != 0)
				MessageDialog.create(null, 'ERROR_DOWNLOAD_MESSAGE', 'ERROR_DOWNLOAD_INSTRUCTION').show();
		}
		
		private function onBack(event:VideoStoreEvent): void 
		{
			var pop:Boolean = false;
			if (_playbookVideoStore.mainTabs.selectedIndex == miscellaneousNavigatorViewIndex && _lastIndex != miscellaneousNavigatorViewIndex)
				pop = true;
				
			_playbookVideoStore.mainTabs.selectedIndex = _lastIndex;
			// this is a fix for the content in miscellaneous view gets overlay and re-create
			if (pop) _playbookVideoStore.miscellaneousNavigatorView.popAll(nullTransition);
		}
		
		private function onSignIn(event:VideoStoreEvent): void 
		{
			if (event.params)
				_service.loginUser(event.params.username, event.params.password, event.callback);
			else
				showMiscView(SignInView);
		}

		private function onSignOut(event:VideoStoreEvent): void 
		{
			_service.logoutUser();
		}

		private function onCreateAccount(event:VideoStoreEvent): void 
		{
			ViewNavigator(_playbookVideoStore.mainTabs.selectedNavigator).pushView(AccountCreationView);
		}

		private function onViewEULA(event:VideoStoreEvent): void 
		{
			var dialog:EULADialog = new EULADialog();
			dialog.currentState = "readonly";
			dialog.show();
		}
		
		private function onViewPrivacy(event:VideoStoreEvent):void
		{
			var req:URLRequest = new URLRequest(_service.settings.conf.Privacy_url);
			navigateToURL(req, "_blank");
		}
		
		private function onViewTerms(event:VideoStoreEvent):void
		{
			var req:URLRequest = new URLRequest(_service.settings.conf.Terms_url);
			navigateToURL(req, "_blank");
		}
		
		private function onChangePassword(event:VideoStoreEvent): void 
		{
			_playbookVideoStore.miscellaneousNavigatorView.pushView(ChangePasswordView);
		}
		
		private function onForgotPassword(event:VideoStoreEvent): void 
		{
			View(event.target).navigator.pushView(ForgotPasswordView);
		}
		
		private function onChangeBilling(event:VideoStoreEvent): void 
		{
			showMiscView(ChangeBillingView);
		}
		
		public function onTitleSelected(event:SelectionEvent): void 
		{
			ViewNavigator(_playbookVideoStore.mainTabs.selectedNavigator).pushView(MovieDetailsView, {titles:event.item, index:event.index});
		}
		
		private function _onMessageOpen($event:MessageBoxEvent): void {
			
			var dialog:MessageDialog = new MessageDialog();
			dialog.title = $event.payload.title;
			dialog.message = $event.payload.message;
			dialog.show();
		}
		
		public function showCheckoutModal($evt:PurchaseEvent): void
		{
			if (_purchaseDialog != null) return;
			
			_purchaseDialog = new PurchaseDialog();
			
			_fsCav = new FullScreenCanvas;
			_fsCav.width = _playbookVideoStore.width;
			_fsCav.height = _playbookVideoStore.height
			_fsCav.addElement(_purchaseDialog);
			
			var dataItem:Object = new Object();
			dataItem.title = $evt.item;
			dataItem.product = $evt.product;
			dataItem.state = 'loading';
			_purchaseDialog.data = dataItem;
			_purchaseDialog.addEventListener(MessageBoxEvent.MESSAGE_CLOSE, hideModal);
//dpy(20140625)-->			_purchaseDialog.addEventListener(VideoStoreEvent.CHANGE_BILLING_INFO, onChangeBilling);
			_playbookVideoStore.stage.addEventListener(MouseEvent.MOUSE_DOWN, onStageMouseDownHandler, false, 0, true);
			
			PopUpManager.addPopUp(_fsCav, _playbookVideoStore, true);
			PopUpManager.centerPopUp(_fsCav);
		}
		
		private function onStageMouseDownHandler(evt:MouseEvent):void
		{
			evt.stopImmediatePropagation();
		}
		
		public function hideModal($evt:Event): void
		{
			$evt.target.removeEventListener(MessageBoxEvent.MESSAGE_CLOSE, hideModal);
			_playbookVideoStore.stage.removeEventListener(MouseEvent.MOUSE_DOWN, onStageMouseDownHandler);
			
			PopUpManager.removePopUp(_fsCav);
			_fsCav.removeAllElements();
			_fsCav = null;
			_purchaseDialog = null;
		}
		
		private function onFault(event:FaultEvent):void
		{
			MessageDialog.createFromFault(null, event.fault).show();
		}
		
		/**
		 * Sets the 
		 * @param event information of the event
		 * 
		 */	
		public function showSearch(event:MenuBarEvent): void {
			if (event.info.searchString.length)
			{
				var data:Object = 
				{
					searchString: event.info.searchString,
					searchData: _service.search(event.info.searchString, event.info.option)
				}
				
				if (_playbookVideoStore.miscellaneousNavigatorView.length)
				{
					if (_playbookVideoStore.miscellaneousNavigatorView.activeView && 
						_playbookVideoStore.miscellaneousNavigatorView.activeView is SearchView &&
						_playbookVideoStore.miscellaneousNavigatorView.length == 1)
					{
						_playbookVideoStore.miscellaneousNavigatorView.activeView.data = data;
					}
					else 
					{
						if( _playbookVideoStore.mainTabs.selectedIndex == miscellaneousNavigatorViewIndex )
						{
							_playbookVideoStore.miscellaneousNavigatorView.popAll(nullTransition);
							_playbookVideoStore.miscellaneousNavigatorView.replaceView(SearchView, data, null, nullTransition);
						}else
						{
							//view update works after miscellaneousNavigatorView is actived.
							_searchData = data;
							_playbookVideoStore.mainTabs.addEventListener(IndexChangeEvent.CHANGE, afterViewChanged, false, 0, true);
						}
					}
				}
				else 
				{
					_playbookVideoStore.miscellaneousNavigatorView.firstView = SearchView;
					_playbookVideoStore.miscellaneousNavigatorView.firstViewData = data;
				}
				
				_playbookVideoStore.mainTabs.selectedIndex = miscellaneousNavigatorViewIndex;
			}
		}
		
		private function afterViewChanged(event:IndexChangeEvent):void
		{
			_playbookVideoStore.mainTabs.removeEventListener(IndexChangeEvent.CHANGE, afterViewChanged);			
			_playbookVideoStore.miscellaneousNavigatorView.popAll(nullTransition);
			_playbookVideoStore.miscellaneousNavigatorView.replaceView(SearchView, _searchData, null, nullTransition);
		}
		
		protected function get miscellaneousNavigatorViewIndex():int
		{
//dpy(20140625)-->			return (_playbookVideoStore.isOnlineMode == false ? 1 : 6);
			return (_playbookVideoStore.isOnlineMode == false ? 1 : 2);
		}

		public function showMiscView(view:Class): void 
		{
			if (_playbookVideoStore.miscellaneousNavigatorView.length)
			{
				if (_playbookVideoStore.mainTabs.selectedIndex != miscellaneousNavigatorViewIndex)
				{
					_playbookVideoStore.miscellaneousNavigatorView.popAll(nullTransition);
					_playbookVideoStore.miscellaneousNavigatorView.replaceView(view, null, null, nullTransition);
				}
				else
				{
					_playbookVideoStore.miscellaneousNavigatorView.pushView(view);
				}
			}
			else 
				_playbookVideoStore.miscellaneousNavigatorView.firstView = view;
			
			_playbookVideoStore.mainTabs.selectedIndex = miscellaneousNavigatorViewIndex;
		}
		
		public function showSettings(event:MenuBarEvent): void 
		{
			if (_playbookVideoStore.mainTabs.selectedIndex != miscellaneousNavigatorViewIndex ||
			 (_playbookVideoStore.miscellaneousNavigatorView.activeView && 
				 (_playbookVideoStore.miscellaneousNavigatorView.activeView is SearchView || _playbookVideoStore.miscellaneousNavigatorView.activeView is MovieDetailsView) ) )
			{
				EventCenter.inst.addEventListener(UIEvent.SETTING_SCREEN_HIDE, onSettingHide, false, 0, true);
				showMiscView(SettingsView);		
			}
		}
		
		private function onSettingHide(e:UIEvent):void
		{
			EventCenter.inst.removeEventListener(UIEvent.SETTING_SCREEN_HIDE, onSettingHide);
		}
		
		public function showPreviousTab(event:MenuBarEvent): void {
			
			trace("showPreviousTab:" + event.info);			
			_playbookVideoStore.mainTabs.selectedIndex = Number(event.info);
		}
	}
}
