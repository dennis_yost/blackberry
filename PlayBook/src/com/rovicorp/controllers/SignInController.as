package com.rovicorp.controllers
{
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.controls.QNXTextInputWrapper;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.events.VideoStoreEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.SignInView;
	import com.sonic.response.Auth.AuthTokenResponse;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.text.TextFormat;
	import flash.ui.KeyboardType;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import mx.core.FlexGlobals;
	import mx.core.IVisualElement;
	import mx.events.ValidationResultEvent;
	import mx.managers.IFocusManagerComponent;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.FaultEvent;
	import mx.utils.StringUtil;
	import mx.validators.Validator;
	
	import qnx.input.IMFConnection;
	
	import spark.components.TextInput;
	import spark.layouts.VerticalLayout;
	
	public class SignInController extends MiscViewController
	{
		private var _view:SignInView;
		private var accountValidators:Array;
		private var focusMap:Dictionary;
		private var _dialog:MessageDialog;
		
		private const ILLEGAL_USERNAME_CHAR:Array = ["/"];
		
		public function SignInController()
		{
			super();
		}
		
		protected override function onCreationComplete():void
		{
			_view = view as SignInView;
			_view.username.addEventListener(DeviceEvent.RETURN_PRESSED, setNextFocus);
			_view.password.addEventListener(DeviceEvent.RETURN_PRESSED, onPasswordEnter);
			_view.username.addEventListener(FocusEvent.FOCUS_IN,focusIn);
			_view.password.addEventListener(FocusEvent.FOCUS_IN,focusIn);
			_view.background.addEventListener(MouseEvent.CLICK,resetState);
			_view.btnSignIn.addEventListener(MouseEvent.CLICK, onBtnSignInClick);
			_view.btnBack.addEventListener(MouseEvent.CLICK, onBack);			
			
			accountValidators = [_view.userNameValidator, _view.passwordLenghtValidator];
			
			focusMap = new Dictionary(true);
			focusMap[_view.username] = _view.password;
			focusMap[_view.password] = null;
			
			if( _view.service && _view.username.text != "" )
			{
				_view.password.setFocus();
			}		
		}
		
		private function onSignInCompleted(result:Object):void
		{
			if (_view.data is Event)
			{
				_view.dispatchEvent(_view.data as Event);
				goBack();
			}
			else if (result.hasOwnProperty("loginFailed") && result.loginFailed == true)
			{
				var msg:String = result.responseMsg;
				if (result.responseCode == 2)
				{
					msg = "User name not registered.";
				}
				else if (result.responseCode == 52)
				{
					msg = "Invalid username or password.";
				}
				
				var dialog:MessageDialog = new MessageDialog();
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Sign In";
				dialog.warning = true;
				dialog.message = msg;
				dialog.setFocus();
				dialog.show();
			}
			else if (result is AuthTokenResponse)
			{
				// login success
				goBack();
			}
			
			_view.currentState = "signIn";
		}

		private function onSignInFault(info:FaultEvent):void
		{
			_view.currentState = "signIn";
			_view.dispatchEvent(new FaultEvent(FaultEvent.FAULT, true, true, info.fault, info.token, info.message));
		}
		
		private function isInputValid():Boolean
		{
			var validationResult:Array = Validator.validateAll(accountValidators);
			if (validationResult.length > 0)
			{
				var dialog:MessageDialog = new MessageDialog();
				var error:ValidationResultEvent;
				var errorMessageArray:Array = [];
				
				for each (error in validationResult)
				{
					errorMessageArray.push(error.message);
					dialog.height = dialog.height + 20;
				}
				
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Sign in Validation";
				dialog.warning = true;
				dialog.message = errorMessageArray.join("\n");
				showDialogAfterAWhile(dialog);
				return false;
			}
			else if (isUserNameContainsIllegalCharacters() == true)
			{
				dialog = new MessageDialog();
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Sign in Validation";
				dialog.message = "Invalid username.";
				dialog.warning = true;
				showDialogAfterAWhile(dialog);
				return false;
			}
			else
			{
				return true;
			}
		}
		
		private function showDialogAfterAWhile(dialog:MessageDialog, time:Number = 500):void
		{
			_dialog = dialog;
			var timer:Timer = new Timer(time, 1);
			timer.addEventListener(TimerEvent.TIMER, onShowDialog, false, 0, true);
			timer.start();
		}
		
		private function onShowDialog(e:TimerEvent):void
		{
			(e.target as Timer).removeEventListener(TimerEvent.TIMER, onShowDialog);
			_dialog.setFocus();
			_dialog.show();
		}
		
		private function isUserNameContainsIllegalCharacters():Boolean
		{
			for each (var str:String in ILLEGAL_USERNAME_CHAR)
			{
				if (_view.username.text.indexOf(str) != -1)
				{
					return true;
				}
			}
			return false;
		}
		
		private function onErrorSelect(event:Event):void
		{
			event.currentTarget.removeEventListener(Event.SELECT,onErrorSelect);
			_dialog = null;
		}

		private function onBtnSignInClick(e:MouseEvent):void
		{
			doSignIn();
		}

		private function onPasswordEnter(event:DeviceEvent):void
		{
			doSignIn();
		}
		
		private function doSignIn():void
		{
			CONFIG::device
			{
				IMFConnection.imfConnection.hideInput();
			}
			
			_view.username.text = StringUtil.trim(_view.username.text);
			
			if (isInputValid() == true)
			{
				var request:RoxioNowDAO = new RoxioNowDAO();
				request.addEventListener(FaultEvent.FAULT, onSignInFault);
				request.loginUser(_view.username.text, _view.password.text, onSignInCompleted);

				_view.currentState = "loading";
			}
		}
		
		private function setNextFocus(event:DeviceEvent):void
		{
			if (focusMap[event.target] == null)
			{
				resetState();
			}
			else if (focusMap[event.target] is QNXTextInputWrapper)
			{
				(focusMap[event.target] as QNXTextInputWrapper).setFocus();
			}
			else
			{
				var nextFocusObj:IFocusManagerComponent = focusMap[event.target];
				_view.focusManager.setFocus(nextFocusObj);
			}
		}
		
		private function focusIn(event:FocusEvent):void
		{
			if( event.currentTarget == _view.password )
			{
				_view.username.text = StringUtil.trim(_view.username.text);
			}
		}
		
		private function resetState(event:MouseEvent=null):void
		{
			//FlexGlobals.topLevelApplication.setFocus();
			_view.stage.focus = null;
		}
		
	}
}