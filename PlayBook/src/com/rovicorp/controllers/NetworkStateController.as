package com.rovicorp.controllers
{
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.UIEvent;

	public class NetworkStateController
	{
		private static var _inst:NetworkStateController;
		
		private var _internetAccessAvailable:Boolean = true;
		private var _startUpWithNoInternet:Boolean = false;
		
		public static function get inst():NetworkStateController
		{
			if (_inst == null)
			{
				_inst = new NetworkStateController(new NetworkStateControllerIniter);
			}
			return _inst
		}
		
		public function NetworkStateController(initer:NetworkStateControllerIniter)
		{
		}

		[Bindable]
		public function get internetAccessAvailable():Boolean
		{
			return _internetAccessAvailable;
		}

		public function set internetAccessAvailable(value:Boolean):void
		{
			_internetAccessAvailable = value;
			if (_startUpWithNoInternet == false && _internetAccessAvailable == true)
			{
				_startUpWithNoInternet = false;
			}
		}
		
		[Bindable]
		public function get startUpWithNoInternet():Boolean
		{
			return _startUpWithNoInternet;
		}
		
		public function set startUpWithNoInternet(value:Boolean):void
		{
			_startUpWithNoInternet = value;
		}

		public function checkInternetAccess(popUpNoInternetDlg:Boolean = false):Boolean
		{
			if (_internetAccessAvailable == false && _startUpWithNoInternet == true && popUpNoInternetDlg == true)
			{
				EventCenter.inst.dispatchEvent(new UIEvent(UIEvent.POPUP_NO_INTERNET_DIALOG));
			}
			return popUpNoInternetDlg;
		}
	}
}

class NetworkStateControllerIniter{}