package com.rovicorp.controllers {
	import com.rovicorp.controls.CVVToolTip;
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.controls.QNXTextInputWrapper;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.AccountCreationView;
	import com.sonic.response.Auth.AuthTokenResponse;
	import com.sonic.response.StandardResponse;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.Dictionary;
	
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.events.FlexMouseEvent;
	import mx.events.ValidationResultEvent;
	import mx.managers.IFocusManagerComponent;
	import mx.managers.PopUpManager;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.FaultEvent;
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	import qnx.input.IMFConnection;
	import qnx.ui.text.KeyboardType;
	
	public class AccountCreationController extends MiscViewController {
		private var server:RoxioNowDAO = new RoxioNowDAO();
		private var _playbookVideoStore:PlaybookVideoStore;
		private var _view:AccountCreationView;
		private var validationResult:Array;
		private var accountValidators:Array;
		private var billingValidators:Array;
		private var _cvvToolTip:CVVToolTip;
		private var focusMap:Dictionary;
		
		public function AccountCreationController() {
			super();
			
			server.addEventListener(FaultEvent.FAULT, _onAccountCreationFault);
		}
		
		protected override function onCreationComplete():void {
			_view = view as AccountCreationView;
			// update view for different country
			var country:String = server.settings.country_name.toLowerCase();
			
			_view.addEventListener('next', _onNext);
			_view.addEventListener('back', _onBack);
			_view.addEventListener('accept', _onTermsChecked);
			_view.addEventListener('submit', _onSubmit);
			_view.addEventListener('cvv', _onCVV);
			_view.statesList.addEventListener(MouseEvent.CLICK,dropdownClicked);
			_view.emailInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			_view.userNameInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			_view.passwordInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			_view.passwordVerifyInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			
			accountValidators = [_view.emailValidator,_view.userNameValidator,_view.passwordValidator,_view.passwordLenghtValidator,_view.passwordRuleValidator]
			billingValidators = [_view.ccNumberValidator,_view.ccNameValidator, _view.expiredDateValidator,
				_view.billingAddress1Validator,
				_view.cityValidator,_view.stateValidator];
				
			if( country == "usa" )
			{
				billingValidators.push(_view.zipValidator);
			}else
			{
				billingValidators.push(_view.CAZipValidator);
			}
			
			focusMap = new Dictionary(true);
			focusMap[_view.emailInput] = _view.userNameInput;
			focusMap[_view.userNameInput] = _view.passwordInput;
			focusMap[_view.passwordInput] = _view.passwordVerifyInput;
			focusMap[_view.passwordVerifyInput] = null;
		}
		
		private function _onNext($e:Event):void {
			validateAndContinue();
		}
		
		private function _onBack($e:Event):void {
			trace('back requested');
			_view.currentState = 'step1';
			_view.loading.visible = false;
		}
		
		private function _onTermsChecked($e:Event):void {
			trace('terms accepted');	
			//_settingsView.keepSignedIn.selected = !_settingsView.keepSignedIn.selected;
			//server.settings.accept = _accountCreationView.accept.selected;
		}
		
		private function _onSubmit($e:Event):void {
			CONFIG::device{ IMFConnection.imfConnection.hideInput(); }
			validateAndSubmit()
		}
		
		private function _onCVV($e:Event):void {
			CONFIG::device{ IMFConnection.imfConnection.hideInput(); }
			_cvvToolTip = PopUpManager.createPopUp(_view, CVVToolTip, true) as CVVToolTip;
			_cvvToolTip.move(750,150);
			_cvvToolTip.addEventListener(Event.CLOSE,onCVVClose);
			_cvvToolTip.addEventListener(FlexMouseEvent.MOUSE_DOWN_OUTSIDE, onCVVClose);
		}
		
		private function onCVVClose(event:Event):void {
			event.target.removeEventListener(Event.CLOSE,onCVVClose);
			event.target.removeEventListener(FlexMouseEvent.MOUSE_DOWN_OUTSIDE, onCVVClose);
			PopUpManager.removePopUp(event.target as IFlexDisplayObject);
		}
		
		private function _registerAccount():void {
			if(!server.isLoggedIn) {
				_view.loading.visible = true;
				_view.submit.setFocus();
				
				var request:RoxioNowDAO = new RoxioNowDAO();
				request.addEventListener(FaultEvent.FAULT, _registrationFault);
				
				request.registerUser(_view.userNameInput.text,_view.passwordInput.text," "," ","","",_view.emailInput.text,_registrationComplete);
			}
		}
		
		private function _registrationComplete($authTokenResponse:AuthTokenResponse):void {
			trace("message: "+$authTokenResponse.ResponseMessage);
			if($authTokenResponse.ResponseCode == 0) {
				_createAccount();
			} else {

				var dialog:MessageDialog = MessageDialog.createFromResponse('CREATE_ACCOUNT_TITLE', $authTokenResponse);
				dialog.addEventListener(Event.SELECT, _onErrorSelect, false, 0, true);
//				dialog.setFocus();
				dialog.show();
				
				_view.loading.visible = false;
				_view.currentState = "step1";
			}
		}
		
		private function _registrationFault($event:FaultEvent):void {
			CONFIG::device{ IMFConnection.imfConnection.hideInput(); }
			MessageDialog.createFromFault('CREATE_ACCOUNT_TITLE', $event.fault).show();
			_view.currentState = "step2";
			_view.loading.visible = false;
		}
		
		private function _createAccount():void {
			var country:String = CONFIG::location;
			
			var state:String = _view.statesList.selectedItem.label.toString();
			
			server.setUserBillingInfo(
				_view.ccNumberInput.text,
				_view.exp_date_picker.formatedValue,
				_view.ccNameInput.text,
				_view.billingAddress1Input.text,
				_view.billingAddress2Input.text,
				_view.cityInput.text,
				state,
				country.toUpperCase(),
				_view.zipInput.text,
				_view.emailInput.text,
				_onAccountCreationResult);
		}
		
		/*private function checkDOB():Boolean
		{
			var pattern:RegExp = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/; 
			var str:String = _view.calPicker.selectedItems[0].label + "/" + _view.calPicker.selectedItems[1].label + "/" + _view.calPicker.selectedItems[2].label;
			return (pattern.test(str));
		}*/
		
		private function validateAndSubmit():void {
			var errorMessageArray:Array = [];
			validationResult = Validator.validateAll(billingValidators);
			
			if (validationResult.length > 0) {
				var error:ValidationResultEvent;
				
				for each (error in validationResult) {
					errorMessageArray.push(error.message);
				}
			}
			
			if (errorMessageArray.length > 0) {
				var dialog:MessageDialog = new MessageDialog();
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Create Account Validation";
				dialog.warning = true;
				dialog.message = errorMessageArray.join("\n");
				dialog.height += 20 * (errorMessageArray.length - 1);	// TODO: Remove when dialog can resize
//				dialog.setFocus();
				dialog.show();
			} else {
				var zip:String = _view.zipInput.text;
				if( CONFIG::location == "canada" && zip.length == 6 )
				{
					_view.zipInput.text = zip.substr(0,3) + " " + zip.substr(3);
				}
				_registerAccount();
			}
		}
		
		private function validateAndContinue():void {
			validationResult = Validator.validateAll(accountValidators);
			
			if (validationResult.length > 0) {
				var dialog:MessageDialog = new MessageDialog();
				var error:ValidationResultEvent;
				var errorMessageArray:Array = [];
				
				for each (error in validationResult) {
					errorMessageArray.push(error.message);
					dialog.height = dialog.height + 20;
				}
				
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Create Account Validation";
				dialog.warning = true;
				dialog.message = errorMessageArray.join("\n");
//				dialog.setFocus();
				dialog.show();
			} else {
				_view.currentState = 'step2';
				_view.loading.visible = false;
				_view.ccNumberInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);				
				_view.ccNameInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
				_view.billingAddress1Input.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
				_view.billingAddress2Input.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
				_view.cityInput.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
				_view.zipInput.addEventListener(DeviceEvent.RETURN_PRESSED,_onSubmit);
				
				focusMap[_view.ccNumberInput] = _view.ccNameInput;
				focusMap[_view.ccNameInput] = null;
				focusMap[_view.billingAddress1Input] = _view.billingAddress2Input;
				focusMap[_view.billingAddress2Input] = _view.cityInput;
				focusMap[_view.cityInput] = null;
				focusMap[_view.zipInput] = null;
				
				// update view for different country
				var country:String = server.settings.country_name.toLowerCase();
				
				switch (country)
				{
					case "usa":
						break;
					case "canada":
						//_view.exp_date_picker.currentState = "showDay";	

						_view.zipInput.restrictString = "0-9 A-Z";
						_view.zipInput.maxChars = 7;
						_view.zipInput.keyboardType=qnx.ui.text.KeyboardType.PASSWORD;
						
						_view.stateValidator.lowerThanMinError = "Enter a province";
						break;
				}

			}
		}
		
		private function _onAccountCreationFault($event:FaultEvent):void {
			var resourceMgr:IResourceManager = ResourceManager.getInstance();
			var dialog:MessageDialog = new MessageDialog();

			dialog.title = resourceMgr.getString('LocalizedStrings', 'CREATE_ACCOUNT_TITLE');
			dialog.heading = resourceMgr.getString('LocalizedStrings', 'CREATE_ACCOUNT2_MESSAGE');
			dialog.message = resourceMgr.getString('LocalizedStrings', 'CREATE_ACCOUNT2_INSTRUCTION', [_view.userNameInput.text]);
			dialog.addEventListener(Event.SELECT, onMessageSelect, false, 0, true);
			dialog.width = 600;	// TODO: Update dialog to automatically adjust size to content 
			dialog.height = 280;
			dialog.show();
		}
		
		private function _onAccountCreationResult($response:StandardResponse):void {
			CONFIG::device{ IMFConnection.imfConnection.hideInput(); }
			server.toggleSubscriberStatus(_view.subscribeNews.selected);

			if ($response.ResponseCode == 0)
			{
				var dialog:MessageDialog = MessageDialog.create('CREATE_ACCOUNT_TITLE', 'CREATE_ACCOUNT_MESSAGE', 'CREATE_ACCOUNT_INSTRUCTION');
				
				dialog.addEventListener(Event.SELECT, onMessageSelect, false, 0, true);
				dialog.width = 600; 
				dialog.height = 280;
				dialog.show();
			}
			else
				_onAccountCreationFault(null);
		}
		
		private function _onErrorSelect(event:Event):void {
			trace("Error on registration");
		}

		private function onMessageSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onMessageSelect);
			goBack();
		}
		
		private function onErrorSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onErrorSelect);
		}
		
		private function setNextFocus(event:DeviceEvent):void
		{
			if (focusMap[event.target] == null)
			{
				resetState();
			}
			else if (focusMap[event.target] is QNXTextInputWrapper)
			{
				(focusMap[event.target] as QNXTextInputWrapper).setFocus();
			}
			else
			{
				var nextFocusObj:IFocusManagerComponent = focusMap[event.target];
				_view.focusManager.setFocus(nextFocusObj);
			}
		}
		
		private function resetState(event:MouseEvent=null):void
		{
			_view.stage.focus = null;
			
			CONFIG::device
			{
				IMFConnection.imfConnection.hideInput();
			}
		}
		
		private function dropdownClicked(event:Event):void {
			CONFIG::device{ IMFConnection.imfConnection.hideInput(); }
		}
	}
}
