package com.rovicorp.controllers
{
	import com.rovicorp.events.SelectionEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.model.Store;
	import com.rovicorp.views.BrowseView;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.MoviesView;
	import com.rovicorp.views.TVShowsView;
	
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	
	import spark.events.IndexChangeEvent;
	
	public class BrowseController extends ViewController
	{
		public function BrowseController()
		{
			super();
		}
		
		protected override function onCreationComplete():void
		{
			if (view is MoviesView)
				view.data = new RoxioNowDAO().movies;
			else if (view is TVShowsView)
				view.data = new RoxioNowDAO().tvShows;

			view.addEventListener(SelectionEvent.CATEGORY_SELECTION_EVENT, onCategorySelection, false, 0, true);
			view.data.categories.addEventListener(CollectionEvent.COLLECTION_CHANGE, onCategoriesChange);
		
			BrowseView(view).categoryList.selectedIndex = 0;
		}

		private function onCategorySelection(event:SelectionEvent):void
		{
			var store:Store = view.data as Store;
			
			if (store)
				store.category = event.item.ID;	
		}
		
		private function onCategoriesChange(event:CollectionEvent):void
		{
			BrowseView(view).categoryList.selectedIndex = 0;
		}
	}
}