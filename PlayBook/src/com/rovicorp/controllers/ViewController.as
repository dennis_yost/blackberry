package com.rovicorp.controllers
{
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.MainAppEvent;
	import com.rovicorp.views.View;
	
	import mx.events.FlexEvent;
	
	import spark.events.ViewNavigatorEvent;
	
	public class ViewController 
	{
		private var _view:View;
		
		public function ViewController()
		{
//			EventCenter.inst.addEventListener(MainAppEvent.NETWORK_CONNECTION_LOST, onNetworkLost, false, 0, true);
		}
		
		protected function onNetworkLost(event:MainAppEvent):void
		{
			try
			{
				view.currentState = "error";
			}
			catch (e:Error)
			{}
		}
		
		public function get view():View
		{
			return _view;
		}

		public function set view(value:View):void
		{
			_view = value;
			_view.addEventListener(FlexEvent.CREATION_COMPLETE, creationComplete, false, 0, true);
		}
		
		private function creationComplete(event:FlexEvent):void
		{
			_view.removeEventListener(FlexEvent.CREATION_COMPLETE, creationComplete);
			onCreationComplete();
		}

		protected function onCreationComplete():void
		{
		}
	}
}