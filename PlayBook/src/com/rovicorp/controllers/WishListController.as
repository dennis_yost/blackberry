package com.rovicorp.controllers
{
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.WishListView;
	
	import mx.binding.utils.ChangeWatcher;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.events.FaultEvent;
	
	import spark.events.IndexChangeEvent;
	import spark.events.ViewNavigatorEvent;

	public class WishListController extends ViewController
	{
		
		private var wishListView:WishListView;
		private var server:RoxioNowDAO = new RoxioNowDAO();
		
		public function WishListController()
		{
			super();
		}
		
		private function setState(state:String):void
		{
			wishListView = view as WishListView;
			
			
			if (state == "loaded")
			{
				if (!server.isLoggedIn) {
					wishListView.currentState = "signedout";
				} else if (server.wishlist.length == 0) 
					wishListView.currentState = "empty";
				else
				{
					wishListView.currentState = "loaded";
				}
			}
			else if (state == "loading")
			{
				wishListView.currentState = "loading";
			}
		}
		
		protected override function onCreationComplete():void 
		{
			wishListView = view as WishListView;
			wishListView.addEventListener(ViewNavigatorEvent.VIEW_ACTIVATE, onViewActivate);
			
			server.wishlist.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onWishlistPropertyChange, false, 0, true);
			server.wishlist.addEventListener(FaultEvent.FAULT, onWishlistFault, false, 0, true);
			
			view.data = server.wishlist;
			setState(server.wishlist.state);

		}
		
		private function onViewActivate(event:ViewNavigatorEvent):void
		{
			if (view.currentState == "error")
				server.wishlist.invalidate();
		}

		private function onWishlistFault(event:FaultEvent):void
		{
			view.setCurrentState("error");
		}
		
		private function onWishlistPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setState(server.wishlist.state);
		}
	}
}