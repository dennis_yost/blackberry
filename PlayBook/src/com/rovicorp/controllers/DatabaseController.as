package com.rovicorp.controllers
{
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.model.DownloadListing;
	import com.rovicorp.service.database.DatabaseManager;
	import com.rovicorp.service.download.LicenseStatus;
	import com.rovicorp.utilities.LogUtil;
	import com.sonic.response.Library.FullTitlePurchased;
	import com.sonic.response.NameAndValue;
	import com.sonic.response.TitleData.BonusAsset;
	import com.sonic.response.TitleData.FullTitle;
	import com.sonic.response.TitleData.Product;
	
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;

	public class DatabaseController
	{
		// a value used to combine all attribuites in object to string
		private const META_SPE_MARKER:String = "+-+";
		// a value used to combine key and value in object
		private const OBJECT_SPE_MARKER:String = "*+*";
		
		private var eventCenter:EventCenter = EventCenter.inst;
		private var dbMgr:DatabaseManager;
		private var dbPath:String = "";
		
		private var taskList:Vector.<DatabaseEvent>;
		private var isTaskProcessing:Boolean = false;
		private var curDatabaseTaskEvent:DatabaseEvent;
		
		public static var inst:DatabaseController;
		
		public function DatabaseController(databaseLocation:String)
		{
			inst = this;
			
			setupListener();
			
			taskList = new Vector.<DatabaseEvent>;
			dbPath = databaseLocation;
			prepareDatabase();
		}
		
		/**
		 * Check if the database is ready for operations or not.
		 * @return Yes or No
		 * 
		 */
		public function get isDatabaseReady():Boolean
		{
			if (dbMgr != null)
			{
				return dbMgr.isDatabaseReady;
			}
			else
			{
				return false;
			}
		}
		
		public function getDBManager():DatabaseManager
		{
			return dbMgr;
		}
		
		private function setupListener():void
		{
			eventCenter.addEventListener(DatabaseEvent.UPDATE_TITLE, onNewTaskComing, false, 0, true);
			eventCenter.addEventListener(DatabaseEvent.UPDATE_LIBRARY_INFO, onNewTaskComing, false, 0, true);
			eventCenter.addEventListener(DatabaseEvent.GET_LIBRARY_INFO, onNewTaskComing, false, 0, true);
			eventCenter.addEventListener(DatabaseEvent.GET_TITLE_INFO, onNewTaskComing, false, 0, true);
			eventCenter.addEventListener(DatabaseEvent.UPDATE_PURCHASED_TITLE, updatePurchasedTitleInfo, false, 0, true);
			eventCenter.addEventListener(DatabaseEvent.GET_PURCHASED_TITLE_INFO, onNewTaskComing, false, 0, true);
			eventCenter.addEventListener(DatabaseEvent.UPDATE_DOWNLOAD_LISTING, onNewTaskComing, false, 0, true);
			eventCenter.addEventListener(DatabaseEvent.GET_DOWNLOAD_LISTING, onNewTaskComing, false, 0, true);
		}
		
		private function prepareDatabase():void
		{
			if (dbMgr != null) return;
			
			var file:File = new File(dbPath);
			dbMgr = new DatabaseManager(dbPath);
			
			if (file.exists)
				dbMgr.openDB(dbOpened, dbOpenFail);
			else
				dbMgr.createFromSQL("assets/db/videostore.sql", dbOpened, dbOpenFail);
		}
		
		private function dbOpened():void
		{
			eventCenter.dispatchEvent(new DatabaseEvent(DatabaseEvent.DATABASE_READY));
			
			if (isTaskProcessing == false)
			{
				processTask();
			}
		}
		
		private function dbOpenFail():void
		{
			eventCenter.dispatchEvent(new DatabaseEvent(DatabaseEvent.DATABASE_OPEN_FAILED));
		}
		
		private function onNewTaskComing(e:DatabaseEvent):void
		{
			//LogUtil.debug("==== task add: ", taskList.length, isTaskProcessing, e.type);
			taskList.push(e);
			if (isTaskProcessing == false)
			{
				processTask();
			}
		}
		
		private function processTask():void
		{
			if (taskList.length == 0 || isDatabaseReady == false)
			{
				//trace("======= ALL DONE");
				// all tasks are done
				isTaskProcessing = false;
				curDatabaseTaskEvent = null;
				return;
			}
			
			isTaskProcessing = true;
			curDatabaseTaskEvent = taskList[0];
			//LogUtil.debug("==== process task", taskList.length, curDatabaseTaskEvent.type);
			
			if (curDatabaseTaskEvent.type == DatabaseEvent.UPDATE_TITLE)
			{
				updateTitle(curDatabaseTaskEvent);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.UPDATE_LIBRARY_INFO)
			{
				updateLibraryInfo(curDatabaseTaskEvent);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.GET_LIBRARY_INFO)
			{
				getLibraryInfo(curDatabaseTaskEvent);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.GET_TITLE_INFO)
			{
				getTitleInfo(curDatabaseTaskEvent);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.GET_PURCHASED_TITLE_INFO)
			{
				getPurchasedTitleInfo(curDatabaseTaskEvent);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.UPDATE_PURCHASED_TITLE)
			{
				updatePurchasedTitleInfo(curDatabaseTaskEvent);
			}			
			else if (curDatabaseTaskEvent.type == DatabaseEvent.UPDATE_DOWNLOAD_LISTING)
			{
				updateDownloadListingInfo(curDatabaseTaskEvent);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.GET_DOWNLOAD_LISTING)
			{
				getDownloadListingInfo(curDatabaseTaskEvent);
			}
		}
		
		private function taskDone(success:Boolean, result:Object = null):void
		{
			//trace("===== db task done: " + success, taskList.length - 1, curDatabaseTaskEvent.type);
			if (success == false)
			{
				trace("\t!!DB Operation ERROR!!", curDatabaseTaskEvent.type);
			}
			
			var e:DatabaseEvent;

			if (curDatabaseTaskEvent.type == DatabaseEvent.UPDATE_TITLE)
			{
				e = new DatabaseEvent(DatabaseEvent.UPDATE_TITLE_DONE);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.UPDATE_LIBRARY_INFO)
			{
				e = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO_DONE);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.GET_LIBRARY_INFO)
			{
				e = new DatabaseEvent(DatabaseEvent.GET_LIBRARY_INFO_DONE);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.GET_TITLE_INFO)
			{
				e = new DatabaseEvent(DatabaseEvent.GET_TITLE_INFO_DONE);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.GET_PURCHASED_TITLE_INFO)
			{
				e = new DatabaseEvent(DatabaseEvent.GET_PURCHASED_TITLE_INFO_DONE);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.UPDATE_PURCHASED_TITLE)
			{
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.UPDATE_FILE_CACHE)
			{
				e = new DatabaseEvent(DatabaseEvent.UPDATE_FILE_CACHE_DONE);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.REMOVE_FILE_CACHE)
			{
				e = new DatabaseEvent(DatabaseEvent.REMOVE_FILE_CACHE_DONE);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.GET_FILE_CACHE)
			{
				e = new DatabaseEvent(DatabaseEvent.GET_FILE_CACHE_DONE);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.UPDATE_DOWNLOAD_LISTING)
			{
				e = new DatabaseEvent(DatabaseEvent.UPDATE_DOWNLOAD_LISTING_DONE);
			}
			else if (curDatabaseTaskEvent.type == DatabaseEvent.GET_DOWNLOAD_LISTING)
			{
				e = new DatabaseEvent(DatabaseEvent.GET_DOWNLOAD_LISTING_DONE);
			}
			
			//return id to callback
			if( curDatabaseTaskEvent.id )
				e.id = curDatabaseTaskEvent.id;
			
			e.success = success;
			e.result = result;
			eventCenter.dispatchEvent(e);
			
			taskList.splice(0, 1);
			processTask();
		}

		private function updateTitle(e:DatabaseEvent):void
		{			
			if (isDatabaseReady == true)
			{
				if (e.id != null && e.id != "")
				{
					dbMgr.getTitle(e.id, onCheckTitleBeforeUpdateSuccess, onCheckTitleBeforeUpdateFail);
				}
				else
				{
					taskDone(false);
				}
			}
		}
		
		private function onCheckTitleBeforeUpdateFail(result:Object):void
		{
			taskDone(false);
		}
		
		private function onCheckTitleBeforeUpdateSuccess(result:Object):void
		{
			var tiid:Object = {};
			if (curDatabaseTaskEvent.titleObj != null)
			{
				tiid = titleObj2FullDatabase(curDatabaseTaskEvent.titleObj, (result.data == null));
			}
			else
			{
				tiid = fullTitle2Database(curDatabaseTaskEvent.title, (result.data == null));
			}
			
			if (result.data != null)
			{
				if (curDatabaseTaskEvent.updateExistingItem == true)
				{
					tiid = mergeObject(tiid, result.data[0]);
					dbMgr.updateTitle(curDatabaseTaskEvent.id, tiid, onUpdateTitleSuccess, onUpdateTitleFail);
				}
				else
				{
					taskDone(false);
				}
			}
			else if (curDatabaseTaskEvent.onlyUpdate == false)
			{
				dbMgr.addTitle(tiid, onUpdateTitleSuccess, onUpdateTitleFail);
			}
			else
			{
				taskDone(false);
			}
		}
		
		private function onUpdateTitleSuccess(result:Object):void
		{
			taskDone(true);
		}
		
		private function onUpdateTitleFail(result:Object):void
		{
			taskDone(false);
		}
		
		private function updatePurchasedTitleInfo(e:DatabaseEvent):void
		{
			var ft:FullTitlePurchased = e.purchasedTitle;
			if (ft == null)
			{
				taskDone(false);
				return;
			}
			
			// save title info to titles table
			var dbe_title:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_TITLE);
			dbe_title.id = ft.TitleID.toString();
			dbe_title.title = ft;
			eventCenter.dispatchEvent(dbe_title);
			
			// save current content item to library
			var liid:Object = {};
			liid._id = ft.PassID;
			liid.title_id = ft.TitleID;
			liid.date_expired = (ft.DateExpired != null) ? ft.DateExpired.toString() : "";
			liid.date_purchased = (ft.DatePurchased != null) ? ft.DatePurchased.toString() : "";
			liid.store_name = ft.StoreName;
			liid.stream_start_time_seconds = ft.StreamStartTimeSeconds;
			liid.expiration_message = ft.ExpirationMessage;
			liid.store_logo_url = ft.StoreLogoUrl;
			liid.stream_play_status = ft.StreamPlayStatus;
			liid.watch_status = ft.WatchStatus;
			
			if (ft.AvailableProducts && ft.AvailableProducts.length > 0)
			{
				var pt:String = ft.AvailableProducts[0].PurchaseType;
				if (pt.indexOf("rent") > -1)
				{
					liid.view_hours = ft.AvailableProducts.getItemAt(0).RentalPeriod;
					liid.purchase_type = 0;
				}

				if (pt.indexOf("buy") > -1) 
					liid.purchase_type = 1;
			}
			
			if( liid.purchase_type == 0 )
			{
				var expired_date:Date = new Date();
				expired_date.setTime(ft.DatePurchased.getTime());
				expired_date.setDate(expired_date.getDate() + 30);
				liid.date_expired = expired_date.toString();
			}

			var dbe_lib:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO);
			dbe_lib.id = ft.PassID.toString();
			dbe_lib.library = liid;
			eventCenter.dispatchEvent(dbe_lib);
			
//			taskDone(true);
		}
		
		private function updateDownloadListingInfo(e:DatabaseEvent):void
		{
			var dl:DownloadListing = e.downloadListing;
			if (dl == null)
			{
				taskDone(false);
				return;
			}
			var ft:FullTitle = dl.Details;
			if (ft == null)
			{
				taskDone(false);
				return;
			}
			
			// save title info to titles table
			var dbe_title:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_TITLE);
			dbe_title.id = ft.TitleID.toString();
			dbe_title.title = ft;
			eventCenter.dispatchEvent(dbe_title);
			
			// save current content item to library
			var liid:Object = {};
			liid._id = dl.PassID;
			liid.title_id = ft.TitleID;
			liid.download_status = dl.DownloadStatus;

			var dbe_lib:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO);
			dbe_lib.id = dl.PassID.toString();
			dbe_lib.library = liid;
			eventCenter.dispatchEvent(dbe_lib);
			
			taskDone(true);
		}

		private function updateLibraryInfo(e:DatabaseEvent):void
		{
			if (isDatabaseReady == true)
			{
				if (e.id != null && e.id != "")
				{
					dbMgr.getLibraryItem(e.id, onCheckLibraryBeforeUpdateSuccess, onCheckLibraryBeforeUpdateFail);
				}
				else
				{
					taskDone(false);
				}
			}
		}
		
		private function onCheckLibraryBeforeUpdateFail(result:Object):void
		{
			taskDone(false);
		}
		
		private function onCheckLibraryBeforeUpdateSuccess(result:Object):void
		{
			var liid:Object = {};
			if (curDatabaseTaskEvent.contentItem != null)
			{
				// update library with content item
				if (result.data != null)
				{
					// merge the existing data with the coming one
					liid = mergeObject(contentItem2Database(curDatabaseTaskEvent.contentItem, false), result.data[0]);
				}
				else
				{
					liid = contentItem2Database(curDatabaseTaskEvent.contentItem);
				}
			}
			else
			{
				liid = completeObject4LibraryTable(curDatabaseTaskEvent.library, (result.data && result.data.length > 0) ? result.data[0] : null);
			}

			if (result.data != null)
			{
				if (curDatabaseTaskEvent.updateExistingItem == true)
				{
					dbMgr.updateLibraryItem(curDatabaseTaskEvent.id, liid, onUpdateDownloadInfoSuccess, onUpdateDownloadInfoFail);
				}
				else
				{
					taskDone(false);
				}
			}
			else if (curDatabaseTaskEvent.onlyUpdate == false)
			{
				dbMgr.addLibraryItem(liid, onUpdateDownloadInfoSuccess, onUpdateDownloadInfoFail);
			}
			else
			{
				taskDone(false);
			}
		}
		
		private function onUpdateDownloadInfoSuccess(result:Object):void
		{
			taskDone(true);
		}
		
		private function onUpdateDownloadInfoFail(result:Object):void
		{
			taskDone(false);
		}
		
		private function getLibraryInfo(e:DatabaseEvent):void
		{
			if (isDatabaseReady == true)
			{
				if (e.id != null && e.id != "")
				{
					dbMgr.getLibraryItem(e.id, onGetDownloadInfoSuccess, onGetDownloadInfoFail);
				}
				else
				{
					dbMgr.getAllLibraryItems(onGetDownloadInfoSuccess, onGetDownloadInfoFail);
				}
			}
		}
		
		private function onGetDownloadInfoSuccess(result:Object):void
		{
			if (result.data != null)
			{
				taskDone(true, result.data);
			}
			else
			{
				taskDone(false);
			}
		}
		
		private function onGetDownloadInfoFail(result:Object):void
		{
			taskDone(false);
		}
		
		private function getTitleInfo(e:DatabaseEvent):void
		{
			if (isDatabaseReady == true)
			{
				if (e.id != null && e.id != "")
				{
					dbMgr.getTitle(e.id, onGetTitleInfoSuccess, onGetTitleInfoFail);
				}
				else
				{
					dbMgr.getAllTitles(onGetTitleInfoSuccess, onGetTitleInfoFail);
				}
			}
		}
		
		private function onGetTitleInfoSuccess(result:Object):void
		{
			if (result.data != null)
			{
				if (result.data is Array)
				{
					var r:Array = [];
					for (var i:int = 0; i < result.data.length; ++i)
					{
						r.push(database2FullTitle(result.data[i]));
					}
					taskDone(true, r);
				}
				else
				{
					taskDone(true, database2FullTitle(result.data));
				}
			}
			else
			{
				taskDone(false);
			}
		}
		
		private function onGetTitleInfoFail(result:Object):void
		{
			taskDone(false);
		}
		
		private function getPurchasedTitleInfo(e:DatabaseEvent):void
		{
			if (isDatabaseReady == true)
			{
				if (e.id != null && e.id != "")
				{
					dbMgr.getCrossItemByID(e.id, onGetPurchasedTitleInfoSuccess, onGetPurchasedTitleInfoFail);
				}
				else
				{
					dbMgr.getAllCrossItems(onGetPurchasedTitleInfoSuccess, onGetPurchasedTitleInfoFail);
				}
			}
		}
		
		private function onGetPurchasedTitleInfoSuccess(result:Object):void
		{
			if (result.data != null)
			{
				if (result.data is Array)
				{
					var r:Array = [];
					for (var i:int = 0; i < result.data.length; ++i)
					{
						r.push(database2FullTitle(result.data[i], true));
					}
					taskDone(true, r);
				}
				else
				{
					taskDone(true, database2FullTitle(result.data, true));
				}
			}
			else
			{
				taskDone(false);
			}
		}
		
		private function onGetPurchasedTitleInfoFail(result:Object):void
		{
			taskDone(false);
		}
		
		private function getDownloadListingInfo(e:DatabaseEvent):void
		{
			if (isDatabaseReady == true)
			{
				if (e.id != null && e.id != "")
				{
					dbMgr.getCrossItemByID(e.id, onGetDownloadListingInfoSuccess, onGetDownloadListingInfoFail);
				}
				else
				{
					dbMgr.getAllCrossItems(onGetDownloadListingInfoSuccess, onGetDownloadListingInfoFail);
				}
			}
		}
		
		private function onGetDownloadListingInfoSuccess(result:Object):void
		{
			if (result.data != null)
			{
				if (result.data is Array)
				{
					var r:Array = [];
					for (var i:int = 0; i < result.data.length; ++i)
					{
						r.push(database2DownloadListing(result.data[i]));
					}
					taskDone(true, r);
				}
				else
				{
					taskDone(true, database2DownloadListing(result.data));
				}
			}
			else
			{
				taskDone(false);
			}
		}
		
		private function onGetDownloadListingInfoFail(result:Object):void
		{
			taskDone(false);
		}
		
		private function mergeObject(from:Object, to:Object):Object
		{
			for (var key:String in from)
			{
				to[key] = from[key];
			}
			return to;
		}
		
		private function metaValue2String(meta:Object):String
		{
			var s:String = "";
			for (var k:String in meta)
			{
				if (s != "") s += META_SPE_MARKER;
				s += k + OBJECT_SPE_MARKER + meta[k];
			}
			return s;
		}
		
		private function string2MetaValue(val:String):Array
		{
			var meta:Array = [];
			var keys:Array = val.split(META_SPE_MARKER);
			var o:Array;
			for (var i:int = 0; i < keys.length; ++i)
			{
				o = String(keys[i]).split(OBJECT_SPE_MARKER);
				try
				{
					var nv:NameAndValue = new NameAndValue;
					nv.KeyName = o[0];
					nv.KeyValue = o[1];
					meta.push(nv);
				}
				catch (e:Error)
				{}
			}
			return meta;
		}
		
		private function contentItem2Database(contentItem:Object, fillAll:Boolean = true):Object
		{
			//var alternateFiles:Array = contentItem.AlternateFiles.AlternateFile;
			var content:Object = contentItem.Content;
			var pass:Object = contentItem.pass;
			var title:Object = contentItem.title;

			var liid:Object = {};
			liid._id = pass.pid;
			liid.title_id = title.titleid;
			// the type in return value could be 1 (rent) or 2 (buy)
			// but in db, it should be 0 or 1.
			liid.purchase_type = pass.type - 1;
			
			var vt:int = pass.amtofunits;
			if (pass.unit == "y") vt = vt * 365 * 24; // make the time unit to hour
			liid.view_hours = vt;
			
			liid.sku_id = pass.sku.skuid;
			liid.dq_id = pass.sku.dqid;
			liid.device = content.file.device;
			liid.autodelete = (pass.autodelete == "0" ? 0 : 1);
			liid.file_name = content.file.filename;
			liid.video_bitrate = content.file.bitrate;
			liid.audio_bitrate = content.file.audiobitrate;
			liid.license_url = content.license.url;
			liid.date_expired = title.titlerules.expiredate;
			liid.store_name = content.friendlystorename;
			liid.stream_start_time_seconds = 0;
			
			if (fillAll == true)
			{
				liid.first_play = "";	// time for play the video at very first time
				liid.expire_time = "";	// first_play + view_hours
				liid.download_order = 0;
				liid.download_status = 0;
				liid.download_url = "";
				liid.target_file_size = 1;
				liid.actual_file_size = 0;
				liid.watch_now_countdown = 1000;
				liid.download_speed = 0;
				liid.user_name = "";
				liid.last_play_position = "0";
				liid.license_receieved = 0;
				liid.deleted = "False";
				liid.expired = 0;
				liid.has_portable = 1;
				liid.is_local = 1;
				liid.is_premium_local = 0;
				liid.is_playable = 0;
				liid.date_purchased = "";
				liid.expiration_message = "";
				liid.store_logo_url = "";
				liid.stream_play_status = "";
				liid.watch_status = "";
				liid.license_url = "";
				liid.custom_data = "";
				liid.download_url = "";
				liid.asset_id = -1;
				liid.friendly_file_name = "";
				liid.license_acknowledge_url = "";
			}
			return liid;
		}
		
		private function completeObject4LibraryTable(source:Object, existingObj:Object = null):Object
		{
			if (!source._id)
			{
				var id:int = getIntFromObject(existingObj, "_id");
				if (id == 0)
				{
					source._id = int(curDatabaseTaskEvent.id);
				}
				else
				{
					source._id == id;
				}
			}

			if (!source.title_id) source.title_id = getIntFromObject(existingObj, "title_id");
			if (!source.purchase_type) source.purchase_type = getIntFromObject(existingObj, "purchase_type");
			if (!source.view_hours) source.view_hours = getIntFromObject(existingObj, "view_hours");
			if (!source.sku_id) source.sku_id = getIntFromObject(existingObj, "sku_id");
			if (!source.dq_id) source.dq_id = getIntFromObject(existingObj, "dq_id");
			if (!source.device) source.device = getIntFromObject(existingObj, "device", 1);
			if (!source.autodelete) source.autodelete = getIntFromObject(existingObj, "autodelete");
			if (!source.file_name) source.file_name = getStringFromObject(existingObj, "file_name");
			if (!source.video_bitrate) source.video_bitrate = getIntFromObject(existingObj, "video_bitrate");
			if (!source.audio_bitrate) source.audio_bitrate = getIntFromObject(existingObj, "audio_bitrate");
			if (!source.license_url) source.license_url = getStringFromObject(existingObj, "license_url");
			if (!source.date_expired) source.date_expired = getStringFromObject(existingObj, "date_expired");
			if (!source.store_name) source.store_name = getStringFromObject(existingObj, "store_name");
			if (!source.stream_start_time_seconds) source.stream_start_time_seconds = getIntFromObject(existingObj, "stream_start_time_seconds");
			if (!source.first_play) source.first_play = getStringFromObject(existingObj, "first_play");
			if (!source.expire_time) source.expire_time = getStringFromObject(existingObj, "expire_time");
			if (!source.download_order) source.download_order = getIntFromObject(existingObj, "download_order");
			if (!source.download_status) source.download_status = getIntFromObject(existingObj, "download_status");
			if (!source.download_url) source.download_url = getStringFromObject(existingObj, "download_url");
			if (!source.target_file_size) source.target_file_size = getIntFromObject(existingObj, "target_file_size", 1);
			if (!source.actual_file_size) source.actual_file_size = getIntFromObject(existingObj, "actual_file_size");
			if (!source.watch_now_countdown) source.watch_now_countdown = getIntFromObject(existingObj, "watch_now_countdown", 1000);
			if (!source.download_speed) source.download_speed = getIntFromObject(existingObj, "download_speed");
			if (!source.user_name) source.user_name = getStringFromObject(existingObj, "user_name");
			if (!source.last_play_position) source.last_play_position = getStringFromObject(existingObj, "last_play_position", "0");
			if (!source.license_receieved) source.license_receieved = getIntFromObject(existingObj, "license_receieved");
			if (!source.deleted) source.deleted = getStringFromObject(existingObj, "deleted", "False");
			if (!source.expired) source.expired = getIntFromObject(existingObj, "expired");
			if (!source.has_portable) source.has_portable = getIntFromObject(existingObj, "has_portable", 1);
			if (!source.is_local) source.is_local = getIntFromObject(existingObj, "is_local", 1);
			if (!source.is_premium_local) source.is_premium_local = getIntFromObject(existingObj, "is_premium_local");
			if (!source.is_playable) source.is_playable = getIntFromObject(existingObj, "is_playable");
			if (!source.date_purchased) source.date_purchased = getStringFromObject(existingObj, "date_purchased");
			if (!source.expiration_message) source.expiration_message = getStringFromObject(existingObj, "expiration_message");
			if (!source.store_logo_url) source.store_logo_url = getStringFromObject(existingObj, "store_logo_url");
			if (!source.stream_play_status) source.stream_play_status = getStringFromObject(existingObj, "stream_play_status");
			if (!source.watch_status) source.watch_status = getStringFromObject(existingObj, "watch_status");
			if (!source.asset_id) source.asset_id = getIntFromObject(existingObj, "asset_id");
			if (!source.license_acknowledge_url) source.license_acknowledge_url = getStringFromObject(existingObj, "license_acknowledge_url");
			if (!source.friendly_file_name) source.friendly_file_name = getStringFromObject(existingObj, "friendly_file_name");
			if (!source.custom_data) source.custom_data = getStringFromObject(existingObj, "custom_data");
			
			return source;
		}
		
		private function getStringFromObject(obj:Object, key:String, defaultValue:String = ""):String
		{
			return (obj && obj.hasOwnProperty(key)) ? obj[key] : defaultValue;
		}
		
		private function getIntFromObject(obj:Object, key:String, defaultValue:int = 0):int
		{
			return (obj && obj.hasOwnProperty(key)) ? obj[key] : defaultValue;
		}
		
		private function fullTitle2Database(ft:FullTitle, fillAll:Boolean = true):Object
		{
			var tiid:Object = {};
			
			tiid.t_id = ft.TitleID;
			tiid.title_name = ft.Name;
			tiid.box_art_prefix = ft.BoxartPrefix;
			tiid.mpaa_rating = ft.MPAARating;
			tiid.actors = ft.Actors;
			tiid.buy_price = ft.BuyPrice;
			tiid.copyright = ft.Copyright;
			tiid.directors = ft.Directors;
			tiid.producers = ft.Producers;
			tiid.rating_reason = ft.RatingReason;
			tiid.release_year = int(ft.ReleaseYear);
			tiid.rent_price = ft.RentPrice;
			tiid.season_title_id = ft.SeasonTitleID;
			tiid.show_title_id = ft.ShowTitleID;
			tiid.synopsys = ft.Synopsys;
			tiid.buy_avail = (ft.BuyAvail) ? "true" : "false";
			tiid.rent_avail = (ft.RentAvail) ? "true": "false";
			tiid.writers = ft.Writers;
			tiid.title_type = ft.TitleType;
			tiid.meta_value = metaValue2String(ft.MetaValues);
			
			var product_buy_set:Boolean = false;
			var product_rent_set:Boolean = false;
			if (ft.AvailableProducts != null && ft.AvailableProducts.length > 0)
			{
				for each (var item:Product in ft.AvailableProducts)
				{
					if (item.PurchaseType.indexOf("buy") > 0)
					{
						tiid.buy_skuid = item.SkuID;
						tiid.buy_expire_date_utc = item.ExpireDateUTC.toString();
						tiid.buy_promotext = item.PromoText;
						tiid.buy_purchasetype = item.PurchaseType;
						
						product_buy_set = true;
						continue;
					}
					
					if (item.PurchaseType.indexOf("rent") > 0)
					{
						tiid.rent_skuid = item.SkuID;
						tiid.rent_expire_date_utc = item.ExpireDateUTC.toString();
						tiid.rent_promotext = item.PromoText;
						tiid.rent_purchasetype = item.PurchaseType;
						tiid.rent_rental_period = item.RentalPeriod;
						
						product_rent_set = true;
						continue;
					}
				}
			}
			
			var bonus_set:Boolean = false;
			if (ft.BonusAssets != null && ft.BonusAssets.length > 0)
			{
				var bItem:BonusAsset = ft.BonusAssets[0];
				tiid.bonus_asset_id = bItem.BonusAssetID;
				bonus_set = true;
			}
			
			if (fillAll == true)
			{
				tiid.is_thx_media_director_enabled = "";
				tiid.hd = "";
				tiid.similar_avail = "";
				tiid.in_user_wishlist = "";
				tiid.run_time = "";
				tiid.air_date = "";
				tiid.your_rating = 0;
				tiid.critics_review = "";
				tiid.flixster = "";
				if (product_buy_set == false)
				{
					tiid.buy_skuid = -1;
					tiid.buy_expire_date_utc = "";
					tiid.buy_promotext = "";
					tiid.buy_purchasetype = "";
				}
				if (product_rent_set == false)
				{
					tiid.rent_skuid = -1;
					tiid.rent_expire_date_utc = "";
					tiid.rent_promotext = "";
					tiid.rent_purchasetype = "";
					tiid.rent_rental_period = -1;
				}
				if (bonus_set == false) tiid.bonus_asset_id = -1;
				tiid.g_id = -1;
				tiid.master_option = "";
				tiid.slave_option = "";
			}
			
			return tiid;
		}
		
		private function database2DownloadListing(data:Object):DownloadListing
		{
			var dl:DownloadListing = new DownloadListing;
			var ft:FullTitle = database2FullTitle(data, true);
			
			dl.PassID = data._id;
			dl.Name = data.title_name;
			dl.BoxartPrefix = data.box_art_prefix;
			
			dl.formatDataFromFullTitlePurchased(ft as FullTitlePurchased);
			dl.DownloadStatus = data.download_status;
			dl.lastPlayPosition = parseInt(data.last_play_position, 10);
			dl.LicenseStatus = data.license_receieved == 1 ? LicenseStatus.LICENSE_RECEIVED : 0;
			
			dl.ExpireDate = (ft as FullTitlePurchased).DateExpired;
			
			if( data.purchase_type == 0 )
			{
				if( dl.ExpireDate.time <= (ft as FullTitlePurchased).DatePurchased.time )
				{
					const MILLISECONDS_PER_DAY:int = 24 *  60 * 60 * 1000;						
					dl.ExpireDate.setTime((ft as FullTitlePurchased).DatePurchased.time + (30 * MILLISECONDS_PER_DAY));
				}
			}
			else
			{
				//DateExpired is inaccurate
				var prods:ArrayCollection = (ft as FullTitlePurchased).AvailableProducts;
				if( prods && prods.length > 0 )
				{
					dl.ExpireDate = (prods.getItemAt(0) as Product).ExpireDateUTC;
				}
			}
			
			if (data.license_receieved)
			{

				if (data.first_play != "" )
				{
					var date:Date = new Date();
					
					date.setTime(Date.parse(data.first_play) + (data.view_hours * 3600 * 1000));
//					date.setHours(date.getHours() + data.view_hours);
					
					//Remove this line code, fix an issue on expired title filter
					//Normally first Play date is always later than rental expired date, but there are some wired data returned from server that the year of expired_date is 19xx.    By Yanlin
					//if (date.time < dl.ExpireDate.time)
						dl.ExpireDate = date;
				}
			}
			
			return dl;
		}
		
		private function database2FullTitle(tiid:Object, addPurchasedInfo:Boolean = false):FullTitle
		{
			var ft:FullTitle;
			if (addPurchasedInfo == true)
			{
				ft = new FullTitlePurchased;
			}
			else
			{
				ft = new FullTitle;
			}
			
			ft.TitleID = tiid.t_id;
			ft.Name = tiid.title_name;
			ft.BoxartPrefix = tiid.box_art_prefix;
			ft.MPAARating = tiid.mpaa_rating;
			ft.Actors = tiid.actors;
			ft.BuyPrice = tiid.buy_price;
			ft.Copyright = tiid.copyright;
			ft.Directors = tiid.directors;
			ft.Producers = tiid.producers;
			ft.RatingReason = tiid.rating_reason;
			ft.ReleaseYear = String(tiid.release_year);
			ft.RentPrice = tiid.rent_price;
			ft.SeasonTitleID = tiid.season_title_id;
			ft.ShowTitleID = tiid.show_title_id;
			ft.Synopsys = tiid.synopsys;
			ft.BuyAvail = (tiid.buy_avail == "true");
			ft.RentAvail = (tiid.rent_avail == "true");
			ft.Writers = tiid.writers;
			ft.TitleType = tiid.title_type;
			ft.MetaValues = string2MetaValue(tiid.meta_value);
			
			if (tiid.bonus_asset_id > 0)
			{
				var b:BonusAsset = new BonusAsset;
				b.BonusAssetID = tiid.bonus_asset_id;
				ft.BonusAssets = new ArrayCollection;
				ft.BonusAssets.addItem(b);
			}
			
			var p:ArrayCollection = new ArrayCollection;			
			if (tiid.buy_skuid > 0)
			{
				if ( (addPurchasedInfo == true && tiid.purchase_type == 1) || addPurchasedInfo == false)
				{
					var pBuy:Product = new Product;
					pBuy.SkuID = tiid.buy_skuid;
					var dBuy:Date = new Date;
					dBuy.setTime(Date.parse(tiid.buy_expire_date_utc));
					pBuy.ExpireDateUTC = dBuy;
					pBuy.PromoText = tiid.buy_promotext;
					pBuy.PurchaseType = tiid.buy_purchasetype;
					p.addItem(pBuy);
				}				
			}
			if (tiid.rent_skuid > 0)
			{
				if ( (addPurchasedInfo == true && tiid.purchase_type == 0) || addPurchasedInfo == false)
				{
					var pRent:Product = new Product;
					pRent.SkuID = tiid.rent_skuid;
					var dRent:Date = new Date;
					dRent.setTime(Date.parse(tiid.rent_expire_date_utc));
					pRent.ExpireDateUTC = dRent;
					pRent.PromoText = tiid.rent_promotext;
					pRent.PurchaseType = tiid.rent_purchasetype;
					pRent.RentalPeriod = tiid.rent_rental_period;
					p.addItem(pRent);
				}	
			}
			if (p.length > 0) ft.AvailableProducts = p;
			
			if (addPurchasedInfo == true)
			{
				var de:Date = new Date;
				de.setTime(Date.parse(tiid.date_expired));
				(ft as FullTitlePurchased).DateExpired = de;
				var db:Date = new Date;
				db.setTime(Date.parse(tiid.date_purchased));
				(ft as FullTitlePurchased).DatePurchased = db;
				(ft as FullTitlePurchased).ExpirationMessage = tiid.expiration_message;
				(ft as FullTitlePurchased).StoreLogoUrl = tiid.store_logo_url;
				(ft as FullTitlePurchased).StoreName = tiid.store_name;
				(ft as FullTitlePurchased).StreamPlayStatus = tiid.stream_play_status;
				(ft as FullTitlePurchased).StreamStartTimeSeconds = tiid.stream_start_time_seconds;
				(ft as FullTitlePurchased).WatchStatus = tiid.watch_status;
				(ft as FullTitlePurchased).PassID = tiid._id;
			}
			
			return ft;
		}
		
		private function titleObj2FullDatabase(ft:Object, fillAll:Boolean = true):Object
		{
			var tiid:Object = {};
			var meta:Object = ft.metadata;
			
			tiid.t_id = ft.titleid;
			tiid.title_name = (meta.name) ? meta.name : "";
			tiid.box_art_prefix = (meta.boxart) ? meta.boxart : "";
			tiid.actors = (meta.actors) ? meta.actors : "";
			tiid.copyright = (meta.copyright) ? meta.copyright : "";
			tiid.directors = (meta.directors) ? meta.directors : "";
			tiid.producers = (meta.producers) ? meta.producers : "";
			tiid.release_year = int((meta.releasedate) ? meta.releasedate : 0);
			tiid.writers = (meta.writers) ? meta.writers : "";
			tiid.title_type = (meta.titletype) ? meta.titletype : "";
			tiid.mpaa_rating = (meta.rating) ? meta.rating : "";
			tiid.run_time = (meta.runtime) ? meta.runtime : "";
			tiid.your_rating = int((meta.userrating) ? meta.userrating : 0);
			tiid.meta_value = (meta.meta_value) ? meta.meta_value : "";
			
			if (fillAll == true)
			{
				tiid.is_thx_media_director_enabled = "";
				tiid.hd = "";
				tiid.similar_avail = "";
				tiid.in_user_wishlist = "";
				tiid.rating_reason = "";
				tiid.buy_price = 0;
				tiid.rent_price = 0;
				tiid.season_title_id = 0;
				tiid.show_title_id = 0;
				tiid.synopsys = "";
				tiid.buy_avail = "true";
				tiid.rent_avail = "true";
				tiid.air_date = "";
				tiid.critics_review = "";
				tiid.flixster = "";
				tiid.buy_skuid = -1;
				tiid.buy_expire_date_utc = "";
				tiid.buy_promotext = "";
				tiid.buy_purchasetype = "";
				tiid.rent_skuid = -1;
				tiid.rent_expire_date_utc = "";
				tiid.rent_promotext = "";
				tiid.rent_purchasetype = "";
				tiid.rent_rental_period = -1;
				tiid.bonus_asset_id = -1;
				tiid.g_id = -1;
				tiid.master_option = "";
				tiid.slave_option = "";
			}
			
			return tiid;
		}
	}
}
