package com.rovicorp.controllers
{
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.MessageBoxEvent;
	import com.rovicorp.events.SelectionEvent;
	import com.rovicorp.model.Library;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.LibraryCategoryView;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.View;
	
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.events.FaultEvent;
	
	import spark.events.IndexChangeEvent;
	import spark.events.ViewNavigatorEvent;
	
	public class LibraryCategoryController extends ViewController
	{
		private var service:RoxioNowDAO = new RoxioNowDAO();
		
		public function LibraryCategoryController()
		{
		}
		
		private function setState(state:String):void
		{
			if (state == "loaded")
			{
				if (service.library.filters.length == 0)
				{
					if (service.isLoggedIn == false)
						view.currentState = "signedout";
					else
						view.currentState = "empty";
				}
				else
				{
					view.data = service.library;
					view.currentState = "loaded";
					LibraryCategoryView(view).categoryList.selectedIndex = service.library.filter;
					setViewState(service.library.state);
				}
			}
			else if (state == "loading")
				view.currentState = "loading";
		}
		
		private function setViewState(state:String):void
		{
			if (view.currentState == "loaded")
			{
				if (state == "loaded")
				{
					if (service.library.length == 0)
					{
						if (service.isLoggedIn == false && service.library.filterId != 'readyToWatch')
							LibraryCategoryView(view).view.currentState = "signedout";
						else
							LibraryCategoryView(view).view.currentState = "empty";
					}
					else
						LibraryCategoryView(view).view.currentState = "loaded";
				}
				else if (state == "loading")
					LibraryCategoryView(view).view.currentState = "loading";
			}
		}
		
		protected override function onCreationComplete():void
		{
			view.addEventListener(ViewNavigatorEvent.VIEW_ACTIVATE, onViewActivate, false, 0, true);
			view.addEventListener(SelectionEvent.CATEGORY_SELECTION_EVENT, onCategorySelection, false, 0, true);
			
			if (RoxioNowDAO.api)
			{
				service.library.filters.addEventListener(FaultEvent.FAULT, onLibraryFiltersFault, false, 0, true);
				service.library.filters.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onLibraryFiltersPropertyChange, false, 0, true);
				
				service.library.addEventListener(FaultEvent.FAULT, onLibraryFault, false, 0, true);
				service.library.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onLibraryPropertyChange, false, 0, true);
				
				setState(service.library.filters.state);
			}
			else
			{
				service.library.addEventListener(FaultEvent.FAULT, onLibraryFault, false, 0, true);
				service.library.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onLibraryPropertyChange, false, 0, true);
				
				view.data = service.library;
				
				setState("loaded");
				LibraryCategoryView(view).categoryList.visible = false;
			}
		}
		
		private function onViewActivate(event:ViewNavigatorEvent):void
		{
			if (view.currentState == "error" || view.currentState == "empty" )
				service.library.invalidateFilters();
		}
		
		private function onViewDeactivate(event:ViewNavigatorEvent):void
		{
			if (view.data)
			{
				view.data.removeEventListener(FaultEvent.FAULT, onLibraryFault);
				view.data.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onLibraryPropertyChange);
				view.data = null;
			}
		}
		
		private function onCategorySelection(event:SelectionEvent):void
		{
			var library:Library = view.data as Library
			
			if (library)
				library.filter = event.index;	
		}
		
		private function onLibraryFault(event:FaultEvent):void
		{
			LibraryCategoryView(view).view.setCurrentState("error");
		}
		
		private function onLibraryPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setViewState(service.library.state);
		}
		
		private function onLibraryFiltersFault(event:FaultEvent):void
		{
			view.setCurrentState("error");
		}
		
		private function onLibraryFiltersPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setState(service.library.filters.state);
		}
	}
}
