package com.rovicorp.controllers
{
	import com.rovicorp.events.SelectionEvent;
	import com.rovicorp.model.Store;
	import com.rovicorp.model.StoreCategory;
	import com.rovicorp.skins.StoreCategoryRenderer;
	import com.rovicorp.views.BrowseView;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.StoreCategoriesView;
	
	import mx.events.FlexEvent;
	import mx.events.ItemClickEvent;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.events.FaultEvent;
	
	import qnx.ui.events.ListEvent;
	
	import spark.events.IndexChangeEvent;
	import spark.events.ViewNavigatorEvent;

	public class StoreCategoriesController extends ViewController
	{
		
		private var _store:Store;
		private var _storeCategoriesView:StoreCategoriesView;

		public function StoreCategoriesController(store:Store)
		{
			_store = store;
		}

		public function get store():Store
		{
			return _store;
		}
		
		protected override function onCreationComplete():void
		{
			_storeCategoriesView = view as StoreCategoriesView; 
			
			view.addEventListener(ViewNavigatorEvent.VIEW_ACTIVATE, onViewActivate, false, 0, true);
			view.addEventListener(FlexEvent.DATA_CHANGE, onDataChange, false, 0, true);
			
//			view.data = store.categories;
			view.addEventListener(SelectionEvent.TITLE_SELECTION_EVENT, onSelectTitle, false, 0, true);
			view.addEventListener(SelectionEvent.CATEGORY_SELECTION_EVENT, onSelectCategory, false, 0, true);
		}
		
		private function onDataChange(event:FlexEvent):void
		{
			view.data.addEventListener(FaultEvent.FAULT, onFault, false, 0, true);
			view.data.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onStoreCategoryPropertyChange, false, 0, true);
			
			setState(view.data.state);
		}
		
		private function onViewActivate(event:ViewNavigatorEvent):void
		{
			if (view.data !== store.categories)
				view.data = store.categories;
		}
		
		private function setState(state:String):void
		{
			if (state == "loaded")
			{
				_storeCategoriesView.currentState = "loaded";
				_storeCategoriesView.list.setSkin(StoreCategoryRenderer);
			}
			else if (state == "loading")
				_storeCategoriesView.currentState = "loading";
		}
		
		private function onFault(event:FaultEvent):void
		{
			view.currentState = "error";
		}
		
		private function onSelectCategory(event:SelectionEvent):void
		{
			var category:StoreCategory = event.item.getItemAt(event.index);
			store.category = category.ID;

			if (store.state == "error")
				store.invalidate();
			
			view.navigator.pushView(BrowseView,{ store:store, title:category.Name });
			event.stopImmediatePropagation();
		}

		private function onSelectTitle(event:SelectionEvent):void
		{
			var info:Object = { titles:event.item, index:event.index };
			view.navigator.pushView(MovieDetailsView, info);

			event.stopImmediatePropagation();
		}
		
		private function onStoreCategoryPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setState(event.newValue as String);
		}
	}
}