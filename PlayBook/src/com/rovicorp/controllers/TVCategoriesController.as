package com.rovicorp.controllers
{
	import com.rovicorp.model.RoxioNowDAO;
	
	public class TVCategoriesController extends StoreCategoriesController 
	{
		public function TVCategoriesController()
		{
			super(new RoxioNowDAO().tvShows);
		}
	}
}