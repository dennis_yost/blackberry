package com.rovicorp.controllers
{
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.MessageBoxEvent;
	import com.rovicorp.model.Library;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.LibraryView;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.View;
	
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.events.FaultEvent;
	
	import spark.events.IndexChangeEvent;
	import spark.events.ViewNavigatorEvent;
	
	public class LibraryController extends ViewController
	{
		private var service:RoxioNowDAO = new RoxioNowDAO();
		
		public function LibraryController()
		{
		}
		
		private function setState(state:String):void
		{
			if (state == "loaded")
			{
				if (service.library.categoryID == "readyToWatch")
					view.currentState = (view.data.length == 0) ? "empty" : "loaded";
				else
				{
					if (service.isLoggedIn == false)
						view.currentState = "signedout";
					else if (view.data.length == 0) 
						view.currentState = "empty";
					else
						view.currentState = "loaded";
				}
			}
			else if (state == "loading")
				view.currentState = "loading";
		}

		protected override function onCreationComplete():void
		{
			view.actionBarVisible = view.navigator.length > 1;
			view.addEventListener(ViewNavigatorEvent.VIEW_DEACTIVATE, onViewDeactivate, false, 0, true);
			
			service.library.addEventListener(FaultEvent.FAULT, onLibraryFault, false, 0, true);
			service.library.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onLibraryPropertyChange, false, 0, true);
			view.data = service.library;

			setState(view.data.state);
		}
		
		private function onViewDeactivate(event:ViewNavigatorEvent):void
		{
			if (view.data)
			{
				view.data.removeEventListener(FaultEvent.FAULT, onLibraryFault);
				view.data.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onLibraryPropertyChange);
				view.data = null;
			}
		}
		
		private function onLibraryFault(event:FaultEvent):void
		{
			view.setCurrentState("error");
		}
		
		private function onLibraryPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setState(event.newValue as String);
		}
	}
}
