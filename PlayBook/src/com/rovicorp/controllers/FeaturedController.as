package com.rovicorp.controllers
{
	import com.rovicorp.controls.BrowseList;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.model.TitleListing;
	import com.rovicorp.skins.FeaturedTitleRenderer;
	import com.rovicorp.skins.TitleCellRenderer;
	import com.rovicorp.utilities.Boxart;
	import com.rovicorp.views.FeaturedView;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.View;
	
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.events.TransformGestureEvent;
	import flash.utils.Timer;
	
	import flashx.textLayout.formats.Direction;
	
	import mx.events.EffectEvent;
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;
	import mx.events.TouchInteractionEvent;
	import mx.rpc.events.FaultEvent;
	
	import qnx.ui.events.ScrollEvent;
	
	import spark.components.List;
	import spark.core.NavigationUnit;
	import spark.effects.Animate;
	import spark.effects.animation.Animation;
	import spark.effects.animation.MotionPath;
	import spark.effects.animation.SimpleMotionPath;
	import spark.effects.easing.Sine;
	import spark.events.IndexChangeEvent;
	import spark.events.ViewNavigatorEvent;
	
	public class FeaturedController extends ViewController
	{
		private var _service:RoxioNowDAO = new RoxioNowDAO();
		private var _featuredView:FeaturedView;
		private var _currentPage:Number=0;
		private var _totalPages:Number;
		private var _delay:uint = 10000;
		private var _repeat:uint = 1;
		private var _myTimer:Timer;
		private var _queued:int;
		private var _direction:int = 1;
		
		private var _featureList:List;
		private var _highlightsList:BrowseList;
		
		public function FeaturedController()
		{
		}

		private var _animator:Animate = null;
		private function get animator():Animate
		{
			if (_animator)
				return _animator;

			_animator = new Animate(_featureList.dataGroup);
			_animator.easer = new Sine(0);
			_animator.addEventListener(EffectEvent.EFFECT_END, onScrollEnd, false, 0, true);
			_animator.addEventListener(EffectEvent.EFFECT_STOP, onScrollEnd, false, 0, true);
			
			return _animator;
		}
		
		private function queueImages(page:int):void
		{
			trace("Queueing page ", page);
			var last:int = Math.min((page + 1) * 3, _service.featured.features.length);
			
			for ( ; _queued < last; _queued++)
			{
				var item:TitleListing = _service.featured.features.getItemAt(_queued) as TitleListing;
				FeaturedTitleRenderer.loaderUtil.addTask(last, Boxart.URL_203 + item.BoxartPrefix + Boxart.POST_FIX_203, onImageLoaded);
			}
		}
		
		private function setState(state:String):void
		{
			if (state == "loaded")
			{
				if (_service.featured.features.length == 0) 
					_featuredView.currentState = "empty";
				else
				{
					_featuredView.currentState = "loaded";
					_totalPages=_featuredView.list1.dataProvider.length / 3;
					
					if (_featureList == null)
					{
						_featureList = _featuredView.list1;
						_featureList.addEventListener(TouchInteractionEvent.TOUCH_INTERACTION_STARTING, onTouchInteractionStarting);
						_featureList.addEventListener(TouchInteractionEvent.TOUCH_INTERACTION_END, onTouchInteractionEnd);
//						_featureList.scroller.horizontalScrollBar.addEventListener(Event.CHANGE, onScrollEnd, false, 0, true);
//						_featureList.scroller.horizontalScrollBar.setStyle("repeatInterval", 2000);       
					}

					if (_highlightsList == null)
					{
						_highlightsList = _featuredView.list2;
						_highlightsList.addEventListener(ScrollEvent.SCROLL_MOVE, onHighlightsScroll, false, 0, true);
					}

					queueImages(1);
					
					if (view.isActive)
						startTimer();
				}
			}
			else if (state == "loading")
			{
				_featuredView.currentState = "loading";
				stopTimer();
			}
		}

		private function startTimer():void
		{
			if (_myTimer == null)
			{
				//						_featuredView.list1.addEventListener(IndexChangeEvent.CHANGE, onSelectionChange, false, 0, true);
				//						_featuredView.list1.addEventListener(TransformGestureEvent.GESTURE_SWIPE,onFeaturedListSwipe);
				//						_featuredView.list2.addEventListener(IndexChangeEvent.CHANGE, onSelectionChange, false, 0, true);
				
				
				_myTimer = new Timer(_delay, _repeat);
				_myTimer.addEventListener(TimerEvent.TIMER_COMPLETE, completeHandler);
				_myTimer.start();	
				
				//_featuredView.extendCollapseButton.addEventListener(MouseEvent.CLICK, onExtendCollapseClick);
			}
			
		}

		private function stopTimer():void
		{
			if (_myTimer)
			{
				_myTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, completeHandler);
				_myTimer.stop();	
				_myTimer = null;
			}
		}
		
		private function completeHandler(e:TimerEvent):void {
			FeaturedTitleRenderer.loaderUtil.pause();
//			goToNextPage();

			var page:int = _featureList.dataGroup.horizontalScrollPosition / _featureList.width + _direction;
			
			if (page < 0)
			{
				_direction = -_direction;
				page = 1;
			}
			else if (page == _totalPages)
			{
				_direction = -_direction;
				page = _totalPages - 2;
			}
			
/*			var pageSize:int = Math.abs(_featureList.scroller.viewport.getHorizontalScrollPositionDelta(
				(_direction) ? NavigationUnit.PAGE_RIGHT : NavigationUnit.PAGE_LEFT));
			
			if (pageSize == 0)
			{
				_direction = !_direction;
				pageSize = Math.abs(_featureList.scroller.viewport.getHorizontalScrollPositionDelta(
					(_direction) ? NavigationUnit.PAGE_RIGHT : NavigationUnit.PAGE_LEFT));
			}
*/
			startAnimation(2000, page * 1140);	// include padding
//			_featuredView.list1.scroller.horizontalScrollBar.changeValueByPage(_increase);
//			_featuredView.list1.scroller.horizontalScrollBar.value +=30;
			
			_myTimer.start();
		}
		
		protected override function onCreationComplete():void
		{
			_featuredView = view as FeaturedView;
			_featuredView.addEventListener(ViewNavigatorEvent.VIEW_ACTIVATE, onViewActivate);
			_featuredView.addEventListener(ViewNavigatorEvent.VIEW_DEACTIVATE, onViewDeactivate);

			view.data = _service.featured;
			setState(_service.featured.state);
			
			_service.featured.addEventListener(FaultEvent.FAULT, onFault, false, 0, true);
			_service.featured.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onFeaturedPropertyChange, false, 0, true);
		}
		
		private function onViewActivate(event:ViewNavigatorEvent):void
		{
			if (_service.featured.state == "error")
				_service.featured.invalidate();

			if (_featuredView.list1)
				startTimer();
			
			if (_highlightsList)
				_highlightsList.enabled = true;
		}

		private function onViewDeactivate(event:ViewNavigatorEvent):void
		{
			if (_highlightsList)
				_highlightsList.enabled = false;
	
			stopTimer();
		}
		
		private function onFault(event:FaultEvent):void
		{
			view.currentState = "error";
		}
		
		private function startAnimation(duration:Number, valueTo:Number, startDelay:Number = 0):void
		{
			animator.stop();
			animator.duration = duration;
			animator.motionPaths = new <MotionPath>[
				new SimpleMotionPath("horizontalScrollPosition", _featureList.dataGroup.horizontalScrollPosition, valueTo)];
			animator.startDelay = startDelay;
			animator.play();
		}

		private function onHighlightsScroll(event:ScrollEvent):void
		{
			if (event.deltaY < 0 && event.phase == "now")
				view.currentState = "extended";
		}
		
		private function onFeaturedPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state")
				setState(event.newValue as String);
		}
		
		private function onExtendCollapseClick(event:MouseEvent):void {
			if (_featuredView.currentState == "loaded") {
				
				
				//_featuredView.currentState = "extended";
			} else {
				
				
				//_featuredView.currentState = "loaded";
			}
		}
		
		private function onScrollEnd(event:Event):void
		{
			FeaturedTitleRenderer.loaderUtil.resume();

			var currentPage:int = _featuredView.list1.scroller.horizontalScrollBar.value / _featuredView.list1.width;
			queueImages(currentPage + 1);
		}
		
		private function onImageLoaded(index:int, data:BitmapData):void
		{
		}
		
		public function onTransitionStart(event:EffectEvent):void
		{
			if (view.currentState == "extended")
			{
				_highlightsList.removeEventListener(ScrollEvent.SCROLL_MOVE, onHighlightsScroll);
				TitleCellRenderer.loaderUtil.pause();
				stopTimer();
			}
//			else if (view.currentState == "loaded")
				_highlightsList.scrollIndexVisible(0,0.1);
		}
		
		public function onTransitionEnd(event:EffectEvent):void
		{
			if (view.currentState == "loaded")
			{
				_highlightsList.addEventListener(ScrollEvent.SCROLL_MOVE, onHighlightsScroll, false, 0, true);
				startTimer();
			}
			else if (view.currentState == "extended")
				TitleCellRenderer.loaderUtil.resume();
		}
		
		private function onTouchInteractionEnd(event:TouchInteractionEvent):void
		{
			FeaturedTitleRenderer.loaderUtil.resume();
		}
		
		private function onTouchInteractionStarting(event:TouchInteractionEvent):void
		{
			stopTimer();
			animator.stop();
			
			FeaturedTitleRenderer.loaderUtil.pause();
		}
		
	}
}
