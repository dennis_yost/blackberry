package com.rovicorp.controllers
{
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.ForgotPasswordView;
	import com.roxionow.flash.util.crypto.XOREncryption;
	import com.sonic.response.StandardResponse;
	
	import flash.events.*;
	import flash.desktop.NativeApplication;
	
	import mx.events.ValidationResultEvent;
	import mx.rpc.events.FaultEvent;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	import qnx.input.IMFConnection;
	
	public class ForgotPasswordController extends ViewController
	{
		private var vResult:ValidationResultEvent;
		
		public function ForgotPasswordController()
		{
			super();
		}
		
		protected override function onCreationComplete():void
		{
			view.currentState = "normal";
			view.addEventListener('send', _onSend);
			ForgotPasswordView(view).email.addEventListener(DeviceEvent.RETURN_PRESSED,_onSend);
			ForgotPasswordView(view).email.setFocus();
		}
		
		private function _onSend($e:Event):void {
			validateAndSubmit();
			CONFIG::device{ IMFConnection.imfConnection.hideInput() };
		}
		
		private function validateAndSubmit():void {
			// Validate the required fields. 
			vResult = ForgotPasswordView(view).emailValidator.validate();

			if (vResult.type==ValidationResultEvent.INVALID) {
				var dialog:MessageDialog = new MessageDialog();
				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Forgot Password Validation";
				dialog.message = vResult.message;
				dialog.show();
			} else {
				var appXML:XML = NativeApplication.nativeApplication.applicationDescriptor;
				var ns:Namespace = appXML.namespace();				
				
				var storeURL:String = "";
				var conf:Object = new RoxioNowDAO().settings.conf;
				if( conf && conf.Brand_support_url )
				{
					storeURL = conf.Brand_support_url;
				}
				
				view.currentState = "loading";
				
				var request:RoxioNowDAO = new RoxioNowDAO;
				request.addEventListener(FaultEvent.FAULT, onEmailPasswordFault);
				request.emailPassword(ForgotPasswordView(view).email.text,storeURL,appXML.ns::name,onEmailPasswordComplete);
			}
		}
		
		private function onEmailPasswordComplete(response:StandardResponse):void {
			trace('Email password sent,response: '+response.ResponseMessage);
			
			view.currentState = "normal";
			
			var dialog:MessageDialog;
			if( response.ResponseCode == 0 )
			{
				dialog = MessageDialog.create("FORGOT_PASSWORD_TITLE", "FORGOT_PASSWORD_MESSAGE", "FORGOT_PASSWORD_INSTRUCTION");
				dialog.addEventListener(Event.SELECT, onMessageSelect, false, 0, true);
			}
			else if (response.ResponseCode == 550 )
			{
				dialog = MessageDialog.create("FORGOT_PASSWORD_TITLE", "", "FORGOT_PASSWORD_ERROR");				
			}else
			{
				var resourceMgr:IResourceManager = ResourceManager.getInstance();
				
				dialog = new MessageDialog();				
				dialog.title = resourceMgr.getString('LocalizedStrings', 'FORGOT_PASSWORD_TITLE');
				dialog.message = response.ResponseMessage;
			}
			
			dialog.show();
		}
		
		private function onEmailPasswordFault(event:FaultEvent):void {
			view.currentState = "normal";
			view.dispatchEvent(new FaultEvent(FaultEvent.FAULT, true, true, event.fault, event.token, event.message));
		}
		
		private function onMessageSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onMessageSelect);
			view.navigator.popView();
		}
		
		private function onErrorSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onMessageSelect);
			
			ForgotPasswordView(view).email.setFocus();
			ForgotPasswordView(view).email.selectAll();
		}
	}
}