package com.rovicorp.controllers
{
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.controls.QNXTextInputWrapper;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.ChangePasswordView;
	import com.sonic.response.StandardResponse;
	
	import flash.display.DisplayObject;
	import flash.events.*;
	import flash.sampler.NewObjectSample;
	import flash.utils.Dictionary;
	
	import mx.core.FlexGlobals;
	import mx.events.ValidationResultEvent;
	import mx.managers.FocusManager;
	import mx.managers.IFocusManagerComponent;
	import mx.rpc.events.FaultEvent;
	import mx.validators.Validator;
	
	import qnx.input.IMFConnection;
		
	public class ChangePasswordController extends ViewController
	{
		
		private var validationResult:Array;
		private var passwordValidators:Array;
		private var changePasswordView:ChangePasswordView;
		
		private var isSuccess:Boolean = false;
		private var focusMap:Dictionary;
				
		public function ChangePasswordController()
		{
			super();
		}
		
		protected override function onCreationComplete():void {
			view.currentState = "normal";
			view.addEventListener('submit', _onSubmit);
			changePasswordView = ChangePasswordView(view);
			passwordValidators = [changePasswordView.passwordValidator,changePasswordView.newPasswordLengthValidator,changePasswordView.passwordRuleValidator];
			changePasswordView.currentPassword.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			changePasswordView.newPassword.addEventListener(DeviceEvent.RETURN_PRESSED,setNextFocus);
			changePasswordView.newPasswordConfirm.addEventListener(DeviceEvent.RETURN_PRESSED,_onSubmit);
			
			focusMap = new Dictionary(true);
			focusMap[changePasswordView.currentPassword] = changePasswordView.newPassword;
			focusMap[changePasswordView.newPassword] = changePasswordView.newPasswordConfirm;
			focusMap[changePasswordView.newPasswordConfirm] = null;
		}
		
		private function _onSubmit($e:Event):void {
			CONFIG::device{ IMFConnection.imfConnection.hideInput();}
			validateAndSubmit();
		}
		
		private function validateAndSubmit():void {
			if( view.currentState == "loading" ) return;
			
			validationResult = Validator.validateAll(passwordValidators);

			if (validationResult.length > 0) {
				var dialog:MessageDialog = new MessageDialog();
				var error:ValidationResultEvent;
				var errorMessageArray:Array = [];

				for each (error in validationResult) {
					errorMessageArray.push(error.message);
					dialog.height = dialog.height + 20;
				}

				dialog.addEventListener(Event.SELECT, onErrorSelect, false, 0, true);
				dialog.title = "Change Password Validation";
				dialog.warning = true;
				dialog.message = errorMessageArray.join("\n");
				dialog.show();
			} else {
				isSuccess = false;
				view.currentState = "loading";
				var request:RoxioNowDAO = new RoxioNowDAO;
				request.addEventListener(FaultEvent.FAULT, onPasswordChangeFault);
				request.setUserPassword(ChangePasswordView(view).currentPassword.text,ChangePasswordView(view).newPassword.text,onPasswordChangeComplete);
			}
		}
		
		private function onPasswordChangeComplete(response:StandardResponse):void {
			view.currentState = "normal";
			var dialog:MessageDialog = new MessageDialog();
			dialog.addEventListener(Event.SELECT, onMessageSelect, false, 0, true);
			dialog.title = "Password Change";
			if( response.ResponseCode == 0 )
			{
				dialog.message = "Your password has been changed";
				isSuccess = true;
			}
			else if( response.ResponseCode == 52 )
			{
				dialog.message = "Invalid password";
				isSuccess = false;
			}
			else{
				dialog.message = response.ResponseMessage;
				isSuccess = false;
			}
			dialog.show();
		}

		private function onPasswordChangeFault(event:FaultEvent):void 
		{
			view.currentState = "normal";
			view.dispatchEvent(new FaultEvent(FaultEvent.FAULT, true, true, event.fault, event.token, event.message));
		}
		
		private function onMessageSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onMessageSelect);
			
			if( isSuccess )
				view.navigator.popView();
			else
				(view as ChangePasswordView).currentPassword.setFocus();
		}
		
		private function onErrorSelect(event:Event):void {
			event.currentTarget.removeEventListener(Event.SELECT,onErrorSelect);
		}
		
		private function setNextFocus(event:DeviceEvent):void
		{
			if (focusMap[event.target] == null)
			{
				resetState();
			}
			else if (focusMap[event.target] is QNXTextInputWrapper)
			{
				(focusMap[event.target] as QNXTextInputWrapper).setFocus();
			}
			else
			{
				var nextFocusObj:IFocusManagerComponent = focusMap[event.target];
				changePasswordView.focusManager.setFocus(nextFocusObj);
			}
		}
		
		private function resetState(event:MouseEvent=null):void
		{
			changePasswordView.stage.focus = null;
			
			CONFIG::device
			{
				IMFConnection.imfConnection.hideInput();
			}
		}
	}
}