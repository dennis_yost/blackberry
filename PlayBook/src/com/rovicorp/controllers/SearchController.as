package com.rovicorp.controllers
{
	import com.rovicorp.model.CastAndCrewCollection;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.views.MovieDetailsView;
	import com.rovicorp.views.SearchView;
	import com.sonic.SearchOption;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.binding.utils.ChangeWatcher;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.FlexEvent;
	import mx.events.ItemClickEvent;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.events.FaultEvent;
	
	import spark.events.IndexChangeEvent;

	public class SearchController extends MiscViewController
	{
		
		private var _searchView:SearchView;
		private var _changeWatcher:ChangeWatcher;
		private var _server:RoxioNowDAO = new RoxioNowDAO();
		
		public function SearchController()
		{
		}
		
		private function setState(state:String):void
		{
			if (state == "loaded")
			{
				if (view.data.searchData.length == 0) 
					_searchView.currentState = "empty";
				else
				{
					if (view.data.searchData is CastAndCrewCollection)
						_searchView.currentState = "searchPerson";
					else
						_searchView.currentState = "loaded";
				}
				
				_searchView.CategorySize.text = "(" + view.data.searchData.length + ")";
			}
			else if (state == "loading")
			{
				_searchView.currentState = "loading";
			}
		}
		
		protected override function onCreationComplete():void
		{
			_searchView = view as SearchView;
			setState(view.data.searchData.state);

			view.data.searchData.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onSearchPropertyChange, false, 0, true);
			view.data.searchData.addEventListener(FaultEvent.FAULT, onSearchFault, false, 0, true);
//			_server.search.addEventListener(CollectionEvent.COLLECTION_CHANGE, onSourceChanged);
			
			view.addEventListener(IndexChangeEvent.CHANGE, onItemSelected);
			view.addEventListener(FlexEvent.DATA_CHANGE, onDataChange);
			_searchView.btnBack.addEventListener(MouseEvent.CLICK, onBack);
		}
		
		private function onDataChange(event:FlexEvent):void
		{
			setState(view.data.searchData.state);
			view.data.searchData.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onSearchPropertyChange, false, 0, true);
			view.data.searchData.addEventListener(FaultEvent.FAULT, onSearchFault, false, 0, true);
		}

		private function onItemSelected(event:IndexChangeEvent):void
		{
			var selectedItem:Object = view.data.searchData.getItemAt(event.newIndex);
			view.navigator.pushView(SearchView, {searchString:selectedItem.Name, searchData:_server.searchTitlesByCast(selectedItem.CastID, view.data.searchData.option)});
		}
		
		private function onSearchFault(event:FaultEvent):void
		{
			view.setCurrentState("error");
		}
		
		private function onSearchPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "state") {
				setState(event.newValue as String);
			}
		}
		
		private function onSourceChanged(event:CollectionEvent):void
		{
			if( _searchView && _searchView.CategorySize )
				_searchView.CategorySize.text = _server.search.length.toString();
		}
		
		private function onSelectionChange(event:IndexChangeEvent):void
		{
			view.navigator.pushView(MovieDetailsView, {titles:view.data.searchData, index:event.newIndex});
		}
	}
}