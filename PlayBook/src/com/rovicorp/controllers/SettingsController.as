package com.rovicorp.controllers
{
	import com.rovicorp.controls.LegalInfoDialog;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.UIEvent;
	import com.rovicorp.events.VideoStoreEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.model.UserSettings;
	import com.rovicorp.views.SettingsView;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;

	public class SettingsController extends MiscViewController
	{
		public function SettingsController(){
			super();
		}

		protected override function onCreationComplete():void
		{
			// TODO: Determine policy for when to use a local controller vs. application controller			
//dpy(20140625)-->			view.addEventListener(VideoStoreEvent.CHANGE_BILLING_INFO, onChangeBillingInfo);
			view.addEventListener(VideoStoreEvent.CHANGE_PASSWORD, onChangePassword);
			view.addEventListener(VideoStoreEvent.VIEW_LEGAL, onViewLegal);
			view.addEventListener(VideoStoreEvent.SUPPORT, onSupport);
			view.addEventListener(VideoStoreEvent.SET_PARENTAL, onParental);
			view.addEventListener(FlexEvent.SHOW, onViewShow);
			SettingsView(view).btnBack.addEventListener(MouseEvent.CLICK, onSettingExit);
			
			SettingsView(view).service.settings.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, settingsChanged);
			
			if (RoxioNowDAO.is_offline == true)
			{
				SettingsView(view).isOffline = true;
			}
		}
		
		private function onSettingExit(e:MouseEvent):void
		{
			EventCenter.inst.dispatchEvent(new UIEvent(UIEvent.SETTING_SCREEN_HIDE));
			onBack(e);
		}
		
		private function settingsChanged(e:PropertyChangeEvent):void
		{
			var settings:UserSettings = SettingsView(view).service.settings;
			if( e.property == "subscribeNewsLetter" )
			{
				SettingsView(view).subscribeNews.selected = settings.subscribeNewsLetter;
			}
		}
		
		protected function onViewShow(event:FlexEvent):void
		{
			if (RoxioNowDAO.is_offline == true)
			{
				SettingsView(view).isOffline = true;
			}
		}
		
		private function onChangeBillingInfo(event:VideoStoreEvent): void 
		{
			trace("change billing requested");
		}
		
		private function onChangePassword(event:VideoStoreEvent): void 
		{
			trace("change password requested");
		}
		
		private function onViewLegal(event:VideoStoreEvent): void 
		{
			var dialog:LegalInfoDialog = new LegalInfoDialog();
			dialog.show();
		}

		public function onSupport($e:VideoStoreEvent):void {
			trace('support requested');	
			var helpURL:String = "help://com.rovi.videostore";
			if( CONFIG::location == "canada" ) helpURL = "help://com.rovi.videostoreca";
			navigateToURL(new URLRequest(helpURL));
		}
		
		private function onParental($e:VideoStoreEvent):void 
		{
			var conf:Object = new RoxioNowDAO().settings.conf;
			if( conf && conf.Parental_url )
			{
				navigateToURL( new URLRequest(conf.Parental_url));
			}
		}
				
	}
}