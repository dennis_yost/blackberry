package com.rovicorp.controllers
{
	import com.rovicorp.events.VideoStoreEvent;
	
	import flash.events.MouseEvent;

	public class MiscViewController extends ViewController
	{
		public function MiscViewController()
		{
			super();
		}
		
		public function goBack():void
		{
			if (view.navigator.length > 1)
				view.navigator.popView();
			else
				view.dispatchEvent(new VideoStoreEvent(VideoStoreEvent.GO_BACK));
		}

		protected function onBack(event:MouseEvent):void
		{
			goBack();
		}
	}
}