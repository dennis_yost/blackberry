package com.rovicorp.controllers
{
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.TitleEvent;
	import com.rovicorp.events.UIEvent;
	import com.rovicorp.model.AsyncCollection;
	import com.rovicorp.model.Store;
	import com.rovicorp.model.TitleListing;
	import com.rovicorp.model.TitleListingBase;
	import com.rovicorp.skins.TitleDetailsRenderer;
	import com.rovicorp.skins.TitlesCategoryRenderer;
	import com.rovicorp.views.MovieDetailsView;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TransformGestureEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	import mx.binding.utils.ChangeWatcher;
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.events.FaultEvent;
	
	import qnx.events.QNXApplicationEvent;
	import qnx.system.QNXApplication;
	
	import spark.components.Button;
	import spark.components.List;
	import spark.components.RichEditableText;
	import spark.core.NavigationUnit;
	
		
	public class TitleDetailsController
	{
		private const SCROLL_TYPE_NONE:int = -1;
		private const SCROLL_TYPE_LEFT:int = 0;
		private const SCROLL_TYPE_RIGHT:int = 1;
		private const SCROLL_TYPE_HEAD:int = 2;
		private const SCROLL_TYPE_END:int = 2;
		
		private var movieDetails:MovieDetailsView;
		private var titles:Store;
		
		[Bindable]
		public var totalPages:Number;
		[Bindable]
		public var currentPage:Number=0;
		
		private var nextScrollType:int = -1;
		
		/**
		 * Constructor for the class, sets a list that will be populated
		 * with the titles to be shown with full details
		 * 
		 */
		public function TitleDetailsController()
		{
		}
		
		
		/**
		 * Creates the elements required for the view to display 
		 * elements when the view is created 
		 * @param event information of the event
		 * 
		 */	
		public function onCreationComplete(event:Event):void
		{
			movieDetails = event.target as MovieDetailsView;

			EventCenter.inst.addEventListener(UIEvent.VIEW_NAVIGATOR_PUSH_TRANSITION_END, onViewNavigatorPushEnd, false, 0, true);
			EventCenter.inst.addEventListener(TitleEvent.WISHLIST_REMOVE_DONE, onTitleRemoveFromWishDone, false, 0, true);
		}
		
		protected function onViewNavigatorPushEnd(e:UIEvent):void
		{
			EventCenter.inst.removeEventListener(UIEvent.VIEW_NAVIGATOR_PUSH_TRANSITION_END, onViewNavigatorPushEnd);
			movieDetails.titlesList.dataProvider = movieDetails.data.titles; 
			
			currentPage = movieDetails.data.index;

			movieDetails.titlesList.addEventListener(FlexEvent.UPDATE_COMPLETE, onUpdateComplete, false, 0, true);
			movieDetails.titlesList.scroller.setStyle("horizontalScrollPolicy", "off");
			movieDetails.titlesList.scroller.setStyle("verticalScrollPolicy", "off");
			movieDetails.titlesList.scroller.horizontalScrollBar.setStyle("smoothScrolling", true);
			movieDetails.titlesList.addEventListener(TransformGestureEvent.GESTURE_SWIPE,handleSwipe);
			movieDetails.backButton.addEventListener(MouseEvent.CLICK,onBackButtonClick);
			movieDetails.leftButton.addEventListener(MouseEvent.CLICK,goToPreviousPage);
			movieDetails.rightButton.addEventListener(MouseEvent.CLICK,goToNextPage);
			totalPages=movieDetails.titlesList.dataProvider.length;
			
			if (checkValueExisting(currentPage) == true)
			{
				movieDetails.currentState = "Movie";
				movieDetails.titlesList.visible = true;
			}
		}
		
		private function onUpdateComplete(event:FlexEvent):void
		{
			movieDetails.titlesList.removeEventListener(FlexEvent.UPDATE_COMPLETE, onUpdateComplete);
			movieDetails.titlesList.ensureIndexIsVisible(currentPage);
		}
		
		/**
		 * Called when the user taps on the back button
		 * @param event information of the mouse event
		 * 
		 */	
		public function onBackButtonClick(e:MouseEvent):void
		{
			movieDetails.backButton.removeEventListener(MouseEvent.CLICK,onBackButtonClick);
		
			if (currentPage == -1)
			{
				EventCenter.inst.removeEventListener(UIEvent.VIEW_NAVIGATOR_PUSH_TRANSITION_END, onViewNavigatorPushEnd);
			}
			
			EventCenter.inst.removeEventListener(TitleEvent.WISHLIST_REMOVE_DONE, onTitleRemoveFromWishDone);
			movieDetails.navigator.popView();
		}
		
		private function onTitleRemoveFromWishDone(e:TitleEvent):void
		{
			onBackButtonClick(null);
		}
		
		/**
		 * Called when the user taps on the back button
		 * @param event information of the mouse event
		 * 
		 */	
		
		private function handleSwipe(event:TransformGestureEvent):void
		{
			if (event.offsetX == 1 && event.phase == "end") {
				goToPreviousPage();
			} else if (event.offsetX == -1  && event.phase == "end") {
				goToNextPage();
			} else {
				return;
			}
			
		}
		
		private function goToNextPage(event:Event = null):void {
			if (currentPage == totalPages-1) {
				currentPage = 0;
				nextScrollType = SCROLL_TYPE_HEAD;
			} else {
				currentPage++;
				nextScrollType = SCROLL_TYPE_RIGHT;
			}	
			if (checkValueExisting(currentPage) == true)
				movieDetails.titlesList.scrollToPosition(currentPage);
		}
		
		private function goToPreviousPage(event:Event = null):void {
			if (currentPage == 0) {
				currentPage = totalPages-1;
				nextScrollType = SCROLL_TYPE_END;
			} else {
				currentPage--;
				nextScrollType = SCROLL_TYPE_LEFT;
			}
			if (checkValueExisting(currentPage) == true)
				movieDetails.titlesList.scrollToPosition(currentPage);
		}
		
		private function checkValueExisting(idx:int):Boolean
		{
			var ac:AsyncCollection = movieDetails.data.titles as AsyncCollection;
			var ar:ArrayCollection = movieDetails.data.titles as ArrayCollection;
			if (ac == null && ar == null) return true;
			
			var tl:TitleListingBase;
			if (ac != null)
			{
				tl = ac.getItemAt(idx) as TitleListingBase;
			}
			else if (ar != null)
			{
				tl = ar.getItemAt(idx) as TitleListingBase;
			}
			
			if (tl == null || checkTitleDetail(tl) == false)
			{
				if (tl != null)
				{
					
//					tlWatcher = ChangeWatcher.watch(tl, "Details", onTLChange, false, true);
					tl.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onTLChange, false, 0, true);
					tl.addEventListener(FaultEvent.FAULT, onTLFault, false, 0, true);
					movieDetails.currentState = "loading";
				}
				return false;
			}
			return true;
		}
		
		private function checkTitleDetail(tl:TitleListingBase):Boolean
		{
			return (tl.Details != null);
		}
		
		private function onTLChange(e:PropertyChangeEvent):void
		{
			if (e.property == "Details")
			{
				e.target.removeEventListener(PropertyChangeEvent, onTLChange);
				e.target.removeEventListener(FaultEvent.FAULT, onTLFault);
				
				if (e.newValue != null)
				{
	//				tlWatcher.unwatch();
	//				tlWatcher = null;
					
					movieDetails.currentState = "Movie";
					movieDetails.titlesList.visible = true;
					doPendingScroll();
				}
			}
		}
		
		private function onTLFault(event:FaultEvent):void
		{
			movieDetails.currentState = "error";
		}
		
		private function doPendingScroll():void
		{
			switch (nextScrollType)
			{
				case SCROLL_TYPE_NONE:
					return;
				default:
					movieDetails.titlesList.scrollToPosition(currentPage);
					break;
			}
			nextScrollType = SCROLL_TYPE_NONE;
		}
		
	}
}