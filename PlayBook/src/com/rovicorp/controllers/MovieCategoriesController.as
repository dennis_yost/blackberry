package com.rovicorp.controllers
{
	import com.rovicorp.model.RoxioNowDAO;
	
	public class MovieCategoriesController extends StoreCategoriesController
	{
		public function MovieCategoriesController()
		{
			super(new RoxioNowDAO().movies);
		}
	}
}