package com.rovicorp.controllers
{
	
	import com.rovicorp.controls.MessageDialog;
	import com.rovicorp.events.DownloadTitleEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.MessageBoxEvent;
	import com.rovicorp.events.TitleEvent;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.service.download.DownloadEvent;
	import com.rovicorp.service.download.DownloadManager;
	import com.rovicorp.views.DownloadsView;
	
	import flash.events.Event;
	
	import mx.events.CollectionEvent;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	import net.rim.aircommons.download.taskmanager.GlobalConstants;

	public class DownloadListController
	{
		
		private static var _instance:DownloadListController;
		
		public static function getIntance():DownloadListController
		{
			if( _instance == null )
			{
				_instance = new DownloadListController();
			}
			return _instance;
		}
		
		//--------------------------------------------------------
		//--------------------------------------------------------
		private var view:DownloadsView;
		
		private var _server:RoxioNowDAO = new RoxioNowDAO();
				
		private var dMgr:DownloadManager = DownloadManager.getInstance();
		
		private var dataInited:Boolean = false;
			
		public function DownloadListController()
		{	
			addEventListeners();
		}
		
		public function bindView( theView:DownloadsView ):void
		{
			view = theView;
			
			if( dataInited )
			{
				setStates("loaded");
			}
			else
			{
				view.currentState = "loading";
			}
			
			//bind downloadlist to List component
			view.data = _server.downloadList;
			setStates("loaded");
		}
		
		private function setStates( state:String ):void
		{
			if( view == null ) return;
			
			if (state == "loaded")
			{				
				if (_server.downloadList.length == 0) 
					view.currentState = "empty";
				else
					view.currentState = "loaded";
			}
			else
				view.currentState = "loading";
		}
		
		private function onSourceChanged(e:Event = null):void
		{			
			//trace("onSourceChanged:" + downloadList.length);
			setStates("loaded");			
		}
		
		private function addEventListeners():void
		{
			_server.downloadList.addEventListener(CollectionEvent.COLLECTION_CHANGE, onSourceChanged);			
			
//			dMgr.addEventListener(DownloadTitleEvent.TASK_STATUS_CHANGED, onTaskHandler);
//			dMgr.addEventListener(DownloadTitleEvent.TASK_PROGRESS, onTaskHandler);
			dMgr.addEventListener(DownloadEvent.DOWNLOAD_ERROR, onDownloadError);			

			var eventCenter:EventCenter = EventCenter.inst;			
			
			eventCenter.addEventListener(DownloadTitleEvent.ADD_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.DOWNLOAD_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.REMOVE_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.RETRY_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.PAUSE_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.RESUME_TASK, onTaskHandler);
			eventCenter.addEventListener(DownloadTitleEvent.WATCH, onTaskHandler);
		}
		
		private function onDownloadError(e:DownloadEvent):void
		{
			if( e.status == GlobalConstants.INSUFFICIENT_DISKSPACE )
			{
				var resourceMgr:IResourceManager = ResourceManager.getInstance();
				
				var messageEvent:MessageBoxEvent = new MessageBoxEvent(MessageBoxEvent.MESSAGE_OPEN);
				messageEvent.payload.title = resourceMgr.getString('LocalizedStrings', 'DOWNLOAD_ERROR_TITLE');	
				messageEvent.payload.message = resourceMgr.getString('LocalizedStrings', 'DOWNLOAD_ERROR_NO_DISKSPACE');
				EventCenter.inst.dispatchEvent(messageEvent);
			}
		}
		
		/**
		 * task event handler, start / pause / resume / remove 
		 */ 
		private function onTaskHandler(e:DownloadTitleEvent):void
		{						
			switch( e.type )
			{
				case DownloadTitleEvent.DOWNLOAD_TASK:
					dMgr.startDownloadItemByID(e.title.PassID.toString());
					break;
				case DownloadTitleEvent.REMOVE_TASK:	
					_deletePassID = e.title.PassID;
					//dMgr.removeDownloadItemByID(e.title.PassID.toString());
					
					//_server.downloadList.removeFakeTask(e.title.PassID);	
					var dialog:MessageDialog = MessageDialog.create('WARNING_TITLE', 'DELETE_DOWNLOAD_Heading', 'DELETE_DOWNLOAD_First');
					dialog.currentState = "okcancel";
					dialog.addEventListener(Event.SELECT, deleteConfirmHandler);
					dialog.height = dialog.height + 60;
					dialog.show();
					break;
				case DownloadTitleEvent.PAUSE_TASK:
					dMgr.pauseDownloadItemByID(e.title.PassID.toString());
					break;
				case DownloadTitleEvent.RESUME_TASK:
					dMgr.resumeDownloadItemByID(e.title.PassID.toString());
					break;
				case DownloadTitleEvent.RETRY_TASK:
					dMgr.restartDownloadItemByID(e.title.PassID.toString());
					break;
				case DownloadTitleEvent.WATCH:
					EventCenter.inst.dispatchEvent(new TitleEvent(TitleEvent.WATCH_TITLE, e.title, null)); 
					break;
			}			
		}
		
		private var _deletePassID:uint;
		
		private function deleteConfirmHandler(event:Event):void
		{
			var dialog:MessageDialog = event.target as MessageDialog;
			dialog.removeEventListener(Event.SELECT, deleteConfirmHandler);	
			
			//no
			if( dialog.selectedIndex == 0 )
			{
				dMgr.removeDownloadItemByID(_deletePassID.toString());
				_server.downloadList.removeFakeTask(_deletePassID);			
			}				
			
			_deletePassID = 0;
		}
	}
}
