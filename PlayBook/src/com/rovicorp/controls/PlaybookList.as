package com.rovicorp.controls
{
	import flash.geom.Point;
	
	import spark.components.List;
	import spark.effects.Animate;
	import spark.effects.animation.MotionPath;
	import spark.effects.animation.SimpleMotionPath;
	import spark.effects.easing.Power;

	public class PlaybookList extends List
	{
		private var _isAnimating:Boolean = false;
		private var animate:Animate;
		private var scrollAnimate:Animate;
		private var easing:Power;
		
		public function PlaybookList()
		{
			super();
			animate = new Animate();
			easing = new Power();
		}
		
		public function scrollToPosition(index:int):void{
			if (!layout)
				return;
			
			if(_isAnimating) {
				animate.stop(); //Quit the current animation
			}
			
			//Get the delta(x and y) required to get the index visible
			var spDelta:Point = dataGroup.layout.getScrollPositionDeltaToElement(index);
			
			// !!important if the index is already visible ->  spDelta == null no scroll needed
			// This may be refined in order to get the 'delta to the top of the list'
			if (spDelta)
			{
				//Vertical
				var smp:SimpleMotionPath = new SimpleMotionPath("verticalScrollPosition");
				if(spDelta.y> 0 &&  (spDelta.y + dataGroup.scrollRect.height - dataGroup.layout.getElementBounds(index).height) <= dataGroup.contentHeight){
					smp.valueBy = spDelta.y + dataGroup.scrollRect.height- dataGroup.layout.getElementBounds(index).height;
				}
				else
					smp.valueBy = spDelta.y;
				
				//Horizontal
				var smpX:SimpleMotionPath = new SimpleMotionPath("horizontalScrollPosition");
				if(spDelta.x> 0 &&  (spDelta.x + dataGroup.scrollRect.width - dataGroup.layout.getElementBounds(index).width) <= dataGroup.contentWidth){
					smpX.valueBy = spDelta.x + dataGroup.scrollRect.width- dataGroup.layout.getElementBounds(index).width;
					
				}
				else
					smpX.valueBy = spDelta.x;
				
				//Animate
				var smps:Vector.<MotionPath> = new Vector.<MotionPath>();
				smps[0] = smp;
				smps[1] = smpX;
				//set the SimpleMotionPath to the Animate Object
				animate.motionPaths = smps;
				_isAnimating = true;
				easing.exponent = 8;
				animate.easer = easing;
				animate.duration = 800;
				animate.play([dataGroup]);  //run the animation
				
			}
		}
	}
}