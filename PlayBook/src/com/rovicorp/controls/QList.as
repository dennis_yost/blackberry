package com.rovicorp.controls
{

	import com.rovicorp.model.ListProvider;
	import com.rovicorp.skins.StoreCategoryRenderer;
	
	import flash.events.Event;
	import flash.utils.getDefinitionByName;
	
	import mx.collections.IList;
	import mx.core.UIComponent;
	import mx.events.ResizeEvent;
	
	import qnx.ui.events.ListEvent;
	import qnx.ui.listClasses.List;
	import qnx.ui.listClasses.ListSelectionMode;

	[Event(name="listItemClicked", type="qnx.ui.events.ListEvent")]
	public class QList extends UIComponent
	{
		private var _dataProvider:IList;
		private var list:List = new List();;
		
		public function QList()
		{
			super();
			init();
		}
		
		protected function init():void
		{
			list.addEventListener(ListEvent.ITEM_CLICKED, onItemClicked, false, 0, true);
			addChild(list);
		}

		public function get dataProvider():IList
		{
			return _dataProvider;
		}
		
		public function set dataProvider(value:IList):void
		{
			list.dataProvider = new ListProvider(value);
		}
		
		public function get selectedIndex():int
		{
			return list.selectedIndex;
		}
		
		public function set selectedIndex(value:int):void
		{
			list.selectedIndex = value;
		}
		
		public function get rowHeight():int
		{
			return list.rowHeight;
		}
		
		public function set rowHeight(value:int):void
		{
			list.rowHeight = value;
		}
		
		public function setSkin(value:Object):void
		{
			list.setSkin(value);				
		}
		
		private function onItemClicked(event:ListEvent):void
		{
			dispatchEvent(event);
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			list.setSize(unscaledWidth, unscaledHeight);
			list.columnWidth = unscaledWidth;
		}
	}
}
