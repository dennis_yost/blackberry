package com.rovicorp.controls
{
	import com.rovicorp.utilities.LogUtil;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import spark.components.mediaClasses.ScrubBar;

	
	public class ClickScrubBar extends ScrubBar
	{
				
		override protected function createChildren():void 
		{
			super.createChildren();			
			
			track.mouseEnabled = true;
						
			track.addEventListener(MouseEvent.CLICK, trackClickHandler);
		}

		private function trackClickHandler(event:MouseEvent):void
		{
			var v:Number = pointToValue(event.localX,event.localY);
			if( v > maximum ) v = maximum;
			if( v < minimum ) v = minimum;
			this.setValue(v);
			
			dispatchEvent( new Event(Event.CHANGE) );
		}
	}

}