package com.rovicorp.model
{
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.service.download.DownloadStatus;
	import com.rovicorp.service.download.LicenseStatus;
	import com.rovicorp.utilities.StringUtil;
	import com.sonic.response.TitleData.Product;
	
	import mx.events.PropertyChangeEvent;
	
	public class TitleListingPurchased extends TitleListing
	{
		public var PassID:int;	
		
		public var titleDuration:Number = 0;
		
		public var DownloadPercent:Number = 0;
		
		
		public function getRentalPeriod():int
		{
			if( this.Details == null ) return 0;			
			
			if(	Details.AvailableProducts && Details.AvailableProducts.length > 0 )
			{				
				for( var i:uint = 0,len:uint = Details.AvailableProducts.length;i<len;i++)					
				{				
					var product:Product = Details.AvailableProducts.getItemAt(i) as Product;					
					if( product && product.PurchaseType.toLowerCase().indexOf("rent") != -1 )
					{
						return product.RentalPeriod;
					}
				}				
			}
			return 0;
		}
		
		protected var _lastPlayPosition:uint = 0;
		public function set lastPlayPosition(val:uint):void
		{
			_lastPlayPosition = val;
			
			checkReadyForWatchStatus();
		}
		
		public function get lastPlayPosition():uint
		{
			return _lastPlayPosition;
		}

		public function TitleListingPurchased()
		{		
			super();
		}
		
		private var _timeRemaining:Number = 0;
		public function set TimeRemaining( value:Number ):void
		{			
			var e:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "TimeRemaining", _timeRemaining, value);
			
			_timeRemaining = value;
			
			checkReadyForWatchStatus();
			
			dispatchEvent(e);
		}
		
		public function get TimeRemaining():Number
		{
			return _timeRemaining;
		}
		
		private var _first_play:String;
		
		public function get First_play():String
		{		
			if( _first_play == null )
			{
				_first_play = "";
				
				getDetailFromLocalDB();
			}
			
			return _first_play;
		}
		
		public function set First_play( value:String ):void
		{			
			var e:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "First_play", _first_play, value);
			
			_first_play = value;			
			dispatchEvent(e);
		}
		
		private function onGetLibraryInfoForPlayingTitle(e:DatabaseEvent):void
		{
			if (e.success == true)
			{
				var diid:Object = e.result[0];		
				if( diid._id == PassID )
				{
					if( diid.first_play && diid.first_play != "null")
					{
						First_play = diid.first_play;
					}
					
					lastPlayPosition = diid.last_play_position;
					
					LicenseStatus = diid.license_receieved == 1 ? com.rovicorp.service.download.LicenseStatus.LICENSE_RECEIVED : 0;

					EventCenter.inst.removeEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, onGetLibraryInfoForPlayingTitle);
				}
			}
			else
			{
				_first_play = "";
			}
		}
		
		
		//private var _server:RoxioNowDAO = new RoxioNowDAO();

		protected override function getDetails():void
		{
			_state = TitleListingBase.LOADING;
			new RoxioNowDAO().getPurchasedTitle(PassID, TitleID, onGetFullSummaryCompleted);
		}
		
		private var _downloadStatus:int;		
		public function set DownloadStatus( value:int ):void
		{			
			if( value == com.rovicorp.service.download.DownloadStatus.TASK_IN_PROGRESS )
			{				
				if( isReadyForWatch() )
				{
					value = com.rovicorp.service.download.DownloadStatus.TASK_READY_WATCH;
				}
			}
			if( value == DownloadStatus ) return;
			
			var e:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "DownloadStatus", _downloadStatus, value);
			
			_downloadStatus = value;			
			dispatchEvent(e);
			
			if( this is DownloadListing) return;	

			//update download status in local database
			var evt:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO, this.PassID.toString());
			var lib:Object = new Object();
			lib._id = this.PassID;
			lib.download_status = _downloadStatus;
			evt.library = lib;			
			EventCenter.inst.dispatchEvent(evt);
		}
		
		
		public function get DownloadStatus():int
		{
			return _downloadStatus;
		}
		
		public function get isDownloading():Boolean
		{
			return this.DownloadStatus == com.rovicorp.service.download.DownloadStatus.TASK_IN_PROGRESS
				|| this.DownloadStatus == com.rovicorp.service.download.DownloadStatus.TASK_READY_WATCH;
		}
		
		private function isReadyForWatch():Boolean
		{
			var isReady:Boolean = false;
			
			if( LicenseStatus != com.rovicorp.service.download.LicenseStatus.LICENSE_RECEIVED ) return isReady;

			if (Details != null)
			{
				titleDuration = StringUtil.convertTitleDuration(Details.MetaValues.RunTime);
			}
			else
			{
				titleDuration = 0;
			}
			if( isDownloading && titleDuration > 0 && TimeRemaining > 0 )
			{		
				if( titleDuration*1000 > TimeRemaining )
				{
					isReady = true;
				}
			}
			return isReady;
		}
		
		private function checkReadyForWatchStatus():void
		{
			var isReady:Boolean = isReadyForWatch();
			
			if( isReady && _downloadStatus != com.rovicorp.service.download.DownloadStatus.TASK_READY_WATCH )
			{
				DownloadStatus = com.rovicorp.service.download.DownloadStatus.TASK_READY_WATCH;
			}
			
			if( isReady == false && _downloadStatus == com.rovicorp.service.download.DownloadStatus.TASK_READY_WATCH )
			{
				DownloadStatus = com.rovicorp.service.download.DownloadStatus.TASK_IN_PROGRESS;
			}
		}
		
		private function getDetailFromLocalDB():void
		{
			var e0:DatabaseEvent = new DatabaseEvent(DatabaseEvent.GET_LIBRARY_INFO);
			e0.id = this.PassID.toString();
			
			EventCenter.inst.addEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, onGetLibraryInfoForPlayingTitle, false, 0, true);
			EventCenter.inst.dispatchEvent(e0);
		}		
		
		private var _licenseStatus:int = -1;
		
		public function get LicenseStatus():int
		{
			if( _licenseStatus == -1 && (this is DownloadListing) == false )
			{
				_licenseStatus = 0;
				
				getDetailFromLocalDB();
			}
			return _licenseStatus;
		}
		
		public function set LicenseStatus(value:int):void
		{
			if (value != _licenseStatus)
			{
				var oldValue:int = _licenseStatus;
				_licenseStatus = value;
				
				checkReadyForWatchStatus();
				
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "LicenseStatus", oldValue, value));
			}
		}
	}
}