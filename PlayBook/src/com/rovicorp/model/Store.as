package com.rovicorp.model
{
	import com.sonic.StreamAPIResponder;
	import com.sonic.response.Browse.BrowseItem;
	import com.sonic.response.Browse.BrowseList;
	import com.sonic.response.Genre;
	import com.sonic.response.GenreList;
	
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.Fault;
	import mx.rpc.events.FaultEvent;
	
	[Bindable]
	public class Store extends StoreCollection
	{
		//The three ids Will be changed by StoreNavigation (line 61)
		public static var MOVIES:int = 6706;
		public static var TV_SHOWS:int = 6798; 
		public static var NEW_ARRIVALS:int = 6756;
		
		public static const STORE_MOVIE:String = "movies";
		public static const STORE_TV:String = "tv";
		
		private static const PRELOAD_COUNT:int = 7;
		
		private var _id:int;
		private var _name:String;
		private var _categories:AsyncCollection;
		//		private var _titles:Dictionary;
		private var _category:int;
		private var _sort:String = "standard";
		private var _purchaseType:String = "any";
		private var _profile:String = "none";
		
//		private var _storeInited:Boolean = false;
		
		public function Store(name:String)
		{
			_name = name;
			
			if (categories.length)
				category = categories.getItemAt(0).ID;
			
			if (_name == STORE_MOVIE)
				_purchaseType = _service.settings.rentalsOnly ? "rent" : "any";
		}
		
		public function get category():int
		{
			return _category;
		}
		
		public function set category(value:int):void
		{
			if (_category !== value)
			{
				var event:Event = PropertyChangeEvent.createUpdateEvent(this, "category", _category, value);
				_category = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
		}
		
		public function get categories():AsyncCollection
		{
			if (_categories == null)
			{
				_categories = new AsyncCollection();
				loadCategories();
			}
		
			return _categories;
		}
		
		public function get sort():String
		{
			return _sort;
		}
		
		public function set sort(value:String):void
		{
			if (_sort !== value)
			{
				var event:Event = PropertyChangeEvent.createUpdateEvent(this, "sort", _sort, value);
				_sort = value;
				
				this.dispatchEvent(event);
				
				loadCategories();
				invalidate();
			}
			
		}
		
		public function get purchaseType():String
		{
			return _purchaseType;
		}
		
		public function set purchaseType(value:String):void
		{
			if (_purchaseType !== value)
			{
				var event:Event = PropertyChangeEvent.createUpdateEvent(this, "purchaseType", _purchaseType, value);
				_purchaseType = value;
				
				this.dispatchEvent(event);

				loadCategories();
				invalidate();
			}
			
		}
		
		public function get profile():String
		{
			return _profile;
		}
		
		public function set profile(value:String):void
		{
			if (_profile != value)
			{
				var event:Event = PropertyChangeEvent.createUpdateEvent(this, "profile", _profile, value);
				_profile = value;
				
				this.dispatchEvent(event);

				loadCategories();
				invalidate();
			}
		}		
		
		public override function invalidate():void
		{
			super.invalidate();
		}
		
		private function loadCategories():void
		{
			_categories.invalidate();
			
			if (_service.navigation.categoryLoaded)
			{				
				if( _name == STORE_MOVIE )
				{
					_service.settings.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onSettingsChange);
					_id = MOVIES;
				}
				else if(_name == STORE_TV)
				{
					_id = TV_SHOWS;
				}
				
				var genres:Array = _service.navigation[_id];
				category = genres[0].ID;
				
/*				var list:String = genres[0].ID;
				for (var i:int = 1; i < genres.length; ++i)
					list += "," + genres[i].ID;
				
				var request:RoxioNowDAO = new RoxioNowDAO();
				
				request.addEventListener(FaultEvent.FAULT, onCategoryFault);
				request.getBrowseListPlural(list, _sort, 1, PRELOAD_COUNT, _purchaseType, _profile, onGetBrowseListPluralCompleted);
*/
				_categories.setSource(genres);
			}
			else 
			{	
				_service.navigation.addEventListener(CollectionEvent.COLLECTION_CHANGE, onNavigationLoaded);
				_service.navigation.addEventListener(FaultEvent.FAULT, onNavigationFault);
			}
		}
			
		private function onCategoryFault(event:FaultEvent):void
		{
			_categories.fault(event);
			_categories = null;			
		}
		
		private function onGetBrowseListPluralCompleted(browseLists:ArrayCollection):void
		{
			var genres:Array = _service.navigation[_id];
			var categories:Array = [];
			
			for (var i:int = 0; i < browseLists.length; ++i) 
			{
				var category:StoreCategory = StoreCategory.fromGenre(genres[i]);
				category.totalItems = browseLists[i].TotalItems;
				category.titles = browseLists[i].Items;
				
				categories.push(category);
			}
			
			_categories.result(categories);
		}
		
		private function onSettingsChange(event:PropertyChangeEvent):void
		{
			if (event.property == "rentalsOnly")
				purchaseType = Boolean(event.newValue) ? "rent" : "any";
		}

		private function onNavigationFault(event:FaultEvent):void
		{
			event.target.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onNavigationLoaded);
			event.target.removeEventListener(FaultEvent.FAULT, onNavigationFault);
			
			_categories.dispatchEvent(event);
			_categories = null;			
		}
		
		private function onNavigationLoaded(event:CollectionEvent):void
		{			
			if( _name == STORE_MOVIE )
			{
				_service.settings.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onSettingsChange);
				_id = MOVIES;
			}
			else if(_name == STORE_TV)
			{
				_id = TV_SHOWS;
			}
			
			if (_service.navigation[_id])
				loadCategories();
			else
				_categories.setSource([]);
		}
		
		protected override function loadPage(pageNo:int):AsyncToken 
		{
			var result:AsyncToken = null;
			if (_category && RoxioNowDAO.api)
				result = _service.getBrowseList(_category, _sort, pageNo, PAGE_SIZE, _purchaseType, _profile);
			
			return result;
		}
		
		override public function result(data:Object):void
		{
			var list:BrowseList = data.result as BrowseList;
			
			if (list.GenreID == NEW_ARRIVALS)
				list.TotalItems = Math.min(100, list.TotalItems);
			
			super.result(data);
		}		
	}
}