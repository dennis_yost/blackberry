package com.rovicorp.model
{
	import com.sonic.Search;
	import com.sonic.SearchOption;
	import com.sonic.response.Browse.BrowseItem;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.events.PropertyChangeEvent;
	import mx.rpc.AsyncToken;
	
	public class Search extends EventDispatcher
	{
		private var _query:String = "";
		private var _option:String = SearchOption.TITLE;
		private var _result:AsyncCollection;
		
		public function Search()
		{
			super();
		}

		public function search(query:String, option:String):AsyncCollection
		{
			if (query != _query || option != _option || (_result && _result.state == "error"))
			{
				switch (option)
				{
					case SearchOption.ACTOR:
					case SearchOption.DIRECTOR:
					case SearchOption.WRITER:
						_result = new CastAndCrewCollection(query, option);
						break;
					
					default:
						var search:SearchCollection = new SearchCollection();
						search.query = query;
						search.option = option;
						
						_result = search;
						break;
				}
			}
			
			return _result;
		}

	}
}