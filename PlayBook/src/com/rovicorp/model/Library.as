package com.rovicorp.model
{
	
	import com.rovicorp.controllers.DatabaseController;
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.service.download.DownloadEvent;
	import com.rovicorp.service.download.DownloadManager;
	import com.rovicorp.service.download.DownloadStatus;
	import com.rovicorp.service.download.DownloadTask;
	import com.rovicorp.utilities.LogUtil;
	import com.sonic.response.Library.UserLibraryCounts;
	import com.sonic.response.Library.UserLibraryList;
	import com.sonic.response.NameAndValue;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.AsyncResponder;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class Library extends AsyncCollection
	{
		public static const DEFAULT_FILTER:int = 0; 
		public static const SORT_STANDARD:String = "standard";
		public static const SORT_ALPHABETICAL:String = "alpha";
		
		// TODO:  Should this really be client structure since readyToWatch is function of download list
		private static const _filterArray:Array = [
			new LibraryCategory("readyToWatch", "Movies,TV_Shows", "All"),
			new LibraryCategory("movies", "Movies", "All"),
			new LibraryCategory("tvShows", "TV_Shows", "All"),
			new LibraryCategory("purchases", "Movies,TV_Shows", "Buy_Only"),
			new LibraryCategory("rentals", "Movie_Rentals", "All"),
//			new LibraryCategory("rentalsExpiringSoon", "Movie_Rentals", "Expiring_Soon"),
			new LibraryCategory("expiredRentals", "Movie_Rentals", "Expired"),
			new LibraryCategory("unavailable", "Movies,TV_Shows", "Unavailable")
		];
		
		private static const _filterArrayOffline:Array = [
			new LibraryCategory("readyToWatch", "Movies,TV_Shows", "All")
		];
		
		private var _server:RoxioNowDAO = new RoxioNowDAO();
		private var _token:AsyncToken;
		
		private var _filter:int = DEFAULT_FILTER;
		private var _sort:String = SORT_STANDARD;
		private var _items:IList;
		private var _downloads:Dictionary;
//		private var _filter:String = "All";
		private var _filters:AsyncCollection;
		
		//
		private var _index:Dictionary = new Dictionary(true);

		public function Library()
		{
			_server.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onServerPropertyChange);
			_server.addEventListener(RoxioNowDAO.TITLE_PURCHASED, onTitlePurchased);
			
			DownloadManager.getInstance().addEventListener(DownloadEvent.TASK_STATUS_CHANGED, onTaskStatusChanged);
			DownloadManager.getInstance().addEventListener(DownloadEvent.TASK_PROGRESS, onTaskStatusChanged);
		}
		
		public function updatePurchaseTitleInfo(id:int, prop:String, value:Object):void
		{
			if (_index)
			{
				var item:TitleListingPurchased = _index[id];
				
				if (item && item.hasOwnProperty(prop) )
				{
					item[prop] = value;
				}
			}
		}

/*      *** Disable V2 functionalty for now		
		public function get filter():String
		{
			return _filter;
		}
		
		public function set filter(value:String):void
		{
			if (_filter !== value)
			{
				var event:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "filter", _filter, value);
				_filter = value;
				
				this.dispatchEvent(event);
				invalidate();
			}
		}

		public function get filters():AsyncCollection
		{
			if (_filters == null)
			{
				_filters = new AsyncCollection();
				
				if (_genres == null)
		_server.getAvailLibSlaveWheelOptionByMasterWheel(_type, onGetAvailLibSlaveWheelOptionsCompleted);
			}
			
			return _filters;
		}
		
		**** And use hard coded values instead
*/
		public function get filter():int
		{
			return _filter;
		}
		
		public function set filter(value:int):void
		{
			if (_filter !== value)
			{
				var event:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "filter", _filter, value);
				_filter = value;
				
				this.dispatchEvent(event);
				_invalidate();
			}
		}

		public function get filterId():String
		{
			return _filterArray[_filter].id;
		}
		
		public function get filters():AsyncCollection
		{
			if (_filters == null)
			{
				_filters = new AsyncCollection();
				loadFilters();
				
				/*			Disabled v2 functionlity
				if (_genres == null)
				_server.getAvailLibSlaveWheelOptionByMasterWheel(_type, onGetAvailLibSlaveWheelOptionsCompleted);
				*/
			}
			
			return _filters;
		}
		
		public function get categoryID():String
		{
			return _filterArray[_filter].id;
		}
		
		private function get items():IList
		{
			return _items;
		}
		
		private function set items(value:IList):void
		{
			if (_items !== value)
			{
				if (_items)
					_items.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onCollectionChanged);
				
				_items = value;
				
				if (_items)
					_items.addEventListener(CollectionEvent.COLLECTION_CHANGE, onCollectionChanged);
			}
		}
		
		private function loadFilters():void
		{
			// Check to see if the library is empty. If there are no 
			// titles in the download manager then check online
			if (DownloadManager.getInstance().getAllTasks().length == 0)
			{
				if (_server.isLoggedIn)
				{
					var request:RoxioNowDAO = new RoxioNowDAO();
					request.addEventListener(FaultEvent.FAULT, onGetUserLibraryCountsFault)
					request.getUserLibraryCounts(onGetUserLibraryCountsCompleted);
				}
				else
				{
					_filters.setSource([]);		// TODO: Check downloads here or at client
				}
			}
			else
				_filters.setSource(_filterArray);
		}

		public function invalidateFilters():void
		{
			if (_filters)
			{
				_filters.invalidate();
				loadFilters();
			}
		}
		
		private function _invalidate():void
		{
			_token = null;
			super.invalidate();
		}
		
		private function readyToWatch(status:int, id:String):Boolean
		{
			var isReady:Boolean = false;
			
			if (status == DownloadStatus.TASK_COMPLETE || status == DownloadStatus.TASK_READY_WATCH ) isReady = true;
			
			var entry:DownloadListing = _downloads[int(id)];
			
			//double confirm, make sure that downloaded title is not expired
			
			// sourceObj is null means we can't find related title in db, so not sure if it's expired or not
			// there are some weird data in db that the year of expired_date is 19xx, so add an additional consideration that filter all years < 1980
			if (isReady && entry && entry.ExpireDate)
			{
				var date:Date = new Date();
				isReady = entry.ExpireDate.time > date.time || entry.ExpireDate.fullYear < 1980;
			}
			
			return isReady;
		}
		
		protected override function setState(value:String):void
		{
			super.setState(value);
			
			if (value == LOADING)
			{
				var category:LibraryCategory = _filterArray[_filter];

				items = null;
				_downloads = null;

				if (category.id == "readyToWatch")
				{
					EventCenter.inst.addEventListener(DatabaseEvent.GET_DOWNLOAD_LISTING_DONE, onGetDownloadInfo, false, 0, true);
					EventCenter.inst.dispatchEvent(new DatabaseEvent(DatabaseEvent.GET_DOWNLOAD_LISTING));
					
					//items = new DownloadList(readyToWatch);
					_index = null;
				}
				else if (_server.isLoggedIn)
				{
					if (category.id == "expiredRentals" || category.id == "rentalsExpiringSoon")
					{
						EventCenter.inst.addEventListener(DatabaseEvent.GET_DOWNLOAD_LISTING_DONE, onGetDownloadInfo, false, 0, true);
						EventCenter.inst.dispatchEvent(new DatabaseEvent(DatabaseEvent.GET_DOWNLOAD_LISTING));
					}
					else
					{
						var types:Array = category.type.split(",");
						
						_token = _server.getUserLibrary("0", types[0], category.filter);
						_token.addResponder(new mx.rpc.AsyncResponder(onResult, onFault, 0) );
					}
				}
				else
					setSource([]);
			}
		}
		

		private function onGetDownloadInfo(event:DatabaseEvent):void
		{
			//Check returned data
			if( event.id ) return;
			
			EventCenter.inst.removeEventListener(DatabaseEvent.GET_DOWNLOAD_LISTING_DONE, onGetDownloadInfo);

			if (event.success)
			{
				_downloads = new Dictionary();
				
				for each (var item:DownloadListing in (event.result as Array))
				{
					_downloads[item.PassID] = item;
				}
			}
			
			var category:LibraryCategory = _filterArray[_filter];
			
			if( category.id == "readyToWatch" )
			{
				items = new DownloadList(readyToWatch);
				
				//refresh data right now, to fix RIMBBP 931
				setSource(items.toArray());	
				return;
			}
			
			var types:Array = category.type.split(",");

			_token = _server.getUserLibrary("0", types[0], category.filter);
			_token.addResponder(new mx.rpc.AsyncResponder(onResult, onFault, 0) );
		}
		
		private function onCollectionChanged(event:CollectionEvent):void
		{
			if (event.kind != CollectionEventKind.UPDATE)
				setSource(items.toArray());	
		}
		
		private function onGetUserLibraryCountsCompleted(response:UserLibraryCounts):void
		{
			var source:Array = [];
			
			if (response.ResponseCode == 0)
			{
				for each (var item:NameAndValue in response.LibraryCounts)
				{
					if (int(item.KeyValue) > 0)
					{
						source = _filterArray;
						break;
					}
				}
			}

			filters.setSource(source);
		}

		private function onGetUserLibraryCountsFault(event:FaultEvent):void
		{
			filters.dispatchEvent(event);
//			_filters = null;
			
		}

		private function onFault(info:FaultEvent, token:Object):void
		{
			fault(info);
			dispatchEvent(info);

			_token = null;
		}

		private function onResult(result:ResultEvent, token:Object):void
		{
			if (result.token == _token)
			{
				_token = null;
				loadLibrary(result.result as UserLibraryList, token as int)
			}
		}

/*		private function rentalsExpiringSoon(item:Object):Boolean
		{
			var entry:DownloadListing = _library[item.PassID];
			
			if (entry)
			{
				var date:Date = new Date();
				date.time -= 1000 * 60 * 24;
				
				return entry.ExpireDate.time > date.time
			}
			
			return true;
		}*/

		private function expiredRentals(item:Object):Boolean
		{
			var result:Boolean = true;
			var entry:DownloadListing = _downloads[item.PassID];
			
			if (entry && entry.ExpireDate)
			{
				var date:Date = new Date();
				result = entry.ExpireDate.time < date.time;
			}
			
			return result;
			
		}
		
		private function loadLibrary(response:UserLibraryList, index:int):void
		{
			var category:LibraryCategory = _filterArray[_filter];
			var types:Array = category.type.split(",");

			if (items == null)
				items = response.Items;
			else
				ArrayCollection(items).addAll(response.Items);
			
			if (++index < types.length)
			{
				_token = _server.getUserLibrary("0", types[index], category.filter);
				_token.addResponder(new mx.rpc.AsyncResponder(onResult, fault, index) );
			}
			else
			{
				var sort:Sort;
	
				if (_downloads)
				{
					if (category.id == "expiredRentals")
					{
						ArrayCollection(items).filterFunction = expiredRentals;
						ArrayCollection(items).refresh();						
					}
//					else if (category.id == "rentalsExpiringSoon")
//						ArrayCollection(items).filterFunction = rentalsExpiringSoon;
				}

				if (_sort == SORT_STANDARD)
				{
					var passSortField:SortField = new SortField();
					passSortField.name = "PassID";
					passSortField.numeric = true;
					passSortField.descending = true;
					
					/* Create the Sort object and add the SortField object created earlier to the array of fields to sort on. */
					sort = new Sort();
					sort.fields = [passSortField];
					
					/* Set the ArrayCollection object's sort property to our custom sort, and refresh the ArrayCollection. */
					ArrayCollection(items).sort = sort;
					ArrayCollection(items).refresh();
				}
				else if (_sort == SORT_ALPHABETICAL)
				{
					var nameSortField:SortField = new SortField();
					nameSortField.name = "Name";
					
					/* Create the Sort object and add the SortField object created earlier to the array of fields to sort on. */
					sort = new Sort();
					sort.fields = [nameSortField];
					
					/* Set the ArrayCollection object's sort property to our custom sort, and refresh the ArrayCollection. */
					ArrayCollection(items).sort = sort;
					ArrayCollection(items).refresh();
				}
				
				_index = new Dictionary(true);
				
				for each (var item:TitleListingPurchased in items)
				{
					var task:DownloadTask = DownloadManager.getInstance().getDownloadTaskById(item.PassID.toString());
					
					if (task)
					{
						item.DownloadStatus = DownloadStatus.fromTaskStatus(task.getStatus());
						item.TimeRemaining = task.getProgressHandle().getEstimatedTimeRemaining();
					}
					
					_index[item.PassID] = item;
				}
				
				setSource(ArrayCollection(items).toArray());
			}
		}
		
		private function onTitlePurchased(event:Event):void
		{
			// Trigger a reload
			invalidate();
		}
		
		
		private function onServerPropertyChange(event:PropertyChangeEvent):void
		{
			if (event.property == "isLoggedIn")
			{
				invalidate();
				invalidateFilters();
			}
		}
		
		private function onTaskStatusChanged(event:DownloadEvent):void
		{
			if (_index)
			{
				var item:TitleListingPurchased = _index[int(event.id)];
				
				if (item)
				{
					var task:DownloadTask = DownloadManager.getInstance().getDownloadTaskById(item.PassID.toString());
					
					item.DownloadStatus = event.status;
					if( task )
					{						
						item.TimeRemaining = task.getProgressHandle().getEstimatedTimeRemaining();
						item.DownloadPercent = task.getDownloadProgress();
					}
				}
			}
		}
	}
}
