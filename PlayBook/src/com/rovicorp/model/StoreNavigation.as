package com.rovicorp.model
{
	import com.sonic.response.Genre;
	import com.sonic.response.GenreList;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;
	
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.rpc.events.FaultEvent;
	
	public dynamic class StoreNavigation extends Dictionary implements IEventDispatcher
	{
		private var _dispatcher:EventDispatcher = new EventDispatcher(this as IEventDispatcher);
		
		private var _categoryLoaded:Boolean = false;
		
		public function StoreNavigation(weakKeys:Boolean=false)
		{
			super(weakKeys);
			
			var request:RoxioNowDAO = new RoxioNowDAO();
			
			request.addEventListener(FaultEvent.FAULT, onGetNavigationFault);
			request.getNavigation(onGetNavigationCompleted);
		}

		public function addEventListener(type:String, listener:Function, useCapture:Boolean=false, priority:int=0, useWeakReference:Boolean=false):void
		{
			_dispatcher.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean=false):void
		{
			_dispatcher.removeEventListener(type, listener, useCapture);
		}
		
		public function dispatchEvent(event:Event):Boolean
		{
			return _dispatcher.dispatchEvent(event);
		}
		
		public function hasEventListener(type:String):Boolean
		{
			return _dispatcher.hasEventListener(type);
		}
		
		public function willTrigger(type:String):Boolean
		{
			return _dispatcher.willTrigger(type);
		}
		
		public function get categoryLoaded():Boolean
		{
			return _categoryLoaded;
		}
		
		private function onGetNavigationCompleted(genreList:GenreList):void
		{

			if (genreList)
			{
				var cate_name:String;
				var homeID:Number = 0;
				
				for each (var item:Genre in genreList.Genres)
				{
					if (item.ParentId != 0)
					{
						var parent:Array = this[item.ParentId];
						
						if (parent == null)
							parent = this[item.ParentId] = [];
						
						parent.push(item);
					}
					else
					{
						homeID = item.ID;
					}

					cate_name = item.Name.toLowerCase();
					//trace(item.ID+":"+item.Name +":"+item.ParentId);
					
					if( cate_name == "movies" && (item.ParentId == homeID || item.ParentId == 0 ) )
					{					
						Store.MOVIES = item.ID;
					}
					
					if( cate_name == "tv" || cate_name == "tv shows" )
					{
						Store.TV_SHOWS = item.ID;
					}
					
					if (cate_name == "new releases" && item.ParentId == Store.MOVIES )
					{
						Featured.NEW_RELEASES = item.ID;						
					}
					
					
					if (cate_name == "new arrivals" && item.ParentId == Store.MOVIES )
					{
						Store.NEW_ARRIVALS = item.ID;
					}
				}
				
				_categoryLoaded = true;
				
				dispatchEvent(new CollectionEvent(CollectionEvent.COLLECTION_CHANGE)); 
			}
		}
		
		private function onGetNavigationFault(event:FaultEvent):void
		{
			dispatchEvent(event);
		}
	}
}