package com.rovicorp.model
{
	import com.rovicorp.service.download.DownloadStatus;
	import com.rovicorp.utilities.LogUtil;
	import com.sonic.response.Library.FullTitlePurchased;
	import com.sonic.response.TitleData.FullTitle;
	
	import flash.filesystem.File;
	
	import mx.events.PropertyChangeEvent;
	
	public class DownloadListing extends TitleListingPurchased
	{	
		
		public var MPAARating:String;
		public var ExpireDate:Date;
				
		//Format title detail from pollForDownloads
		public function formatDataFromContentitem( contentItem:Object):void
		{			
			var pass:Object = contentItem.pass;
			var title:Object = contentItem.title;
			
			this.TitleID = title.titleid;
			this.PassID = pass.pid;
			this.Name = title.metadata.name;
			
			var item:FullTitlePurchased = new FullTitlePurchased();			
			item.TitleID = title.titleid;			
			item.PassID = pass.pid;
			item.BoxartPrefix = title.metadata.boxart;
			item.MPAARating = title.metadata.rating;
			item.Name = title.metadata.name;
			
			this.Details = item;
		}
		
		public function formatDataFromFullTitlePurchased(item:FullTitlePurchased):void
		{
			if (item == null) return;
			
			this.TitleID = item.TitleID;
			this.PassID = item.PassID;
			this.Name = item.Name;			
			
			this.setDetails(item);
		}
		
		private var _bytesDownloaded:uint;		
		public function set BytesDownloaded( value:uint ):void
		{
			var e:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "BytesDownloaded", _bytesDownloaded, value);
			
			_bytesDownloaded = value;
			
			if( BytesTotal > 0 )
			{
				DownloadPercent = _bytesDownloaded/BytesTotal;
			}
			
			dispatchEvent(e);
		}
		
		public function get BytesDownloaded():uint
		{
			return _bytesDownloaded;
		}
		
		private var _BytesTotal:uint;
		
		public function set BytesTotal( value:uint ):void
		{
			var e:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "BytesTotal", _BytesTotal, value);
			
			_BytesTotal = value;
			
			dispatchEvent(e);
		}

		public function get BytesTotal():uint
		{
			return _BytesTotal;
		}
		
		override public function set TitleID(value:int):void
		{
			_titleID = value;
			
			if( RoxioNowDAO.is_offline ) return;
			
			super.TitleID = value;
		}
		
		override public function get Details():FullTitle
		{	
			if( RoxioNowDAO.is_offline ) return _details;
			
			return super.Details;
		}
		
		override public function get RecommendationItems():AsyncCollection
		{
			if( RoxioNowDAO.is_offline ) return null;
			
			return super.RecommendationItems;
		}
	}
}
