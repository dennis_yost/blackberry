package com.rovicorp.model
{
	import com.adobe.utils.StringUtil;
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.MainAppEvent;
	import com.rovicorp.service.download.DownloadEvent;
	import com.rovicorp.service.download.DownloadManager;
	import com.rovicorp.service.download.DownloadTask;
	import com.rovicorp.utilities.Boxart;
	import com.rovicorp.utilities.ImagePool;
	import com.rovicorp.utilities.StringUtil;
	import com.roxionow.flash.util.crypto.XOREncryption;
	import com.sonic.StreamAPIResponder;
	import com.sonic.StreamAPIWse;
	import com.sonic.StreamDeviceRequestSettings;
	import com.sonic.StreamEvent;
	import com.sonic.response.Auth.AuthTokenResponse;
	import com.sonic.response.Download.ContentItem;
	import com.sonic.response.Download.ContentItemCollection;
	import com.sonic.response.Genre;
	import com.sonic.response.GenreList;
	import com.sonic.response.Library.FullTitlePurchased;
	import com.sonic.response.TitleData.FullTitle;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	import flash.xml.XMLDocument;
	
	import mx.events.PropertyChangeEvent;
	import mx.rpc.AsyncResponder;
	import mx.rpc.AsyncToken;
	import mx.rpc.CallResponder;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.xml.SimpleXMLDecoder;
	
	import qnx.system.Device;
	
	public final class RoxioNowDAO extends CallResponder  
	{
		// Events
		public static const NAVIGATION_LOADED:String = "onNavigationLoaded";
		public static const TITLE_PURCHASED:String = "onPurchase";
		
		public static const CLIENT_NAME:String = "BlackBerry PlayBook"; // TODO: determine client name
		public static var VERSION:String = "1.0.0";
		public static var BUILD:String = "50";
		
		public static const API_INIT_STATE_IDLE:int = 0;
		public static const API_INIT_STATE_LOADING:int = 1;
		public static const API_INIT_STATE_LOADED:int = 2;
		public static const API_INIT_STATE_LOAD_FAIL:int = 3;
		
		public static const COUNTRY_US:int = 1;
		public static const COUNTRY_CANADA:int = 41;
		
		public static var CURRENT_COUNTRY_ID:int = 0;
		
		private static const KEY_4_FILE:String = "05CA5E60-6443-927A-ABA6-2F708D836908";
		private static const WEEK:int = 1000 * 3600 * 24 * 7;
		
		private static var _api:StreamAPIWse;
		private static var _dispatcher:EventDispatcher = new EventDispatcher();
		private static var _cache:Dictionary;

		private static var _isLoggedIn:Boolean = false;
		
		//0:not loaded, 1:loading, 2:loaded
		private static var load_conf_status:int = 0;
		private static var api_init_status:int = API_INIT_STATE_IDLE;
		private static var _is_offline:Boolean = false;
		private static var _api_init_try_count:int = 0;

		private static var _featured:Featured;
		private static var _library:Library;
		private static var _movies:Store;
		private static var _tvShows:Store;
		private static var _navigation:StoreNavigation;
		private static var _search:Search;
		private static var _settings:UserSettings = new UserSettings();
		private static var _wishlist:Wishlist;
		private static var _downloadList:DownloadList;
		
		private static var defaultSettings:StreamDeviceRequestSettings;
		
		public function RoxioNowDAO()
		{
			EventCenter.inst.addEventListener(MainAppEvent.RE_INIT_API, onReInitApp, false, 0, true);
			_dispatcher.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onPropertyChange, false, 0, true);			
			
			if (_api == null || _api.DeviceSettings == null)
			{
				_dispatcher.addEventListener(StreamEvent.READY, onAPIReady, false, 0, true);
				_dispatcher.addEventListener(StreamEvent.FAULT, onAPIFault, false, 0, true);
			}
			
			if( load_conf_status == 0 )
			{
				load_conf_status = 1;
				var conf_file:File = File.applicationStorageDirectory.resolvePath("assets" + File.separator + "db" + File.separator + "dbc_" + settings.country_name + ".db");
				if( conf_file.exists == false )
				{
					conf_file = File.applicationDirectory.resolvePath("assets" + File.separator + "db" + File.separator + "dbc_" + settings.country_name + ".db");
				}
				
				var by:ByteArray = new ByteArray;
				var conf_file_Stream:FileStream = new FileStream;
				conf_file_Stream.open(conf_file, FileMode.READ);
				conf_file_Stream.readBytes(by);
				var s:String = by.readUTF();

				XOREncryption.key = KEY_4_FILE;
				var out:String = XOREncryption.decrypt(s);
				var x:XML = new XML(out);
				configLoaded(x);
			}
		}
		
		public static function get api():StreamAPIWse
		{
			return _api;
		}
		
		private function configLoaded(x:XML):void
		{
			trace("config file Loaded");
			load_conf_status = 2;
			
			RoxioNowDAO.getDeviceSettings();

			var xmlDoc:XMLDocument = new XMLDocument(x.toXMLString());
			var sd:SimpleXMLDecoder = new SimpleXMLDecoder;				
			var item:Object = sd.decodeXML(xmlDoc).settings;
			
			_settings.conf = item;
			
			if( item.APIKey )
			{
				defaultSettings.settingEncryptionItem = true;
				defaultSettings.APIKey = item.APIKey;
				defaultSettings.settingEncryptionItem = false;
			}
			if( item.Username )
			{
				defaultSettings.settingEncryptionItem = true;
				defaultSettings.Username = item.Username;
				defaultSettings.settingEncryptionItem = false;
			}
			if( item.Password )
			{
				defaultSettings.settingEncryptionItem = true;
				defaultSettings.Password = item.Password;
				defaultSettings.settingEncryptionItem = false;
			}
			if( item.CurrentEnvironment )
			{
				defaultSettings.settingEncryptionItem = true;
				defaultSettings.CurrentEnvironment = item.CurrentEnvironment;
				defaultSettings.settingEncryptionItem = false;
			}
			if( item.DestinationTypeID )
			{
				defaultSettings.settingEncryptionItem = true;
				defaultSettings.DestinationTypeID = item.DestinationTypeID;
				defaultSettings.settingEncryptionItem = false;
			}

			initAPI();
		}
		
		public function onReInitApp(e:MainAppEvent):void
		{
			if (api_init_status == API_INIT_STATE_IDLE || api_init_status == API_INIT_STATE_LOAD_FAIL)
			{
				trace("RE INIT APP");
				initAPI();
			}
		}
		
		private function initAPI():void
		{
			if (_api != null)
			{
				// clean up
				_api.removeEventListener(StreamEvent.READY, onStreamEvent);
				_api.removeEventListener(StreamEvent.FAULT, onStreamEvent);
				_api = null;
			}
			
			api_init_status = API_INIT_STATE_LOADING;
			_api_init_try_count++;
			
			RoxioNowDAO.getDeviceSettings().Endpoints = null;
			_api = new StreamAPIWse(getDeviceSettings());
			_api.addEventListener(StreamEvent.READY, onStreamEvent, false, 0, true);
			_api.addEventListener(StreamEvent.FAULT, onStreamEvent, false, 0, true);
		}

		private static function getDeviceSettings():StreamDeviceRequestSettings
		{
			if (defaultSettings == null)
			{
				defaultSettings = new StreamDeviceRequestSettings;
				
				if (_settings.keepSignedIn == false)
					_settings.authToken = "";
				
				// TODO: Load settings from config file
				/*			defaultSettings = new StreamDeviceRequestSettings()
				defaultSettings.APIKey = "tuKwA4yrdBfkg5fwCZuA8BJbYCuOoS4lso9dcQOZdwbc/tF16LNJmA==";
				defaultSettings.DestinationTypeID = "51";
				defaultSettings.Username = "SonicTestAPI";
				defaultSettings.Password = "CN@s0N!cT35t&toK3N";
				defaultSettings.CurrentEnvironment = null;
				defaultSettings.AuthToken = _settings.authToken;
				//			defaultSettings.FileFilter = "downloadonly";
				defaultSettings.CurrentEnvironment = "stgapi";
				*/
				defaultSettings = new StreamDeviceRequestSettings()
				/*
				defaultSettings.APIKey = "CHb1hy1Jbr1kH+AZ2PSyVVHTMg0J5F2arkoaBRMSN7Z9Ixft1eCVjw==";
				defaultSettings.DestinationTypeID = "449";
				defaultSettings.Username = "EVAL_RIM_MOB_RIM_V1.2_NUI_NULL_052611";
				defaultSettings.Password = "IO4ug2mci17e6cpbs8wGx2nsSocJX1cWUp/FhYJ7a/Om5qxqj6ZEeA==";
				*/	
				defaultSettings.CurrentEnvironment = null;
				defaultSettings.AuthToken = _settings.authToken;
				defaultSettings.FileFilter = "downloadonly";
				
				if (_settings.parentChild) defaultSettings.ParentChild = _settings.parentChild;
				
				/*var _storage:SharedObject = SharedObject.getLocal("cache.db");
				
				if (_storage.data.Endpoints)
				{
					var now:Date = new Date();
					if (now.time - _storage.data.EndpointsUTC.time < WEEK)
					{
						defaultSettings.CurrentEnvironment = _storage.data.CurrentEnvironment;
						defaultSettings.Endpoints = _storage.data.Endpoints;
					}
					else
						delete _storage.data.Endpoints;
				}*/

				CONFIG::device 
				{
					// update user agent for api call with device hardware id
//dpy(20140626)-->					flash.net.URLRequestDefaults.userAgent += " RIM_Playbook CNSDK: |" + VERSION + " CNMODEL:" + Device.device.hardwareID;
// Since "RIM_Playbook" App is being blocked as unavailable to stop purchasing
//    this is the new UserAgent string.
//
					flash.net.URLRequestDefaults.userAgent += " BB_Playbook CNSDK: |" + VERSION + "_" + BUILD + " CNMODEL:" + Device.device.hardwareID;
//<--dpy(20140626)					
					if (Device.device.serialNumber)
						defaultSettings.DestinationUniqueID = "BBPB" + Device.device.serialNumber.toString(); 
				}
			}

			// DEBUG code
			if (defaultSettings.DestinationUniqueID == null)
			{
				var storage:SharedObject = SharedObject.getLocal("debug-settings");
				
				if (storage.data.DestinationUniqueID == null)
					storage.data.DestinationUniqueID = "BBPB" + Math.floor(Math.random() * 100000); //

				defaultSettings.DestinationUniqueID = storage.data.DestinationUniqueID;
			}
			
			_isLoggedIn = defaultSettings.AuthToken.length > 0;
			
			return defaultSettings;
		}

		////////////////////////////////////////////////////////////////////////////////
		// Properties
		
		public static function get api_init_try_count():int
		{
			return _api_init_try_count;
		}
		
		public static function get is_offline():Boolean
		{
			return _is_offline;
		}
		
		public static function get apiInitState():int
		{
			return api_init_status;
		}
		
		[Bindable]
		public function get isLoggedIn(): Boolean
		{
			return _isLoggedIn;
		}

		public function set isLoggedIn(value:Boolean):void
		{
			// set function is only for binding purposes	
		}
		
		public function get deviceSettings():Object
		{
			if (_api)
				return _api.DeviceSettings;
					
			return null;		
		}
		
		public function get featured():Featured
		{
			if (_featured == null)
				_featured = new Featured();
			
			return _featured;
		}
		
		public function get library():Library
		{
			if (_library == null)
				_library = new Library();
			
			return _library;
		}
		
		public function get movies():Store
		{
			if (_movies == null)
				_movies = new Store(Store.STORE_MOVIE);
			
			return _movies;
		}

		public function get tvShows():Store
		{
			if (_tvShows == null)
				_tvShows = new Store(Store.STORE_TV);
			
			return _tvShows;
		}
		
		public function get settings():UserSettings
		{
			return _settings;
		}

		/**
		 * Gets the navigation list for the designated category
		 *  
		 * @param categoryId 
		 * @param callback 
		 */
		internal function get navigation():StoreNavigation
		{
			if (_navigation == null)
			{
				_navigation = new StoreNavigation();
				_navigation.addEventListener(FaultEvent.FAULT, onNavigationFault, false, 0, true);
			}
			
			return _navigation;
		}

		public function get downloadList():DownloadList
		{
			if (_downloadList == null)
				_downloadList = new DownloadList();
			
			return _downloadList;
		}

		public function get wishlist():Wishlist
		{
			if (_wishlist == null)
				_wishlist = new Wishlist();
			
			return _wishlist;
		}

		////////////////////////////////////////////////////////////////////////////////
		// Implementation

		/**
		 * Gets/Creates cache for the specified object type
		 * @params type Object or Class that defines type of cache
		 *
		 * @return Dictionary to populate with objects to be cached
		 */ 
		private function getCache(type:Object):Dictionary
		{
			if (_cache == null)
				_cache = new Dictionary();
			
			var key:String = flash.utils.getQualifiedClassName(type);
			var cache:Dictionary = _cache[key];
			
			if (cache == null)
				_cache[key] = cache = new Dictionary();
			
			return cache;
		}
		
		private function deleteCache(type:Object):void
		{
			if (_cache != null)
			{
				var key:String = flash.utils.getQualifiedClassName(type);
			
				if (_cache[key])
					delete _cache[key];
			}
		}
		
		override public function fault(data:Object):void
		{
			trace("API CALL FAULT!!!");
			var fe:FaultEvent = data as FaultEvent;
			if (fe)
			{
				if (fe.type == "fault")
				{
					RoxioNowDAO._is_offline = true;
					EventCenter.inst.dispatchEvent(new MainAppEvent(MainAppEvent.NETWORK_CONNECTION_LOST));
				}
			}
			super.fault(data);
		}
		
		override public function result(data:Object):void
		{
			RoxioNowDAO._is_offline = false;
			super.result(data);
		}
		
		public function fault2(info:Object, token:Object):void
		{
			fault(info);
		}
		
		////////////////////////////////////////////////////////////////////////////////
		// API
		
		public function acceptEula(callback:Function):void
		{
			api.Utilities.acceptEula(callback).addResponder(this);
		}

		public function addItemToWishList(TitleID:int, callback:Function = null) : void
		{
			var token:AsyncToken = api.Wishlist.addItemToWishList(TitleID, callback);
			token.addResponder(new mx.rpc.AsyncResponder(onAddItemToWishList, fault2, TitleID));
		}
		
		public function removeItemFromWishList(TitleID:int, callback:Function = null) : void
		{
			var token:AsyncToken = api.Wishlist.removeItemFromWishList(TitleID, callback);
			token.addResponder(new mx.rpc.AsyncResponder(onRemoveItemFromWishList, fault2, TitleID));
		}	
		
		public function doPurchase(SKUID:int, CouponCode:String, callback:Function = null):void 
		{
			api.Commerce.doPurchase(SKUID, CouponCode, callback).addResponder(new Responder(onPurchaseCompleted, fault));
		}
		
		public function lookupPurchaseDetailsForTitle(TitleID:int, callback:Function):void
		{
			api.Commerce.lookupPurchaseDetailsForTitle(TitleID, callback).addResponder(this);
		}
		
		public function applyGiftCode(CouponCode:String, callback:Function):void
		{
			api.Commerce.applyGiftCode(CouponCode, callback).addResponder(this);
		}
		
		public function calcOrderTax(SKUID:int, Code:String, callback:Function):void
		{
			api.Commerce.calcOrderTax(SKUID, Code, callback).addResponder(this);
		}
		
		public function getBonusAssetLocation(BonusAssetID:int, callback:Function):void
		{
			api.Stream.getBonusAssetLocation(BonusAssetID, callback).addResponder(this);
		}
		
		public function getBillingInfo(callback:Function):void 
		{
			api.Commerce.getBillingInfo(callback).addResponder(this);
		}
		
		public function setUserBillingInfo(CCNum:String, CCExp:String, FullName:String, Address:String, Address2:String, City:String, State:String, Country:String, Zip:String, Email:String, callback:Function = null):void
		{
			api.User.setUserBillingInfo(CCNum, CCExp, FullName, Address, Address2, City, State, Country, Zip, Email, callback).addResponder(this);
		}
		
		public function setUserPassword(CurrentPassword:String, NewPassword:String, callback:Function = null):void
		{
			api.User.setUserPassword(CurrentPassword, NewPassword, callback).addResponder(this);
		}
		
		public function emailPassword(Email:String, StoreURL:String, StoreName:String, callback:Function = null):void
		{
			api.User.emailPassword(Email, StoreURL, StoreName, callback).addResponder(this);
		}
		
		public function getBonusAssetURL(bonusAssetId:int, callback:Function = null):void
		{
			api.Download.getBonusAssetURL(bonusAssetId, callback);		
		}
		
		public function getBrowseList(genreId:int, sort:String, pageNum:int, itemsPerPage:int, purchaseType:String, profile:String, callback:Function = null):AsyncToken
		{
			var token:AsyncToken = api.Browse.getBrowseList(genreId, sort, pageNum, itemsPerPage, purchaseType, profile, callback);
			token.addResponder(this);
			return token;
		}
		
		public function getBrowseListPlural(genres:String, sort:String, pageNum:int, itemsPerPage:int, purchaseType:String, profile:String, callback:Function = null):void
		{
			api.Browse.getBrowseListPlural(genres, sort, pageNum, itemsPerPage, purchaseType, profile, callback).addResponder(this);
		}
		
		public function getBundleListing(titleId:int, callback:Function = null):void
		{
			api.Browse.getBundleListing(titleId, callback).addResponder(this);
		}
		
		public function getCustomersAlsoWatched(TitleID:int, callback:Function):void
		{
			api.Browse.getCustomersAlsoWatched(TitleID, callback).addResponder(this);
		}

		public function getEulaText(callback:Function = null):void
		{
			api.Utilities.getEulaText(callback).addResponder(this);			
		}
		
		public function getLegalInfo(callback:Function = null):void
		{
			api.Utilities.getLegalInfo(callback).addResponder(this);			
		}
		
		public function getFullSummary(titleID:int, includePurchaseData:Boolean, callback:Function = null):AsyncToken 
		{
			var cache:Dictionary = getCache(FullTitle);
			var value:Object = cache[titleID];
			var token:AsyncToken;
			
			if (value == null)
			{
				token = api.TitleData.getFullSummary(titleID, includePurchaseData, callback);
				token.addResponder(new mx.rpc.AsyncResponder(onGetFullSummaryCompleted, onGetFullSummaryFault, titleID));	
				
				cache[titleID] = token;
			}
			else if (value is FullTitle)
			{
				if (callback != null)
					callback(value);
				
				lastResult = value;
			}
			else if (value is AsyncToken)
			{	
				token = value as AsyncToken;
				token.addResponder(new StreamAPIResponder(callback));
			}
			
			return token;
		}
		
		public function getNavigation(callback:Function = null):void
		{
			api.Browse.getNavigation(callback).addResponder(this);
		}
		
		public function getPurchasedTitle(passId:int, titleId:int, callback:Function):void
		{
			var cache:Dictionary = getCache(FullTitlePurchased);
			var value:Object = cache[passId];			
			if (value == null)
			{
				var token:AsyncToken = api.Library.getPurchasedTitle(passId, titleId, callback);
				token.addResponder(new mx.rpc.AsyncResponder(onGetPurchasedTitleCompleted, onGetPurchasedTitleFault, titleId));	
				
				cache[passId] = token;
			}
			else if (value is FullTitlePurchased)
				callback(value);
			else if (value is AsyncToken)
				AsyncToken(value).addResponder(new StreamAPIResponder(callback));
		}
		
		public function getAssetLocation(passId:int):String
		{
			var result:String = "";
			var task:DownloadTask = DownloadManager.getInstance().getDownloadTaskById(passId.toString());
			
			if (task)
			{
				result = task.getLocalFileURL();
				result = "file://" + result;
			}
			
			return result;
		}
		
		public function loginUser(username:String, password:String, callback:Function = null):void
		{
			settings.userName = username;
			api.User.loginUser(username, password, CLIENT_NAME, VERSION).addResponder(new mx.rpc.AsyncResponder(onLoginUserCompleted, fault2, callback));
		}
		
		public function logoutUser():void
		{
			api.setAuthToken("");
			defaultSettings.ParentChild = "Parent";
			settings.authToken = "";
			
//			deleteCache(FullTitle);

			_isLoggedIn = false;
			_dispatcher.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "isLoggedIn", true, false));
			
			_cache = null;

			if (_featured)
				_featured.invalidate();
			
			if (_movies)
				_movies.invalidate();
			
			if (_tvShows)
				_tvShows.invalidate();
			
			if (_search)
				_search = null;
		}
		
		private function onSubscriberStatus(result:Boolean):void
		{
			_settings.subscribeNewsLetter = result;
		}
		
		public function toggleSubscriberStatus(value:Boolean,callback:Function = null):void
		{
			api.User.toggleNewsLetterSubscription(value, (callback == null) ? getUserSubscribedToNewsletterState : callback);
		}
		
		private function getUserSubscribedToNewsletterState(inParam:Object = null):void
		{
			api.User.isUserSubscribedToNewsletter(onSubscriberStatus);
		}
		
		public function queueDownload(passid:int, callback:Function = null):void
		{
			api.Download.queueDownload(passid, callback).addResponder(new mx.rpc.AsyncResponder(onQueueDownloadCompleted, fault2, passid));
		}
		
		public function registerUser(username:String, password:String, firstName:String, lastName:String, gender:String, age:String, email:String, callback:Function = null):void
		{
			settings.userName = username;
			api.User.registerUser(username, password, firstName, lastName, gender, age, email, CLIENT_NAME, VERSION).addResponder(new mx.rpc.AsyncResponder(onRegisterUserCompleted, fault2, callback));
		}
		
		public function sendDownloadStatusUpdate(DownloadQueueID:int, StatusID:int, callback:Function = null):void
		{
			api.Download.sendDownloadStatusUpdate(DownloadQueueID, StatusID, callback).addResponder(this);
		}
		
		public function search(query:String, option:String):AsyncCollection
		{
			if (_search == null)
				_search = new Search();
			
			return _search.search(query, option);
		}
		
		public function searchTitlesByCast(castID:int, searchOption:String):AsyncCollection
		{
			var search:SearchCollection = new SearchCollection();
			search.query = castID.toString();
			search.option = searchOption;
			
			return search;
		}
		
		public function searchPerson(query:String, searchOption:String, callback:Function = null):void
		{
			api.Search.searchPerson(query, searchOption, callback).addResponder(this);
		}
		
		public function searchTitleSetOptions(Query:String, SearchOption:String, Sort:String, PageNum:int, ItemsPerPage:int, PurchaseType:String, Profile:String, callback:Function = null):AsyncToken
		{
			var token:AsyncToken = api.Search.searchTitleSetOptions(Query, SearchOption, Sort, PageNum, ItemsPerPage, PurchaseType, Profile, callback);
			token.addResponder(this);
			return token;
		}
		
		public function getBrowseListByCastRole(CastID:int, SearchOption:String, Sort:String, PageNum:int, ItemsPerPage:int, PurchaseType:String, Profile:String, SimilarTitleID:int = 0, callback:Function = null) : AsyncToken
		{
			var token:AsyncToken = api.Browse.getBrowseListByCastRole(CastID, SearchOption, Sort, PageNum, ItemsPerPage, PurchaseType, Profile, SimilarTitleID, callback);
			token.addResponder(this);
			return token;
		}
		
		public function getWishlist(TitleTypeFilter:String, callback:Function = null):AsyncToken
		{
			var token:AsyncToken = api.Wishlist.getWishlist(TitleTypeFilter, callback);
			token.addResponder(this);
			return token;
		}
		
		public function getAvailLibSlaveWheelOptionByMasterWheel(MasterWheel:String, callback:Function):void
		{
			api.Library.getAvailLibSlaveWheelOptionByMasterWheel(MasterWheel, callback).addResponder(this);
		}
		
		public function getUserLibrary(GenreId:String, MasterWheel:String, SlaveWheel:String, callback:Function = null):AsyncToken
		{
			var token:AsyncToken = api.Library.getUserLibrary(GenreId, MasterWheel, SlaveWheel, callback);
			token.addResponder(this);
			return token;
		}
		
		public function getUserLibraryCounts(callback:Function = null):void
		{
			api.Library.getUserLibraryCounts(callback).addResponder(this);
		}

		
		////////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		
		private function onAPIReady(event:StreamEvent):void
		{
			_dispatcher.removeEventListener(StreamEvent.READY, onAPIReady);
			_dispatcher.removeEventListener(StreamEvent.FAULT, onAPIFault);
			
			defaultSettings.ParentChild = event.params.ParentalControlsConfigured ? "Child" : "Parent";
			settings.parentChild = defaultSettings.ParentChild;

			dispatchEvent(event);
		}
		
		private function onAPIFault(event:StreamEvent):void
		{
			fault(event.params.event);
			dispatchEvent(event);
		}

		private function onStreamEvent(event:StreamEvent):void
		{
			_api.removeEventListener(StreamEvent.READY, onStreamEvent);
			_api.removeEventListener(StreamEvent.FAULT, onStreamEvent);
			
			if (event.type == StreamEvent.FAULT)
			{
				trace("API INIT ERROR!!!");
				api_init_status = API_INIT_STATE_LOAD_FAIL;
				_is_offline = true;
				_api = null;
				
				if (event.params && event.params.fault && event.params.fault.faultCode)
				{
					if (event.params.fault.faultCode == "InvalidSecurityToken" || event.params.fault.faultCode == "MessageExpired")
					{
						EventCenter.inst.dispatchEvent(new MainAppEvent(MainAppEvent.SYSTEM_TIME_FORMAT_ERROR));
						return;
					}
				}
			}
			else
			{
				// TODO: Replace with cacheing mechanism
				/*var _storage:SharedObject = SharedObject.getLocal("cache.db");
				
				if (!_storage.data.Endpoints)
				{
					_storage.data.Endpoints = defaultSettings.Endpoints;
					_storage.data.CurrentEnvironment = defaultSettings.CurrentEnvironment;
					_storage.data.EndpointsUTC = new Date();
				}*/
				
				if (event.params.hasOwnProperty("SystemOffline") && com.rovicorp.utilities.StringUtil.parseBoolean(event.params.SystemOffline, false) == true)
				{
					var e:MainAppEvent = new MainAppEvent(MainAppEvent.SYSTEM_OFFLINE_MESSAGE);
					e.offlineMessage = event.params.SystemUnavailableMessage;
					EventCenter.inst.dispatchEvent(e);
					return;
				}
				else
				{
					StreamAPIWse.registerClass("BrowseItem", com.rovicorp.model.TitleListing);
					StreamAPIWse.registerClass("BrowseItemPurchased", com.rovicorp.model.TitleListingPurchased);
					StreamAPIWse.registerClass("BundleItem", com.rovicorp.model.BundleItem);
					StreamAPIWse.registerClass("FullTitle", com.rovicorp.model.TitleDetails);
					
					if (_library) _library = null;
					api_init_status = API_INIT_STATE_LOADED;
					_is_offline = false;
				}
			}

			_dispatcher.dispatchEvent(event);
		}
		
		private function onNavigationFault(event:FaultEvent):void
		{
			_navigation = null;
		}
		
		private function onAddItemToWishList(data:ResultEvent, titleID:int):void
		{
			result(data);
			
			var cache:Dictionary = getCache(FullTitle);
			var title:TitleDetails = cache[titleID];
			
			if (title)
				title.InUserWishlist = true;
			
			if (_wishlist)
				_wishlist.invalidate();
		}

		private function onRemoveItemFromWishList(data:ResultEvent, titleID:int):void
		{
			result(data);

			var cache:Dictionary = getCache(FullTitle);
			var title:TitleDetails = cache[titleID];
			
			if (title)
				title.InUserWishlist = false;

			if (_wishlist)
				_wishlist.invalidate();
		}
		
		private function onLoginUserCompleted(data:ResultEvent, callback:Function):void
		{
			var response:AuthTokenResponse = data.result as AuthTokenResponse;
			
			if (response.ResponseCode == 0)
			{
				// TODO: Determine what data we need to keep
				var parentChild:String = defaultSettings.ParentChild;
				
				api.setAuthToken(response.AuthToken);
				settings.authToken = response.AuthToken;
				
				defaultSettings.ParentChild = response.ParentalControlsConfigured ? "Child" : "Parent";
				settings.parentChild = defaultSettings.ParentChild;
				
				_isLoggedIn = true;
				_dispatcher.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "isLoggedIn", false, true));
				
				if (callback != null)
					callback(data.result);
				
				if (parentChild != defaultSettings.ParentChild)
				{
					if (_movies)
						_movies.invalidate();
					
					if (_tvShows)
						_tvShows.invalidate();
					
					if (_featured)
						_featured.invalidate();
					
					_search = null;
				}
				
				//get user subscriber status 
				getUserSubscribedToNewsletterState();
			}
			else if (response.ResponseCode == 52)
			{
				// Password Mismatch
				if (callback != null)
					callback({loginFailed: true, responseCode: response.ResponseCode, responseMsg: response.ResponseMessage});
			}
			else if (response.ResponseCode == 2)
			{
				// Invalid User ID
				if (callback != null)
					callback({loginFailed: true, responseCode: response.ResponseCode, responseMsg: response.ResponseMessage});
			}
			
			result(data);
		}

		private function onRegisterUserCompleted(data:ResultEvent, callback:Function):void
		{
			var response:AuthTokenResponse = data.result as AuthTokenResponse;
			
			if (response.ResponseCode == 0)
			{
				// TODO: Determine what data we need to keep
				api.setAuthToken(response.AuthToken);
				settings.authToken = response.AuthToken;
				
				_isLoggedIn = true;
				_dispatcher.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "isLoggedIn", false, true));
			}
			if (callback != null)
				callback(data.result);
			
			result(data);
		}
		
		private function onPurchaseCompleted(data:ResultEvent):void
		{
			if (data.result.ResponseCode == 0 && _library)
			{
				_library.invalidate();
			}
			
			result(data);
		}
		
		private function onGetFullSummaryCompleted(data:ResultEvent, titleId:int):void
		{
			getCache(FullTitle)[data.result.TitleID] = data.result;
			result(data);			
		}

		private function onGetFullSummaryFault(info:FaultEvent, titleId:int):void
		{
			getCache(FullTitle)[titleId] = null;
			fault(info);
		}
		
		private function onGetPurchasedTitleCompleted(data:ResultEvent, titleId:int):void
		{
			// Do NOT cache title as the content is time specific
			getCache(data.result)[data.result.PassID] = null;  //data.result;
			result(data);
			
			// update purchased title info to db
			var ft:FullTitlePurchased = data.result as FullTitlePurchased;
			var dbe:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_PURCHASED_TITLE);
			dbe.id = ft.PassID.toString();
			dbe.purchasedTitle = ft;
			EventCenter.inst.dispatchEvent(dbe);
		}

		private function onGetPurchasedTitleFault(info:FaultEvent, titleId:int):void
		{
			getCache(FullTitlePurchased)[titleId] = null;
			fault(info);
		}
		
		private function onQueueDownloadCompleted(result:Object, passid:int):void
		{
			// result is ResultEvent
			// result.result is com.sonic.response.Download.v1.AssignPassToDownloadDestinationResponse
			// DownloadQueueID can be get from here.
			
			// AssignPassToDownloadDestinationResponse:
			// ResponseCode: 0
			// ResponseMessage: "Success"
			// DownloadDestinationID: 63510
			// DownloadQueueID: 652456
			// PurchaseQueueID: 0
			// SKUID: 406783
			// TotalDownloads: 3
			// TotalLicenses: 1
			
			this.result(result);
			
			if (result.result.ResponseCode == 0)
			{
				api.Download.pollForDownloads(onPollForDownloadsCompleted);

				var evt:DownloadEvent = new DownloadEvent(DownloadEvent.ADD_FAKE_TASK);
				evt.id = passid.toString();
				EventCenter.inst.dispatchEvent(evt);
			}
	
		}
		
		private function onPollForDownloadsCompleted(result:Object):void
		{
			var items:ContentItemCollection = result as ContentItemCollection;
			if (items != null)
			{
				if (items.ResponseCode == 0)
				{
					for each(var contentItem:ContentItem in items.ContentItems)
					{
						if (contentItem is FullTitlePurchased)
						{
							// save title's boxart to local disk for offline mode
							ImagePool.getInstance().saveRemoteImageToDisk(Boxart.URL_77 + contentItem.BoxartPrefix + Boxart.POST_FIX_77, null);
							
							// update to db
							var dbe:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_PURCHASED_TITLE);
							dbe.id = contentItem.PassID.toString();
							dbe.purchasedTitle = contentItem;
							EventCenter.inst.dispatchEvent(dbe);
							
							// update the some other info
							var li:Object = {};
							li._id = contentItem.PassID;
							li.license_url = contentItem.PrimaryFile.LicenseURL;
							li.custom_data = contentItem.PrimaryFile.CustomData;
							li.download_url = contentItem.PrimaryFile.DownloadURL;
							li.asset_id = contentItem.PrimaryFile.AssetID;
							li.friendly_file_name = contentItem.PrimaryFile.FriendlyFileName;
							li.license_acknowledge_url = contentItem.PrimaryFile.LicenseAcknowledgeURL;
							
							var e1:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO);
							e1.id = contentItem.PassID.toString();
							e1.library = li;
							EventCenter.inst.dispatchEvent(e1);
							
							if (contentItem.PrimaryFile != null)
							{
								var f:File;
								if (contentItem.PrimaryFile.FriendlyFileName)
								{
									f = new File("/accounts/1000/shared/videos/" + 
										com.adobe.utils.StringUtil.replace(contentItem.PrimaryFile.FriendlyFileName, ".wmv", ".ismv"));
								}
								else
								{
									// TODO: get file name from URL
									f = new File("/accounts/1000/shared/videos/" + contentItem.TitleID + "_" +contentItem.PassID);
								}
								
								if (contentItem.PrimaryFile.DownloadURL)
								{	
									EventCenter.inst.dispatchEvent(new DatabaseEvent(DatabaseEvent.GET_DOWNLOAD_LISTING, contentItem.PassID.toString()));
									
									DownloadManager.getInstance().addDownloadTask(
																	contentItem.PassID.toString(),
																	contentItem.PrimaryFile.DownloadURL,
																	//"http://trailer.roxionow.com.edgesuite.net/10000_bc_cdaabbe3_SD_TRAILER_2000k.mp4",
																	f.nativePath,
																	contentItem.DownloadQueueID.toString());									
								}
							}
						}
					}
				}
				else
				{
					trace("Error returns in PollForDownload");
				}
			}
			else
			{
				trace("pollForDownload Error");
			}
		}
		
		private function onPropertyChange(event:PropertyChangeEvent):void
		{
			dispatchEvent(event);
		}

	}
}
