package com.rovicorp.model
{
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.service.download.DownloadEvent;
	import com.rovicorp.service.download.DownloadManager;
	import com.rovicorp.service.download.DownloadStatus;
	import com.rovicorp.service.download.LicenseEvent;
	import com.rovicorp.service.download.LicenseStatus;
	import com.rovicorp.utilities.LogUtil;
	
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayList;
	
	import net.rim.aircommons.download.taskmanager.Task;
	
	[Bindable]
	public class DownloadList extends ArrayList
	{
//		private var downloadMgr:DownloadManager = DownloadManager.getInstance();
		private var index:Dictionary = new Dictionary();
		private var service:RoxioNowDAO = new RoxioNowDAO;
		private var filter:Function = downloads;
		
		public function DownloadList(filter:Function = null):void
		{
			if (filter != null)
				this.filter = filter;
			
			// Subscribe to download manager events
			var downloadManager:DownloadManager = DownloadManager.getInstance();
			downloadManager.addEventListener(DownloadEvent.TASK_STATUS_CHANGED, onTaskStatusChanged, false, 0, true);
			downloadManager.addEventListener(DownloadEvent.TASK_PROGRESS, onTaskProgress, false, 0, true);
			downloadManager.addEventListener(DownloadEvent.DOWNLOAD_ERROR, onDownloadError, false, 0, true);			
			downloadManager.addEventListener(LicenseEvent.LICENSE_EVENT, onLicenseEvent, false, 0, true);			
			
			// Subscribe to database event for getting library ino
			EventCenter.inst.addEventListener(DatabaseEvent.GET_DOWNLOAD_LISTING_DONE, onPurchaseTitleInfoDone, false, 0, true);			
			
			EventCenter.inst.addEventListener(DownloadEvent.ADD_FAKE_TASK, onAddFakeTask, false, 0, true);

			// Build download list
			//
			var tasks:Array = downloadManager.getAllTasks();
			var items:Array = new Array();
			
			for each (var task:Task in tasks)
			{
				if (this.filter( DownloadStatus.fromTaskStatus(task.getStatus()), task.getId() ))
				{
					var item:DownloadListing = new DownloadListing();

					item.PassID = int(task.getId())
					item.BytesTotal = task.getProgressHandle().getLength();
					item.BytesDownloaded = task.getProgressHandle().getPosition();
					item.TimeRemaining = task.getProgressHandle().getEstimatedTimeRemaining();
					item.DownloadStatus = DownloadStatus.fromTaskStatus(task.getStatus());
					
					items.push(item);
					index[item.PassID] = item;
					EventCenter.inst.dispatchEvent(new DatabaseEvent(DatabaseEvent.GET_DOWNLOAD_LISTING, task.getId()));
				}
			}
			
			this.source = items;
			
			downloadManager.autoStart(service.settings.lastDownloadId);
		}

		private function getItemByID(id:int):DownloadListing
		{
			var item:DownloadListing = index[id];
			
			if (item == null)
			{
				item = new DownloadListing();
				item.PassID = id;
				
				addItem(item);
				index[item.PassID] = item;
				EventCenter.inst.dispatchEvent(new DatabaseEvent(DatabaseEvent.GET_DOWNLOAD_LISTING, id.toString()));
			}
			
			return item;
		}
		
		public function hasError():Boolean
		{
			for each( var item:DownloadListing in index )
			{
				if( item != null && item.DownloadStatus == DownloadStatus.TASK_ERROR )
				{
					return true;
				}
			}
			return false;
		}
				
		private function onTaskStatusChanged(event:DownloadEvent):void
		{
			var item:DownloadListing = index[int(event.id)];
			
			if (item == null && filter(event.status, event.id))
				item = getItemByID(int(event.id));
			
			if ( (RoxioNowDAO.api != null && RoxioNowDAO.api.DeviceSettings != null && RoxioNowDAO.api.DeviceSettings.CountryID != RoxioNowDAO.CURRENT_COUNTRY_ID) && item && item.LicenseStatus != LicenseStatus.LICENSE_RECEIVED )
			{
				if( item.DownloadStatus == DownloadStatus.TASK_IN_PROGRESS )
					DownloadManager.getInstance().pauseDownloadItemByID(item.PassID.toString());
			}
			
			if( item && event.status == DownloadStatus.TASK_IN_PROGRESS )
			{
				service.settings.lastDownloadId = event.id;
			}
				
			if (item && item.DownloadStatus != event.status)
			{				
				item.DownloadStatus = event.status;				
				
				if (RoxioNowDAO.api)
					service.sendDownloadStatusUpdate(event.dqid, item.DownloadStatus);

				if (!filter(item.DownloadStatus, event.id))
				{
					removeItemAt(getItemIndex(item));
					delete index[int(event.id)];
				}
			}
		}
		
		private function onAddFakeTask(e:DownloadEvent):void
		{
			var item:DownloadListing = getItemByID(int(e.id));
			item.DownloadStatus = DownloadStatus.TASK_WAITING;
			
			service.library.updatePurchaseTitleInfo(int(e.id), "DownloadStatus", item.DownloadStatus);
			
			var evt:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_DOWNLOAD_LISTING, e.id);
			evt.downloadListing = item;
			EventCenter.inst.dispatchEvent(evt);

		}
		
		public function removeFakeTask( id:int ):void
		{
			var item:DownloadListing = index[id];
			
			if (item != null && item.DownloadStatus == DownloadStatus.TASK_WAITING )
			{
				removeItemAt(getItemIndex(item));
				delete index[id];
			}
		}
		
		public static function downloads(status:int, id:String):Boolean
		{
			return status != DownloadStatus.TASK_COMPLETE && status != DownloadStatus.TASK_UNAVAILABLE;
		}
		
		private function onTaskProgress(event:DownloadEvent):void
		{
			var item:DownloadListing = index[int(event.id)];
			
			if (item)
			{
				item.BytesTotal = event.bytesTotal;
				item.BytesDownloaded = event.bytesDownloaded;
				item.TimeRemaining = event.timeRemaining;
				item.DownloadPercent = item.BytesDownloaded / item.BytesTotal;
			}
		}

		private function onDownloadError(event:DownloadEvent):void
		{
			var item:DownloadListing = index[int(event.id)];
			
			if (item)
				item.DownloadStatus = DownloadStatus.TASK_ERROR;
		}
		
		private function onLicenseEvent(event:LicenseEvent):void
		{
			var item:DownloadListing = index[int(event.id)];
			
			if (item)
			{
				item.LicenseStatus = event.status;
				service.library.updatePurchaseTitleInfo(int(event.id), "LicenseStatus", item.LicenseStatus);
			}
		}
		
		private function onPurchaseTitleInfoDone(event:DatabaseEvent):void
		{
			if (event.success)
			{
				var title:DownloadListing = event.result[0] as DownloadListing;
				var item:DownloadListing = index[title.PassID];
				
				if (item)
				{
					item.TitleID = title.TitleID;
					item.BoxartPrefix = title.Details.BoxartPrefix;
					item.Name = title.Name;
					item.MPAARating = title.Details.MPAARating;					

					item.setDetails(title.Details);
					
					item.lastPlayPosition = title.lastPlayPosition;
					
					item.LicenseStatus = title.LicenseStatus;
					
					if( RoxioNowDAO.api != null && RoxioNowDAO.api.DeviceSettings != null && RoxioNowDAO.api.DeviceSettings.CountryID != RoxioNowDAO.CURRENT_COUNTRY_ID && item.LicenseStatus != LicenseStatus.LICENSE_RECEIVED )
					{
						DownloadManager.getInstance().pauseDownloadItemByID(item.PassID.toString());
					}
				}
			}
		}
	}
}