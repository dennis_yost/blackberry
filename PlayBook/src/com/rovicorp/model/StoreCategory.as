package com.rovicorp.model
{
	import com.sonic.response.Genre;
	
	import mx.collections.ArrayCollection;
	
	public class StoreCategory 
	{
		public var ID:int;
		public var Name:String;
		public var titles:ArrayCollection = new ArrayCollection();
		
		internal var totalItems:int = 0;
		
		static internal function fromGenre(genre:Genre):StoreCategory
		{
			var category:StoreCategory = new StoreCategory();
			
			category.ID = genre.ID;
			category.Name = genre.Name;
			
			return category;
		}
		
		public function StoreCategory()
		{
			super();
		}
	}
}