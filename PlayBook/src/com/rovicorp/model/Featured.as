package com.rovicorp.model
{
	import com.sonic.StreamEvent;
	
	import flash.events.EventDispatcher;
	import flash.net.SharedObject;
	
	import mx.events.CollectionEvent;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;
	
	public class Featured extends EventDispatcher implements IResponder
	{
		//Will be changed by StoreNavigation (line 61)
		public static var NEW_RELEASES:int = 6706; 
		
		private var _errorCode:int;
		private var _state:String = "loading";
		private var _service:RoxioNowDAO = new RoxioNowDAO();
		
		private var _features:AsyncCollection;
		private var _recommended:AsyncCollection;
		
		public function Featured()
		{
			_service.addEventListener(FaultEvent.FAULT, fault, false, 0, true);
			_service.settings.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onSettingsChange);
			
			if (_service.deviceSettings == null)
				_service.addEventListener(StreamEvent.READY, onAPIReady);
			else					
				invalidate();
		}
		
		[Bindable]
		public function get errorCode():int
		{
			return _errorCode;
		}
		
		public function set errorCode(value:int):void 
		{
			if (value != _errorCode)
			{
				var oldValue:int = _errorCode;
				
				_errorCode = value;
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "errorCode", oldValue, value));
			}
		}

		public function get features():AsyncCollection
		{
			if (_features == null)
				_features = new AsyncCollection();
			
			return _features;
		}

		public function get recommended():AsyncCollection
		{
			if (_recommended == null)
				_recommended = new AsyncCollection();
			
			return _recommended;
		}
		
		[Bindable]
		public function get state():String
		{
			return _state;
		}
		
		public function set state(value:String):void 
		{
			if (value != _state)
			{
				var oldState:String = _state;

				_state = value;
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "state", oldState, value));
			}
		}

		public function invalidate():void
		{
			/*var _storage:SharedObject = SharedObject.getLocal("cache.db");
			
			if (_storage.data.Featured)
			{
				var items:Array = [];
				
				for each (var item:Object in _storage.data.Featured)
				{
					var listing:TitleListing = new TitleListing();
					listing.BoxartPrefix = item.BoxartPrefix;
					listing.TitleID = item.TitleID;
					listing.Name = item.Name;
					
					items.push(listing);
				}
				
				loadData(items);
			}
			else*/
				state = "loading";
			
			$loadCategories();
		}
		
		private function $loadCategories():void
		{			
			if (_service.navigation.categoryLoaded)
			{				
				var purchaseType:String = _service.settings.rentalsOnly ? "rent" : "any";
				_service.getBrowseList(NEW_RELEASES, "standard", 1, 21, purchaseType, "none").addResponder(this);
			}
			else 
			{	
				_service.navigation.addEventListener(CollectionEvent.COLLECTION_CHANGE, onNavigationLoaded);
				_service.navigation.addEventListener(FaultEvent.FAULT, onNavigationFault);
			}
		}
		
		private function onNavigationLoaded(e:CollectionEvent):void
		{
			$loadCategories();
		}
		
		private function onNavigationFault(e:FaultEvent):void
		{
			fault(e);
		}
		
		public function fault(info:Object):void
		{
			// TODO: Figure out error codes, if network error listen for network update event
			state = "error";
			dispatchEvent(info as FaultEvent);
		}

		public function result(data:Object):void
		{
			var array:Array = data.result.Items.toArray();
			loadData(array);

			/*var items:Array = [];
			for each (var item:TitleListing in array)
				items.push({TitleID: item.TitleID, BoxartPrefix: item.BoxartPrefix, Name: item.Name});

			var _storage:SharedObject = SharedObject.getLocal("cache.db");
			_storage.data.Featured = items;*/
		}
		
		private function loadData(array:Array):void
		{
			trace("[" + new Date().toTimeString() + "] Featured.loadData()" + array.length);
			
			features.setSource(array.slice(0, 12));
			recommended.setSource(array);
			
			state = "loaded";
		}		

		private function onAPIReady(event:StreamEvent):void
		{
			_service.removeEventListener(StreamEvent.READY, onAPIReady);
			invalidate();
		}
		
		private function onSettingsChange(event:PropertyChangeEvent):void
		{
			if (event.property == "rentalsOnly")
				invalidate();
		}
	}
}