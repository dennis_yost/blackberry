package com.rovicorp.model
{
	import com.sonic.response.Browse.BrowseItem;
	import com.sonic.response.Browse.BrowseList;
	import com.sonic.response.Browse.BundleList;
	import com.sonic.response.TitleData.FullTitle;
	import com.sonic.response.TitleData.ShortTitle;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.events.FaultEvent;
	
	[Bindable]
	public class TitleListingBase extends EventDispatcher 
	{
		public static const LOADING:int = 1;
		
		protected var _state:int = 0;
		protected var _details:FullTitle;
		
		public var Name:String;
		protected var _titleID:int;
		
		private var _recommendationItems:AsyncCollection;
		
		public function TitleListingBase()
		{
			super();
		}
		
		protected function getDetails():void
		{
			_state = LOADING;
		
			if (TitleID)
			{
				var request:RoxioNowDAO = new RoxioNowDAO;
				request.addEventListener(FaultEvent.FAULT, onFault);
				request.getFullSummary(TitleID, true, onGetFullSummaryCompleted);	
			}
		}
		
		public function get TitleID():int
		{
			return _titleID;	
		}
		
		public function set TitleID(value:int):void
		{
			_titleID = value;
			
			if (_state == LOADING)
			{
				var request:RoxioNowDAO = new RoxioNowDAO;
				request.addEventListener(FaultEvent.FAULT, onFault);
				request.getFullSummary(TitleID, true, onGetFullSummaryCompleted);	
			}
		}
		
		public function get Details():FullTitle
		{			
			if (_details == null && _state != LOADING)
				getDetails();	
			
			return _details;
		}
		
		public function setDetails(value:FullTitle):void
		{
			_details = value;
			_state = 0;
			
//			dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "Details", null, value));
		}
		
		public function set Details(value:FullTitle):void
		{
			setDetails(value);
		}
		
		public function get RecommendationItems():AsyncCollection
		{
			if (_recommendationItems == null && Details != null && (Details.TitleType == "TV_Episode" || Details.TitleType == "Movie") )
			{
				_recommendationItems = new AsyncCollection();

				var request:RoxioNowDAO = new RoxioNowDAO;
				request.addEventListener(FaultEvent.FAULT, onFault);
				request.getCustomersAlsoWatched(TitleID, onGetAlsoWatchListingCompleted);
			}
			
			return _recommendationItems;
		}
		
		public function set RecommendationItems(value:AsyncCollection):void
		{
			//			_recommendationItems = value;
		}
		
		private function onGetAlsoWatchListingCompleted(data:BrowseList):void
		{
			var items:Array = data.Items.toArray();
			/*if( items.length < 5 )
			{
				var n:int = 5 - items.length;
				var tvShows:Store = new RoxioNowDAO().tvShows;
				var total:uint = tvShows.length;
				
				var item:TitleListing;
				
				for( var i:uint = 0;i<total;i++)
				{
					item = tvShows.getItemAt(i) as TitleListing;
					
					if( item != null && item.TitleID != this.TitleID )
					{
						items.push(item);
						n--;
						
						if( n == 0 ) break;
					}
				}
			}*/
			_recommendationItems.setSource(items);
		}
		
		
		protected function onGetFullSummaryCompleted(data:FullTitle):void
		{			
			Details = data;
		}
		
		private function onFault(event:FaultEvent):void
		{
			dispatchEvent(event);
			_state = 0;			
		}
	}
}