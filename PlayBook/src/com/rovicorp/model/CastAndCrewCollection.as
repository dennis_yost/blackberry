package com.rovicorp.model
{
	import com.sonic.response.Search.CastAndCrew;
	import com.sonic.response.Search.CastAndCrewList;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;

	public class CastAndCrewCollection extends AsyncCollection 
	{
		private var _option:String;
		
		public function CastAndCrewCollection(query:String, option:String)
		{
			super();
			_option = option;

			var request:RoxioNowDAO = new RoxioNowDAO();
			request.addEventListener(FaultEvent.FAULT, onSearchPersonFault);
			request.searchPerson(query, option, onSearchPersonCompleted);
		}

		public function get option():String
		{
			return _option;
		}
		
		private function onSearchPersonCompleted(data:CastAndCrewList):void
		{
			setSource(data.CastMembers.toArray());
		}
		
		private function onSearchPersonFault(event:FaultEvent):void
		{
			dispatchEvent(event);
		}

	}
}