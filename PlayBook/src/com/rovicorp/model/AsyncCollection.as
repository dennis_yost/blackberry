package com.rovicorp.model
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.IList;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;
	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class AsyncCollection extends EventDispatcher implements IList, IResponder
	{
		public static const LOADING:String = "loading";
		public static const LOADED:String = "loaded";
		public static const ERROR:String = "error";
		
		private var _length:int;
		private var _source:Array = [];
		private var _state:String;
		
		public function AsyncCollection()
		{
		}

		/////////////////////////////////////////////////////////////
		// Properties

		public function get source():Array
		{
			return _source;
		}
		
		public function get state():String
		{
			if (_state == null)
				setState(LOADING);
			
			return _state;
		}
		
		public function set state(value:String):void 
		{
			// We need the state to be bindable so we have to
			// provide the setter but we don't want to allow 
			// external setting of the state
		}

		/////////////////////////////////////////////////////////////
		// Implementation

		protected function setState (value:String):void 
		{
			if (_state != value)
			{
				var oldState:String = _state;
				_state = value;
				
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "state", oldState, state));
			}
		}
		
		internal function setSource(source:Array, length:int = 0):void
		{
			if (_source != source)
			{
				var oldState:String = state;
				
				_source = source;
				_length = length;
				
				if (!length)
					_length = source.length;
				
				dispatchEvent(new CollectionEvent(CollectionEvent.COLLECTION_CHANGE, false, false, CollectionEventKind.REFRESH));
				setState(LOADED);
			}
		}
		
		public function invalidate():void
		{
			var oldState:String = _state;
			
			_length = 0;
			_source = [];
			_state = null;

			if (oldState != _state)
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "state", oldState, null));
				
			dispatchEvent(new CollectionEvent(CollectionEvent.COLLECTION_CHANGE, false, false, CollectionEventKind.REFRESH));
		}

		/////////////////////////////////////////////////////////////
		// IResponder overrides
		
		public function fault(info:Object):void
		{
			setState(ERROR);
		}
		
		public function result(data:Object):void
		{
			setSource(data as Array);
		}
		
		/////////////////////////////////////////////////////////////
		// IList overrides
		
		public function get length():int
		{
			if (_state == null)
				setState(LOADING);
			
			return _length;
		}
		
		public function addItem(item:Object):void
		{
		}
		
		public function addItemAt(item:Object, index:int):void
		{
		}
		
		public function getItemAt(index:int, prefetch:int=0):Object
		{
			if (index < 0 || index >= _length)
				throw new RangeError("Index out of range");

			return _source[index];
		}
		
		public function getItemIndex(item:Object):int
		{
			return _source.indexOf(item);
		}
		
		public function itemUpdated(item:Object, property:Object=null, oldValue:Object=null, newValue:Object=null):void
		{
		}
		
		public function removeAll():void
		{
			_source = [];
			_length = 0;
		}
		
		public function removeItemAt(index:int):Object
		{
			return null;
		}
		
		public function setItemAt(item:Object, index:int):Object
		{
			if (index < 0 || index >= _length)
				throw new RangeError("Index out of range");

			var result:Object = _source[index];
			_source[index] = item;
			
			if (result != null)
				dispatchEvent(new CollectionEvent(CollectionEvent.COLLECTION_CHANGE, false, false, CollectionEventKind.REPLACE, index, index, [item]));
			
			return result;
		}
		
		public function toArray():Array
		{
			return _source;
		}
	}
}