package com.rovicorp.model
{
	import com.sonic.StreamAPIResponder;
	
	import mx.events.PropertyChangeEvent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class Wishlist extends AsyncCollection
	{
		private var _service:RoxioNowDAO = new RoxioNowDAO();
		private var _type:String = "All";
//		private var _wishListInited:Boolean = false;
		
		public function Wishlist()
		{
			_service.addEventListener(FaultEvent.FAULT, fault, false, 0, true);
			_service.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onSettingsChange);
			
/*			if (RoxioNowDAO.apiInitState == RoxioNowDAO.API_INIT_STATE_LOAD_FAIL)
			{
				this.fault(null);
			}
			else					
				invalidate(); */
		}
		
		override public function invalidate():void
		{
/*			if (RoxioNowDAO.api == null)
			{
				state = "error";
			}
			else if (_wishListInited == false)
			{
				_wishListInited = true;
				RoxioNowDAO.api.Wishlist.getOperation("addItemToWishList").addEventListener(ResultEvent.RESULT, onWishlistChanged, false, 0, true);
				RoxioNowDAO.api.Wishlist.getOperation("removeAllItemsFromWishList").addEventListener(ResultEvent.RESULT, onWishlistChanged, false, 0, true);
				RoxioNowDAO.api.Wishlist.getOperation("removeItemFromWishList").addEventListener(ResultEvent.RESULT, onWishlistChanged, false, 0, true);
				state = "loading";
			}
*/			
			super.invalidate();
		}
		
		override public function fault(info:Object):void
		{
			state = "error";
			dispatchEvent(info as FaultEvent);
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function set type(value:String):void
		{
			if (_type !== value)
			{
				var event:PropertyChangeEvent = PropertyChangeEvent.createUpdateEvent(this, "type", _type, value);
				_type = value;
				
				dispatchEvent(event);
				invalidate();
			}
		}
		
		override protected function setState(value:String):void
		{
			super.setState(value);
			
			if (value == LOADING)
			{
				if (_service.isLoggedIn)
					_service.getWishlist(_type).addResponder(this);	
				else
					setSource([]);
			}
		}
		
		override public function result(data:Object):void
		{
			super.result(data.result.Items.toArray());
		}

		private function onSettingsChange(event:PropertyChangeEvent):void
		{
			if (event.property == "isLoggedIn")
				invalidate();
		}
		
/*		private function onWishlistChanged(data:ResultEvent):void
		{
			if (data.result.ResponseCode == 0)
				invalidate();
		}
*/		
	}
}