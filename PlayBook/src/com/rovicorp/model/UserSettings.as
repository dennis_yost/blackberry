﻿package com.rovicorp.model
{
//	import flash.data.EncryptedLocalStore;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.net.SharedObject;
	import flash.utils.ByteArray;
	
	import com.roxionow.flash.util.crypto.XOREncryption;
	
	import mx.events.PropertyChangeEvent;
	
	public class UserSettings extends EventDispatcher
	{
		private var _storage:SharedObject = SharedObject.getLocal("user-settings");
		
		private var _conf:Object;
		
		public const country_name:String = CONFIG::location;
		
		public function UserSettings(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public function get conf():Object
		{
			return _conf;
		}
		
		public function set conf(obj:Object):void
		{
			_conf = obj;
			
			XOREncryption.key = XOREncryption.defaultKey;
			
			if( _conf.Parental_url )
			{
				_conf.Parental_url = XOREncryption.decrypt(_conf.Parental_url);
			}
			
			if( _conf.Brand_support_url )
			{
				_conf.Brand_support_url = XOREncryption.decrypt(_conf.Brand_support_url);
			}
			
			if( _conf.Privacy_url )
			{
				_conf.Privacy_url = XOREncryption.decrypt(_conf.Privacy_url);
			}
			if( _conf.Terms_url )
			{
				_conf.Terms_url = XOREncryption.decrypt(_conf.Terms_url);
			}
		}

		public function get acceptedEULA(): Boolean
		{
			return _storage.data.acceptedEULA;
		}
		
		public function set acceptedEULA(value:Boolean):void
		{
			var oldValue:Boolean = _storage.data.acceptedEULA;
			
			if (value != oldValue)
			{
				_storage.data.acceptedEULA = value;
				_storage.flush();
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "acceptedEULA", oldValue, value));			
			}
		}

		public function get deleteExpiredRentals(): Boolean
		{
			return _storage.data.deleteExpiredRentals;
		}

		public function set deleteExpiredRentals(value:Boolean):void
		{
			var oldValue:Boolean = _storage.data.deleteExpiredRentals;
			
			if (value != oldValue)
			{
				_storage.data.deleteExpiredRentals = value;
				_storage.flush();
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "deleteExpiredRentals", oldValue, value));			
			}
		}

		public function get keepSignedIn(): Boolean
		{
			if (_storage.data.keepSignedIn == null)
			{
				_storage.data.keepSignedIn = true;
				_storage.flush();
			}
			
			return _storage.data.keepSignedIn;
		}
		
		public function set keepSignedIn(value:Boolean):void
		{
			var oldValue:Boolean = _storage.data.keepSignedIn;
			
			if (value != oldValue)
			{
				_storage.data.keepSignedIn = value;
				_storage.flush();
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "keepSignedIn", oldValue, value));			
			}
		}
		
		public function get rentalsOnly(): Boolean
		{
			return _storage.data.rentalsOnly;
		}
		
		public function set rentalsOnly(value:Boolean):void
		{
			var oldValue:Boolean = _storage.data.rentalsOnly;
			
			if (value != oldValue)
			{
				_storage.data.rentalsOnly = value;
				_storage.flush();
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "rentalsOnly", oldValue, value));			
			}
		}
		
		public function get subscribeNewsLetter(): Boolean
		{
			if (_storage.data.subscribeNewsLetter == null)
			{
				_storage.data.subscribeNewsLetter = false;
				_storage.flush();
			}
			return _storage.data.subscribeNewsLetter;
		}
		
		public function set subscribeNewsLetter(value:Boolean):void
		{
			var oldValue:Boolean = _storage.data.subscribeNewsLetter;			
			if (value != oldValue)
			{
				_storage.data.subscribeNewsLetter = value;
				_storage.flush();
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "subscribeNewsLetter", oldValue, value));			
			}
		}
		
		public function get lastDownloadId():String
		{
			return _storage.data.lastDownloadId;
		}
		
		public function set lastDownloadId(value:String):void
		{
			var oldValue:Boolean = _storage.data.lastDownloadId;			
			if (value != oldValue)
			{
				_storage.data.lastDownloadId = value;
				_storage.flush();			
			}
		}
		
		public function get userName(): String
		{
			return _storage.data.userName;
		}
		
		public function set userName(value:String):void
		{
			var oldValue:String = _storage.data.userName;
			
			if (value != oldValue)
			{
				_storage.data.userName = value;
				_storage.flush();
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "userName", oldValue, value));			
			}
		}
		
		public function get parentChild(): String
		{
			return _storage.data.parentChild;
		}
		
		public function set parentChild(value:String):void
		{
			var oldValue:String = _storage.data.parentChild;
			
			if (value != oldValue)
			{
				_storage.data.parentChild = value;
				_storage.flush();
				dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, "parentChild", oldValue, value));			
			}
		}
		
		public function get dataChargeWarning(): Boolean
		{
			if (_storage.data.dataChargeWarning == null)
			{
				_storage.data.dataChargeWarning = true;
				_storage.flush();
			}
			return _storage.data.dataChargeWarning;
		}
		
		public function set dataChargeWarning(value:Boolean):void
		{
			var oldValue:Boolean = _storage.data.dataChargeWarning;
			
			if (value != oldValue)
			{
				_storage.data.dataChargeWarning = value;
				_storage.flush();			
			}
		}
		
		internal function get authToken():String
		{
			var result:String = "";
			/* TODO: EncryptedLocalStore is not supported in current builds.  Will need to
			 * switchover authtoken storage as soon as it is available.
			var storedValue:ByteArray = EncryptedLocalStore.getItem("authtoken");
			
			if (storedValue)
			{
				result = storedValue.readUTFBytes(storedValue.length); 
				setLoggedIn(result.length > 0);
			}
			*/
			if (_storage.data.authToken)
				result = _storage.data.authToken;
			
			return result;
		}

		internal function set authToken(value:String):void
		{
			/* TODO: EncryptedLocalStore is not supported in current builds.  Will need to
			 * switchover authtoken storage as soon as it is available.
			var bytes:ByteArray = new ByteArray();
			bytes.writeUTFBytes(value);

			EncryptedLocalStore.setItem("authtoken", bytes);
			*/
			_storage.data.authToken = value;
			_storage.flush();
		}
	}
}