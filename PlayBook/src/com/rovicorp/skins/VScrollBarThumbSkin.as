package com.rovicorp.skins
{
	import spark.skins.mobile.VScrollBarThumbSkin;
	
	public class VScrollBarThumbSkin extends spark.skins.mobile.VScrollBarThumbSkin
	{
		public function VScrollBarThumbSkin()
		{
			super();
			
			paddingRight = 2;
		}
		
		//--------------------------------------------------------------------------
		//
		//  Overridden methods
		//
		//--------------------------------------------------------------------------	
		
		/**
		 *  @protected
		 */ 
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void
		{
			var thumbWidth:Number = unscaledWidth - paddingRight;
			
			graphics.beginFill(0x808080, 0.5);
//			graphics.lineStyle(1, 0x000000, 1, false, LineScaleMode.NORMAL, CapsStyle.NONE, JointStyle.ROUND);
			graphics.drawRoundRect(0.5, paddingVertical + 0.5, 
				thumbWidth, unscaledHeight - 2 * paddingVertical, 
				thumbWidth, thumbWidth);
			
			graphics.endFill();
		}
	}
}