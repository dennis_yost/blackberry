package com.rovicorp.skins
{
	import spark.skins.mobile.HScrollBarSkin;
	
	public class HScrollBarSkin extends spark.skins.mobile.HScrollBarSkin
	{
		public function HScrollBarSkin()
		{
			super();
			
			thumbSkinClass = com.rovicorp.skins.HScrollBarThumbSkin;
		}
	}
}