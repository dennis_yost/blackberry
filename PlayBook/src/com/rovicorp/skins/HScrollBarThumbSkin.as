package com.rovicorp.skins
{
	import spark.skins.mobile.HScrollBarThumbSkin;
	
	public class HScrollBarThumbSkin extends spark.skins.mobile.HScrollBarThumbSkin
	{
		public function HScrollBarThumbSkin()
		{
			super();
			
			paddingBottom = 2;
		}
		
		//--------------------------------------------------------------------------
		//
		//  Overridden methods
		//
		//--------------------------------------------------------------------------
		
		/**
		 *  @private
		 */
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void
		{
			var thumbHeight:Number = unscaledHeight - paddingBottom;
			
			graphics.beginFill(0x808080, 0.5);
			graphics.drawRoundRect(paddingHorizontal + .5, 0.5, 
				unscaledWidth - 2 * paddingHorizontal, thumbHeight, 
				thumbHeight, thumbHeight);
			
			graphics.endFill();
		}    
		
	}
}