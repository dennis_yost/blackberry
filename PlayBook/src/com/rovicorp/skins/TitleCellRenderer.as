package com.rovicorp.skins
{
	import com.rovicorp.model.DownloadListing;
	import com.rovicorp.utilities.Boxart;
	import com.rovicorp.utilities.ImageLoaderUtil;
	import com.rovicorp.utilities.ImagePool;
	import com.rovicorp.utilities.StringUtil;
	import com.sonic.response.TitleData.FullTitle;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.EventDispatcher;
	import flash.geom.Matrix;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import mx.events.PropertyChangeEvent;
	
	import qnx.ui.listClasses.CellRenderer;
	import qnx.ui.text.Label;
	
	public class TitleCellRenderer extends CellRenderer
	{
		public static const IMAGE_WIDTH:int = 102;
		public static const IMAGE_HEIGHT:int = 139;
		
		//protected var image:Image;
		protected var title:Label;	
		protected var shadow:Bitmap;
		protected var image:Bitmap;
		
		private static var textFormat:TextFormat;
		
		[Embed(source="assets/images/title_boxart_placeholder_normal.png")] 
		public static var shadow_asset:Class;			
		public static var asset_instance:Object = new shadow_asset();
		
		public static var loaderUtil:ImageLoaderUtil = new ImageLoaderUtil(IMAGE_WIDTH, IMAGE_HEIGHT);
		
		public function TitleCellRenderer()
		{
			super();
		}
		
		override protected function init():void 
		{
			super.init();
			this.skin.visible = false;
			
			if (image == null)
			{				
				image = new Bitmap();
				image.x = 3;
				image.y = 4;
				//image.setSize(IMAGE_WIDTH, IMAGE_HEIGHT);
				//image.addEventListener(Event.COMPLETE, onImageLoaded, false, 0, true);
				//image.addEventListener(IOErrorEvent.IO_ERROR,onIOError, false, 0, true);
				
				title = new Label();
				title.format = getTextFormat();
				title.setSize(IMAGE_WIDTH, 22);
				title.y = image.y+IMAGE_HEIGHT+5;
				title.x = 3;
				
				shadow = new Bitmap();
				shadow.x = -12;
				shadow.y = -9;
				
				this.addChild(shadow);
				this.addChild(image);
				this.addChild(title);
			}
			
			this.setSize(89, 162);
		}
		
		override public function set data(value:Object) : void
		{
			if (super.data)
				(data as EventDispatcher).removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onPropertyChange);
			
			super.data = value;
			
			image.bitmapData = null;
			
			if( shadow.bitmapData == null )
			{		
				shadow.bitmapData = asset_instance.bitmapData;
			}			
			(value as EventDispatcher).addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onPropertyChange, false, 0, true);				
			
			if (value.BoxartPrefix != null)
				loadImage();						
			
			showName();		
		}
		
		protected function loadImage():void
		{	
			var imageUrl:String = Boxart.URL_107 + data.BoxartPrefix + Boxart.POST_FIX_107;	
						
			var bd:BitmapData = ImagePool.getInstance().getCachedImageBitmapData(imageUrl);
			if( bd )
			{				
				showImage(index, bd);				
			}
			
		}		
		
		static private function getTextFormat():TextFormat
		{
			if (textFormat == null)
			{
				textFormat = new TextFormat();
				textFormat.font = "MyriadPro";
				textFormat.size = 14;
				textFormat.align = TextFormatAlign.CENTER;
				textFormat.bold = false;
				textFormat.color = 0x191919;
			}
			
			return textFormat;
		}
		
		protected function onPropertyChange(event:PropertyChangeEvent) : void
		{			
			showName();
			
			if (event.property == "BoxartPrefix" || event.property == "Details")
			{
				loaderUtil.addTask(index, Boxart.URL_107 + data.BoxartPrefix + Boxart.POST_FIX_107, showImage);
			}
		}
		
		protected function showName():void
		{
			var label:String = data.Name;
			if( !label ) label = "Loading...";

			label = StringUtil.convertHtmlToPlainText(label);
			
			var add:String = "";
			
			if( data is DownloadListing )
			{
				var fullTitle:FullTitle = (data as DownloadListing).Details;
				
				if( fullTitle && fullTitle.TitleType == "TV_Episode" && fullTitle.MetaValues)
				{					
					try
					{
						add = fullTitle.MetaValues["ShowName"] + " - " + formatNumber(fullTitle.MetaValues["SeasonName"]) + " - " +formatNumber(fullTitle.MetaValues["EpisodeNumber"]) +  " - ";
					}catch(e:Error)
					{
						
					}
					label = add + label;
				}
			}
			
			var reg:RegExp = /(.*) - season (\d+) - episode (\d+) - /i; 
			var info:Array = reg.exec(label);
			
			var shortName:String = label;	
			add = "";
			
			if( info != null )
			{
				shortName = info[1];
				add = "S:" + info[2] + " E:" + info[3] + "  ";
			}
			else
			{
				reg = /(.*) - season (\d+)$/i; 
				info = reg.exec(label);
				if( info )
				{
					shortName = info[1];
					add = "S:" + info[2] + "  ";
				}
			}
			
			if(shortName.length>12) {
				shortName = shortName.substr(0,12)+"...";
			} 
			
			title.text = shortName;		
			
			if( add )
			{
				title.setSize(IMAGE_WIDTH, 45);
				title.text += "\n " + add;
			}
		}
		
		private function formatNumber( str:String ):String
		{
			var items:Array = str.split(" ");
			
			if( items.length == 2 )
			{
				if( items[1] && String( items[1]).length == 1 )
				{
					items[1] = "0" + items[1];
				}
			}
			return items.join(" ");
		}
		
		public function showImage( index:int, bd:BitmapData ):void
		{
			if( bd == null ) return;
			
			if( bd.width != IMAGE_WIDTH || bd.height != IMAGE_HEIGHT )
			{
				var scaledBD:BitmapData = new BitmapData(IMAGE_WIDTH, IMAGE_HEIGHT, false, 0xFFFFFFFF);
				var matrix:Matrix = new Matrix();						
				matrix.scale(IMAGE_WIDTH / bd.width, IMAGE_HEIGHT/ bd.height);
				scaledBD.draw(bd, matrix);	
				image.bitmapData = scaledBD;
			}
			else
			{
				image.bitmapData = bd;
			}
		}
		
		
		private function setMaxLabel(label:String):String
		{
			if(label.length>12) {
				return label.toString().substr(0,12)+"...";
			} else {
				return label;
			}
		}
		
		
	}
}
