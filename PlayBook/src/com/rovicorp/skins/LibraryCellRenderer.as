package com.rovicorp.skins
{
	import com.rovicorp.model.DownloadListing;
	import com.rovicorp.utilities.Boxart;
	import com.rovicorp.utilities.DownloadUtil;
	import com.rovicorp.utilities.LogUtil;
	
	import mx.events.PropertyChangeEvent;
	
	import qnx.ui.display.Image;
	
	public class LibraryCellRenderer extends TitleCellRenderer
	{
		//show status icon
		private var status_icon:Image;
		
		public function LibraryCellRenderer()
		{
			super();
		}
		
		override protected function init():void 
		{
			super.init();
			
			if( status_icon == null )
			{
				status_icon = new Image();
				status_icon.width = 12;
				status_icon.height = 12;
				status_icon.x = (TitleCellRenderer.IMAGE_WIDTH - status_icon.width)/2;
				
				shadow.y += status_icon.height + status_icon.y -1;
				image.y = status_icon.height + status_icon.y + 3;
				title.y = image.y+TitleCellRenderer.IMAGE_HEIGHT+5;	
				
				this.addChild(status_icon);
			}
		}
		
		override public function set data( value:Object ):void
		{
			super.data = value;
			
			//whether in library list page?
			//data is TitleListingPurchased?
			if( data.hasOwnProperty("DownloadStatus") )
			{				
				updateStatusIcon();
			}
		}
		
		override protected function loadImage():void
		{
			if( data is DownloadListing )
			{				
				loaderUtil.addTask(index, Boxart.URL_107 + data.BoxartPrefix + Boxart.POST_FIX_107, showImage);
				return;
			}
			super.loadImage();
		}
		
		private var _source:String;
		
		private function updateStatusIcon():void
		{
			var newSource:String = com.rovicorp.utilities.DownloadUtil.getStatusIconURL(data.DownloadStatus);
			
			if( _source == newSource ) return;
			status_icon.setImage(newSource);			
			_source = newSource;
		}
		
		
		override protected function onPropertyChange(event:PropertyChangeEvent) : void
		{
			super.onPropertyChange(event);
			
			if ( event.property == "DownloadStatus")
				updateStatusIcon();
		}
	}
}