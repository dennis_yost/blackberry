package com.rovicorp.skins
{
	import com.rovicorp.utilities.Boxart;
	
	import flash.events.Event;
	
	import qnx.ui.display.Image;
	import qnx.ui.listClasses.CellRenderer;
	import qnx.ui.listClasses.List;
	import qnx.ui.text.Label;
	
	public class TitlesCategoryRenderer extends CellRenderer
	{
		public var title:Object;
		protected var _image:Image;
		protected var _titleLabel:Label;
		
		public function TitlesCategoryRenderer()
		{
			super();
			label.visible = false;
			createUI();
		}
		
		override public function set data(value:Object):void
		{
			title = value;
			if (value) {
				_image.setImage(com.rovicorp.utilities.Boxart.URL_107+value.BoxartPrefix+com.rovicorp.utilities.Boxart.POST_FIX_107);
				_titleLabel.text = title.Name;
			}
		}
		
		/**
		 * setSize() is called everytime the tiles size are changed
		 * this is where we add our method setlayout() to reposition/redraw
		 * various parts of the cell renderer.
		 */ 
		override public function setSize(w:Number, h:Number):void {
			super.setSize(w, h);
			if (stage) {
				setLayout();
			}
		} 
		
		/**
		 * 	Create all the cell renderer components we need to display titles
		 */
		private function createUI():void {
			_image = new Image();
			_titleLabel = new Label();
			_image.setPosition(10,20);
			_image.addEventListener(Event.COMPLETE, onComplete);
			_image.smoothing = true;
			addChild(_image);
			addChild(_titleLabel);
		}
		
		/**
		 * Reposition/redraw the renderer parts 
		 * everytime the tile size is changed
		 */ 
		private function setLayout():void {
			_titleLabel.y = height - 20;
			onComplete(new Event(Event.COMPLETE));
		}
		
		private function onComplete(e:Event):void {
			_image.setSize(77,107);
		}
	}
}