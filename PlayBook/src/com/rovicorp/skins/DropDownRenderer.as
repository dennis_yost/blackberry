package com.rovicorp.skins
{
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import qnx.ui.skins.SkinStates;
	import qnx.fuse.ui.listClasses.DropDownCellRenderer;
	
	public class DropDownRenderer extends DropDownCellRenderer
	{
		public function DropDownRenderer()
		{
			super();
			
			var format:TextFormat = new TextFormat();
			format.font = "MyriadProReg";
			format.size = 18;
			format.color = 0x000000;
			//format.align = TextFormatAlign.CENTER;
			
			var formatDown:TextFormat = new TextFormat();
			formatDown.font = "MyriadProReg";
			formatDown.size = 18;
			formatDown.color = 0xFFFFFF;
			//formatDown.align = TextFormatAlign.CENTER;
			
			var formatDisable:TextFormat = new TextFormat();
			formatDisable.font = "MyriadProReg";
			formatDisable.size = 18;
			formatDisable.color = 0xCCCCCC;
			//formatDisable.align = TextFormatAlign.CENTER;
			
			setTextFormatForState(formatDisable,SkinStates.DISABLED);
			setTextFormatForState(format,SkinStates.UP);
			setTextFormatForState(formatDown,SkinStates.DOWN);
			setTextFormatForState(format,SkinStates.SELECTED);
		}
	}
}