package com.rovicorp.skins
{
	import spark.skins.mobile.VScrollBarSkin;
	
	public class VScrollBarSkin extends spark.skins.mobile.VScrollBarSkin
	{
		public function VScrollBarSkin()
		{
			super();

			thumbSkinClass = com.rovicorp.skins.VScrollBarThumbSkin;
		}
	}
}