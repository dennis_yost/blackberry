package com.rovicorp.service.download
{
	import net.rim.aircommons.download.DownloadManagerFacade;
	import net.rim.aircommons.download.taskmanager.events.DownloadEvent;
	//import net.rim.aircommons.download.taskmanager.wakeup.WakeupPolicy;
	
	public class VideoDownloadManager extends DownloadManagerFacade
	{
		public function VideoDownloadManager()
		{
			super();
			
			//Fix className conflict
			com.rovicorp.service.download.DownloadEvent.DOWNLOAD_ERROR = net.rim.aircommons.download.taskmanager.events.DownloadEvent.DOWNLOAD_ERROR;
		}
		/*
		TODO - default wakeup policy will check WIFI connect
		override public function getWakeupPolicy():WakeupPolicy
		{
			return new VideoWakeupPolicy();
		}
		*/
	}
}