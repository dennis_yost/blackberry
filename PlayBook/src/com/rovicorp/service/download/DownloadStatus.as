package com.rovicorp.service.download
{
	public class DownloadStatus
	{
	
		private static const StatusMap:Array = [
			DownloadStatus.TASK_UNAVAILABLE,
			DownloadStatus.TASK_INITIALIZED, 	// STATUS_INITIALIZED(1)
			DownloadStatus.TASK_IN_PROGRESS, 	// STATUS_IN_PROGRESS(2)
			DownloadStatus.TASK_COMPLETE, 		// STATUS_SUCCEEDED(3)
			DownloadStatus.TASK_ERROR, 			// STATUS_FAILED(4)
			DownloadStatus.TASK_UNAVAILABLE, 	// STATUS_CANCELLED(5)
			DownloadStatus.TASK_PAUSED, 		// STATUS_PAUSED(6)
			DownloadStatus.TASK_PENDING, 		// STATUS_PENDING(7)
			DownloadStatus.TASK_PENDING, 		// STATUS_AVAILABLE(8)
		];
		
		//not in download queue
		public static const TASK_UNAVAILABLE:int = 0;		
		
		public static const TASK_INITIALIZED:uint = 1;
		
		public static const TASK_IN_PROGRESS:uint = 2;
		
		public static const TASK_READY_WATCH:uint = 3;
		
		public static const TASK_PAUSED:uint = 4;
		
		public static const TASK_RESUMED:uint = 5;
		
		public static const TASK_PENDING:uint = 6;
		
		public static const TASK_COMPLETE:uint = 7;
		
		public static const TASK_ERROR:uint = 8;
		
		//waiting for pollForDownloads result, download task not started right now, just add a fake task
		public static const TASK_WAITING:uint = 9;
		
		public static function fromTaskStatus(status:int):int
		{
			return StatusMap[status];
		}
		
		/*
		0: Request Acknowledged *
		1: Download Initialized *
		2: Download Started
		3: Ready to watch
		4: Download paused
		5: Download resumed
		6: Download reset
		7: Download completed*
		*/
		
	}
}