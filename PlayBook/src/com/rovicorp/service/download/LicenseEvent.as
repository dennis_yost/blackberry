package com.rovicorp.service.download
{
	import flash.events.Event;
	
	public class LicenseEvent extends Event
	{
		public static const LICENSE_EVENT:String = "license_event";
		
		public var id:String;
		public var status:int;
		
		public function LicenseEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return createEvent(id, status, bubbles, cancelable);
		}
		
		static public function createEvent(id:String, status:int, bubbles:Boolean=false, cancelable:Boolean=false):LicenseEvent
		{
			var event:LicenseEvent = new LicenseEvent(LICENSE_EVENT, bubbles, cancelable);
			
			event.id = id;
			event.status = status;
			
			return event;
		}
	}
}