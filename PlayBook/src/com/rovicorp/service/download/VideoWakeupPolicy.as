package com.rovicorp.service.download
{
	import net.rim.aircommons.download.taskmanager.wakeup.WakeupPolicy;
	import net.rim.aircommons.download.taskmanager.wakeup.WiFiCondition;
	import net.rim.aircommons.download.taskmanager.wakeup.WiFiTrigger;

	public class VideoWakeupPolicy extends WakeupPolicy
	{
		private var wifiTrigger:WiFiTrigger;
		private var wifiCondition:WiFiCondition;
			
		public function VideoWakeupPolicy()
		{    
			wifiTrigger = new WiFiTrigger(this);
			wifiCondition = new WiFiCondition();
			this.addWakeupCondition(wifiCondition, false);             
			this.addWakeupTrigger(wifiTrigger, false);
		}
	}
}