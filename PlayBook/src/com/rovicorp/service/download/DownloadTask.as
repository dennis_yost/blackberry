package com.rovicorp.service.download
{
	import flash.events.Event;
	
	import net.rim.aircommons.download.taskmanager.FileTransferTask;
	import net.rim.aircommons.download.taskmanager.Task;
	import net.rim.aircommons.download.taskmanager.utils.ProgressHandle;
	
	import qnx.system.QNXApplication;
	import qnx.system.QNXSystem;
	import qnx.system.QNXSystemPowerMode;
	
	public class DownloadTask extends FileTransferTask
	{
		public static const TYPE_DOWNLOAD_TASK:int = 2;
		
		private static var _downloadInProgress:Boolean;
		private static var _previousPowerMode:String;
		
		
		// static
		{
			CONFIG::device
			{
				QNXApplication.qnxApplication.addEventListener(Event.ACTIVATE, onSystemStateChanged);
				QNXApplication.qnxApplication.addEventListener(Event.DEACTIVATE, onSystemStateChanged);
			}
		}
		
		public function DownloadTask(id:String=null, remoteFileURL:String=null, localFileURL:String=null, download:Boolean=false)
		{
			super(id, remoteFileURL, localFileURL, download);			
		}
		
		private static function onSystemStateChanged(event:Event):void 
		{
			// Maintain the system active while we have pending downloads 
			_previousPowerMode = QNXSystem.system.powerMode;

			if (_downloadInProgress && _previousPowerMode == QNXSystemPowerMode.STANDBY) 
				QNXSystem.system.powerMode = QNXSystemPowerMode.THROTTLED;
		}
		
		public override function preRun():void
		{
			super.preRun()
			
			CONFIG::device
			{
				if (QNXSystem.system.powerMode == QNXSystemPowerMode.STANDBY) 
				{
					_previousPowerMode = QNXSystem.system.powerMode;
					_downloadInProgress = true;

					QNXSystem.system.powerMode = QNXSystemPowerMode.THROTTLED;
				}
			}
		}
		
		public override function isRunnableState():Boolean 
		{
			return (getStatus() == Task.STATUS_PENDING || getStatus() == Task.STATUS_IN_PROGRESS);
		}
		
		public override function setStatusAndCommit(status:int, commit:Boolean, fireEvent:Boolean):void 
		{
			super.setStatusAndCommit(status, commit, fireEvent);
			
			// Restore the STANDBY state
			if (status == STATUS_SUCCEEDED && _previousPowerMode) 
			{
				CONFIG::device { QNXSystem.system.powerMode = _previousPowerMode; }
				
				_previousPowerMode = null;
				_downloadInProgress = false;
				
			} 
			else 
				_downloadInProgress = super.isRunnableState();
		}
		
		override public function getTaskType() : int
		{
			return DownloadTask.TYPE_DOWNLOAD_TASK;
		}
		
		override public function get useTempFile():Boolean
		{			
			return false;
		}
		
		override public function getTmpFileURL() : String
		{
			return this.getLocalFileURL();			
		}
		
		override public function cleanup():void 
		{ 			
			if(!isCorrupt()) 
			{ 
				super.cleanup(); 
			} 
		}  
		
		override public function setCurrentOffset(currentOffset:Number):void
		{
			super.setCurrentOffset(currentOffset);
			
			if( getCurrentOffset() == getContentLength() && getContentLength() > 0 )
			{
				setStatus(Task.STATUS_SUCCEEDED);
			}
		}
		
		public function getDownloadProgress():Number
		{
			var handle:ProgressHandle = getProgressHandle();
			
			if( handle.getLength() > 0 )
			{
				return handle.getPosition()/handle.getLength();
			}
			return 0;			
		}
	}
}