package com.rovicorp.service.download
{		
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.events.DeviceEvent;
	import com.rovicorp.model.DownloadList;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.utilities.LogUtil;
	import com.sonic.response.Library.FullTitlePurchased;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	
	import net.rim.aircommons.download.DownloadManagerFacade;
	import net.rim.aircommons.download.taskmanager.FileTransferError;
	import net.rim.aircommons.download.taskmanager.FileTransferTask;
	import net.rim.aircommons.download.taskmanager.GlobalConstants;
	import net.rim.aircommons.download.taskmanager.Task;
	import net.rim.aircommons.download.taskmanager.TaskError;
	import net.rim.aircommons.download.taskmanager.TaskQueue;
	import net.rim.aircommons.download.taskmanager.ds.TaskSerializer;
	import net.rim.aircommons.download.taskmanager.events.TaskChangeEvent;
	import net.rim.aircommons.download.taskmanager.events.TaskProgressEvent;
	import net.rim.aircommons.download.taskmanager.utils.ProgressHandle;
	
 
	/**
	 * 
	 * Manage all download behavior: add new download task, listen task status and progress event...
	 * 
	 * We should create a singleton instance of this class
	 * 
	 * @author yanlin_qiu
	 * 
	 */	
	public class DownloadManager extends EventDispatcher
	{
		private static var instance:DownloadManager;
		
		public static function getInstance():DownloadManager
		{
			if( instance == null )
			{
				instance = new DownloadManager();
			}
			return instance;
		}
		
		//----------------------------------------------------------------------
		
		private var _mgr:DownloadManagerFacade;	
		private var _currentTask:Task;
				
		public function DownloadManager()
		{				
			if( instance != null )
			{
				throw new Error("You have to access the instance via getInstance.");
			}
			
			//Add download task to availables_types
			TaskSerializer.AVAILABLE_TYPES[DownloadTask.TYPE_DOWNLOAD_TASK] = DownloadTask;			
			addEventListener(LicenseEvent.LICENSE_EVENT, licenseEvent, false, 0, true);
			
			EventCenter.inst.addEventListener(DeviceEvent.NETWORK_AVAILABILITY_CHANGE, onNetworkChanged);
			
			_mgr = new VideoDownloadManager();		

			loadExistTasks();
		}
		
		private function get currentTask():Task
		{
			return _currentTask;
		}
		
		private function set currentTask(value:Task):void
		{
			if (_currentTask != value)
			{
				if (_currentTask)
					EventCenter.inst.removeEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, getLibraryInfoCompleted);
			
				_currentTask = value;
			
				if (value)
				{
					EventCenter.inst.addEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, getLibraryInfoCompleted, false, 0, true);
					EventCenter.inst.dispatchEvent(new DatabaseEvent( DatabaseEvent.GET_LIBRARY_INFO, value.getId() ));
				}
			}
		}
		
		private function onNetworkChanged(e:DeviceEvent):void
		{
			if( e.networkAvailable )
			{
				var id:String ="";
				if( _currentTask != null )
				{
					id = _currentTask.getId()
				}
				_hasCheckAutoStarted = false;
				autoStart(id);
			}
			else
			{
				pauseAllDownload();
			}
		}
		
		private function checkTaskLicenseStatus(value:Task):void
		{
			if (_currentTask != value)
			{
				if (_currentTask)
					EventCenter.inst.removeEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, getLibraryInfoCompleted);
				
				_currentTask = value;
			}
				
			if (value)
			{
				EventCenter.inst.addEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, getLibraryInfoCompleted, false, 0, true);
				EventCenter.inst.dispatchEvent(new DatabaseEvent( DatabaseEvent.GET_LIBRARY_INFO, value.getId() ));
			}
		}
		
		private function loadExistTasks():void
		{
			var tasks:Array = _mgr.getAllTasks();
			var task:FileTransferTask;			
			
			var items:Array = new Array();			
			
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as FileTransferTask;
				
				LogUtil.debug("task -> " + task.getId() +", "+ Task.getStatusDescription(task.getStatus()));
				if( task == null) continue;
				
				if( task.getStatus() == Task.STATUS_FAILED )
				{
					LogUtil.debug("task error -> " + task.getError());
				}
	
				if( task.validateTaskComplete() == false )
				{
					// Add listener to be notified of download task state changes 
					task.addEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);			
					
					// Add listener to be notified of download progress updates
					task.addEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
					
					task.addEventListener(DownloadEvent.DOWNLOAD_ERROR, onDownloadError);
					
					//Fix an issue that reset task's status when file already exist
					if( task.getContentLength() >0 && task.getCurrentOffset() == task.getContentLength() )
					{
						task.setStatus(Task.STATUS_SUCCEEDED);
					}
				}
				else
				{
					var file:File = new File(task.getLocalFileURL());
					
					if (!file.exists)
						_mgr.removeTask(task, true);
				}
			}
		}
		
		private var _hasCheckAutoStarted:Boolean = false;
		
		public function autoStart(lastTaskId:String):void
		{
			if( _hasCheckAutoStarted == true ) return;
			
			_hasCheckAutoStarted = true;
			
			if( lastTaskId )
			{
				var task:Task = getDownloadTaskById(lastTaskId);
				
				if( task && task.getStatus() == Task.STATUS_PAUSED )
				{
					_mgr.resumeTask(task);
					return;
				}
			}
			
			startTaskAtIdle();
		}
		
		/**
		 * start downloading if no task is started automatically
		 * 
		 * find the first "paused" one in the queue and resume it.
		 */		
		private function startTaskAtIdle():void
		{
			
			var item:Task = getRunningTask();	
			LogUtil.debug("startTaskAtIdle -- running " + item);
			if( item != null ) return;
			
			item = findNextHighestTask();
			LogUtil.debug("startTaskAtIdle--" + item);
			
			if( item != null ) 
			{				
				try
				{
					_mgr.resumeTask(item);
				}
				catch( e:Error)
				{
					
				}
			}
		}
		
		/**
		 * Get the running task
		 */ 
		public function getRunningTask():Task
		{
			var tasks:Array = _mgr.getAllTasks();
			var task:FileTransferTask;			
			
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as FileTransferTask;
				
				if( task == null) continue;
				
				if( task.getStatus() == Task.STATUS_IN_PROGRESS )
				{
					return task;
				}
			}	
			return null;
		}
		
		/**
		 * resume all download tasks 
		 */
		/*
		public function resumeAllDownload():void
		{
			var item:DownloadItem;
			
			for( var i:int = 0,len:int = downloadItems.length;i<len;i++)
			{
				item = downloadItems[i] as DownloadItem;
				if( item.task != null)
				{
					item.resume();
				}
			}	
		}*/
		
		/**
		 * pause any running task if exist 
		 */			
		private function pauseAllDownload():void
		{
			var tasks:Array = _mgr.getAllTasks();
			var task:FileTransferTask;			
						
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as FileTransferTask;
				
				if( task == null) continue;
				
				if( task.getStatus() == Task.STATUS_IN_PROGRESS )
				{
					_mgr.pauseTask(task);
				}
			}
		}		
		
		private function findNextHighestTask():Task
		{
			
			var tasks:Array = _mgr.getAllTasks();
			var task:FileTransferTask;			
			
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as FileTransferTask;
				
				if( task == null) continue;
				
				var status:int = task.getStatus();
				if( status == Task.STATUS_PAUSED )
				//if( status != Task.STATUS_CANCELLED || status != Task.STATUS_FAILED || status != Task.STATUS_SUCCEEDED)
				{
					return task;
				}
			}
			return null;
		}
		
		/**
		 * add a new DownloadItem, and start download it now
		 * 
		 * @param contentID
		 * @param remoteURL
		 * @param saveLocation
		 * @param itemInfo		Content information
		 * @param customData	customData information
		 * @return 
		 * 
		 */		
		public function addDownloadTask( contentID:String, remoteURL:String, saveLocation:String, customData:String = "" ):DownloadTask
		{			
			var task:DownloadTask = new DownloadTask(contentID, remoteURL, saveLocation, true );
			if( customData != "" ) task.setCustomData(customData);
			
			LogUtil.debug("addDownloadTask -> " + contentID );
			
			var item:Task = this.getDownloadTaskById(contentID);			
			
			if( item != null )
			{
				LogUtil.debug("id already exist");
				return null;
			}			
			// Add listener to be notified of download task state changes 
			// Add listener to be notified of download progress updates
			task.addEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);			
			task.addEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
			task.addEventListener(DownloadEvent.DOWNLOAD_ERROR, onDownloadError);
			_mgr.addTask(task);
			
			//Only downloading one file at once
			//startDownloadItem(item);			
			
			return task;
		}
		
		/**
		 *
		 * Find downloadItem by ID 
		 * 
		 * @param id
		 * @return DownloadItem
		 * 
		 */		
		public function getDownloadTaskById( id:String ):DownloadTask
		{
			/*
			var tasks:Array = _mgr.getAllTasks();
			var task:DownloadTask;			
			
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i] as DownloadTask;
				
				if( task == null) continue;
				
				if( task.getId() == id )
				{
					return task;
				}
			}				
			return null;
			*/
			
			return TaskQueue.getInstance().getTaskById(id) as DownloadTask;
		}

		public function getAllTasks():Array
		{
			return _mgr.getAllTasks();
		}

		public function startDownloadItemByID(id:String):void
		{
			var item:Task = getDownloadTaskById(id);
			if( item != null )
			{
				_mgr.restartTask(item);				
			}	
		}
		
		public function restartDownloadItemByID(id:String):void
		{
			var item:Task = getDownloadTaskById(id);
			if( item != null )
			{	
				//pause running task
				//pauseAllDownload();
				
				item.setStatus(Task.STATUS_PENDING);
				//_mgr.restartTask(item);
				_mgr.getWakeupPolicy().checkConditions();
			}	
		}
		
		/**
		 * 
		 * @param id
		 * 
		 */		
		public function pauseDownloadItemByID( id:String ):void
		{
			var item:Task = getDownloadTaskById(id);			
			if( item != null && item.isRunnableState() )
			{
				_mgr.pauseTask(item);
			}
		}
		
		/**
		 * 
		 * @param id
		 * 
		 */		
		public function resumeDownloadItemByID( id:String ):void
		{
			var item:Task = getDownloadTaskById(id);
			var handler:ProgressHandle 
			
			if( item != null && item.getStatus() == Task.STATUS_PAUSED )
			{
				//pause running task
				pauseAllDownload();
				
				if( checkDiskSpace( item.getContentLength() - item.getCurrentOffset() ) == false )
				{
					var e:DownloadEvent = new DownloadEvent(DownloadEvent.DOWNLOAD_ERROR);
					e.status = GlobalConstants.INSUFFICIENT_DISKSPACE;
					dispatchEvent(e);		
					return;
				}
				
				_mgr.resumeTask(item);
			}			
		}
		
		/**
		 * remove an exsit DownloadItem and downloaded file
		 * 
		 * @param item			 DownloadItem
		 * @param removeTmpFile  whether remove download temporary file
		 */		
		public function removeDownloadItemByID( id:String, clearUp:Boolean = true ):Boolean
		{			
			var task:Task = getDownloadTaskById(id);
			
			if( task == null ) return false;
			
			task.removeEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);		
			task.removeEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
			task.removeEventListener(DownloadEvent.DOWNLOAD_ERROR, onDownloadError);
			
			var evt:DownloadEvent = new DownloadEvent(DownloadEvent.TASK_STATUS_CHANGED);
			
			evt.id = task.getId();
			evt.dqid = int(task.getCustomData());
			evt.status = DownloadStatus.TASK_UNAVAILABLE;
			dispatchEvent(evt);
			
			_mgr.removeTask(task, clearUp);
			task = null;
						
			return true;
		}
		
		/**
		 * remove all download task
		 * 
		 * @param removeTmpFile Boolean
		 * 
		 */		
		public function removeAllDownloadItem(clearUp:Boolean = true):void
		{
			var tasks:Array = _mgr.getAllTasks();
			var task:Task;
			for( var i:uint = 0,len:uint = tasks.length;i<len;i++)
			{
				task = tasks[i] as Task;
				task.removeEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);				
				task.removeEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
				task = null;
			}
			
			_mgr.removeAllTasks(clearUp);
		}	
		
		private function onDownloadError(event:Object):void
		{
			var e:DownloadEvent = new DownloadEvent(DownloadEvent.DOWNLOAD_ERROR);
			if( event.getError() is FileTransferError )
			{
				e.status = (event.getError() as FileTransferError).getCause();
			}
			
			dispatchEvent(e);		
		}
		
		private function getLibraryInfoCompleted(event:DatabaseEvent):void
		{
			if (event.success)
			{
				// Check for license
				if (currentTask && currentTask.getId() == event.result[0]._id)
				{
					EventCenter.inst.removeEventListener(DatabaseEvent.GET_LIBRARY_INFO_DONE, getLibraryInfoCompleted);

					CONFIG::device
					{
						if (event.result[0].license_receieved != 1)
							new LicenseRequest(event.result[0], currentTask as FileTransferTask);
					}
				}
			}
		}

		private function taskProgressChanged(event:TaskProgressEvent):void 
		{
			var task:Task = event.task;
			var handle:ProgressHandle = task.getProgressHandle();	
			
			if( checkDiskSpace( handle.getLength()- handle.getPosition() ) == false )
			{
				pauseAllDownload();
				
				var e:DownloadEvent = new DownloadEvent(DownloadEvent.DOWNLOAD_ERROR);
				e.status = GlobalConstants.INSUFFICIENT_DISKSPACE;
				dispatchEvent(e);						
				return;
			}
			
			var evt:DownloadEvent = new DownloadEvent(DownloadEvent.TASK_PROGRESS);
			
			evt.id = task.getId();
			evt.dqid = int(task.getCustomData());
			evt.bytesTotal = handle.getLength();
			evt.bytesDownloaded = handle.getPosition();
			evt.timeElapsed = handle.getElapsedTime();
			evt.timeRemaining = handle.getEstimatedTimeRemaining();
			evt.status = DownloadStatus.fromTaskStatus(task.getStatus());
			
			dispatchEvent(evt);
		}
		
		private function checkDiskSpace( requiredSize:int ) : Boolean
		{			
			var leftSize:Number = File.applicationDirectory.spaceAvailable;		
			
			if( requiredSize == 0 ) requiredSize = MAX_SPACE;
			
			if (requiredSize >= leftSize)
			{
				return false;
			}
			return true;
		}
		
		private const MAX_SPACE:int = 1024 * 1024 * 20;
		
		private function taskStatusChanged(event:TaskChangeEvent):void 
		{
			
			var task:Task = event.task;
			
			var status:int = event.status;
			
			//LogUtil.debug("status change", task.getId(), status);
			if( status != Task.STATUS_SUCCEEDED )
			{
				//fix an issue that we should reset status when file already exists
				if(  task.getContentLength() >0 && task.getCurrentOffset() == task.getContentLength() )
				{
					task.setStatus(Task.STATUS_SUCCEEDED);
				}
			}
			else
			{
				//Try to start next top task
				startTaskAtIdle();
			}
			if( status == Task.STATUS_IN_PROGRESS )
			{	
				if( checkDiskSpace( task.getContentLength() - task.getCurrentOffset() ) == false )
				{
					pauseAllDownload();
					
					var e:DownloadEvent = new DownloadEvent(DownloadEvent.DOWNLOAD_ERROR);
					e.status = GlobalConstants.INSUFFICIENT_DISKSPACE;
					dispatchEvent(e);						
					return;
				}

				currentTask = event.task;
			}
			else if ( status == Task.STATUS_SUCCEEDED || status == Task.STATUS_AVAILABLE )
			{
				checkTaskLicenseStatus(event.task);
			}
			else if ( status == Task.STATUS_FAILED )
			{
				var error:TaskError = event.task.getError() as TaskError;
				if( error )
				{
					LogUtil.debug("download task failed: "+ error  + ":"+ error.getCause() + ":" + task.getSerializedError() );
				}
				else
				{
					LogUtil.debug("download task failed: "+ error );
				}
			}
			else if (currentTask == event.task)
				currentTask = null;
			
			var evt:DownloadEvent = new DownloadEvent(DownloadEvent.TASK_STATUS_CHANGED);
			var handle:ProgressHandle = task.getProgressHandle();
			
			evt.id = task.getId();
			evt.dqid = int(task.getCustomData());
			evt.bytesTotal = handle.getLength();
			evt.bytesDownloaded = handle.getPosition();
			evt.timeElapsed = handle.getElapsedTime();
			evt.timeRemaining = handle.getEstimatedTimeRemaining();
			evt.status = DownloadStatus.fromTaskStatus(task.getStatus());
			
			dispatchEvent(evt);
		
		}
		
		private function licenseEvent(event:LicenseEvent):void
		{
			if (event.status == LicenseStatus.LICENSE_RECEIVED)
			{
				var e:DatabaseEvent = new DatabaseEvent(DatabaseEvent.UPDATE_LIBRARY_INFO);

				e.id = event.id;
				e.library = { _id:event.id, license_receieved:1};
				EventCenter.inst.dispatchEvent(e);
			}
			else if (event.status == LicenseStatus.LICENSE_ERROR)
			{
				pauseDownloadItemByID(event.id);
			}
		}
	}
}