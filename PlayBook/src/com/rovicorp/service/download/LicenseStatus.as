package com.rovicorp.service.download
{
	public class LicenseStatus
	{
		public static const LICENSE_REQUESTED:uint = 1;
		public static const LICENSE_RECEIVED:uint = 2;
		public static const LICENSE_ERROR:uint = 3;
	}
}