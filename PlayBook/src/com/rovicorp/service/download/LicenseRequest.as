package com.rovicorp.service.download
{
	import com.adobe.utils.StringUtil;
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.utilities.LogUtil;
	
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	import flash.utils.Timer;
	import flash.utils.setInterval;
	
	import net.rim.aircommons.download.taskmanager.FileTransferTask;
	import net.rim.aircommons.download.taskmanager.events.TaskChangeEvent;
	import net.rim.aircommons.download.taskmanager.events.TaskProgressEvent;
	
	import qnx.drm.MSPlayReady;
	import qnx.drm.event.MSPlayReadyEvent;

	public class LicenseRequest
	{
		private var info:Object;
		private var task:FileTransferTask;
		private var request:MSPlayReady;
		private var retryCount:int = 0;
	
		static private var requests:Array;
		static private var current:LicenseRequest;
		
		static private const DRM_E_FAIL:uint = 0x80004005;
		static private const MAX_RETRY_COUNT:int = 3;
		static private const LOCAL_LOG:Boolean = false;

		static private var logFS:FileStream;
		
		public function LicenseRequest(info:Object, task:FileTransferTask)
		{
			this.info = info;
			this.task = task;

			var file:File = new File(task.getLocalFileURL());
			
			if (file.size < 2048)
			{
				task.addEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);
				task.addEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
			}
			else 
				requestLicense();
		}
		
		private function log(...paras):void
		{
			LogUtil.debug(paras);
			
			if (LOCAL_LOG)
			{
				try
				{
					if (logFS == null)
					{
						var file:File = new File("/accounts/1000/shared/videos/videostore.log");
						
						logFS = new FileStream();
						logFS.open(file, FileMode.WRITE);
					}
					
					var now:Date = new Date();
					logFS.writeUTFBytes("[" + now.toTimeString() + "] " + paras.toString() + "\r\n");
				}
				catch (e:Error)
				{
				}
			}
		}
		
		private function requestLicense(event:TimerEvent = null):void
		{
			if (current != null)
			{
				if (requests == null)
					requests = [];

				requests.push(this);
				return;	
			}
			
			current = this;
			
			var file:File = new File(task.getLocalFileURL());
			log("License Request: " + task.getLocalFileURL());
			
			var fs:FileStream = new FileStream();
			
			fs.endian = Endian.LITTLE_ENDIAN; 
			fs.open(file, FileMode.READ);
			fs.position = 208;
			
			var length:int = fs.readShort();
			var contents:ByteArray = new ByteArray();
			fs.readBytes(contents, 0, length);

			var xml:XML = new XML(contents.readMultiByte(length, "unicode"));
			var ns:Namespace = xml.namespace();
			
			var header:String = 
				'<WRMHEADER xmlns="http://schemas.microsoft.com/DRM/2007/03/PlayReadyHeader" version="4.0.0.0">' +
					'<DATA>' +
						'<PROTECTINFO>' +
							'<KEYLEN>16</KEYLEN>' +
							'<ALGID>AESCTR</ALGID>' +
						'</PROTECTINFO>' +
						'<KID>' + xml.ns::DATA.ns::KID + '</KID>' +
						'<CHECKSUM>' + xml.ns::DATA.ns::CHECKSUM + '</CHECKSUM>' +
						'<LA_URL>' + info.license_url + '</LA_URL>' +
					'</DATA>'+
				'</WRMHEADER>';
			
			log("License Content: " + header);
			
			contents = new ByteArray();
			contents.endian = Endian.LITTLE_ENDIAN;
			contents.writeInt((header.length * 2) + 10);
			contents.writeShort(1);
			contents.writeShort(1);
			contents.writeShort(header.length * 2);
			contents.writeMultiByte(header, "unicode");
			contents.position = 0;

			var customData:ByteArray = new ByteArray();
			customData.writeUTFBytes(info.custom_data);
			customData.position = 0;
			
			try 
			{
				DownloadManager.getInstance().dispatchEvent(LicenseEvent.createEvent(task.getId(), LicenseStatus.LICENSE_REQUESTED));

				request = new MSPlayReady();
				request.addEventListener(MSPlayReadyEvent.LICENSE, licenseEvent);
				
				log("License: contentSetProperty");
				request.contentSetProperty(contents);
				
				var rights:Vector.<String> = Vector.<String>(["Play"]);

				log("License: acquireLicense(" + rights + ", " + info.custom_data);
				request.acquireLicense(rights, customData);
			}
			catch (e:Error)
			{
				log("Error:" + e);
				DownloadManager.getInstance().dispatchEvent(LicenseEvent.createEvent(task.getId(), LicenseStatus.LICENSE_ERROR));
				current = null;
				
				if (requests && requests.length)
				{
					current = requests.shift();
					current.requestLicense();
				}
			}
		}
		
		private function taskStatusChanged(event:TaskChangeEvent):void
		{
			task.removeEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);
			task.removeEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
		}
		
		private function taskProgressChanged(event:TaskProgressEvent):void
		{
			var file:File = new File(task.getLocalFileURL());
			
			if (file.size > 2048)
			{
				task.removeEventListener(TaskChangeEvent.TASK_STATUS_CHANGED, taskStatusChanged);
				task.removeEventListener(TaskProgressEvent.TASK_PROGRESS_CHANGED, taskProgressChanged);
				requestLicense();
			}
		}
		
		private function licenseEvent(event:MSPlayReadyEvent):void
		{
			log("License Event:" + event);
			
			request.removeEventListener(MSPlayReadyEvent.LICENSE, licenseEvent);
			request.dispose();
			request = null;
			
			if (event.resultCode == 0)
			{
				DownloadManager.getInstance().dispatchEvent(LicenseEvent.createEvent(task.getId(), LicenseStatus.LICENSE_RECEIVED));
			}
/*			else if (++retryCount < MAX_RETRY_COUNT)
			{
				log("Retrying license acquistion (attempt " + (retryCount + 1) + " of " + MAX_RETRY_COUNT + ")");
				requestLicense();
				var timer:Timer = new Timer(100);
				timer.addEventListener(TimerEvent.TIMER, requestLicense);
				timer.repeatCount = 1;
				timer.start();			
			} */
			else
				DownloadManager.getInstance().dispatchEvent(LicenseEvent.createEvent(task.getId(), LicenseStatus.LICENSE_ERROR));
			
			current = null;
			
			if (requests && requests.length)
			{
				current = requests.shift();
				current.requestLicense();
			}
		}
	}
}