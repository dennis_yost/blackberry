package com.rovicorp.service.download
{
	import flash.events.Event;
	
	public class DownloadEvent extends Event
	{
		public static const TASK_PROGRESS:String = "task_progress";
		public static const TASK_STATUS_CHANGED:String = "task_status_changed";	
		public static var DOWNLOAD_ERROR:String = "DOWNLOAD_ERROR";
		
		public static const ADD_FAKE_TASK:String = "add_fake_task";
		
		public static const PLAY_POSITION_CHANGED:String = "PLAY_POSITION_CHANGED";
		
		public var id:String;
		public var dqid:int;
		public var bytesTotal:uint;
		public var bytesDownloaded:uint;
		public var timeElapsed:int;
		public var timeRemaining:int;
		public var status:int;
		
		public var playPosition:int;
		
		public function DownloadEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			var result:DownloadEvent = new DownloadEvent(type, bubbles, cancelable);
			result.bytesTotal = bytesTotal;
			result.bytesDownloaded = bytesDownloaded;
			result.timeElapsed = timeElapsed;
			result.timeRemaining = timeRemaining;
			result.status = status;
			
			return result;
		}
	}
}