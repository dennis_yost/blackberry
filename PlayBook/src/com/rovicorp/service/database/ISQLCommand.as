package com.rovicorp.service.database
{
	import flash.utils.Dictionary;

	public interface ISQLCommand
	{
		function run( queryResponder:QueryResponder = null, params:Object = null ):void
			
		function get name():String
		
		function set name( value:String ):void
	}
}