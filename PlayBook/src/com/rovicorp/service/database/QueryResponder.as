package com.rovicorp.service.database
{			
	public class QueryResponder
	{
		
		public function QueryResponder( result:Function, fault:Function ) 
		{
			_resultHandler = result;
			_faultHandler = fault;
		}
		
		public function result(data:Object):void
		{
			if ( _resultHandler != null )
				_resultHandler(data);
		}
		
		/**
		 *  This method is called by a service when an error has been received.
		 *
		 */
		public function fault(info:Object):void
		{
			if ( _faultHandler != null )
				_faultHandler(info);
		}
		
		/**
		 *  @private
		 */
		private var _resultHandler:Function;
		
		/**
		 *  @private
		 */
		private var _faultHandler:Function;
		
	}
		
}