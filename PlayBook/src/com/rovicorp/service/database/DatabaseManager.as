package com.rovicorp.service.database
{	
	import com.adobe.utils.StringUtil;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.Responder;
	import flash.utils.Dictionary;
	
	/**
	 * Manage media content database 
	 */ 
	public class DatabaseManager
	{
		// Tables
		public static const TABLE_LIBRARY:String = "Library";
		public static const TABLE_TITLE:String = "Titles";
		public static const TABLE_FILE_CACHE:String = "FileCache";
		
		private var conn:SQLConnection;
		private var opened:Boolean = false;
		private var dbPath:String = "";
		
		private var successCB:Function;
		private var faultCB:Function;
		private var updateItem:Object;
		private var targetId:String;
		
		public function DatabaseManager(dbPath:String)
		{
			this.dbPath = dbPath;
		}
		
		/**
		 * Check if the database if opened.
		 * 
		 * @return Yes or No
		 */
		public function get isDatabaseReady():Boolean
		{
			return opened;
		}
		
		/**
		 * Open the database.
		 * 
		 * @param success Callback function when open db successfully.
		 * @param fault Callback function when open db failed.
		 */
		public function openDB(success:Function = null, fault:Function = null):void
		{
			if (dbPath == "" || opened == true) return;
			
			successCB = success;
			faultCB = fault;
			
			conn = new SQLConnection();
			conn.addEventListener(SQLEvent.OPEN, onDBOpenHandler, false, 0, true);
			conn.addEventListener(SQLErrorEvent.ERROR, onDBOpenError, false, 0, true);
			conn.openAsync(new File(dbPath), SQLMode.CREATE);				
		}

		public function createFromSQL(path:String, success:Function = null, fault:Function = null):void
		{
			conn = new SQLConnection();
			conn.open(new File(dbPath), SQLMode.CREATE);				
			
			var file:File = File.applicationDirectory.resolvePath(path);
			var fs:FileStream = new FileStream();
			fs.open(file, FileMode.READ);

			var buffer:String = fs.readUTFBytes(fs.bytesAvailable);
			var commands:Array = buffer.split(";");
			
			conn.begin();
			
			for each (var command:String in commands)
				execSQL(StringUtil.trim(command));

			conn.commit(/*new Responder(success, fault)*/);
			openDB(success, fault);
		}
		
		private function onDBOpenHandler(e:SQLEvent):void
		{
			conn.removeEventListener(SQLEvent.OPEN, onDBOpenHandler);
			conn.removeEventListener(SQLErrorEvent.ERROR, onDBOpenError);
			
			opened = true;			
			if (successCB != null) successCB();
		}
		
		private function onDBOpenError(e:SQLErrorEvent):void
		{
			conn.removeEventListener(SQLEvent.OPEN, onDBOpenHandler);
			conn.removeEventListener(SQLErrorEvent.ERROR, onDBOpenError);
			
			if (faultCB != null) faultCB();
		}
		
		/**
		 * Execute one sql query
		 * 
		 * @param sqlQuery the sql language string
		 * @param success success callback function
		 * @param fault fault callback function
		 * @param params parameters send with the sql string
		 */		 
		public function execSQL( sqlQuery:String,
								 success:Function = null,
								 fault:Function = null,
								 params:Object = null ):void
		{
			if (sqlQuery.length)
			{
				var sqlCommand:SQLQuery = new SQLQuery(sqlQuery, conn);
				sqlCommand.run(new QueryResponder(success, fault), params);
			}
		}
		
		/**
		 * Add a new title with the given information
		 * 
		 * @param item an object with all information
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function addTitle(item:Object, success:Function = null, fault:Function = null):void 
		{
			var sql:String = "INSERT INTO " + TABLE_TITLE
				+ " (t_id, title_name, box_art_prefix, mpaa_rating, actors, buy_price, copyright, directors, producers, rating_reason, release_year, rent_price, season_title_id, show_title_id, synopsys, buy_avail, rent_avail, writers, title_type, is_thx_media_director_enabled, hd, similar_avail, in_user_wishlist, run_time, air_date, your_rating, critics_review, flixster, buy_skuid, buy_expire_date_utc, buy_promotext, buy_purchasetype, rent_skuid, rent_expire_date_utc, rent_promotext, rent_purchasetype, rent_rental_period, bonus_asset_id, g_id, master_option, slave_option, meta_value )"
				+ "VALUES (:t_id, :title_name, :box_art_prefix, :mpaa_rating, :actors, :buy_price, :copyright, :directors, :producers, :rating_reason, :release_year, :rent_price, :season_title_id, :show_title_id, :synopsys, :buy_avail, :rent_avail, :writers, :title_type, :is_thx_media_director_enabled, :hd, :similar_avail, :in_user_wishlist, :run_time, :air_date, :your_rating, :critics_review, :flixster, :buy_skuid, :buy_expire_date_utc, :buy_promotext, :buy_purchasetype, :rent_skuid, :rent_expire_date_utc, :rent_promotext, :rent_purchasetype, :rent_rental_period, :bonus_asset_id, :g_id, :master_option, :slave_option, :meta_value)";
			this.execSQL(sql, success, fault, paramConvertor(item));
		}
		
		private function paramConvertor(o:Object):Object
		{
			var p:Object = {};
			for (var k:String in o)
			{
				p[(":" + k)] = o[k];
			}
			return p;
		}

		/**
		 * Update the title item with the given information
		 * 
		 * @param id the id of the item needs to be updated
		 * @param updateItem an object with all information
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function updateTitle(id:String,
									  updateItem:Object,
									  success:Function = null,
									  fault:Function = null):void 
		{
			var sql:String = "UPDATE " + TABLE_TITLE + " SET "
				+ "title_name = :title_name, box_art_prefix = :box_art_prefix, mpaa_rating = :mpaa_rating, actors = :actors, buy_price = :buy_price, copyright = :copyright, directors = :directors, "
				+ "producers = :producers, rating_reason = :rating_reason, release_year = :release_year, rent_price = :rent_price, season_title_id = :season_title_id, show_title_id = :show_title_id, "
				+ "synopsys = :synopsys, buy_avail = :buy_avail, rent_avail = :rent_avail, writers = :writers, title_type = :title_type, is_thx_media_director_enabled = :is_thx_media_director_enabled, "
				+ "hd = :hd, similar_avail = :similar_avail, in_user_wishlist = :in_user_wishlist, run_time = :run_time, air_date = :air_date, your_rating = :your_rating, critics_review = :critics_review, flixster = :flixster, "
				+ "buy_skuid = :buy_skuid, buy_expire_date_utc = :buy_expire_date_utc, buy_promotext = :buy_promotext , buy_purchasetype = :buy_purchasetype, rent_skuid = :rent_skuid, rent_expire_date_utc = :rent_expire_date_utc, "
				+ "rent_promotext = :rent_promotext, rent_purchasetype = :rent_purchasetype, rent_rental_period = :rent_rental_period, bonus_asset_id = :bonus_asset_id, g_id = :g_id, master_option = :master_option, "
				+ "meta_value = :meta_value, slave_option = :slave_option WHERE t_id = :t_id";
			this.execSQL(sql, success, fault, paramConvertor(updateItem));
		}
		
		/**
		 * Get the title with the given information
		 * 
		 * @param id the id of the target title
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getTitle(id:String, success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_TITLE + " WHERE t_id = " + id;
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Get all titles with the given information
		 * 
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getAllTitles(success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_TITLE;
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Add a new library with the given information
		 * 
		 * @param item an object with all information
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function addLibraryItem(item:Object, success:Function = null, fault:Function = null):void
		{
			var sql:String = "INSERT INTO " + TABLE_LIBRARY
				+ " (_id, title_id, purchase_type, first_play, view_hours, expire_time, expired, has_portable, is_local, is_premium_local, is_playable, sku_id, download_order, download_status, download_url, dq_id, device, target_file_size, actual_file_size, watch_now_countdown, autodelete, download_speed, file_name, video_bitrate, audio_bitrate, license_url, license_receieved, user_name, deleted, last_play_position, date_expired, date_purchased, expiration_message, store_logo_url, store_name, stream_play_status, stream_start_time_seconds, watch_status, asset_id, license_acknowledge_url, friendly_file_name, custom_data )"
				+ " VALUES (:_id, :title_id, :purchase_type, :first_play, :view_hours, :expire_time, :expired, :has_portable, :is_local, :is_premium_local, :is_playable, :sku_id, :download_order, :download_status, :download_url, :dq_id, :device, :target_file_size, :actual_file_size, :watch_now_countdown, :autodelete, :download_speed, :file_name, :video_bitrate, :audio_bitrate, :license_url, :license_receieved, :user_name, :deleted, :last_play_position, :date_expired, :date_purchased, :expiration_message, :store_logo_url, :store_name, :stream_play_status, :stream_start_time_seconds, :watch_status, :asset_id, :license_acknowledge_url, :friendly_file_name, :custom_data)";
			this.execSQL(sql, success, fault, paramConvertor(item));
		}

		/**
		 * Get library info with the given information
		 * 
		 * @param id the target id of download info
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getLibraryItem(id:String, success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_LIBRARY + " WHERE _id = " + id;
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Get library info with the given title id
		 * 
		 * @param id the target id of download info
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getLibraryItemByTitleID(id:String, success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_LIBRARY + " WHERE title_id = " + id;
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Get all library with the given information
		 * 
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getAllLibraryItems(success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_LIBRARY;
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Update the library item with the given information
		 * 
		 * @param id the id of the item needs to be updated
		 * @param updateItem an object with all information
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function updateLibraryItem(id:String,
										   updateItem:Object,
										   success:Function = null,
										   fault:Function = null):void
		{
			var sql:String = "UPDATE " + TABLE_LIBRARY + " SET "
				+ "title_id = :title_id, purchase_type = :purchase_type, first_play = :first_play, view_hours = :view_hours, expire_time = :expire_time, expired = :expired, has_portable = :has_portable, "
				+ "is_local = :is_local, is_premium_local = :is_premium_local, is_playable = :is_playable, sku_id = :sku_id, download_order = :download_order, download_status = :download_status, "
				+ "download_url = :download_url, dq_id = :dq_id, device = :device, target_file_size = :target_file_size, actual_file_size = :actual_file_size, watch_now_countdown = :watch_now_countdown, "
				+ "autodelete = :autodelete, download_speed = :download_speed, file_name = :file_name, video_bitrate = :video_bitrate, audio_bitrate = :audio_bitrate, license_url = :license_url, "
				+ "license_receieved = :license_receieved , user_name = :user_name, deleted = :deleted, last_play_position = :last_play_position, date_expired = :date_expired, date_purchased = :date_purchased, "
				+ "expiration_message = :expiration_message, store_logo_url = :store_logo_url, store_name = :store_name, stream_play_status = :stream_play_status, stream_start_time_seconds = :stream_start_time_seconds, "
				+ "watch_status = :watch_status, asset_id = :asset_id, license_acknowledge_url = :license_acknowledge_url, friendly_file_name = :friendly_file_name, custom_data = :custom_data WHERE _id = :_id";
			this.execSQL(sql, success, fault, paramConvertor(updateItem));
		}
		
		/**
		 * Get all item with has the same value of title_id in Library and t_id in Titles
		 * 
		 * @param success success success callback
		 * @param fault fault callback
		 */
		public function getAllCrossItems(success:Function = null, fault:Function = null):void
		{
			var sql:String = "SELECT t.*, l.* FROM " + TABLE_LIBRARY + " l INNER JOIN " + TABLE_TITLE + " t on t.t_id = l.title_id";
			this.execSQL(sql, success, fault);
		}
		
		/**
		 * Get item with has the same value of title_id in Library and t_id in Titles by the given _id in Library
		 * 
		 * @param id the id of the item to be found
		 * @param success success success callback
		 * @param fault fault callback
		 */
		public function getCrossItemByID(id:String,
										 success:Function = null,
										 fault:Function = null):void
		{
			var sql:String = "SELECT t.*, l.* FROM " + TABLE_LIBRARY + " l INNER JOIN " + TABLE_TITLE + " t on t.t_id = l.title_id WHERE _id = " + id;
			this.execSQL(sql, success, fault);
		}
		
		/**
		 * Add a new file cache with the given information
		 * 
		 * @param item an object with all information
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function addFileCacheItem(item:Object, success:Function = null, fault:Function = null):void
		{
			var sql:String = "INSERT INTO " + TABLE_FILE_CACHE
				+ " (Source, FilePath, Size, UpdateTime )"
				+ " VALUES (:Source, :FilePath, :Size, :UpdateTime)";
			this.execSQL(sql, success, fault, paramConvertor(item));
		}
		
		/**
		 * Get file cache info with the given information
		 * 
		 * @param id the target id of download info
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getFileCacheItem(id:String, success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_FILE_CACHE + " WHERE Source = \"" + id + "\"";
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Remove file cache info with the given information
		 * 
		 * @param id the target id of download info
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function removeFileCacheItem(id:String, success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "DELETE FROM " + TABLE_FILE_CACHE + " WHERE Source = \"" + id + "\"";
			this.execSQL(searchSql, success, fault);
		}
		
		/**
		 * Get all file cache with the given information
		 * 
		 * @param success success callback
		 * @param fault fault callback
		 */
		public function getAllFileCacheItems(success:Function = null, fault:Function = null):void
		{
			var searchSql:String = "SELECT * FROM " + TABLE_FILE_CACHE;
			this.execSQL(searchSql, success, fault);
		}
	}
}
