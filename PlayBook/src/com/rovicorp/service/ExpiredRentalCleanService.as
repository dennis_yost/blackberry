package com.rovicorp.service
{
	import com.rovicorp.events.DatabaseEvent;
	import com.rovicorp.events.EventCenter;
	import com.rovicorp.model.RoxioNowDAO;
	import com.rovicorp.model.TitleListingPurchased;
	import com.rovicorp.service.download.DownloadManager;
	import com.rovicorp.controllers.DatabaseController;
	import com.rovicorp.utilities.StringUtil;
	
	public class ExpiredRentalCleanService implements IServiceTask
	{
		
		//private var service:RoxioNowDAO = new RoxioNowDAO();
		
		public function ExpiredRentalCleanService()
		{
			
		}
		
		public function get name():String
		{
			return "ExpiredRentalCleanService";
		}
		
		public function run():void
		{
			var dbController:DatabaseController = DatabaseController.inst;
			if( dbController && dbController.isDatabaseReady )
			{
				dbController.getDBManager().getAllLibraryItems(onResultHandler,onFaultHandler);
			}			
		}
		
		private function onResultHandler(result:Object):void
		{
			if (result.data != null)
			{				
				var titles:Array = result.data as Array;
				
				if( titles == null ) return;
				
				var obj:Object;
				
				var expired_date:Date;
				
				var current_date:Date = new Date();
				current_date.setDate( current_date.getDate() + 30 );
				
				var downloadMgr:DownloadManager = DownloadManager.getInstance();
				
				for( var i:int = 0,len:int = titles.length;i<len;i++)
				{
					obj = titles[i];
					
					if( obj.purchase_type != 0 )
					{
						continue;
					}
					
					expired_date = new Date();
					expired_date.setTime(Date.parse(obj.date_expired));
					
					//after 30 days
					if( expired_date.time > current_date.time )
					{
						//TODO
						//Universal Studios rentals are always deleted after 30 days
						//How to know the title is from Universal Studios??						
//						if( service.settings.deleteExpiredRentals )
//						{
							//expired
							downloadMgr.removeDownloadItemByID(obj._id.toString());
//						}
					}
				}
			}
		}
		
		private function onFaultHandler(fault:Object):void
		{
			
		}
	}
}