package com.rovicorp.service
{	
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class BackgroundService
	{
		
		private static var _instance:BackgroundService;
		
		public static function getInstance():BackgroundService
		{
			if( _instance == null )
			{
				_instance = new BackgroundService();
			}
			return _instance;
		}
		
		
		//
		private var tasks:Vector.<IServiceTask> = new Vector.<IServiceTask>;
		
		private var timer:Timer;
		
		//default delay: 10 minutes
		public function BackgroundService( delay:Number = 1000*60*10 )
		{
			timer = new Timer(delay);
			
			timer.addEventListener(TimerEvent.TIMER, onTimer);
			
			start();			
		}
		
		public function start():void
		{
			if( timer.running == false )
			{
				timer.start();
			}
		}
		
		public function get isRunning():Boolean
		{
			return timer.running;
		}
		
		public function stop():void
		{
			timer.reset();
		}
		
		private function onTimer(e:TimerEvent):void
		{
			var task:IServiceTask;
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i];
				task.run();
			}
		}
		
		public function addTask( task:IServiceTask ):void
		{
			tasks.push( task );
		}
		
		//
		public function removeTask(name:String):void
		{
			var index:int = findTaskByName(name);
			if( index != -1 )
			{
				tasks.splice(index,1);
			}
		}
		
		private function findTaskByName(name:String):int
		{
			var task:IServiceTask;
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{
				task = tasks[i];
				if( task.name == name )
				{
					return i;
				}
			}
			return -1;
		}
	}
}