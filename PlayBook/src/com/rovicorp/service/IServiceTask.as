package com.rovicorp.service
{
	public interface IServiceTask
	{
		function run():void
		
		function get name():String
	}
}