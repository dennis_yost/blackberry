package com.rovicorp.utilities
{
	import com.rovicorp.service.download.DownloadStatus;

	public class DownloadUtil
	{
		public function DownloadUtil()
		{
		}
		
		
		
		public static function getStatusIconURL($downloadStatus:int):String			
		{
			var fileName:String = "download_not_in_queue_icon";
			switch( $downloadStatus )
			{
				case DownloadStatus.TASK_COMPLETE:
					fileName = "downloaded_icon";
					break;
				case DownloadStatus.TASK_INITIALIZED:
				case DownloadStatus.TASK_IN_PROGRESS:
				case DownloadStatus.TASK_RESUMED:
				case DownloadStatus.TASK_READY_WATCH:
					fileName = "downloading_icon";
					break;				
				case DownloadStatus.TASK_ERROR:
				case DownloadStatus.TASK_PAUSED:					
					fileName = "download_paused_icon";
					break;
				case DownloadStatus.TASK_PENDING:	
				case DownloadStatus.TASK_WAITING:		
					fileName = "download_pending_icon";
					break;				
				case DownloadStatus.TASK_UNAVAILABLE:					
					fileName = "download_not_in_queue_icon";
					break;
			}
			
			return "assets/images/" + fileName + ".png";	
		}
	}
}