package com.rovicorp.utilities
{
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Matrix;
	import flash.net.URLRequest;
	
	public class ImageLoaderUtil
	{
		//private const cacheSize:uint = 200;
		
		//private var caches:Vector.<CacheData> = new Vector.<CacheData>();
	
		private var tasks:Vector.<QueueTask> = new Vector.<QueueTask>();
		
		private var loaders:Vector.<Loader> = new Vector.<Loader>();
		private var loader_num:int = 0;
		
		private var isRunning:Boolean = false;
		
		private var isPaused:Boolean = false;
		
		private var targetWidth:uint;
		private var targetHeight:uint;
		
		public function ImageLoaderUtil(imageWidth:uint, imageHeight:uint)
		{			
			targetWidth = imageWidth;
			targetHeight = imageHeight;
		}		
		
		public function addTask(index:int, url:String, handler:Function):void
		{
			//removeTask(renderer);			
			//LogUtil.debug("addTask-----------------------------------------------" + tasks.length);
			//check cache first
			var data:BitmapData = ImagePool.getInstance().getCachedImageBitmapData(url);
			if( data )
			{				
				handler(index, data);
				return;
			}			
			
			var task:QueueTask = new QueueTask();
			task.id = index;
			task.url = url;	
			task.handler = handler;			
			tasks.push(task);
			
			run();
		}
		/*
		public function removeTask(renderer:TitleCellRenderer):void
		{
			var task:QueueTask;
			for( var i:uint = 0,len:uint = tasks.length;i<len;i++)
			{
				task = tasks[i];
				if( task.cellRenderer == renderer )
				{
					task.destroy();
					tasks.splice(i,1);
					return;
				}
			}
		}*/
		
		private function getTaskByLoader(loader:Loader):int
		{
			var task:QueueTask;
			for( var i:uint = 0,len:uint = tasks.length;i<len;i++)
			{
				task = tasks[i];
				if( task.loader == loader )
				{
					return i;
				}
			}
			return -1;
		}
		
		private function run():void
		{
			if( isPaused == true ) return;
						
			//LogUtil.debug("run:" + tasks.length +","+ isRunning);
			if( tasks.length == 0 ) return;	
			
			var loader:Loader = getLoader();
			var task:QueueTask;
			var url:String;
			while( loader != null )
			{
				task = null;
				
				for( var i:int = 0,len:int = tasks.length;i<len;i++)
				{					
					if( tasks[i].isRunning == false )
					{
						task = tasks[i];
						task.isRunning = true;
						break;
					}
				}
				//trace("task:" + task);
				if( task == null )
				{
					loaders.push(loader);
					break;
				}
				task.loader = loader;
				//LogUtil.debug("task.url:" + task.url);
				url = ImagePool.getInstance().tryToGetLocalCache(task.url);
				//LogUtil.debug("url:" + url);
				loader.load( new URLRequest(url));					
				loader = getLoader();
			}			
		}
		
		public function pause():void
		{
			//LogUtil.debug("loader pause:" + loaders.length + ":" + tasks.length);
			if( isPaused == false )
			{
				isPaused = true;
			}
		}
		
		public function resume():void
		{
			//LogUtil.debug("loader resume:" + loaders.length + ":" + tasks.length);
			if( isPaused == true )
			{
				isPaused = false;	
				run();
			}
		}	
		
		public function clear():void
		{
			var task:QueueTask;
			for( var i:int = 0,len:int = tasks.length;i<len;i++)
			{	
				task = tasks[i];
				
				task.destroy();
				
			}
			tasks.length = 0;
		}
		
		private function getLoader():Loader
		{
			if( loaders.length > 0 )
			{
				return loaders.shift() as Loader;
			}
			
			if( loader_num >= 6) return null;
			
			loader_num++;			
			
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onLoadError);		
			return loader;
			
		}
		
		private function onLoadComplete(e:Event):void
		{
			var loader:Loader = e.target.loader as Loader;
			//LogUtil.debug("onLoadComplete");
			imageLoadResult(loader, true);
		}
		
		private function onLoadError(e:Event):void
		{
			var loader:Loader = e.target.loader as Loader;
			LogUtil.debug("load error:" + loader.contentLoaderInfo.url);
			imageLoadResult(loader, false);
		}
		
		private function imageLoadResult(loader:Loader, success:Boolean = true):void
		{	
			var index:int = this.getTaskByLoader(loader);
			var task:QueueTask;
			if( index != -1 )
			{
				task = tasks[index];
			}
			
			if( success )
			{				
				var bd:Bitmap = loader.content as Bitmap;
				var newBD:BitmapData = bd.bitmapData;
				
				if (targetWidth && targetHeight && (targetWidth != bd.width || targetHeight != bd.height))
				{
					var matrix:Matrix = new Matrix();						
					matrix.scale(targetWidth / bd.width, targetHeight/ bd.height);

					newBD = new BitmapData(targetWidth, targetHeight, false, 0xFFFFFFFF);
					newBD.draw(loader, matrix);
				}
				
				var url:String = loader.contentLoaderInfo.url;
				if( url.toLowerCase().indexOf("app-storage:") == -1) 
				{
					ImagePool.getInstance().saveRemoteImageToDisk(url);
				}				
				
				if(task)
				{
					task.handler(task.id, newBD);	
					url = task.url;
				}
				if( ImagePool.getInstance().isImageCached(url) == false )
				{
					ImagePool.getInstance().addImageCache(url, newBD);
				}
			}
			else
			{
				if( task )
					LogUtil.debug("load error:" + task.url);
			}
			loaders.push(loader);
			loader.unloadAndStop();
			
			if( task )
			{
				tasks.splice(index,1);
				task.destroy();
			}
			
			run();
			
		}	
		
	}
}


import flash.display.BitmapData;
import flash.display.Loader;

class QueueTask
{
	public var url:String;
	public var id:int;
	public var handler:Function;
	public var loader:Loader;
	
	public var isRunning:Boolean = false;
	
	public function destroy():void
	{
		handler = null;
		loader = null;
	}
	
}

class CacheData
{
	public var url:String;
	public var data:BitmapData;
}