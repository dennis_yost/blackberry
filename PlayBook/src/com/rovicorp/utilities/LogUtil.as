package com.rovicorp.utilities
{
	import flash.display.DisplayObject;
	import flash.events.Event;

	public class LogUtil
	{
		import com.junkbyte.console.Console;		
		import com.junkbyte.console.Cc;
		import com.junkbyte.console.ConsoleChannel;
		import com.junkbyte.console.core.Remoting;		
		
		CONFIG::debug
		public static function init( ui:DisplayObject, host:String = "10.178.15.45", Port:int = 2012 ):void
		{
			Cc.startOnStage(ui, "`"); // "`" - change for password. This will start hidden
			//Cc.visible = true; // Show console, because having password hides console.
			Cc.fpsMonitor = true;
			Cc.memoryMonitor = true;
			
			Cc.config.commandLineAllowed = true; // enable advanced (but security risk) features.
			Cc.config.tracing = true; // Also trace on flash's normal trace
			
			Cc.remotingSocket(host, Port);
			
			Cc.remotingPassword = null; // Just so that remote don't ask for password
			Cc.remoting = true; // Start sending logs to remote (using LocalConnection)
			
			Cc.commandLine = true; // Show command line
			
			//Cc.height = 200;
			
			Cc.log("Log initalizing ...");
		}
		
		public static function info(...paras):void
		{
			CONFIG::debug
			{
				Cc.info(paras);
			}
		}
		
		public static function log(...paras):void
		{
			CONFIG::debug
			{
				Cc.log(paras);
			}
		}
		
		public static function debug(...paras):void
		{
			CONFIG::debug
			{
				Cc.debug(paras);
			}
			trace(paras);
		}
		
		public static function error(...paras):void
		{
			CONFIG::debug
			{
				Cc.error(paras);
			}
		}
		
	}
}