package com.rovicorp.utilities
{
	public class ImagePoolTask 
	{
		public var isCancelled:Boolean = false;
		public var loadFromWeb:Boolean;
		public var type:String;
		public var url:String;
		public var callback:Function;
		public var saveToDisk:Boolean = false;
		
		public function ImagePoolTask(type:String, url:String, callback:Function = null, saveToDisk:Boolean = true)
		{
			this.type = type;
			this.url = url;
			this.callback = callback
			this.saveToDisk = saveToDisk;
		}
		
		public function cancel():void
		{
			isCancelled = true;
		}
		
		public function destroy():void
		{
			this.callback = null;
		}
	}
}