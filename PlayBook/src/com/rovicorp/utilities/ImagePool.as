﻿package com.rovicorp.utilities
{	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.FileListEvent;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	

	/**
	 * ImagePool caches image bitmapdata for future use. It will load the image if it can not be found in cache or local disk.
	 * just one place for holding all image data.
	 * 
	 * @author ZunlinZhang
	 */
	public class ImagePool
	{
		// ==================  [VARIABLES]   ==================
		//private static var _inst:ImagePool;
		private static var _instances:Dictionary = new Dictionary();
		
		private var _imagePool:Dictionary;
		private var _imgPoolKeys:Vector.<String>;
		private var _cacheLength:int = 500;
		
		// Element: Object, {type: TASK_TYPE, url:String, cbFn:Function}
		private var _taskList:Vector.<ImagePoolTask>;
		private var _curTask:ImagePoolTask;
		
		private var _useCache:Boolean = true;
		private var _isProcessing:Boolean = false;
		private var _isPaused:Boolean = false;
		private var _loader:Loader;
		private var _urlLoader:URLLoader;
		
		
		private var _fileTaskList:Vector.<FileTask>;
		private var _sharedFileStream:FileStream;
		private var _isWriting:Boolean;
		
		private var _lastCheckSizeTime:uint;
		
		private const LOCAL_FILE_PREFIX:String = "assets/boxart/";
		private const LOCAL_CACHE_SIZE_LIMIT:Number = 5 * 1024 * 1024;	// 5M
		
		private const TASK_TYPE_GET_IMAGE_DATA:String = "TASK_TYPE_GET_IMAGE_DATA";
		private const TASK_TYPE_CHECK_CACHE_LIMIT:String = "TASK_TYPE_CHECK_CACHE_LIMIT";
		
		public static const MISC_GROUP:String = "misc";

		/**
		 * This is a singleton class. User should never call the constructor.
		 * @param val
		 */
		public function ImagePool(val:ImagePoolConstHelper)
		{
			_imagePool = new Dictionary;
			_imgPoolKeys = new Vector.<String>;
			_taskList = new Vector.<ImagePoolTask>;
			
			_fileTaskList = new Vector.<FileTask>;
			
			_loader = new Loader;
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onMediaLoaded, false, 0, true);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onMediaLoadError, false, 0, true);
			
			_urlLoader = new URLLoader();
			_urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			_urlLoader.addEventListener(Event.COMPLETE, onLoadImageDataDone, false, 0, true);
			_urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onLoadImageDataError, false, 0, true);
			
		}
		
		public static function getInstance( name:String = "default" ):ImagePool
		{
			if (_instances[name] == null)
			{
				_instances[name] = new ImagePool(new ImagePoolConstHelper());
			}
			
			return _instances[name] as ImagePool;
		}
		
		public function resume():void
		{
			if( _isPaused == true )
			{
				_isPaused = false;
				
				processTask();
				
				$saveFile();
			}
		}
		
		public function pause():void
		{
			if( _isPaused == false )
			{
				_isPaused = true;
			}
		}
		
		public function clear():void
		{
			if( _taskList == null ) return;
			
			var task:ImagePoolTask;
			for( var i:int = 0,len:int = _taskList.length;i<len;i++)
			{	
				task = _taskList[i] as ImagePoolTask;
				
				task.destroy();
			}
			
			_taskList.length = 0;
		}
		
		public function getTaskLength():int
		{
			if( _taskList ) return _taskList.length;
			
			return 0;
		}
		/**
		 * Turn on / off the cache.
		 * 
		 * @param val
		 */
		public function set useCache(val:Boolean):void
		{
			_useCache = val;
		}
		
		/**
		 * Set how many image should be cached. Old image cache will be removed if newer one is coming.
		 * 
		 * @param val the number for image cache
		 */
		public function set cacheLength(val:int):void
		{
			_cacheLength = val;
			checkCacheLength();
		}
		
		/**
		 * Return the image cache number
		 * @return 
		 */
		public function get cacheLength():int
		{
			return _cacheLength;
		}
		
		/**
		 * Remove all image cache.
		 */
		public function emptyCache():void
		{
			var l:int = _imgPoolKeys.length;
			for (var i:int = 0; i < l; ++i)
			{
				(_imagePool[_imgPoolKeys[i]] as BitmapData).dispose();
				delete _imagePool[_imgPoolKeys[i]];
			}
			_imgPoolKeys.splice(0, l);
		}
		
		/**
		 * Get the image bitmapdata by given key (image load url).
		 * 
		 * @param key Image load url as the Key.
		 * @param cb call back function
		 * @param saveToDisk save image file to disk after loaded
		 * @param priority
		 */
		public function getImageBitmapData(key:String, cb:Function = null, saveToDisk:Boolean = true, priority:Boolean = false):ImagePoolTask
		{
			//trace("ADD NEW TASK: " +  _isProcessing);
			var result:ImagePoolTask = new ImagePoolTask(TASK_TYPE_GET_IMAGE_DATA, key, cb, saveToDisk);
			
			if( priority && _taskList.length > 0 )
			{
				_taskList.splice(1,0,result);
			}
			else
			{
				_taskList.push(result);
			}
			if (_isProcessing == false)
			{
				processTask();
			}
			
			return result;
		}
		
		/**
		 * Save remote image to disk.
		 * 
		 * @param key Image load url as the Key.
		 * @param cb call back function
		 */
		public function saveRemoteImageToDisk(key:String, cb:Function = null):ImagePoolTask
		{	
			var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX + convertFileName(key));
			if( f.exists ) return null;
			
			return getImageBitmapData(key, cb, true);
		}
		
		public function tryToGetLocalCache(url:String):String
		{
			var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX + convertFileName(url));
			if( f.exists ) return f.url;
			
			return url;
		}
		
		/**
		 * Load a image from local disk. Curerntly it works the same way as getImageBitmapData().
		 * @param key image url
		 * @param cb callback function
		 * @return if the file has been cached locally or not
		 * @see getImageBitmapData
		 */
		/*
		public function loadLocalImage(key:String, cb:Function):Boolean
		{
			var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX + convertFileName(key));
			if (f.exists == false)
			{
				return false;
			}
			else
			{
				getImageBitmapData(key, cb);
				return true;
			}
		}*/
		
		/**
		 * Get the cached image bitmapdata by given key (image load url).
		 * 
		 * @param key Image load url as the Key.
		 * @return Image data bitmapdata if there is.
		 */
		public function getCachedImageBitmapData(key:String):BitmapData
		{
			if (_useCache)
			{
				return _imagePool[key];
			}
			else
			{
				return null;
			}
		}
		
		/**
		 * Check if one image is cached.
		 * 
		 * @param key Image load url as the Key.
		 * @return true or false
		 */
		public function isImageCached(key:String):Boolean
		{
			return (_imagePool[key] != null);
		}
		
		/**
		 * Cache one image.
		 * 
		 * @param key Image load url as the Key.
		 * @param bp The bitmapdata for that image.
		 */
		public function addImageCache(key:String, bp:BitmapData):void
		{
			if (isImageCached(key) == false)
			{
				_imgPoolKeys.push(key);
			}
			_imagePool[key] = bp;
			
			checkCacheLength();
		}
		
		/**
		 * Remove specific image's cache by given key.
		 * 
		 * @param key Image load url as the Key.
		 */
		public function removeImageCache(key:String):void
		{
			if (isImageCached(key) == true)
			{
				(_imagePool[key] as BitmapData).dispose();
				delete _imagePool[key];
				var i:int = findKeyIndex(key);
				if (i != -1)
				{
					_imgPoolKeys.splice(i, 1);
				}
			}
		}
		
		private function processTask():void
		{	
			if( _isPaused || _isProcessing == true ) return;
			
			if (_taskList.length == 0)
			{
				// all tasks are done
				_isProcessing = false;
				return;
			}
			
			_isProcessing = true;
			//trace("START NEW TASK, REMAIN: ", _taskList.length - 1);
			_curTask = _taskList[0];
			
			while (_curTask.isCancelled)
			{
				_taskList.shift();
				
				if (_taskList.length == 0)
				{
					_curTask = null;
					return;
				}
				
				_curTask = _taskList[0];
			}
			
			if (_curTask.type == TASK_TYPE_GET_IMAGE_DATA)
			{
				// check if current request is cached
				var bd:BitmapData = getCachedImageBitmapData(_curTask.url);
				if (bd != null && _curTask.callback != null)
				{
					//trace("load from cache: ", _curTask.url);
					taskDone(true, bd);
					return;
				}
	
				// check if it's located on disk
				var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX + convertFileName(_curTask.url));
				if (f.exists)
				{
					// read bitmap from file
					//trace("load from local: ", f.size, _curTask.url);
					_curTask.loadFromWeb = false;
					_urlLoader.load(new URLRequest(f.url));
				}
				else
				{
					// load from web server
					//trace("load from server: ", _curTask.url);
					_curTask.loadFromWeb = true;
					_urlLoader.load(new URLRequest(_curTask.url));
				}
			}
			else if (_curTask.type == TASK_TYPE_CHECK_CACHE_LIMIT)
			{
				checkLocalCacheLimit();
			}
		}
		
		private function onLoadImageDataDone(e:Event):void
		{
			if (_urlLoader.data)
			{
				if (_curTask.loadFromWeb == true && _curTask.saveToDisk == true)
				{
					//LogUtil.debug("WRITE FILE: ", convertFileName(_curTask.url));
					
					var bytes:ByteArray = _urlLoader.data as ByteArray;					
					
					loadImageFromByteArray(bytes);
					
					saveBytesAsFile(bytes,convertFileName(_curTask.url));
				}
				else
				{
					loadImageFromByteArray(_urlLoader.data);
				}
			}
			else
			{
				if (_curTask.loadFromWeb == false)
				{
					// reload from webservice
					_urlLoader.load(new URLRequest(_curTask.url));
				}
				else
				{
					taskDone(false, "LoadBoxartFromWebFault");
				}
			}
		}
		
		private function saveBytesAsFile( bytes:ByteArray, path:String ):void
		{
			//LogUtil.debug("saveBytesAsFile:" + path);
			
			var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX + path);
			if( f.exists ) return;
			
			var newTask:FileTask = new FileTask();
			newTask.data = bytes;
			newTask.path = path;
			
			_fileTaskList.push(newTask);	
			
			$saveFile();
		}
		
		private function $saveFile():void
		{
				
			if( _isPaused || _isWriting == true || _fileTaskList.length == 0 ) return;			
			
			var curTask:FileTask = _fileTaskList[0];
			// write to file
			var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX + curTask.path);
			if( f.exists )
			{
				onFileSaved();
				return;
			}
			
			if( _sharedFileStream == null)
			{
				_sharedFileStream = new FileStream;	
				_sharedFileStream.addEventListener(Event.CLOSE, onFileSaved, false, 0 ,true);
			}
			try
			{
				if( File.applicationStorageDirectory.spaceAvailable > curTask.data.length + 1024 * 1024 )
				{
					_sharedFileStream.openAsync(f, FileMode.WRITE);	
					
					_sharedFileStream.writeBytes(curTask.data);
					_sharedFileStream.close();
				}
			}catch (err:Error)
			{
				trace("[--] Write to file error:" + err.getStackTrace());
			}
		}
		
		private function onFileSaved(e:Event = null):void			
		{
			//LogUtil.debug("onFileSaved:" +  e);				
			var curTask:FileTask = _fileTaskList.shift();
			
			if( curTask )
			{
				curTask.data.clear();
				curTask.data = null;
			}
			
			$saveFile();
			
			checkLocalCacheLimit();
		}
		
		private function onLoadImageDataError(e:IOErrorEvent):void
		{
			if (_curTask.loadFromWeb == false)
			{
				// reload from webservice
				_urlLoader.load(new URLRequest(_curTask.url));
			}
			else
			{
				taskDone(false, "LoadBoxartFromWebFault");
			}
		}
		
		private function loadImageFromByteArray(ba:ByteArray):void
		{
			_loader.loadBytes(ba);
		}
		
		private function onMediaLoadError(e:IOErrorEvent):void
		{
			taskDone(false, "LoadBoxartFromByteArrayFault");
		}
		
		private function onMediaLoaded(e:Event):void
		{
			var li:LoaderInfo = e.target as LoaderInfo;
			
			if (li.content && (li.content is Bitmap))
			{
				var bd:BitmapData = (li.content as Bitmap).bitmapData.clone();
				// save bd to cache
				//trace("[+++] add new cache from loaded file");
				addImageCache(_curTask.url, bd);
				taskDone(true, bd);
			}
			else
			{
				taskDone(false, "LoadBoxartFromWebFault");
			}
		}
		
		private function taskDone(success:Boolean, result:Object = null):void
		{
			//trace("TASK DONE: ", success, _curTask.url);
			if (_curTask.callback != null && !_curTask.isCancelled)
			{
				_curTask.callback({success: success, result: result});
			}
			
			_loader.unloadAndStop();
			_taskList.shift();
			
			_isProcessing = false;
			
			processTask();
		}
		
		private function checkLocalCacheLimit():void
		{			
			
			var _storage:SharedObject = SharedObject.getLocal("user-settings");
			_lastCheckSizeTime = 0;
			if( _storage.data.lastCheckSizeTime )
			{
				_lastCheckSizeTime = _storage.data.lastCheckSizeTime;
			}
			
			var t:uint = new Date().getTime();
			
			//LogUtil.debug("checkLocalCacheLimit:" + (t - _lastCheckSizeTime) );
			if( (t - _lastCheckSizeTime) < 10 * 60 * 1000 ) return;			
			
			var f:File = File.applicationStorageDirectory.resolvePath(LOCAL_FILE_PREFIX);
			
			if( f.exists && f.isDirectory )
			{				
				f.addEventListener(FileListEvent.DIRECTORY_LISTING, onGetFilesHandler);
				f.getDirectoryListingAsync();				
			}
			
			_storage.data.lastCheckSizeTime = t;
		}
		
		private function sortOnCreationDate(a:File, b:File):Number 
		{
			var aDate:Number = a.creationDate.getTime();
			var bDate:Number = b.creationDate.getTime();
			
			if(aDate > bDate) {
				return 1;
			} else if(aDate < bDate) {
				return -1;
			} else  {
				//aDate == bDate
				return 0;
			}
		}
		
		private function onGetFilesHandler(e:FileListEvent):void
		{
			
			e.target.removeEventListener(FileListEvent.DIRECTORY_LISTING, onGetFilesHandler);
			
			var files:Array = e.files;
			
			var i:uint, len:uint;
			len = files.length;
			
			var temp_file:File;
			var totalSize:Number = 0;
			
			for( i = 0;i<len;i++)
			{
				temp_file = files[i] as File;
				if( temp_file )
				{
					totalSize += temp_file.size;
				}
			}
			
			LogUtil.debug("local cache Size:" + totalSize);
			if( totalSize <= LOCAL_CACHE_SIZE_LIMIT ) return;
			
			//remove 40% files
			var removeFactor:int = int(0.4 *files.length) + 1; 
			//sort by creation date
			files.sort(sortOnCreationDate);
			
			LogUtil.debug("clear local cache");
			for( i = 0;i<removeFactor;i++)
			{
				temp_file = files[i] as File;
				if( temp_file )
				{
					LogUtil.debug("remove:" + temp_file.nativePath);
					temp_file.deleteFileAsync();
				}
			}
		}
		
		private function convertFileName(name:String):String
		{
			while (name.indexOf("/") > -1)
			{
				name = name.replace("/", "-");
			}
			while (name.indexOf(":") > -1)
			{
				name = name.replace(":", "-");
			}

			//trace("File name: ", name);
			return name;
		}
		
		private function checkCacheLength():void
		{
			var diff:int = _imgPoolKeys.length - _cacheLength;
			if (diff > 0)
			{
				for (var i:int = 0; i < diff; ++i)
				{
					(_imagePool[_imgPoolKeys[i]] as BitmapData).dispose();
					delete _imagePool[_imgPoolKeys[i]];
				}
				_imgPoolKeys.splice(0, diff);
			}
		}
		
		private function findKeyIndex(key:String):int
		{
			var l:int = _imgPoolKeys.length;
			for (var i:int = 0; i < l; ++i)
			{
				if (_imgPoolKeys[i] == key)
				{
					return i;
				}
			}
			return -1;
		}

	}
}

class ImagePoolConstHelper{}

import flash.utils.ByteArray;
class FileTask
{
	public var path:String;
	public var data:ByteArray;
}

