package com.rovicorp.events
{
	import flash.events.Event;
	import flash.net.NetworkInterface;

	public class DeviceEvent extends Event
	{
		public static const LOW_MEMORY:String = "NETWORK_CHANGE";
		public static const BATTERY_LEVEL_CHANGE:String = "BATTERY_LEVEL_CHANGE";
		
		public static const NETWORK_AVAILABILITY_CHANGE:String = "NETWORK_AVAILABILITY_CHANGE";
		
		public static const ORIENTATION_CHANGE:String = "ORIENTATION_CHANGE";
		
		public static const REFRESH_GEO_LOCATION:String = "REFRESH_GEO_LOCATION";
		public static const GEO_LOCATION_UPDATED:String = "GEO_LOCATION_UPDATED";
		public static const GEO_LOCATION_UPDATE_FAILED:String = "GEO_LOCATION_UPDATE_FAILED";
		
		public static const RETURN_PRESSED:String = "RETURN_PRESSED";
		
		public var networkAvailable:Boolean;
		public var orientation:int = 0;
		public var latitude:Number;
		public var longitude:Number;
		public var country:String;
		public var batteryLevel:int;
		public var batteryState:int;
		
		public function DeviceEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

	}
}