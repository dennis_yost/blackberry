package com.rovicorp.events
{
	import flash.events.Event;
	
	public class MainAppEvent extends Event
	{
		public static const APP_CREATION_COMPLETE:String = "APP_CREATION_COMPLETE";
		public static const START_STARTUP_PROCESS:String = "START_STARTUP_PROCESS";
		public static const STARTUP_PROCESS_DONE:String = "STARTUP_PROCESS_DONE";
		
		public static const LOAD_DEFAULT_STRING:String = "LOAD_DEFAULT_STRING";
		public static const LANGUAGE_STRING_REFRESH_DONE:String = "LANGUAGE_STRING_REFRESH_DONE";
		
		public static const USER_KEY_DOWN:String = "USER_KEY_DOWN";
		public static const USER_KEY_UP:String = "USER_KEY_UP";
		
		public static const APP_ACTIVE:String = "APP_ACTIVE";
		public static const APP_DEACTIVE:String = "APP_DEACTIVE";
		
		public static const RE_INIT_API:String = "RE_INIT_API";
		
		public static const CHECK_USER_CREDENTIALS:String = "CHECK_USER_CREDENTIALS";
		
		public static const DO_SEARCH:String = "DO_SEARCH";
		
		public static const NETWORK_CONNECTION_LOST:String = "NETWORK_CONNECTION_LOST";
		public static const SYSTEM_TIME_FORMAT_ERROR:String = "SYSTEM_TIME_FORMAT_ERROR";
		
		public static const SYSTEM_OFFLINE_MESSAGE:String = "SYSTEM_OFFLINE_MESSAGE";

		public var app:PlaybookVideoStore;
		public var keyCode:uint;
		public var language:String;
		public var searchContent:String;
		public var searchType:String;
		public var offlineMessage:String;
		
		public function MainAppEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

	}
}