package com.rovicorp.events
{
	import flash.events.Event;

	public class UIEvent extends Event
	{
		public static const SHOW_LOADING_OVERLAY:String = "SHOW_LOADING_OVERLAY";
		public static const HIDE_LOADING_OVERLAY:String = "HIDE_LOADING_OVERLAY";
		
		public static const STAGE_SIZE_UPDATE:String = "STAGE_SIZE_UPDATE";
		
		public static const SHOW_SECONDARY_MENU:String = "SHOW_SECONDARY_MENU";
		public static const HIDE_SECONDARY_MENU:String = "HIDE_SECONDARY_MENU";
		
		public static const SETTING_SCREEN_HIDE:String = "SETTING_SCREEN_HIDE";
		
		public static const SWITCH_SCREEN:String = "SWITCH_SCREEN";
		
		public static const POPUP_NO_INTERNET_DIALOG:String = "POPUP_NO_INTERNET_DIALOG";
		
		public static const VIEW_NAVIGATOR_POP_TRANSITION_END:String = "VIEW_NAVIGATOR_POP_TRANSITION_END";
		public static const VIEW_NAVIGATOR_PUSH_TRANSITION_END:String = "VIEW_NAVIGATOR_PUSH_TRANSITION_END";

		public var width:Number;
		public var height:Number;
		public var targetScreen:String;
		public var errorMsg:String = "";
		
		public function UIEvent(type:String)
		{
			super(type);
		}

	}
}