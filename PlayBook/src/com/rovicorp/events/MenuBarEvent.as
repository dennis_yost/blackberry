package com.rovicorp.events
{
	import flash.events.Event;
	
	public class MenuBarEvent extends Event
	{
		
		public var info:Object;
		public static const SEARCH:String = "search";
		public static const SETTINGS:String = "settings";
		public static const BACK:String = "settings";
		
		public function MenuBarEvent(type:String, _info:Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.info = _info;
		}
		
		public override function toString():String
		{
			return formatToString("MenuBarEvent", "info", "type", "bubbles", "cancelable");
		}
	}
}