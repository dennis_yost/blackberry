package com.rovicorp.events
{
	import flash.events.Event;
	
	public class VideoStoreEvent extends Event
	{
		public static const AUTHORIZE_DEVICE:String = "onAuthorizeDevice";
		public static const GO_BACK:String = "onBack";
		public static const SIGN_IN:String = "onSignIn";
		public static const SIGN_OUT:String = "onSignOut";
//dpy(20140625)--> 		public static const CREATE_ACCOUNT:String = "onCreateAccount";
//dpy(20140625)-->		public static const CHANGE_BILLING_INFO:String = "onChangeBillingInfo";
		public static const CHANGE_PASSWORD:String = "onChangePassword";
		public static const FORGOT_PASSWORD:String = "onForgotPassword";
		public static const VIEW_EULA:String = "onViewEULA";
		public static const VIEW_LEGAL:String = "onViewLegal";
		public static const VIEW_PRIVACY:String = "onViewPrivacy";
		public static const VIEW_TERMS:String = "onViewTerms";
		public static const SUPPORT:String = "onSupport";
		public static const SET_PARENTAL:String = "onSetParental";
		
		public var params:*;
		public var callback:Function;
		
		public function VideoStoreEvent(type:String, params:* = null, callback:Function = null, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			this.params = params;
			this.callback = callback;
			
		}

		public override function toString():String
		{
			return formatToString("VideoStoreEvent", "item", "index", "type", "bubbles", "cancelable");
		}
	}
}