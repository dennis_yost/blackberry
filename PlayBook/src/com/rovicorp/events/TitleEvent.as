package com.rovicorp.events
{
	import flash.events.Event;
	
	public class TitleEvent extends Event
	{
		public static const WISHLIST_ADD:String = "onWishlistAdd";
		public static const WISHLIST_REMOVE:String = "onWishlistRemove";
		public static const WISHLIST_REMOVE_DONE:String = "onWishlistRemoveDone";
		public static const WATCH_TRAILER:String = "onWatchTrailer";
		public static const WATCH_TITLE:String = "onWatchTitle";
		public static const DOWNLOAD_TITLE:String = "onDownloadTitle";

		public var item:Object;
		public var params:Object;
		
		public function TitleEvent(type:String, item:Object, params:Object, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			this.item = item;
			this.params = params;
		}
		
		public override function clone():Event
		{
			return new TitleEvent(type, item, params, bubbles, cancelable);
		}

		public override function toString():String
		{
			return formatToString("SelectionEvent", "item", "data", "type", "bubbles", "cancelable");
		}
	}
}