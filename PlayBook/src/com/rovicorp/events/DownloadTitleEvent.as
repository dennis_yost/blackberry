package com.rovicorp.events
{		
	import com.rovicorp.model.TitleListingPurchased;
	
	import flash.events.Event;
	
	public class DownloadTitleEvent extends Event
	{
		
		public static const ADD_TASK:String = "add_task";
		
		public static const ADD_TASK_NONE:String = "add_task_done";
		
		public static const GET_DOWNLOAD_DETAIL:String = "get_download_detail";
		
		public static const WATCH:String = "watch";
		
		
		//		
		public static const REMOVE_TASK:String = "remove_task";
		
		public static const PAUSE_TASK:String = "pause_task";
		
		public static const RESUME_TASK:String = "resume_task";
		
		public static const DOWNLOAD_TASK:String = "download_task";
		
		public static const RETRY_TASK:String = "retry_task";
		
		
		public function DownloadTitleEvent(type:String, title:TitleListingPurchased)
		{
			super(type, false, false);
			this.title = title;
		}
		
		//Title information
		public var title:TitleListingPurchased;
	}
}