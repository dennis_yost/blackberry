CREATE TABLE Cast (title_id INTEGER,case_id INTEGER,name TEXT, role TEXT, bio TEXT, snap_url TEXT );
CREATE TABLE "FileCache" ("Source" TEXT PRIMARY KEY  NOT NULL , "FilePath" TEXT, "Size" INTEGER, "UpdateTime" TEXT);
CREATE TABLE Genre (genre_id INTEGER,title_id INTEGER,position_id INTEGER);
CREATE TABLE [Library] (
[_id] INTEGER  PRIMARY KEY NULL,
[title_id] INTEGER  NOT NULL,
[purchase_type] INTEGER  NULL,
[first_play] TEXT  NULL,
[view_hours] INTEGER  NULL,
[expire_time] TEXT  NULL,
[expired] INTEGER  NULL,
[has_portable] INTEGER  NULL,
[is_local] INTEGER  NULL,
[is_premium_local] INTEGER  NULL,
[is_playable] INTEGER  NULL,
[sku_id] INTEGER  NULL,
[download_order] INTEGER  NULL,
[download_status] INTEGER  NULL,
[download_url] TEXT  NULL,
[dq_id] INTEGER  NULL,
[device] INTEGER  NULL,
[target_file_size] INTEGER  NULL,
[actual_file_size] INTEGER  NULL,
[watch_now_countdown] INTEGER  NULL,
[autodelete] INTEGER  NULL,
[download_speed] REAL  NULL,
[file_name] TEXT  NULL,
[video_bitrate] INTEGER  NULL,
[audio_bitrate] INTEGER  NULL,
[license_url] TEXT  NULL,
[license_receieved] INTEGER  NOT NULL,
[user_name] TEXT  NULL,
[deleted] TEXT  NULL,
[last_play_position] TEXT DEFAULT '0' NULL,
[date_expired] TEXT  NULL,
[date_purchased] TEXT  NULL,
[expiration_message] TEXT  NULL,
[store_logo_url] TEXT  NULL,
[store_name] TEXT  NULL,
[stream_play_status] TEXT  NULL,
[stream_start_time_seconds] INTEGER  NULL,
[watch_status] TEXT  NULL,
[asset_id] INTEGER  NULL,
[license_acknowledge_url] TEXT  NULL,
[friendly_file_name] TEXT  NULL,
[custom_data] TEXT  NULL
);
CREATE TABLE Navigation (id INTEGER,parent_id INTEGER,name STRING, visiable BOOL );
CREATE TABLE [Titles] (
[t_id] INTEGER  PRIMARY KEY NULL,
[title_name] TEXT  NULL,
[box_art_prefix] TEXT  NULL,
[mpaa_rating] TEXT  NULL,
[actors] TEXT  NULL,
[buy_price] DOUBLE  NULL,
[copyright] TEXT  NULL,
[directors] TEXT  NULL,
[producers] TEXT  NULL,
[rating_reason] TEXT  NULL,
[release_year] INTEGER  NULL,
[rent_price] DOUBLE  NULL,
[season_title_id] INTEGER  NULL,
[show_title_id] INTEGER  NULL,
[synopsys] TEXT  NULL,
[buy_avail] TEXT  NULL,
[rent_avail] TEXT  NULL,
[writers] TEXT  NULL,
[title_type] TEXT  NULL,
[is_thx_media_director_enabled] TEXT  NULL,
[hd] TEXT  NULL,
[similar_avail] TEXT  NULL,
[in_user_wishlist] TEXT  NULL,
[run_time] TEXT  NULL,
[air_date] TEXT  NULL,
[your_rating] INTEGER  NULL,
[critics_review] TEXT  NULL,
[flixster] TEXT  NULL,
[buy_skuid] INTEGER  NULL,
[buy_expire_date_utc] TEXT  NULL,
[buy_promotext] TEXT  NULL,
[buy_purchasetype] TEXT  NULL,
[rent_skuid] INTEGER  NULL,
[rent_expire_date_utc] TEXT  NULL,
[rent_promotext] TEXT  NULL,
[rent_purchasetype] TEXT  NULL,
[rent_rental_period] INTEGER  NULL,
[bonus_asset_id] INTEGER  NULL,
[g_id] INTEGER  NULL,
[master_option] TEXT  NULL,
[slave_option] TEXT  NULL,
[meta_value] TEXT  NULL
);
CREATE TABLE "bb_meta" (locale TEXT);
INSERT INTO bb_meta VALUES('en_US');